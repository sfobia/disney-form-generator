do ->
  Candidate = ($resource) ->
    BASE_URL = '/V1/candidate'
    $resource BASE_URL + '/:query/:id', { page:"@page", limit: "@limit" },
      {
        update: { method: 'PUT' }
        search: { method: 'POST', isArray : true }
        export: { method: 'POST' }
      }

  angular.module('app').factory 'Candidate', Candidate
  Candidate.$inject = ['$resource']
  return

do ->
  Accepted = ($resource) ->
    BASE_URL = '/V1/candidate/accepted'
    $resource BASE_URL + '/:id', {},

  angular.module('app').factory 'Accepted', Accepted
  Accepted.$inject = ['$resource']
  return


do ->
  Preconfirm = ($resource) ->
    BASE_URL = '/V1/candidate/preconfirm'
    $resource BASE_URL + '/:id', {},

  angular.module('app').factory 'Preconfirm', Preconfirm
  Preconfirm.$inject = ['$resource']
  return

do ->
  Reject = ($resource) ->
    BASE_URL = '/V1/candidate/reject'
    $resource BASE_URL + '/:id', {},

  angular.module('app').factory 'Reject', Reject
  Reject.$inject = ['$resource']
  return

do ->
  RejectAll = ($resource) ->
    BASE_URL = '/V1/candidate/reject/all'
    $resource BASE_URL + '/:id', {},

  angular.module('app').factory 'RejectAll', RejectAll
  RejectAll.$inject = ['$resource']
  return


do ->
  AcceptPreconfirmed = ($resource) ->
    BASE_URL = '/V1/candidate/acceptPreconfirmed'
    $resource BASE_URL + '/:id', {},

  angular.module('app').factory 'AcceptPreconfirmed', AcceptPreconfirmed
  AcceptPreconfirmed.$inject = ['$resource']
  return

do ->
  SendMailToCandidates = ($resource) ->
    BASE_URL = '/V1/candidate/sendMail'
    $resource BASE_URL + '/:id', {},

  angular.module('app').factory 'SendMailToCandidates', SendMailToCandidates
  SendMailToCandidates.$inject = ['$resource']
  return

do ->
  SendTrackingToCandidates = ($resource) ->
    BASE_URL = '/V1/candidate/sendTracking'
    $resource BASE_URL + '/:id', {},

  angular.module('app').factory 'SendTrackingToCandidates', SendTrackingToCandidates
  SendTrackingToCandidates.$inject = ['$resource']
  return

do ->
  SendSurveyToCandidates = ($resource) ->
    BASE_URL = '/V1/candidate/sendSurvey'
    $resource BASE_URL + '/:id', {},

  angular.module('app').factory 'SendSurveyToCandidates', SendSurveyToCandidates
  SendSurveyToCandidates.$inject = ['$resource']
  return