do ->
  User = ($resource) ->
    BASE_URL = '/V1/profile'
    $resource BASE_URL + '/:id', {},
      {
        search: {method: 'POST', isArray : true}
        update: {method: 'PUT'}
      }

  angular.module('app').factory 'User', User
  User.$inject = ['$resource']
  return

do ->
  UserQuery = ($resource) ->
    BASE_URL = '/V1/profile'
    $resource BASE_URL + '/:id', {},
      {
        search: {method: 'POST', isArray : true}
      }

  angular.module('app').factory 'UserQuery', UserQuery
  UserQuery.$inject = ['$resource']
  return

do ->
  UserInfo = ($resource) ->
    BASE_URL = '/V1/profile'
    $resource BASE_URL + '/info', {}, query: {method: 'GET', isArray: false }

  angular.module('app').factory 'UserInfo', UserInfo
  UserInfo.$inject = ['$resource']
  return

do ->
  UserAdmin = ($resource) ->
    BASE_URL = '/account'
    $resource BASE_URL + '/:action', {},
      {
        login: {method: 'POST', isArray : false },
        logout: {method: 'GET' },
        search: {method: 'POST' }
      }

  angular.module('app').factory 'UserAdmin', UserAdmin
  UserAdmin.$inject = ['$resource']
  return