do ->
  userDetailController = (Shared, User, UserInfo, Candidate, Accepted,Reject, $state, $stateParams, UserAdmin,Preconfirm) ->
    vm = this
    vm.info = UserInfo.query()
    query = {}
    vm.now    = new Date().toISOString()

    vm.updateCandidate = (candidate, item, campaign, active, rejected) ->
      if active
        if confirm 'sei sicuro?'
          Accepted.save id: campaign, [candidate], (data) ->
            item.active = true
            return

      if rejected
        if confirm 'sei sicuro?'
          Reject.save id: campaign, [candidate], (data) ->
            item.rejected = true
            return


    vm.updateReliability = (reliability) ->
      query = {
        status: reliability
      }
      User.update id: vm.data._id, query, (data) ->
        console.log data

    vm.getUserCampaigns = (userId, campaignId ) ->
      query.user = userId
      query.campaign = campaignId if campaignId

      Candidate.search { id:"query", limit:1000 }, query, (data) ->
        for item in data
          item.form = vm.data.form.filter (form) ->
            if form.campaign
              return form.campaign._id == item.campaign._id
            false
        vm.listCandidate = data
      return

    vm.resendUserToken = ->

      userId = vm.data.user._id

      payload =
        user: userId

      UserAdmin.search { action: 'resend-token' }, payload, ( data ) ->
        vm.resendEnd = true

      return

    vm.removeUser = ->
      # todo cerco se ha candidature accettate e preconfermate
      Candidate.search { query:'query' }, { user: vm.data.user._id } , (data) ->
        vm.removeStart = true
        if data.length > 0
          txt = ''
          for item in data
            txt += "\n-------\n"+item.campaign.title
            txt += " > PRECONFIRMED "     if item.preconfirmed
            txt += " > ACTIVE "           if item.active
            txt += " > REJECTED "         if item.rejected
            txt += " > SENZA REQUISITI "  if !item.preconfirmed

          alert("Attenzione l'utente ha candidature" + txt)

    vm.removeUserConfirm = ->
      if confirm "Sei sicuro? l'utente non sarà recuperabile"
        User.delete id: vm.data.user._id, (data) ->
          if data.status == 'ok'
            alert "Cancellato con successo \n"+JSON.stringify(data.message).toString()
            $state.go('user.list')


    vm.changePreconfirm = (candidate)->
      if confirm "Vuoi cambiare lo stato preconfirmed?"
        Preconfirm.save { id: candidate._id}, { preconfirmed: !candidate.preconfirmed }, (data) ->
          candidate.preconfirmed = !candidate.preconfirmed
          return

    if $stateParams.id
      User.get id: $stateParams.id, (data) ->
        $state.go('user.list') if !data._id
        vm.data = data

        campaignId = $stateParams.campaignId
        vm.getUserCampaigns data.user._id, campaignId
    else
      $state.go('user.list')

    return

  'use strict'
  angular.module('app').controller 'userDetailController', userDetailController
  userDetailController.$inject = ['Shared', 'User', 'UserInfo', 'Candidate', 'Accepted','Reject', '$state', '$stateParams', 'UserAdmin','Preconfirm' ]
  return