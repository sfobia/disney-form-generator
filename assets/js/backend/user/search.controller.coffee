do ->
  userSearchController = (Shared, User, Campaign) ->
    vm = this
    vm.data = User.query()
    vm.campaigns = Campaign.query()

    vm.search = {}

    vm.advancedSearch = ->

      query = {}

      if vm.search.gender != "" && typeof vm.search.gender != 'undefined'
        query.gender = vm.search.gender

      if vm.search.first != "" && typeof vm.search.first != 'undefined'
        query['name.first'] = vm.search.first

      if vm.search.last != "" && typeof vm.search.last != 'undefined'
        query['name.last'] = vm.search.last

      if vm.search.email != "" && typeof vm.search.email != 'undefined'
        query['user.email'] = vm.search.email

      User.search id:'search', query, (data) ->
        vm.data = data

      return

#    console.log vm.campaigns
    return


  'use strict'
  angular.module('app').controller 'userSearchController', userSearchController
  userSearchController.$inject = ['Shared', 'User', 'Campaign']
  return
