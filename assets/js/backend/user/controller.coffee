do ->
  userController = (Shared, User, UserQuery, $timeout) ->
    vm        = this
    vm.data   = []
    vm.dataUser = []
    vm.page   = 1
    vm.limit  = Math.ceil($(".content").height() / 42)
    vm.order  = 'email'
    vm.sort   = '-'
    vm.listType = 'profile'

    vm.searchField = {}

    vm.search = (page, order, sort) ->
      query = {}
      if vm.queryString
        query =  q : vm.queryString

      search = {}
      search.id = 'search'
      search.page = page || vm.page
      search.limit = vm.limit
      search.order = order || vm.order
      search.sort = sort || vm.sort

      vm.listType = 'profile'

      if vm.searchField.first || vm.searchField.last
        query = {}
        query['name.first'] = vm.searchField.first if vm.searchField.first
        query['name.last'] = vm.searchField.last if vm.searchField.last

      if vm.searchField.email
        vm.listType = 'user'
        query = {}
        search.id = 'mailSearch'
        query['email'] = vm.searchField.email
        UserQuery.search search, query , (data,headers) ->
          headers   = headers()
          vm.total  = headers["x-total-count"]
          vm.dataUser = data
        return
      else
        User.search search, query , (data,headers) ->
          headers   = headers()
          vm.total  = headers["x-total-count"]
          vm.data = data
        return

      if vm.sort == ' '
        vm.sort = '-'
      else
        vm.sort = ' '
      return

    vm.search()
    return


  'use strict'
  angular.module('app').controller 'userController', userController
  userController.$inject = ['Shared', 'User', 'UserQuery', '$timeout']
  return
