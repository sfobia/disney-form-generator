do ->
  mediaDetailController = ($scope, $http, Media, $state, $stateParams) ->
    vm = this
    vm.data = {}

    if $stateParams.id
      Media.get id: $stateParams.id, (data) ->
        vm.data = data
        console.log data
    else
      $state.go('media.create')

    vm.remove = (i) ->
      delete $scope.image[i]

    vm.save = ->

      tags = ''
      if vm.data.tag and vm.data.tag.length > 0
        for tag in vm.data.tag
          tags = tags + tag.text + ','
      tags = tags.slice(0, -1)

      if vm.data._id
        Media.update id: vm.data._id, {'name': vm.data.name, 'tag': tags}, (data) ->
          console.log "ok"
      else
        Media.save vm.data, (data, status) ->
          $state.go('media.detail', id: data._id) if data._id
      return
      return
    return

  'use strict'
  angular.module('app').controller 'mediaDetailController', mediaDetailController
  mediaDetailController.$inject = ['$scope', '$http', 'Media', '$state', '$stateParams']
  return
