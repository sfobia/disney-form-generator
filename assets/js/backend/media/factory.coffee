do ->
  Media = ($resource) ->
    BASE_URL = '/V1/media'
    $resource BASE_URL + '/:id',{page:"@page",limit:"@limit",q:"@q"},
      {
        update: { method: 'PUT'}
        delete: { method: 'DELETE', isArray: true}
      }

  angular.module('app').factory 'Media', Media
  Media.$inject = ['$resource']
  return

do ->
  MediaTags = ($resource) ->
    BASE_URL = '/V1/media/tags'
    $resource BASE_URL, {}

  angular.module('app').factory 'MediaTags', MediaTags
  MediaTags.$inject = ['$resource']
  return