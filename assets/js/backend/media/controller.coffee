do ->
  mediaController = (Media,$state,$scope) ->
    vm = this
    vm.data = []
    vm.page   = 1
    vm.limit  = 8

    delay = do ->
      timer = 0
      (callback, ms) ->
        clearTimeout timer
        timer = setTimeout(callback, ms)
        return

    $scope.$watch 'vm.searchMedia', (key) ->
      if key and key.length >= 2
        delay (->
          vm.load(1)
          return
        ), 500
      else if key and key.length == 0
        vm.load(1)
      return

    vm.load = (page)->
      search = {}
      search.page = page || vm.page
      search.limit = vm.limit
      search.q = vm.searchMedia

      Media.query {}, search, (data,headers) ->
        headers   = headers()
        vm.total  = headers["x-total-count"]
        vm.data   = data
      return

    vm.delete = (item)->
      if confirm('sei sicuro?')
        Media.delete id: item._id,  (data) ->
          console.log data
          # controllo se la foto è utilizzata
          if data.length == 0
            vm.data.splice(vm.data.indexOf(item),1)
            return
          if data
            alert('La foto è utilizzata :  '+ data.join(", "))
      return


    vm.load()
    return


  'use strict'
  angular.module('app').controller 'mediaController', mediaController
  mediaController.$inject = ['Media','$state','$scope']
  return
