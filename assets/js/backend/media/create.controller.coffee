do ->
  createMediaController = ($scope, $http, Media, $state, $stateParams) ->
    vm = this

    vm.remove = (i) ->
      delete $scope.image[i]
      console.log $scope.image

    vm.save = ->
      for img in $scope.image
        if img.info.removed
          continue

        tags = []
        if img.info.tag and img.info.tag.length > 0
          for tag in img.info.tag
            tags.push tag.text

        formData = new FormData
        formData.append('name', img.info.name)
        formData.append('tag', tags)
        formData.append('image', img)
        $http.post('/V1/media', formData,
          headers:
            'Content-Type': undefined
          transformRequest: angular.identity
        ).success (data, status) ->
          for im in $scope.image
            if status == 200 && data.name == im.name
              im.info.removed = true
          return

      return
    return

  'use strict'
  angular.module('app').controller 'createMediaController', createMediaController
  createMediaController.$inject = ['$scope', '$http', 'Media', '$state', '$stateParams']
  return
