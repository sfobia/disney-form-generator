do ->
  Client = ($resource) ->
    BASE_URL = '/V1/client'
    $resource BASE_URL + '/:id', {page:"@page",limit:"@limit"},
      {
        update: {method: 'PUT'}
      }

  angular.module('app').factory 'Client', Client
  Client.$inject = ['$resource']
  return