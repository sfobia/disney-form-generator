do ->
  clientController = ( Shared, Client ) ->
    vm        = this
    vm.data   = Client.query()

    return

  'use strict'
  angular.module('app').controller 'clientController', clientController
  clientController.$inject = [ 'Shared', 'Client']
  return
