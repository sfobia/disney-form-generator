do ->
  clientDetailController = (Shared, Client, $state, $stateParams) ->
    vm = this

    if $stateParams.id
      Client.get id: $stateParams.id, (data) ->
        vm.data = data
    else
      vm.data = {}

    vm.save = ->
      if vm.data._id
        Client.update id: vm.data._id, vm.data, (data) ->
          console.log "ok"
      else
        Client.save vm.data, (data, status) ->
          $state.go('client.detail', id: data._id) if data._id
      return

    vm.info = ( field ) ->
      if !field.info
        field.info = {}

    vm.delete = (item)->
      if confirm('sei sicuro?')
        Client.delete id: item, ->
          $state.go('client.list')

    return


  'use strict'
  angular.module('app').controller 'clientDetailController', clientDetailController
  clientDetailController.$inject = ['Shared', 'Client', '$state', '$stateParams']
  return
