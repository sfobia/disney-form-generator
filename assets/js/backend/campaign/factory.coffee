do ->
  Campaign = ($resource) ->
    BASE_URL = '/V1/campaign'
    $resource BASE_URL + '/:id', { page:"@page", limit:"@limit", published:"@published" },
      {
        update: {method: 'PUT'}
        search: {method: 'POST', isArray : true}
        get_all: {method: 'GET', isArray : true}
      }

  angular.module('app').factory 'Campaign', Campaign
  Campaign.$inject = ['$resource']
  return

do ->
  Campaigns = ($resource) ->
    BASE_URL = '/V1/profile/campaigns'
    $resource BASE_URL + '/:action/:id', {page:"@page",limit:"@limit"},
      {
        get: {method: 'GET', isArray: true }
        getSingle: {method: 'GET', isArray: false }
        update: {method: 'PUT'}
        search: {method: 'POST', isArray : false }
      }

  angular.module('app').factory 'Campaigns', Campaigns
  Campaigns.$inject = ['$resource']
  return

do ->
  CampaignInfo = ($resource) ->
    BASE_URL = '/V1/campaign'
    $resource BASE_URL + '/info', {}, query: {method: 'GET', isArray: false }

  angular.module('app').factory 'CampaignInfo', CampaignInfo
  CampaignInfo.$inject = ['$resource']
  return

do ->
  AdminInfo = ($resource) ->
    BASE_URL = '/V1/admin'
    $resource BASE_URL + '/stats', {}, query: {method: 'GET' }

  angular.module('app').factory 'AdminInfo', AdminInfo
  AdminInfo.$inject = ['$resource']
  return


do ->
  CampaignSetStatus = ($resource) ->
    BASE_URL = '/V1/campaign/status'
    $resource BASE_URL + '/:id', {}, update: {method: 'PUT'}

  angular.module('app').factory 'CampaignSetStatus', CampaignSetStatus
  CampaignSetStatus.$inject = ['$resource']
  return

do ->
  CampaignExport = ($resource) ->
    BASE_URL = '/V1/campaign/responses'
    $resource BASE_URL + '/:id', {}, query: {method: 'GET' }

  angular.module('app').factory 'CampaignExport', CampaignExport
  CampaignExport.$inject = ['$resource']
  return

do ->
  UserFilter = ($resource) ->
    BASE_URL = '/V1/campaign/filterCandidates'
    $resource BASE_URL + '/:id', {}, filterSimulate: {method: 'POST' }

  angular.module('app').factory 'UserFilter', UserFilter
  UserFilter.$inject = ['$resource']
  return
