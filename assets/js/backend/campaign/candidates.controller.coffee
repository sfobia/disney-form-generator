do ->
  campaignCandidatesController = (Shared, Campaign, CampaignExport, Candidate, $state, $stateParams, Accepted, Reject, AcceptPreconfirmed, SendMailToCandidates, $http, SendTrackingToCandidates, ReportExport ) ->
    vm = this
    vm.selected = []
    vm.rejected = []
    vm.selectedAll = false
    vm.rejectedAll = false
    vm.limit = Math.ceil($(".content").height() / 42)
    vm.page = 1
    vm.dataCampaign = {}
    vm.now    = new Date().toISOString()

    vm.changeLimit = (page) ->
      query =
        campaign:  $stateParams.id

      search = {}
      search.id = 'query'
      search.page = page || vm.page
      search.limit = vm.limit

      Candidate.search search, query, (data, headers) ->
        headers   = headers()
        vm.total  = headers["x-total-count"]
        vm.data   = data
        return
      return

    Candidate.get query:"total-accepted", id: $stateParams.id  , (total) ->
      vm.totalActived = total.total

    vm.init = (order) ->
      if $stateParams.id
        query =
          campaign:  $stateParams.id

        vm.campaignId = $stateParams.id

        Campaign.get id:$stateParams.id, (data) ->
          vm.dataCampaign = data

        search        = {}
        search.id     = 'query'
        search.page   = vm.page
        search.limit  = vm.limit
        search.order   = order if order

        Candidate.search search, query, (data, headers) ->
          headers   = headers()
          vm.total  = headers["x-total-count"]
          vm.data   = data
          return

    vm.init()

    return
  'use strict'
  angular.module('app').controller 'campaignCandidatesController', campaignCandidatesController
  campaignCandidatesController.$inject = ['Shared','Campaign', 'CampaignExport', 'Candidate', '$state', '$stateParams', 'Accepted', 'Reject', 'AcceptPreconfirmed', 'SendMailToCandidates', '$http', 'SendTrackingToCandidates', 'ReportExport' ]
  return
