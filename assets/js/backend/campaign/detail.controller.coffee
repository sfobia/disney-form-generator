do ->
  campaignDetailController = ($scope, Shared, Campaign, $state, $stateParams, CampaignInfo, Form, Categories, Client ) ->
    vm = this
    vm.isDataHasChanges = false
    vm.rnd = Math.random()

    vm.gridColumn = [
      "col-md-2"
      "col-md-4"
      "col-md-6"
      "col-md-8"
      "col-md-12"
    ]

    # fix per mantenimento
    vm.init = ->
      vm.data = {}
      vm.data.type = 'test'
      vm.data.pieces = 10
      vm.data.piecesInitial  = 10
      vm.data.order = 1
      vm.data.slug = "slug-" + new Date().getTime()

    vm.reloadInfo = ->
      Form.query (data)->
        vm.form = data.filter (item) ->
          item.formType == "campaign"

      Form.query (data)->
        vm.survey = data.filter (item) ->
          item.formType == "survey"

    vm.formUpdate = ->

      Form.get id: vm.data.form, (data)->
        form = data
        if form.multiProduct
          vm.multiProduct = true
          count = 0
          for product in form.products
            count = parseInt(count)+parseInt(product.quantity)
          vm.data.piecesInitial =count
        else
          vm.multiProduct = false

    vm.reloadInfo()

    vm.info = CampaignInfo.query()

    vm.client = Client.query()
    vm.categorie
    s = Categories.query()
    vm.time = []
#      hours: 11
#      minutes: 0

    vm.reset = ->
      {
        dates:
          startCampaign: new Date
          endCampaign: new Date
          startCandidations: new Date
          endCandidations: new Date
          releaseFeedback: new Date
          startSurvey: new Date
          endSurvey: new Date
        media:
          images: []
          logo: []
          skin: []
          cover: []
          footer: []
        tag: []
        content: {}
      }

    if $stateParams.id
      Campaign.get id: $stateParams.id, (data) ->
        angular.forEach data.dates, ( value,i ) ->
          data.dates[i] = new Date( value )

        vm.time.hours = data.dates.startCampaign.getHours()
        vm.time.minutes = data.dates.startCampaign.getMinutes()

        vm.time.endHours = data.dates.endCampaign.getHours()
        vm.time.endMinutes = data.dates.endCampaign.getMinutes()

        vm.data = data
        vm.originalData = JSON.stringify(vm.data)
        vm.formUpdate()
    else
      vm.data = vm.reset()


    vm.save = ->

      time = new Date
      time.setHours(vm.time.hours)
      time.setMinutes(vm.time.minutes)

      vm.data.dates.startCampaign.setHours(time.getHours(),time.getMinutes(),0)

      timeEnd = new Date
      timeEnd.setHours(vm.time.endHours)
      timeEnd.setMinutes(vm.time.endMinutes)

      vm.data.dates.endCampaign.setHours(timeEnd.getHours(),timeEnd.getMinutes(),0)

#      angular.forEach vm.data.dates, ( value,i ) ->
#        value.setHours(time.getHours(),time.getMinutes(),0)

      if vm.data._id
        Campaign.update id: vm.data._id, vm.data, (data) ->
          vm.originalData = JSON.stringify(vm.data)
      else
        Campaign.save vm.data, (data) ->
          $state.go('campaign.detail', id: data._id) if data._id
      return

    vm.delete = ->
      if confirm('sei sicuro di voler cancellare '+vm.data.title+' ?')
        if confirm('sei veramanete sicuro di voler cancellare '+vm.data.title+' ? NON POTRA ESSERE RIPRISTINATA')
          if vm.data._id
              Campaign.delete id: vm.data._id, (res) ->
                $state.go('campaign.list')
                return
      return

    vm.addTag = ->
      vm.data.tag.push({})
      return

    vm.removeTag = ( index ) ->
      vm.data.tag.splice(index, 1)
      return

    vm.addContent = ->
      vm.data.content.push({
        html: ''
        column: 'col-md-6'
        section: 0
      })

    vm.addContentCustom = (item) ->
      item.customs = item.customs || []
      item.customs.push({})

    vm.addBoxRisultati = () ->
      vm.data.content.risultati = vm.data.content.risultati || {}
      vm.data.content.risultati.box = vm.data.content.risultati.box || []
      vm.data.content.risultati.box.push({})

    vm.removeBoxRisultati = (item) ->
      vm.data.content.risultati.box.splice(vm.data.content.risultati.box.indexOf(item),1)

    vm.removeCustom = (list,item) ->
      list.customs.splice(list.customs.indexOf(item),1)


    ###
    window.onbeforeunload =  ->
      if vm.originalData != JSON.stringify(vm.data)
        return "Non hai salvato"

    $scope.$on '$destroy', () ->
      window.onbeforeunload = null
    ###

    return

  'use strict'
  angular.module('app').controller 'campaignDetailController', campaignDetailController
  campaignDetailController.$inject = ['$scope','Shared', 'Campaign', '$state', '$stateParams', 'CampaignInfo', 'Form', 'Categories', 'Client' ]
  return
