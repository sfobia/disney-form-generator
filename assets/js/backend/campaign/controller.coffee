do ->
  campaignController = (Shared, Campaign,Candidate,$timeout,moment) ->
    vm        = this
    vm.data   = []
    vm.page   = 1
    vm.limit  = Math.ceil($(".content").height() / 42)
    vm.order  = 'dates.startCampaign'
    vm.sort   = '-'
    vm.weeks = {}

    vm.search = (page, order, sort) ->
      query = {}
      if vm.queryString
        query =  q : vm.queryString

      search = {}
      search.id = 'query'
      search.page = page || vm.page
      search.limit = vm.limit
      search.order = order || vm.order
      search.sort = sort || vm.sort
      search.published = null || vm.published

      Campaign.search search, query, (data,headers) ->
        headers   = headers()
        vm.total  = headers["x-total-count"]
        vm.data   = data

      if vm.sort == ' '
        vm.sort = '-'
      else
        vm.sort = ' '
      return

    vm.getCandidate = (e,item) ->
      if item.count == null or item.count == undefined
        $(e.target).addClass("rotating")
        item.count = -1
        Candidate.get query:"total", id: item._id , (data) ->
          item.count = data.total
          $(e).removeClass("rotating")
        return
      return

    vm.getProgress = (item) ->
      if item.progress == null or item.progress == undefined
        if item.status
          count = 0
          for k,x of item.status
            if x == true
              count++
          item.progress = Math.ceil((count * 100) / 7)
        return



    vm.getWeekDay = (weekNumber) ->
      time = moment()
      day = time.day("Wednesday").week(weekNumber)
      return day.format('dddd D MMM')

    vm.existWeekNumber = (week) ->
      weekBool = if vm.weeks[week] then wm.weeks[week] else false
      if !vm.weeks[week]
        vm.weeks[week] = week
      weekBool

    vm.search()
    return

  'use strict'
  angular.module('app').controller 'campaignController', campaignController
  campaignController.$inject = ['Shared', 'Campaign','Candidate','$timeout','moment']
  return
