do ->
  trackingCandidatesController = (Shared, Campaign, Candidate, $state, $stateParams, Accepted, Reject, AcceptPreconfirmed, SendMailToCandidates, $http, SendTrackingToCandidates ) ->
    vm = this
    vm.selected = []
    vm.rejected = []
    vm.selectedAll = false
    vm.rejectedAll = false
    vm.limit = Math.ceil($(".content").height() / 42)
    vm.page = 1
    vm.dataCampaign = {}
    vm.now    = new Date().toISOString()

    vm.sendTrackingMail = ->
      if confirm 'vuoi inviare il tracking code ai candidati accettati?'
        if $stateParams.id
          SendTrackingToCandidates.save id: $stateParams.id, null, (data) ->
#            alert 'Inviate ' + data.numberSentMail + ' email'
          return
        vm.init()
      return

    vm.importCsv = ->
      $('#BtnCsv').button('loading')
      formData = new FormData
      formData.append('csv', vm.csv[0])

      $http.post( '/V1/candidate/import/csv', formData,
        headers:
          'Content-Type': undefined
        transformRequest: angular.identity
      ).success (data, status) ->
        window.location.reload()
      return

    vm.init = ->

      if $stateParams.id
        query =
          campaign:  $stateParams.id

        vm.campaignId = $stateParams.id

        Campaign.get id:$stateParams.id, (data) ->
          vm.dataCampaign = data

        search        = {}
        search.id     = 'query'
        search.page   = vm.page
        search.limit  = vm.limit

        Candidate.search search, query, (data, headers) ->
          headers   = headers()
          vm.total  = headers["x-total-count"]
          vm.data   = data
          return

    vm.init()

    return
  'use strict'
  angular.module('app').controller 'trackingCandidatesController', trackingCandidatesController
  trackingCandidatesController.$inject = ['Shared','Campaign', 'Candidate', '$state', '$stateParams', 'Accepted', 'Reject', 'AcceptPreconfirmed', 'SendMailToCandidates', '$http', 'SendTrackingToCandidates' ]
  return
