do ->
  processAController = (Shared, Campaign, Candidate, $state, $stateParams, Accepted, RejectAll, UserFilter, AcceptPreconfirmed, SendMailToCandidates, $http, SendTrackingToCandidates,CampaignSetStatus,SendSurveyToCandidates ) ->
    vm = this
    vm.campaignId = $stateParams.id
    vm.totalPre = 0
    vm.attesa = false
    vm.fasi =
      'accepted' : true
      'rejected' : false
      'mailAccepted' : false
      'fase3' : false
      'exportXls' : false
      'importXls' : false
      'mailTracking' : false
      'mailSurvey' : false
    vm.info = {}

    vm.sex = 'any'
    vm.age = {min: '', max: ''}
    vm.zone = 'any'
    vm.simulate = true

    vm.init = ->
      Campaign.get id: vm.campaignId, (data) ->
        vm.data = data
        vm.checkStatus()
        Candidate.get query:"total-preconfirmed", id: data._id , (total) ->
          vm.totalPre = total.total
        Candidate.get query:"total-accepted", id: data._id , (total) ->
          vm.totalActived = total.total
        Candidate.get query:"total-rejected", id: data._id , (total) ->
          vm.totalRejected = total.total

    vm.filter = (sex, age, zone, simulate) ->
      msg = 'Vuoi filtrare gli utenti con queste specifiche?'
      if !simulate
        msg = 'Attenzione stai attivando gli utenti filtrati'

      if confirm msg
        vm.attesa = true
        if vm.campaignId
          query = {
            simulate: simulate
          }
          if vm.age.min >= 1 and vm.age.max <= 1000
            query.age = vm.age
          if vm.zone != 'any'
            query.zone = vm.zone
          if vm.sex != 'any'
            query.sex = vm.sex
          UserFilter.filterSimulate {id: vm.campaignId}, query, (data) ->
            if !simulate
              vm.setStatus 'accepted'
              vm.info.accepted = data.count


            vm.attesa = false
            vm.info.filtered = data.count

            vm.candidates = data.candidates

    vm.downloadPreviewSelected = ->
      if confirm 'Confermi l\'anteprima?'
        $('#BtnExpCsv').button('loading')
        Candidate.export { query: 'export', id: vm.campaignId, candidatesIds: vm.candidates  }, null, (data) ->
          if data.file
            location.replace( data.file );
          return


    vm.accepted = ->
      if confirm 'Vuoi confermare gli utenti pre confermati?'
        if vm.campaignId
          AcceptPreconfirmed.save id: vm.campaignId, null, (data) ->
            vm.info.accepted = data.numberAccepted
            vm.setStatus 'accepted'

    vm.rejected = ->
      if confirm 'Vuoi rigettare gli utenti?'
        if vm.campaignId
          RejectAll.save id: vm.campaignId, null, (data) ->
            vm.info.rejected = data.numberRejected
            vm.setStatus 'rejected'

    vm.exportXls = ->
      if confirm 'Confermi l\'export?'
        $('#BtnExpCsv').button('loading')
        Candidate.export { query: 'export', id: vm.campaignId  }, null, (data) ->
          vm.setStatus 'exportXls'
          $('#BtnExpCsv').button('reset')
          if data.file
            location.replace( data.file );
          return
        return

    vm.mailAccepted = ->
      if confirm 'Vuoi inviare le mail di conferma ai candidati accettati?'
        SendMailToCandidates.save id: vm.campaignId, null, (data) ->
          vm.setStatus 'mailAccepted'
          vm.info.mailAccepted = data.numberSentMail
          return

    vm.mailTracking = ->
      if confirm 'Vuoi inviare il tracking ai candidati accettati?'
        SendTrackingToCandidates.save id: vm.campaignId, null, (data) ->
          vm.setStatus 'mailTracking'
          vm.info.mailTracking = data.total
        return
      return

    vm.mailSurvey = ->
      if confirm 'Vuoi inviare il survey ai candidati accettati?'
        SendSurveyToCandidates.save id: vm.campaignId, null, (data) ->
          vm.setStatus 'mailSurvey'
          vm.info.mailSurvey = data.total
        return
      return

    vm.importXls = ->
      $('#BtnCsv').button('loading')
      formData = new FormData
      formData.append('csv', vm.csv[0])

      $http.post( '/V1/candidate/import/csv', formData,
        headers:
          'Content-Type': undefined
        transformRequest: angular.identity
      ).success (data, status) ->
        $('#BtnCsv').button('reset')
        vm.setStatus 'importXls'
        vm.info.importXls = data.total
      return


    vm.checkStatus = ->
      for k,st of vm.data.status
        vm.fasi[k] = st
      return

    vm.setStatus = (fase) ->
      CampaignSetStatus.update { id: vm.campaignId} , status: fase, (data) ->
        vm.fasi[fase] = true
        vm.init()
      return


    vm.init()

    return
  'use strict'
  angular.module('app').controller 'processAController', processAController
  processAController.$inject = ['Shared','Campaign', 'Candidate', '$state', '$stateParams', 'Accepted', 'RejectAll', 'UserFilter', 'AcceptPreconfirmed', 'SendMailToCandidates', '$http', 'SendTrackingToCandidates','CampaignSetStatus','SendSurveyToCandidates' ]
  return
