do ->
  ChooserImages = ($rootScope, Media, Shared) ->
    {
      scope:
        multiple: '='
        model: '='
      restrict: 'A'
      templateUrl: 'partials/media/chooser.html',
      controller: ($scope) ->

        $rootScope.$on 'loadMedia', (event, mass) ->
          $scope.load()

        delay = do ->
          timer = 0
          (callback, ms) ->
            clearTimeout timer
            timer = setTimeout(callback, ms)
            return

        $scope.$watch 'mediaSearch', (key) ->
          if key and key.length >= 2
            delay (->
              $scope.load()
              return
            ), 500
          else if key and key.length == 0
            $scope.load()
          return

        $scope.load = ->
          $scope.loader = true
          query = {}

          if $scope.mediaSearch
            query =
              q: $scope.mediaSearch

          Media.query query, (data) ->
            $scope.data = data
            $scope.loader = false

        $scope.close = ->
          Shared.showChooser(null)

        $scope.choose = (item) ->
          if ! $rootScope.multiple
            $rootScope.source.splice(0,$rootScope.source.length)

          $rootScope.source.push
            name: item.name
            path: item.image.original.url
            ref: item._id
        return
    }

  angular.module('app').directive('chooserImages', ChooserImages)
  ChooserImages.$inject = ['$rootScope', 'Media', 'Shared']