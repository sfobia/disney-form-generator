do ->
  authController = (UserAdmin) ->
    vm = this
    vm.user = {}

    vm.login = ->
      $('#BtnLogin').button('loading')
      UserAdmin.login action: 'login', vm.user, (data) ->
        if data.token
          localStorage.setItem 'token', data.token
          document.cookie = 'tokenom' + '=' + data.token  + '; expires=Fri, 31 Dec 2030 23:59:59 GMT; path=/;'
          return window.location.href = '/admin'
        else
          $('#BtnLogin').button('reset')
        return



    return
  'use strict'
  angular.module('app').controller 'authController', authController
  authController.$inject = ['UserAdmin']
  return
