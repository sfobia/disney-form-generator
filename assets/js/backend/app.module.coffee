do ->
  'use strict'
  angular.module('app', ['ngConstants','ui.router', 'ngResource', 'ngTagsInput','bw.paging','trumbowyg-ng','angular.filter', 'angularMoment']).run [
    '$rootScope'
    '$state'
    'Shared'
    ($rootScope, $state, Shared) ->
      $rootScope.$on '$stateChangeStart', (event, toState, toParams, fromState, fromParams) ->
        Shared.showChooser(false)

        ###
        if toState.name != 'auth' and !Auth.authenticate()
          event.preventDefault()
          $state.go 'auth'
        ####

        if toState.redirectTo
          event.preventDefault()
          $state.go toState.redirectTo

        return
      return
  ]
  return
