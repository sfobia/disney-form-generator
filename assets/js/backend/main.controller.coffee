do ->
  mainController = (environment,$state,$location,Shared,$rootScope,$timeout,AdminInfo) ->
    vm = this
    vm.isAuth =  false
    vm.days = ->
      [1..31]
    vm.environment = environment
    vm.menu = [
#      {
#        title: "User"
#        path: "user"
#        col:'8'
#        items: [
#          {
#            title: "List"
#            path: "user.list"
#          }
#        ]
#      }
      {
        title: "Questionari"
        path: "campaign"
        col:'4'
        items: [
          {
            title: "List"
            path: "campaign.list"
          }
          {
            title: "Create"
            path: "campaign.create"
          }
        ]
      }
#      {
#        title: "Review Tester"
#        path: "review"
#        col: '4'
#        items: [
#          {
#            title: "List"
#            path: "review.testerlist"
#          }
#        ]
#      }
#      {
#        title: "Review"
#        path: "review"
#        col:'2'
#        items: [
#          {
#            title: "List"
#            path: "review.list"
#          }
#        ]
#      }
#      {
#        title: "Client"
#        path: "client"
#        col:'4'
#        items: [
#          {
#            title: "List"
#            path: "client.list"
#          }
#          {
#            title: "Create"
#            path: "client.create"
#          }
#        ]
#      }
      {
        title: "Form"
        path: "form"
        col:'4'
        items: [
          {
            title: "List"
            path: "form.list"
          }
          {
            title: "Create"
            path: "form.detail"
          }
        ]
      }
#      {
#        title: "Category"
#        path: "category"
#        col:'4'
#        items: [
#          {
#            title: "List"
#            path: "category.list"
#          }
#          {
#            title: "Create"
#            path: "category.detail"
#          }
#        ]
#      }
#      {
#        title: "Template"
#        path: "template"
#        col:'4'
#        items: [
#          {
#            title: "List"
#            path: "template.list"
#          }
#          {
#            title: "Create"
#            path: "template.detail"
#          }
#        ]
#      }
#      {
#        title: "Media"
#        path: "media"
#        col:'8'
#        items: [
#          {
#            title: "List"
#            path: "media.list"
#          }
#          {
#            title: "Upload"
#            path: "media.create"
#          }
#        ]
#      }
      {
        title: "Report"
        path: "report"
        col:'8'
        items: [
#          {
#            title: "List"
#            path: "report.list"
#          }
          {
            title: "Export"
            path: "report.export"
          }
          {
            title: "Download"
            path: "report.download"
          }
        ]
      }
    ]

    findById = (source, id,total,stats,images) ->
      i = 0
      while i < source.length
        if source[i].path == id
          source[i].total = total
          source[i].stats  = stats if stats
          source[i].images = images if images
          return source[i]
        i++
      throw 'Couldn\'t find object with id: ' + id
      return

    AdminInfo.get {}, (data,headers) ->
      findById vm.menu, "campaign", data.campaigns
      findById vm.menu, "form",     data.forms
      findById vm.menu, "user",     data.users, data.usersStats
#      findById vm.menu, "media",    data.media, null, data.mediaImages
#      findById vm.menu, "category", data.categories
#      findById vm.menu, "client",   data.clients
#      findById vm.menu, "template", data.templates


    $rootScope.$on("auth", (bool,val) ->
      vm.isAuth = val.status
    )

    vm.hideStatusError = ->
      console.log $rootScope.statusError
      $timeout ->
        $rootScope.statusError = null

    vm.logout = ->
      localStorage.removeItem("token")
      window.location.href = '/admin#auth'


    vm.go = (path) ->
      $state.go(path + '.list')

    vm.parseLocation = (location) ->
      locationSubstring = location.substring(1)
      obj = {}
      if locationSubstring
        pairs = locationSubstring.split('&')
        pair = undefined
        i = undefined
        for i of pairs
          `i = i`
          if pairs[i] == ''
            continue
          pair = pairs[i].split('=')
          obj[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1])
      obj

    vm.saveToken = ->
      search = $location.search()
      location_parse = vm.parseLocation(window.location.search)
      params = if Object.keys(search).length==0 then location_parse else search
      token = params.auth_token || params.token
      if token
        localStorage.setItem("token",token)
        document.cookie = 'tokenom' + '=' + token  + '; expires=Fri, 31 Dec 2030 23:59:59 GMT; path=/;'
        window.location.href = '/admin'

    vm.saveToken()

    return


  'use strict'
  angular.module('app').controller 'mainController', mainController
  mainController.$inject = ['environment','$state','$location','Shared','$rootScope','$timeout','AdminInfo']
  return
