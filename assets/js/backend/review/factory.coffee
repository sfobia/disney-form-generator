do ->
  Review = ($resource) ->
    BASE_URL = '/V1/review'
    $resource BASE_URL + '/:id', { page:"@page", limit:"@limit", published:"@published" },
      {
        search: {method: 'POST', isArray : true}
        update: {method: 'PUT'}
      }

  angular.module('app').factory 'Review', Review
  Review.$inject = ['$resource']
  return