do ->
  reviewDetailController = (Shared, Review, $state, $stateParams) ->
    vm = this

    vm.statusOptions = ["published", "unpublished", "rejected"]

    if $stateParams.id
      Review.get id: $stateParams.id, (data) ->
        vm.data = data
    else
      vm.data = {}

    vm.delete = (item) ->
      if confirm('sei sicuro?')
        Review.delete id: item, ->
          $state.go('review.list')

    vm.save = ->
      console.log vm.data
      obj = {
        status: vm.data.status
      }
      console.log obj
      Review.update id: vm.data._id, obj, (data) ->
        console.log "ok"
      return
    return


  'use strict'
  angular.module('app').controller 'reviewDetailController', reviewDetailController
  reviewDetailController.$inject = ['Shared', 'Review', '$state', '$stateParams']
  return