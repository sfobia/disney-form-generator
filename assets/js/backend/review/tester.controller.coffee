do ->
  reviewTesterController = (Shared, Review, Campaign, $timeout) ->
    vm        = this
    vm.data   = []
    vm.page   = 1
    vm.limit  = Math.ceil($(".content").height() / 42)
    vm.order  = 'dates.submitted'
    vm.sort   = '-'
    vm.weeks = {}
    vm.campaign = ''
    vm.status = ''

    vm.statusOptions = ["", "published", "unpublished", "rejected"]

    vm.campaignList = []
    Campaign.get_all {limit: 999999}, (data) ->
      vm.campaignList = data
      console.log vm.campaignList

    vm.updatePublished = (id, published) ->
      obj = {
        status: published
      }
      Review.update id: id, obj, (data) ->
        console.log "ok"
      return

    vm.search = (campaign, status, page, order, sort) ->
      query = {}
      if vm.queryString
        query =  q : vm.queryString

      search = {}
      search.id = 'query'
      if campaign
        search.campaign = campaign
        vm.campaign = campaign
      if status
        search.status = status
        vm.status = status
      search.page = page || vm.page
      search.limit = vm.limit
      search.order = order || vm.order
      search.sort = sort || vm.sort
      search.published = null || vm.published
      search.tester = true

      Review.search search, query, (data,headers) ->
        headers   = headers()
        vm.total  = headers["x-total-count"]
        vm.data   = data

      if vm.sort == ' '
        vm.sort = '-'
      else
        vm.sort = ' '
      return

    vm.search()
    return

  'use strict'
  angular.module('app').controller 'reviewTesterController', reviewTesterController
  reviewTesterController.$inject = ['Shared', 'Review', 'Campaign', '$timeout']
  return