angular.module('app').directive('fileModel', [
  '$parse'
  ($parse) ->
    {
    restrict: 'A'
    link: (scope, element, attrs) ->
      model = $parse(attrs.fileModel)
      modelSetter = model.assign
      element.bind 'change', ->
        scope.$apply ->
          for img in element[0].files
            img.info = {}
          modelSetter scope, element[0].files
          return
        return
      return

    }
])

angular.module('app').directive('imagePreview',
  () ->
    {
      restrict: 'EAC'
      link: (scope, element, attrs) ->
        scope.getImage = (file)->
          reader = new FileReader()
          reader.onload = (e) ->
            element[0].style.backgroundImage = "url(#{e.target.result})"
          reader.readAsDataURL(file)
        scope.getImage(scope.file)
        return

    }
)

angular.module('app').filter 'bytes', ->
  (bytes, precision) ->
    if isNaN(parseFloat(bytes)) or !isFinite(bytes)
      return '-'
    if typeof precision == 'undefined'
      precision = 1
    units = [
      'bytes'
      'KB'
      'MB'
      'GB'
      'TB'
      'PB'
    ]
    number = Math.floor(Math.log(bytes) / Math.log(1024))
    (bytes / 1024 ** Math.floor(number)).toFixed(precision) + ' ' + units[number]
