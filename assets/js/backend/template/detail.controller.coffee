do ->
  templateDetailController = (Shared, Template, $state, $stateParams) ->
    vm = this
    vm.types = ["site","email","site+email"]
    if $stateParams.id
      Template.get id: $stateParams.id, (data) ->
        vm.data = data
    else
      vm.data = {}

    vm.save = ->
      if vm.data._id
        Template.update id: vm.data._id, vm.data, (data) ->
          console.log "ok"
      else
        Template.save vm.data, (data, status) ->
          $state.go('template.detail', id: data._id) if data._id
      return
    return



  'use strict'
  angular.module('app').controller 'templateDetailController', templateDetailController
  templateDetailController.$inject = ['Shared', 'Template', '$state', '$stateParams']
  return
