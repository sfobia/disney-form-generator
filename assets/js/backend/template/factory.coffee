do ->
  Template = ($resource) ->
    BASE_URL = '/V1/template'
    $resource BASE_URL + '/:id', {}, update: {method: 'PUT'}

  angular.module('app').factory 'Template', Template
  Template.$inject = ['$resource']
  return