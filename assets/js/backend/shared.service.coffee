do ->
  num = ->
    (input) ->
      parseInt input.replace("%","")

  angular.module('app').filter('num', num)

do ->
  Shared = ($rootScope, $timeout) ->
    shared = {}

    shared.changeState = (type)->
      $rootScope.statusClass = type
      shared.loader(false)
      $timeout(->
        $rootScope.statusClass = null
      , 3000)

    shared.loader = (state) ->
      $rootScope.startLoader = state

    shared.statusError = (state) ->
      $rootScope.statusError = state
      $timeout(->
        $rootScope.statusError = null
      , 60000)

    shared.checkError = (fields) ->
      $(".form-group").removeClass("has-error")
      for f in fields
        #$("#field-" + f).parent(".form-group").addClass("has-error")
        return

    shared.showChooser = (model,multiple) ->
      $rootScope.source       = model
      $rootScope.multiple     = multiple
      $rootScope.showChooser  = model
      $rootScope.$emit 'loadMedia' if  $rootScope.showChooser

    shared
  angular.module('app').service('Shared', Shared)
  Shared.$inject = ['$rootScope', '$timeout']


do ->
  APIInterceptor = (Shared, $timeout,$location,$rootScope) ->
    service = this

    service.request = (config) ->
      config.headers = config.headers || {}

      if localStorage.token
        token = localStorage.token
        config.headers.Authorization = 'Bearer ' + token

      Shared.loader true
      config

    service.response = (response) ->
      if $location.$$path != "/auth"
        $rootScope.$broadcast("auth", status:true)
      else
        $rootScope.$broadcast("auth", status:false)

      if response.config.method == 'POST' or response.config.method == 'PUT' or response.config.method == 'DELETE'
        Shared.changeState("success")
      Shared.loader false
      Shared.checkError []
      response

    service.responseError = (response) ->
      if response.status == 401 || response.status == 403
        $rootScope.$broadcast("auth", status:false)
        window.location.href = '/admin#auth'

      if response.data.message
        Shared.statusError response.data.message
        Shared.checkError response.data.validation.keys if response.data.validation

      Shared.loader false
      Shared.changeState("error")
      response

    service
  angular.module('app').factory('APIInterceptor', APIInterceptor)
  APIInterceptor.$inject = ['Shared', '$timeout','$location','$rootScope']