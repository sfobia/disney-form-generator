do ->
  reportDownloadController = ( Shared, Report, ReportExport, ReportInfo, $http ) ->
    vm = this


    vm.download = ->
      Report.query {page:'messages'}, (data) ->
        vm.downloadList = data
        return

    vm.downloadFile = (file)->

      $http(
        method: 'GET'
        cache: false
        url: '/V1/report/download/' + file._id
        responseType: 'arraybuffer'
        headers:
          'Content-Type': 'application/csv; charset=utf-8'
          'fileID': file.id
      ).success((data, status, headers, config) ->
        console.log status
        if status == 404
          alert 'IL file non è più presente'
          return
        csv = new Blob([ data ], type: 'application/csv')

        fileURL = URL.createObjectURL( csv )
        a = document.createElement('a')
        a.href = fileURL
        a.target = '_blank'
        a.download = file.name+'.csv'
        document.body.appendChild a
        a.click()
        vm.download()
        return

      ).error (data, status, headers, config) ->
        return

    vm.delete = (item) ->
      if confirm "Sei sicuro? se cancelli questo riga, l'export inizierà dal prima utente"
        ReportExport.remove {id: item._id}, ( data ) ->
          vm.download()
      return

    vm.download()

    return
  'use strict'
  angular.module('app').controller 'reportDownloadController', reportDownloadController
  reportDownloadController.$inject = [ 'Shared', 'Report', 'ReportExport', 'ReportInfo', '$http']
  return
