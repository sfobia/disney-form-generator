do ->
  Report = ($resource) ->
    BASE_URL = '/V1/report'
    $resource BASE_URL + '/:page/:slug', {month:"@month", year:"@year"}, {}

  angular.module('app').factory 'Report', Report
  Report.$inject = ['$resource']
  return

do ->
  ReportExport = ($resource) ->
    BASE_URL = '/V1/report'
    $resource BASE_URL + '/:query/:id', { page:"@page", limit: "@limit" },
      {
        export: { method: 'GET' },
        pending: { method: 'POST' }
        search: { method: 'POST', isArray: false }
      }

  angular.module('app').factory 'ReportExport', ReportExport
  ReportExport.$inject = ['$resource']
  return

do ->
  ReportInfo = ($resource) ->
    BASE_URL = '/V1/admin'
    $resource BASE_URL + '/stats', {}, query: {method: 'GET' }

  angular.module('app').factory 'ReportInfo', ReportInfo
  ReportInfo.$inject = ['$resource']
  return