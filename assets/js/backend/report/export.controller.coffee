do ->
  reportExportController = ( Shared, Report, ReportExport, ReportInfo, Campaign, CampaignExport, Form ) ->
    vm = this
    vm.data = {}
    vm.limit = 5000
    vm.limitReport = null
    vm.page = 1
    vm.totalUsers = 0
    vm.pages = 0
    vm.limitPages = []
    vm.userMonth = {}
    vm.now = new Date()

    vm.anni = [2015..vm.now.getFullYear()]

    vm.campaignList = []
    Campaign.get_all {limit: 999999}, (data) ->
      vm.campaignList = data

    Campaign.get_all {limit: 999999}, (data) ->
      vm.campaignSurveyList = data

    Form.query (data)->
      vm.profilationForm = data.filter (item) ->
        item.formType == "registration"

    vm.getUsersWithoutShippingForm = () ->
      Report.query { page:'getUsersWithoutShippingForm' }, (data) ->
        return

    vm.getUsersExport = ( type, campaign  ) ->

      campaignId = undefined
      formId = undefined

      switch type
        when 'registration'
          formId = vm.profilationForm[0]._id
        when 'campaign'
          if !campaign
            alert 'Seleziona una campagna'
            return
          c = campaign.split(":")
          campaignId = c[0]
          formId = c[1]
        when 'survey'
          if !campaign
            alert 'Seleziona una campagna'
            return
          c = campaign.split(":")
          campaignId = c[0]
          formId = c[1]

      payload =
        typeForm: type
        formId: formId

      if campaignId
        payload.campaignId = campaignId

      if vm.limitReport
        payload.limit = vm.limitReport


      if confirm 'Confermi l\'export?'
        if vm.surveyNotSent && campaignId
          ReportExport.search { query: 'surveyNotSent' }, payload, (data) ->
            if data.msg == "ok"
              alert 'Export è in elaborazione - Verifica lo stato nella pagina download'
        else
          ReportExport.search { query: 'getUsersExport' }, payload, (data) ->
            if data.msg == "ok"
              alert 'Export è in elaborazione - Verifica lo stato nella pagina download'
            return
          return
      return


    vm.exportNeverActived = (limit) ->
      query = {}
      if limit
        query.limit = limit

      if confirm 'Confermi l\'export?'
        ReportExport.export { query: 'getNeverActivedUsers' }, query, (data) ->
          if data.msg == "ok"
            alert 'Export è in elaborazione - Verifica lo stato nella pagina download'
          return
        return
      return

    vm.exportUsersNewsletterGenerica = (limit) ->
      payload =
        limit: limit
        reportType: 'users-newsletter-generica'
        newsletter:
          generica: true
      if confirm 'Confermi l\'export?'
        ReportExport.search { query: 'queryProfile' }, payload, (data) ->
          if data.msg == "ok"
            alert 'Export è in elaborazione - Verifica lo stato nella pagina download'
          return
        return
      return

    vm.exportUsersNewsletterTest = (limit) ->
      payload =
        limit: limit
        reportType: 'users-newsletter-test'
        newsletter:
          test: true
      if confirm 'Confermi l\'export?'
        ReportExport.search { query: 'queryProfile' }, payload, (data) ->
          if data.msg == "ok"
            alert 'Export è in elaborazione - Verifica lo stato nella pagina download'
          return
        return
      return

    vm.exportUsersCampaignAccepted = () ->
      typeAccept = ''
      if vm.limitUserProfileAcceptedReport
        limit = vm.limitUserProfileAcceptedReport

      payload =
        limit: limit
        accepted:
          marketing: true
          dem: true
          profiling: true

      if vm.candidateActive
        typeAccept = 'accepted-'
        payload.active = true
      if vm.candidateRejected
        typeAccept = 'rejected-'
        payload.rejected = true

      payload.campaign = vm.campaign

      if vm.candidateActive && vm.candidateRejected
        payload.active = undefined
        payload.rejected = undefined
        typeAccept = 'all-'

      if vm.campaign
        payload.reportType = 'candidates-'+typeAccept
      else
        payload.reportType = 'candidates'

      if confirm 'Confermi l\'export?'
        ReportExport.search { query: 'queryProfile' }, payload, (data) ->
          if data.msg == "ok"
            alert 'Export è in elaborazione - Verifica lo stato nella pagina download'
          return
        return
      return


    return
  'use strict'
  angular.module('app').controller 'reportExportController', reportExportController
  reportExportController.$inject = [ 'Shared', 'Report', 'ReportExport', 'ReportInfo', 'Campaign', 'CampaignExport', 'Form' ]
  return
