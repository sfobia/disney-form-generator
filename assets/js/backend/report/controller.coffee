do ->
  reportController = ( Shared, Report, ReportExport, ReportInfo, Campaign, CampaignExport, Form ) ->
    vm = this
    vm.data = {}
    vm.limit = 5000
    vm.limitReport = null
    vm.page = 1
    vm.totalUsers = 0
    vm.pages = 0
    vm.limitPages = []
    vm.userMonth = {}
    vm.now = new Date()
    vm.menu = [
      {
        title: "User"
        path: "user"
        col:'8'
        items: [
          {
            title: "List"
            path: "user.list"
          }
        ]
      }
    ]
    vm.anni = [2015..vm.now.getFullYear()]
    vm.campaign = {}

    vm.campaignList = []
    Campaign.get_all {limit: 999999}, (data) ->
      vm.campaignList = data

    Form.query (data)->
      vm.profilationForm = data.filter (item) ->
        item.formType == "registration"

    vm.changeStatsLimit = () ->
      vm.limitPages = []
      vm.pages = Math.trunc((vm.totalUsers/vm.limit)+1)
      vm.limitPages.push {limit: vm.limit, page: i} for i in [1 .. vm.pages]

    Report.query {page:'getUsersCount'}, (data) ->
      vm.data.getUsersCount = data

    Report.query {page:'getActiveUsersCount'}, (data) ->
      vm.data.getActiveUsersCount = data

    Report.query {page:'getProfileCompletedCount'}, (data) ->
      vm.data.getProfileCompletedCount = data
      count = 0
      for item in data
        count += item.total
      vm.totalUsers = count

    Report.query {page:'getNewsletterUsersCount'}, (data) ->
      vm.data.getNewsletterUsersCount = data

    vm.getUsersWithoutShippingForm = () ->
      Report.query { page:'getUsersWithoutShippingForm' }, (data) ->
        return

    vm.getUsersExport = ( type, campaign  ) ->

      campaignId = undefined
      formId = undefined

      switch type
        when 'registration'
          formId = vm.profilationForm[0]._id
        when 'campaign'
          if !campaign
            alert 'Seleziona una campagna'
            return
          c = campaign.split(":")
          campaignId = c[0]
          formId = c[1]
        when 'survey'
          if !campaign
            alert 'Seleziona una campagna'
            return
          c = campaign.split(":")
          campaignId = c[0]
          formId = c[1]

      payload =
        typeForm: type
        formId: formId

      if campaignId
        payload.campaignId = campaignId

      if vm.limitReport
        payload.limit = vm.limitReport


      if confirm 'Confermi l\'export?'
        ReportExport.search { query: 'getUsersExport' }, payload, (data) ->
          if data.msg == "ok"
            alert 'Export è in elaborazione - Verifica lo stato nella pagina download'
          return
        return
      return

    vm.filterUsersReport = (month, year) ->
      Report.get {page:'stats', month: month, year: year}, (data) ->
        vm.userMonth.total = data.users
        vm.userMonth.stats  = data.usersStats
        return
      return



    vm.filterUsersReport(vm.now.getMonth()+1, vm.now.getFullYear())


    return
  'use strict'
  angular.module('app').controller 'reportController', reportController
  reportController.$inject = [ 'Shared', 'Report', 'ReportExport', 'ReportInfo', 'Campaign', 'CampaignExport', 'Form' ]
  return
