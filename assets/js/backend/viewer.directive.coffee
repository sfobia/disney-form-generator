do ->
  ViewerImages = (Shared) ->
    {
      scope:
        multiple: '='
        ngModel: '='
      restrict: 'AEC'
      templateUrl: 'partials/media/viewer.html',
      controller: ($scope) ->
        $scope.add = ->
          if !$scope.ngModel
            $scope.ngModel = []
          Shared.showChooser($scope.ngModel,$scope.multiple)

        $scope.remove = (item) ->
          $scope.ngModel.splice($scope.ngModel.indexOf(item), 1)

        return
    }

  angular.module('app').directive('viewerImages', ViewerImages)
  ViewerImages.$inject = ['Shared']