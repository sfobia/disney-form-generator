angular.module("ngConstants", [])

.constant("baseUrl", "http://localhost:8100")

.constant("baseUrlAssets", "http://localhost:8000")

.constant("environment", "development")

;