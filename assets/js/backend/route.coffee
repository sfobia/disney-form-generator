do ->
  routeConfig = ($stateProvider, $urlRouterProvider, $httpProvider) ->
    $urlRouterProvider.otherwise("/")
    $httpProvider.interceptors.push('APIInterceptor');


    $stateProvider
    .state 'dashboard',
      url: "/"
      templateUrl: "partials/dashboard/index.html"
    .state 'auth',
      url: "/auth"
      templateUrl: "partials/auth/index.html"
      controller: 'authController'
      controllerAs: 'vm'

# USER ######
    .state 'user',
      url: "/user"
      templateUrl: "partials/user/index.html"
      redirectTo: 'user.list'
    .state 'user.list',
      url: "/list"
      templateUrl: "partials/user/list.html"
      controller: 'userController'
      controllerAs: 'vm'
    .state 'user.search',
      url: "/search"
      templateUrl: "partials/user/search.html"
      controller: 'userSearchController'
      controllerAs: 'vm'
    .state 'user.create',
      url: "/create"
      templateUrl: "partials/user/detail.html"
      controller: 'userDetailController'
      controllerAs: 'vm'
    .state 'user.detail',
      url: "/detail/:id"
      templateUrl: "partials/user/detail.html"
      controller: 'userDetailController'
      controllerAs: 'vm'
    .state 'user.detailCampaign',
      url: "/detail/:id/:campaignId"
      templateUrl: "partials/user/detail.html"
      controller: 'userDetailController'
      controllerAs: 'vm'

# campaign #####
    .state 'campaign',
      url: "/campaign"
      templateUrl: "partials/campaign/index.html"
      redirectTo: 'campaign.list'
    .state 'campaign.list',
      url: "/list"
      templateUrl: "partials/campaign/list.html"
      controller: 'campaignController'
      controllerAs: 'vm'
    .state 'campaign.create',
      url: "/create"
      templateUrl: "partials/campaign/detail.html"
      controller: 'campaignDetailController'
      controllerAs: 'vm'
    .state 'campaign.detail',
      url: "/detail/:id"
      templateUrl: "partials/campaign/detail.html"
      controller: 'campaignDetailController'
      controllerAs: 'vm'
    .state 'campaign.candidates',
      url: "/candidates/:id"
      templateUrl: "partials/campaign/users.html"
      controller: 'campaignCandidatesController'
      controllerAs: 'vm'
    .state 'campaign.tracking',
      url: "/candidates/tracking/:id"
      templateUrl: "partials/campaign/tracking.html"
      controller: 'trackingCandidatesController'
      controllerAs: 'vm'
    .state 'campaign.processA',
      url: "/process/:id"
      templateUrl: "partials/campaign/process-a.html"
      controller: 'processAController'
      controllerAs: 'vm'
# survey #####
    .state 'survey',
      url: "/survey"
      templateUrl: "partials/survey/index.html"
      redirectTo: 'survey.list'
    .state 'survey.list',
      url: "/list"
      templateUrl: "partials/survey/list.html"
      controller: 'surveyController'
      controllerAs: 'vm'
    .state 'survey.create',
      url: "/create"
      templateUrl: "partials/survey/detail.html"
      controller: 'surveyDetailController'
      controllerAs: 'vm'
    .state 'survey.detail',
      url: "/detail/:id"
      templateUrl: "partials/survey/detail.html"
      controller: 'surveyDetailController'
      controllerAs: 'vm'

# media #####
    .state 'media',
      url: "/media"
      templateUrl: "partials/media/index.html"
      redirectTo: 'media.list'
    .state 'media.list',
      url: "/list"
      templateUrl: "partials/media/list.html"
      controller: 'mediaController'
      controllerAs: 'vm'
    .state 'media.create',
      url: "/create"
      templateUrl: "partials/media/upload.html"
      controller: 'createMediaController'
      controllerAs: 'vm'
    .state 'media.detail',
      url: "/detail/:id"
      templateUrl: "partials/media/detail.html"
      controller: 'mediaDetailController'
      controllerAs: 'vm'


# form #####
    .state 'form',
      url: "/form"
      templateUrl: "partials/form/index.html"
      redirectTo: 'form.list'

    .state 'form.list',
      url: "/list"
      templateUrl: "partials/form/list.html"
      controller: 'formController'
      controllerAs: 'vm'

    .state 'form.create',
      url: "/create"
      templateUrl: "partials/form/detail.html"
      controller: 'formDetailController'
      controllerAs: 'vm'

    .state 'form.detail',
      url: "/form/:id"
      templateUrl: "partials/form/detail.html"
      controller: 'formDetailController'
      controllerAs: 'vm'

    .state 'form.preview',
      url: "/preview/:id/:type"
      templateUrl: "partials/form/preview.html"
      controller: 'formPreviewController'
      controllerAs: 'pc'

# client #####
    .state 'client',
      url: "/client"
      templateUrl: "partials/client/index.html"
      redirectTo: 'client.list'

    .state 'client.list',
      url: "/list"
      templateUrl: "partials/client/list.html"
      controller: 'clientController'
      controllerAs: 'vm'

    .state 'client.create',
      url: "/create"
      templateUrl: "partials/client/detail.html"
      controller: 'clientDetailController'
      controllerAs: 'vm'

    .state 'client.detail',
      url: "/client/:id"
      templateUrl: "partials/client/detail.html"
      controller: 'clientDetailController'
      controllerAs: 'vm'

# category #####
    .state 'category',
      url: "/category"
      templateUrl: "partials/category/index.html"
      redirectTo: 'category.list'
    .state 'category.list',
      url: "/list"
      templateUrl: "partials/category/list.html"
      controller: 'categoryController'
      controllerAs: 'vm'
    .state 'category.create',
      url: "/create"
      templateUrl: "partials/category/detail.html"
      controller: 'categoryDetailController'
      controllerAs: 'vm'
    .state 'category.detail',
      url: "/detail/:id"
      templateUrl: "partials/category/detail.html"
      controller: 'categoryDetailController'
      controllerAs: 'vm'

# template #####
    .state 'template',
      url: "/template"
      templateUrl: "partials/template/index.html"
      redirectTo: 'template.list'
    .state 'template.list',
      url: "/list"
      templateUrl: "partials/template/list.html"
      controller: 'templateController'
      controllerAs: 'vm'
    .state 'template.create',
      url: "/create"
      templateUrl: "partials/template/detail.html"
      controller: 'templateDetailController'
      controllerAs: 'vm'
    .state 'template.detail',
      url: "/detail/:id"
      templateUrl: "partials/template/detail.html"
      controller: 'templateDetailController'
      controllerAs: 'vm'
# report #####
    .state 'report',
      url: "/report"
      templateUrl: "partials/report/index.html"
      redirectTo: 'report.list'
    .state 'report.list',
      url: "/list"
      templateUrl: "partials/report/list.html"
      controller: 'reportController'
      controllerAs: 'vm'
    .state 'report.export',
      url: "/export"
      templateUrl: "partials/report/export.html"
      controller: 'reportExportController'
      controllerAs: 'vm'
    .state 'report.download',
      url: "/download"
      templateUrl: "partials/report/download.html"
      controller: 'reportDownloadController'
      controllerAs: 'vm'
# review #####
    .state 'review',
      url: "/review"
      templateUrl: "partials/review/index.html"
      redirectTo: 'review.list'
    .state 'review.list',
      url: "/list"
      templateUrl: "partials/review/list.html"
      controller: 'reviewController'
      controllerAs: 'vm'
    .state 'review.detail',
      url: "/review/:id"
      templateUrl: "partials/review/detail.html"
      controller: 'reviewDetailController'
      controllerAs: 'vm'

    .state 'review.testerlist',
      url: "/testerlist"
      templateUrl: "partials/review/testerlist.html"
      controller: 'reviewTesterController'
      controllerAs: 'vm'
    .state 'review.testerdetail',
      url: "/testerdetail/:id"
      templateUrl: "partials/review/testerdetail.html"
      controller: 'reviewTesterDetailController'
      controllerAs: 'vm'


    return
  'use strict'
  angular.module('app').config(routeConfig)
  routeConfig.$inject = [
    '$stateProvider'
    '$urlRouterProvider'
    '$httpProvider'
  ]
  return