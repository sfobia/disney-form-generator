do ->
  categoryDetailController = (Shared, Category, $state, $stateParams) ->
    vm = this


    if $stateParams.id
      Category.get id: $stateParams.id, (data) ->
        vm.data = data
    else
      vm.data =
        media:
          images: []
          logo: []

    vm.save = ->
      if vm.data._id
        Category.update id: vm.data._id, vm.data, (data) ->
          console.log "ok"
      else
        Category.save vm.data, (data) ->
          $state.go('category.detail', id: data._id) if  data._id
      return

    vm.delete = (item)->
      if confirm('sei sicuro?')
        Category.delete id: item, ->
          $state.go('category.list')



    return

  'use strict'
  angular.module('app').controller 'categoryDetailController', categoryDetailController
  categoryDetailController.$inject = ['Shared', 'Category', '$state', '$stateParams']
  return
