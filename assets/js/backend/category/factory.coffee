do ->
  Category = ($resource) ->
    BASE_URL = '/V1/category'
    $resource BASE_URL + '/:id', {}, update: {method: 'PUT'}

  angular.module('app').factory 'Category', Category
  Category.$inject = ['$resource']
  return

do ->
  Categories = ($resource) ->
    BASE_URL = '/V1/category'
    $resource BASE_URL, {}

  angular.module('app').factory 'Categories', Categories
  Categories.$inject = ['$resource']
  return