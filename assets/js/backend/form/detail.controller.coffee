do ->
  formDetailController = (Shared, Form, $state, $stateParams) ->
    vm = this

    vm.types = ['input', 'radio', 'checkbox', 'ranges', 'textarea', 'onlydescription', 'multiradio' ]
    vm.formTypes = ['registration', 'campaign', 'survey','shipping']

    if $stateParams.id
      Form.get id: $stateParams.id, (data) ->
        vm.data = data
    else
      vm.data = {
        fields: [{
          value: []
          radiovalue: []
        }]
        products: []
      }
      console.log( vm.data )

    vm.preview = ->
      $state.go('form.preview', {id: vm.data._id, type: vm.data.formType })

      return

    vm.clone = ->
      if confirm 'sei sicuro?'
        if vm.data._id
          vm.data._id = undefined
          vm.data.title = "Copy of " + vm.data.title
          for item in vm.data.fields
            delete item._id
          Form.save vm.data, (data) ->
            vm.data = data
            $state.go('form.detail', id: data._id)  if data._id
            $state.go('form.list')                  if data.statusCode == 400
        return

    vm.assignProductValidate = (field) ->
      field.type = 'radio'

    vm.validate = ->
      if vm.data.multiProduct
        count = 0
        for field in vm.data.fields
          if field.assignProduct
            if field.type != 'radio'
              alert "La domanda assegna prodotto deve essere associata al tipo 'radio'"
            count++
        if count != 1
          alert "Una sola domanda può essere usata come 'assegna prodotto'"
        else
          vm.save()
      else
        vm.save()


    vm.save = ->
      if vm.data._id
        Form.update id: vm.data._id, vm.data, (data) ->
          console.log "ok"
      else
        Form.save vm.data, (data, status) ->
          $state.go('form.detail', id: data._id) if data._id
      return

    vm.addMulti = ->
      if vm.data?
        vm.data.products = []

    vm.info = ( field ) ->
      if !field.info
        field.info = {}
#      if !field.permitted
#        field.permitted = {}

    vm.delete = (item)->
      if confirm('sei sicuro?')
        Form.delete id: item, ->
          $state.go('form.list')

    vm.removeField = (item)  ->
      if confirm("sei sicuro?")
        index = vm.data.fields.indexOf item
        if (index > -1)
          vm.data.fields.splice(index, 1)
        return

    vm.removeValue = ( item, valuex ) ->
      index = item.value.indexOf valuex
      if (index > -1)
        item.value.splice(index, 1)
      return

    vm.removeRadiovalue = ( item, valuex ) ->
      index = item.radiovalue.indexOf valuex
      if (index > -1)
        item.radiovalue.splice(index, 1)
      return

    vm.setFieldRequired = ( field ) ->

      angular.forEach field.permitted, ( value, key ) ->
        if value == true
          field.requiredField = true
          return
        return

    vm.addField = ->
      vm.data.fields.push({value:[],radiovalue:[]})
      return

    vm.addProducts = ->
      vm.data.products.push {
        name: ''
      }

    vm.removeProduct = (item)  ->
      if confirm("Elimino prodotto?")
        index = vm.data.products.indexOf item
        if (index > -1)
          vm.data.products.splice(index, 1)

    vm.addValue = (item)->
      item.value.push({})
      return

    vm.addRadiovalue = (item)->
      item.radiovalue.push({})
      return

    vm.setIdentifier = ( item ) ->

      if !item.identifier
        return vm.createIdentifier()
      else
        return item.identifier

    vm.createIdentifier = ->
      return Date.now().toString() + vm.randomString(32)

    vm.randomString = ( length ) ->
      chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
      result = ''
      i = length
      while i > 0
        result += chars[Math.floor(Math.random() * chars.length)]
        --i
      return result

    return


  'use strict'
  angular.module('app').controller 'formDetailController', formDetailController
  formDetailController.$inject = ['Shared', 'Form', '$state', '$stateParams']
  return
