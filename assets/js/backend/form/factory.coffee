do ->
  Form = ($resource) ->
    BASE_URL = '/V1/form'
    $resource BASE_URL + '/:id', {},
      {
        update: {method: 'PUT'}
      }

  angular.module('app').factory 'Form', Form
  Form.$inject = ['$resource']
  return