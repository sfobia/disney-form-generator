do ->
  formPreviewController = (Campaigns, Shared, Form, $state, $stateParams) ->
    vm = this
    vm.formType = $stateParams.type
    vm.form = {}
    vm.sform = {}
    vm.isFormCampaign = false
    vm.isProfilation = false
    vm.shippingfields = []
    vm.steps = 0
    vm.dataUser = {}
    vm.address = {}
    vm.cap = {}
    vm.province = {}
    vm.comuni = {}
    vm.regione = {}
    vm.choices = {}

    vm.setCurrentForm = () ->
      data = vm.data
      vm.form.ref = vm.data._id
      vm.form.name = vm.data.title
      vm.form.formType = vm.data.formType
      vm.form.values = {}
      vm.form.campaign = vm.campaignId
      vm.required = 0
      vm.completed = 0

      vm.classnameform = document.getElementById("profilationForm").className

      if vm.steps == 0
        angular.forEach data.fields, ( value ) ->
          vm.steps = value.step if value.step > vm.steps
          vm.required++ if value.requiredField == true

        if vm.isProfilation
          vm.steps++
        vm.calculateProgress()
      return

    completato = false
    vm.initProfilation = ->
      vm.setCurrentForm()
      return
      ###
      Profilation.getone id: 'registration', ( data ) ->

        vm.isProfilation = true

        warning = true

        Profilation.getone id: 'shipping', ( shippingdata ) ->

          vm.sform.ref = shippingdata._id
          vm.sform.name = shippingdata.title
          vm.sform.formType = shippingdata.formType
          vm.sform.values = {}

          if vm.steps == 0
            angular.forEach data.fields, ( value ) ->
              vm.steps = value.step if value.step > vm.steps
              vm.required++ if value.requiredField == true

            vm.steps++

          fieldslen = data.fields.length
          vm.shippingdata = shippingdata

          angular.forEach shippingdata.fields, ( shvalue, shkey ) ->
            shvalue.step = vm.steps
            data.fields[fieldslen++] = shvalue
            vm.shippingfields.push shvalue.identifier

          vm.setCurrentForm data
          vm.formtype = 'registration'
      return
      ###

    vm.initShipping = () ->
      vm.setCurrentForm()
      return
      ###
      Profilation.getone id: 'shipping', ( data ) ->
        vm.setCurrentForm data
        vm.formtype = 'shipping'
        vm.isShipping = true
      return
      ###

    vm.initSurvey = ( campaignId ) ->
      vm.setCurrentForm()
      return
      console.log 'initSurvey'
      vm.campaignId = campaignId
      # se dovesse entrare nella pagina dei test

      if campaignId

        vm.form.campaign = campaignId
        vm.dates = []
        Campaign.get { id: campaignId }, (data) ->
          vm.dataCampaign = data
          angular.forEach vm.dataCampaign.dates, ( value, key ) ->
            vm.dates[key] = new Date(value).getTime()
            return
          ###
          Survey.getSingle id: campaignId, ( datac ) ->

            vm.setCurrentForm datac
            vm.formtype = 'survey'
            vm.isSurvey = true
          ###
        return
      return

    vm.initFormCampaign = (campaignId)->
      console.log 'initCampaign'
      vm.setCurrentForm()
      return

    vm.init = () ->
      Form.get id: $stateParams.id, (data) ->
        vm.data = data

        page = $stateParams.type
        campaignId = $stateParams.id
        console.log page, campaignId

        if( page == 'profilation' )
          vm.initProfilation()
        else if( page == 'shipping' )
          vm.initShipping()
        else if( page == 'survey' )
          vm.initSurvey( campaignId )
        else
          vm.initFormCampaign( campaignId )
        return

    vm.checkDependentField = ( field ) ->
      if field.depends && field.depends!='-'
        identifiers = field.depends.split ":"
        field_identifier = identifiers[0]
        value_identifier = identifiers[1]

        if !vm.form.values[ field_identifier ]
          return false

        if vm.form.values[ field_identifier ].refvalue == value_identifier
          vm.choices[ field_identifier ] = value_identifier
          return true

        return false

      return true

    vm.skipValidation = {}
    vm.checkD = ( e ) ->
      field_identifier = $(e.target).closest('.field-form-profilation').attr('data-id')
      value_identifier = $(e.target).attr('data-identifier')

      if vm.choices
        if vm.choices[ field_identifier ] != value_identifier

          $(e.target).closest('.field-form-profilation').nextAll('.field-form-profilation').each ( el ) ->
            sub_depends = $(this).data('depends')
            if !sub_depends
              delete vm.skipValidation[$(this).data("id")]
              $(this).removeClass("ng-hide")


          $(e.target).closest('.field-form-profilation').nextAll('.field-dependent').each ( el ) ->

            sub_depends = $(this).attr('data-depends')
            fid = $(this).attr('data-id')

            if sub_depends && sub_depends!='-'
              sub_identifiers = sub_depends.split ":"
              sub_field_identifier = sub_identifiers[0]
              $this = $(this)
              $this.find('input:radio').prop('checked', false )

              vm.form.values[ fid ].value = null
# perche?
#vm.form.values[ sub_field_identifier ].refvalue = null
        else
# li nascondo tutti tranne quelli dipendenti
# solo se clicco su una che ha un action dipendent
          existDep = $(".field-dependent[data-depends='"+field_identifier+':'+value_identifier+"']").length
          if existDep
            $(e.target).closest('.field-form-profilation').nextAll('.field-form-profilation').each ( el ) ->
              sub_depends = $(this).data('depends')
              if !sub_depends
                vm.skipValidation[$(this).data("id")] = true
                $(this).addClass("ng-hide")

        return
      return

    vm.nextStep = ->
      if vm.currentStep < vm.steps
        vm.currentStep += 1
        document.getElementById("profilationForm").className = vm.classnameform
        #profilationForm
        $("html, body").animate(
          scrollTop: $("#profilationForm").offset().top - 50
        ,300)
        vm.calculateProgress()

      return

    vm.prevStep = ->
      if vm.currentStep > 1
        vm.currentStep -= 1

        vm.calculateProgress()
      return

    vm.arrayComuni = []

    vm.caricaCitta = ( identifier, province ) ->

      vm.cap[identifier] = []
      c = vm.arrayComuni.filter ( item ) ->
        return item.provincia == province
      vm.comuni[identifier] = c
      regione = $("#"+identifier+"-provincia option:selected").attr('data-regione')
      vm.regione[identifier] = selectCap.regioni regione
      vm.regione[identifier] = vm.regione[identifier][0]
      vm.form.values[identifier].value.regione = vm.regione[identifier].nomeRegione
      vm.form.values[identifier].value.zona = vm.regione[identifier].zona
      return

    vm.caricaProvince = ( identifier ) ->
      selectCap.promise_comuni().then ( json ) ->
        vm.arrayComuni = json.data
        vm.province[identifier] = selectCap.province()
        $timeout ->
          prov = vm.form.values[identifier].value.provincia #$("#"+identifier+"-provincia").val()
          citta = vm.form.values[identifier].value.citta
          if prov
# carico i relativi comuni
            c = vm.arrayComuni.filter ( item ) ->
              return item.provincia == prov
            vm.comuni[identifier] = c

          if citta
            caps = vm.comuni[identifier].filter ( item ) ->
              return item.nome == citta
            vm.cap[identifier] = caps[0].codiciCap

          $timeout ->
            $("#"+identifier+"-provincia").val(vm.form.values[identifier].value.provincia)
            $("#"+identifier+"-citta").val(vm.form.values[identifier].value.citta)
            $("#"+identifier+"-cap").val(vm.form.values[identifier].value.cap)

      return

    vm.caricaCap = ( identifier ) ->
      $timeout ->
        cap = $("#"+identifier+"-citta option:selected").attr('data-cap')
        vm.cap[identifier] =  JSON.parse cap
      return

    vm.validate = ->
      document.getElementById("profilationForm").className += ' ng-submitted ';

      radiovalid = true
      rangevalid = true
      vm.requiredInStep = 0
      vm.requiredCompleted = 0
      v = true

      angular.forEach vm.form.values, ( value, key ) ->
        f = vm.data.fields.filter ( field ) ->
          return field.identifier == value.ref


        if f[0] && !$(".field-form-profilation[data-id='"+f[0].identifier+"']").hasClass("ng-hide")

          if document.getElementById(f[0].identifier)
            el = document.getElementById(f[0].identifier)
            el.className = ' box-personal-question ng-scope ';

          if f[0].type == 'ranges' || f[0].type == 'ranges-type2'
            re = false
            if parseInt(vm.currentStep) == parseInt(f[0].step)

              if f[0].requiredField == true
                vm.requiredInStep++
                re = true

              $("*[type=range]",".field-form-profilation[data-id='"+f[0].identifier+"']").each ->
                if !$(this).hasClass('ng-dirty')
                  v = false
                  rangevalid = false

              if rangevalid && re
                vm.requiredCompleted++

          else if f[0].requiredField == true

            if parseInt(vm.currentStep) == parseInt(f[0].step)
              vm.requiredInStep++

              if f[0].type == 'cap'
                cap = $( '#' + f[0].identifier+ '-cap' ).val()
                if ( (cap != 'NULL') && (cap != '') && ( cap && cap.indexOf('?') == -1 ) )
                  vm.requiredCompleted++
                else
                  el.className += ' validate-attention '
              else if angular.isDefined(value.value) && (value.value != '')
                vm.requiredCompleted++
              else if f[0].type == 'checkbox' || f[0].type == 'radio'
                el.className += ' validate-attention '

          return

      if v
        $('*[data-permitted=true]').each ->
          if !$(this).is(':checked') && ( parseInt($(this).attr('data-step')) == parseInt(vm.currentStep) )
            $(this).closest('.box-personal-question').addClass('validate-attention')
            radiovalid = false
            v = false
          return

      if radiovalid and rangevalid
        v = vm.requiredInStep == vm.requiredCompleted

      if !v

        if !rangevalid
#          UtilsDom.showPopup("Attenzione","Per poterti registrare devi compilare tutti i campi" )
        else if (vm.currentStep==1) and (vm.formtype=='registration')
          UtilsDom.showPopup("Attenzione","Per poterti registrare devi compilare tutti i campi del form" )

        p = jQuery('.validate-form.ng-submitted .ng-invalid.ng-untouched').first()

        setTimeout( ->
#p.focus()
          return
        , 1000)

        #scrollo la pagina fino al punto dell'errore
        if $(".validate-attention, .ng-invalid-required").first().length > 0
          pos   = $(".validate-attention").first()
          if pos.length > 0
            pos = pos.offset().top

          pos2  = $(".ng-invalid-required").first()
          if pos2.length > 0
            pos2 = pos2.offset().top

          primo = Math.min(pos,pos2)
          $("html, body").animate(
            scrollTop: primo - 50
          ,300)

      return v

    vm.calculateProgress = ( last ) ->
      #progress.calculate( vm, last )
      #spostare lato server
      return

    vm.submitForm = ->
      alert('OK');
      return


    return

  'use strict'
  angular.module('app').controller 'formPreviewController', formPreviewController
  formPreviewController.$inject = ['Campaigns', 'Shared', 'Form', '$state', '$stateParams']
  return
