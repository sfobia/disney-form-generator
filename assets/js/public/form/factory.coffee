do ->
  Form = ($resource, Utils) ->

    BASE_URL =  Utils.BASE_URL + '/V1/profile/form'
    $resource BASE_URL + '/:id', {}

  angular.module('app').factory 'Form', Form
  Form.$inject = ['$resource', 'Utils']
  return

do ->
  formOnChange = ($parse) ->
    {
      require: 'form'
      link: (scope, element, attrs) ->
        cb = $parse(attrs.formOnChange)
        element.on 'change', ->
          cb scope
          return
        return

    }

  'use strict'
  angular.module('app').directive 'formOnChange', formOnChange
  formOnChange.$inject = ['$parse']
  return


do ->
  progress = ->
    { calculate: ( $scope, last ) ->

      vm = $scope
      vm.percent = 0

      if !last
        cs = vm.currentStep - 1
      else
        cs = vm.currentStep

      if cs > 0
        vm.percent = Math.ceil( (parseInt(cs)*100)/(vm.steps) )

      vm.continue = true

      return
    }
  angular.module('app').factory 'progress', progress