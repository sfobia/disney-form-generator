do ->
  User = ($resource, Utils) ->

    BASE_URL = Utils.BASE_URL + '/account'
    $resource BASE_URL + '/:action', {},
      {
        login: {method: 'POST', isArray : false },
        logout: {method: 'GET' }
      }

  angular.module('app').factory 'User', User
  User.$inject = ['$resource', 'Utils']
  return