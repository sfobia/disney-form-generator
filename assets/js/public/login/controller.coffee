do ->
  loginController = (User, Profile, $http, $scope,UtilsAuth, UtilsDom) ->
    vm        = this
    vm.loader = true
    vm.user   = {}

    Profile.get id: 'me', (data) ->
      if data.statusCode >= 400
          vm.logged = false
          vm.loader = false
          #rimuovo anche il token
          localStorage.removeItem( 'token' );
          document.cookie = 'tokenom=;expires=Thu, 01 Jan 1970 00:00:01 GMT;path=/;'
          #window.location.href = "/"
        else
          vm.user = data
          vm.read = data.notifyRead
          vm.logged = true
          vm.loader = false
          $('.go-iscr').hide()

    vm.logout =->
      User.logout action: 'logout', (data) ->
        localStorage.removeItem( 'token' );
        document.cookie = 'tokenom=;expires=Thu, 01 Jan 1970 00:00:01 GMT;path=/;'
        window.location.href = "/"
      return

    validateEmail = (email)->
      re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
      return re.test(email);

    vm.submit = ->
      vm.user.email = vm.user.email.toLowerCase()
      if !validateEmail(vm.user.email)
        return
      if !vm.user.password || vm.user.password == ''
        return

      if $scope.loginForm.$valid
        $('#BtnLoginProfile').button('loading')
        User.login action: 'login', vm.user, (data) ->
          if data.token
            localStorage.setItem 'token', data.token
            document.cookie = 'tokenom' + '=' + data.token  + '; expires=Fri, 31 Dec 2030 23:59:59 GMT; path=/;'

            return window.location.href = '/it/profilo.html'
          else
            $('#BtnLoginProfile').button('reset')
            UtilsDom.showPopup("Attenzione", data.message )
          return
      return

    return

  'use strict'
  angular.module('app').controller 'loginController', loginController
  loginController.$inject = ['User', 'Profile' ,'$http', '$scope','UtilsAuth', 'UtilsDom' ]
  return