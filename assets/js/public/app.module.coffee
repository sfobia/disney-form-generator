do ->
  setTimeout( ->
    if window.location.hash
      hash = window.location.hash.replace("/","")
      if hash != '#_=_' and hash != '#/_=_' and $(hash).length > 0
        divPos = $(hash).offset().top
        if divPos
          $("html, body").animate(
            scrollTop: divPos - 90
          , 500)
  ,500)

do ->
  'use strict'
  angular.module('app', ['ngConstants','ngSanitize','ngResource','bw.paging', 'infinite-scroll' ]).run( [
    '$rootScope'
    '$location'
    '$anchorScroll'
    'UtilsDom'
    ($rootScope,$location,$anchorScroll,UtilsDom) ->
      console.log 'run app DISNEY'

      # $rootScope.$on '$routeChangeSuccess', (newRoute, oldRoute) ->
      #  if $location.hash()
      #    $anchorScroll()

      # todo da togliere
      $rootScope.parseLocation = (location) ->
        i = undefined
        locationSubstring = undefined
        obj = undefined
        pair = undefined
        pairs = undefined
        locationSubstring = location.substring(1)
        obj = {}
        if locationSubstring
          pairs = locationSubstring.split('&')
          pair = undefined
          i = undefined
          for i of pairs
            `i = i`
            i = i
            if pairs[i] == ''
              continue
            pair = pairs[i].split('=')
            obj[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1])
        obj

      $rootScope.saveToken = ->
        location_parse = undefined
        params = undefined
        search = undefined
        token = undefined
        search = $location.search()
        location_parse = $rootScope.parseLocation(window.location.search)
        params = if Object.keys(search).length == 0 then location_parse else search
        token = params.auth_token or params.token
        if token
          document.cookie = 'tokenom' + '=' + token  + '; expires=Fri, 31 Dec 2030 23:59:59 GMT; path=/;'
          localStorage.setItem 'token', token
          window.location.href = '/it/profilo.html'
        return

      $rootScope.confirmMail = ->
        location_parse = undefined
        params = undefined
        search = undefined
        search = $location.search()
        location_parse = $rootScope.parseLocation(window.location.search)
        params = if Object.keys(search).length == 0 then location_parse else search
        confirmMail = params.confirmMail
        if confirmMail
          UtilsDom.showPopup("Grazie!","Stiamo mandando una mail al tuo indirizzo. Per completare la registrazione clicca sul link di conferma che ti abbiamo inviato. ", "ok","/profilo/login.html" )
        return

      $rootScope.confirmMail()
      $rootScope.saveToken()
      return

  ]).config( [
    '$httpProvider','$interpolateProvider'
    ($httpProvider,$interpolateProvider) ->
      $interpolateProvider.startSymbol('{[{').endSymbol('}]}');

      $httpProvider.interceptors.push('APIInterceptor')
      token = undefined
      if localStorage.token
        token = localStorage.token
        $httpProvider.defaults.headers.common.Authorization = 'Bearer ' + token
      return
    ])
  return

do ->
  APIInterceptor = () ->
    service = this
    service.responseError = (response) ->
      #if response.status == 401 || response.status == 403
      #  window.location.href = '/account.html' if window.location.pathname != '/it/account.html'
      response

    service
  angular.module('app').factory('APIInterceptor', APIInterceptor)