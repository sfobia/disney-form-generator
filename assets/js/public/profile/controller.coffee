do ->
  profileController = (Profile, User, selectCap, UtilsAuth, progress, Profilation,$http,Utils,$scope,UtilsDom,CalendarData) ->
    vm              = this
    vm.province     = selectCap.province()
    vm.form         = {}
    vm.shippingData = {}
    vm.profile      = {}
    vm.privacy_accepted  = false
    vm.birthdate    = {}

    vm.mesi   = CalendarData.mesi()
    vm.giorni = CalendarData.giorni()
    vm.anni   = CalendarData.anni()


    Profile.get id: 'me', ( data ) ->
      if !data.accepted and window.location.pathname == '/it/profilo.html'
        window.location.href = "/it/profilo/modifica.html"
        return

      if data.statusCode > 400

        localStorage.removeItem( 'token' )
        document.cookie = 'tokenom=;expires=Thu, 01 Jan 1970 00:00:01 GMT;path=/;'

        window.location.href = "/it/profilo/login.html"
        return

      vm.shippingValues = 0
      vm.shippingData = data.form.filter( (item) ->
        return item.formType == 'shipping'
      )[0]
      if vm.shippingData && vm.shippingData.values
        vm.shippingValues = vm.shippingData.values.length
      vm.profile = data
      vm.logged = true

      if vm.profile.user.email.indexOf('@twitter.com') > -1
        vm.profile.user.email = ""

      if vm.profile.accepted and vm.profile.accepted.marketing != null
        vm.privacy_accepted  = true
        vm.conversions()

      d =  new Date(vm.profile.birthDate)
      vm.giorno = d.getDate()
      vm.mese   = d.getMonth()
      vm.anno   = d.getFullYear()
      vm.profile.birthDate = d

      if vm.profile.profileCompleted
        vm.percent = 100

      if !data.profileCompleted and window.location.pathname == '/it/profilo.html'
        UtilsDom.showPopup("Benvenuto","Completa il tuo profilo per poterti candidare ai test!" )
      else if data.profileCompleted and window.location.pathname == '/it/profilo.html' and ( vm.profile.shippingCompleted == false || vm.shippingValues == 0 )
        UtilsDom.showPopup("Info","Per poterti candidare devi completare il form con i dati per la spedizione", "redirect","/it/profilo/spedizione.html")


    vm.conversions = ->

      if vm.profile.user.sendConversion==true

        if window.ga and ga.create
          ga('send', 'pageview', '/thankyoupage' );

        img = $('<img  />')
        img.attr 'src', 'http://www.googleadservices.com/pagead/conversion/939372860/?label=bpZVCJLY-mMQvOL2vwM&amp;guid=ON&amp;script=0'
        img.attr 'width', 1
        img.attr 'height', 1
        img.attr 'style', 'border-style:none;'
        img.appendTo '#imageconv'

        User.save {action: "conversion"}, {}, (data) ->
          return


    vm.showGender = ( gender ) ->
      if gender == 'Male'
        return "Uomo"
      else if gender == "Female"
        return "Donna"
      else
        return gender

    vm.submit = ->

      genderSel = document.getElementById("gender-selector")
      genderSel.className = ' col-md-6 sex-selector condition-box'

      marketingBox = document.getElementById("privacy-marketing")
      marketingBox.className = ' condition-box '

      demBox = document.getElementById("privacy-dem")
      demBox.className = ' condition-box '

      profilationBox = document.getElementById("privacy-profilation")
      profilationBox.className = ' condition-box '

      dateBox = document.getElementById("bdate-selector")
      dateBox.className = ' col-md-12 '

      valid = true

      if !CalendarData.validate( vm.giorno, vm.mese, vm.anno )
        valid = false
        dateBox.className += ' validate-attention ';

      if !vm.profile || !vm.profile.gender
        valid = false
        genderSel.className += ' validate-attention '

      if !vm.privacy_accepted
        if !vm.profile ||  !vm.profile.accepted || (typeof vm.profile.accepted.marketing == 'undefined' or vm.profile.accepted.marketing == null)
          valid = false
          marketingBox.className += ' validate-attention '

        if !vm.profile || !vm.profile.accepted || (typeof vm.profile.accepted.dem == 'undefined' or vm.profile.accepted.dem == null)
          valid = false
          demBox.className += ' validate-attention ';

        if !vm.profile || !vm.profile.accepted || (typeof vm.profile.accepted.profiling == 'undefined' or vm.profile.accepted.profiling == null)
          valid = false
          profilationBox.className += ' validate-attention ';

      if $scope.registrationForm.$valid and valid
        user =
          email: vm.profile.user.email
          userName: vm.profile.user.userName

        user.password = vm.profile.user.password if vm.profile.user.password

        vm.profile.birthDate = CalendarData.toDb(vm.giorno,vm.mese,vm.anno)

        profile =
          birthDate: vm.profile.birthDate
          gender: vm.profile.gender
          province: vm.profile.province
          name:
            last: vm.profile.name.last
            first: vm.profile.name.first
          accepted: vm.profile.accepted
          newsletter: vm.profile.newsletter
        payload =
          user: user
          profile: profile

        $('#BtnRegistration').button('loading')
        aggiorno = Profile.update id: 'me', payload
        aggiorno.$promise.then ( response ) ->
          $('#BtnRegistration').button('reset')

          if response.email
#            console.log 'changed email'
            localStorage.removeItem( 'token' );
            UtilsDom.showPopup("Conferma mail","Stiamo mandando una mail al tuo indirizzo. Per confermare la nuova mail, clicca sul link di conferma che ti abbiamo inviato.", "ok","/it/profilo/login.html" )

          else if response.password

            localStorage.removeItem( 'token' );
            UtilsDom.showPopup("Attenzione","Devi rifare il login", "ok","/it/profilo/login.html" )

          else
            if response.message
              UtilsDom.showPopup("Attenzione", response.message, "error" )
              return
            window.location.href = "/it/profilo.html"

      else

        pos = $('#registrationForm').offset().top
        $("html, body").animate(
          scrollTop: pos - 50
        , 300)

        return

    vm.savePicture = ->
      $('#BtnPictureProfile').button('loading')
      formData = new FormData
      formData.append('image', $scope.image[0])
      $http.post(Utils.BASE_URL + '/V1/profile/me/picture', formData,
        headers:
          'Content-Type': undefined
        transformRequest: angular.identity
      ).success (data, status) ->
        window.location.reload()
      return

    return

  'use strict'
  angular.module('app').controller 'profileController', profileController
  profileController.$inject = ['Profile', 'User', 'selectCap', 'UtilsAuth', 'progress', 'Profilation','$http','Utils','$scope','UtilsDom','CalendarData']
  return
