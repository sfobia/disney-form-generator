do ->
  CalendarData = ->
    return {
      'mesi': ->
        ['Gennaio','Febbraio','Marzo','Aprile','Maggio','Giugno','Luglio','Agosto','Settembre','Ottobre','Novembre','Dicembre']
      'giorni': ->
        [1..31]
      'anni': ->
        anno = new Date().getFullYear() - 17
        [1900..anno].reverse()

      'toDb': (g,m,a) ->
        giorno  = parseInt(g)
        mese    = parseInt(m)+1
        anno    = parseInt(a)
        giorno  = '0'+giorno if giorno.length == 1
        mese    = '0'+mese   if mese.length == 1
        new Date(anno+'/'+mese+'/'+giorno)

      'digits': (n ) ->
        n = parseInt( n )
        if n > 9
          return n
        else
          return "0"+n

      'validate': ( g, m, a ) ->

        modelValue = @digits( g )+'/'+@digits( parseInt(m)+1 )+'/'+a
        re = /^(((0[1-9]|[12]\d|3[01])\/(0[13578]|1[02])\/((19|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)\/(0[13456789]|1[012])\/((19|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])\/02\/((19|[2-9]\d)\d{2}))|(29\/02\/((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$/
        m = undefined

        v = false

        if (m = re.exec( modelValue )) != null
          fromDate = new Date()
          toDate = new Date()
          fromDate.setYear( fromDate.getFullYear() - 100 )
          toDate.setYear( toDate.getFullYear() - 18 )
          data = modelValue.split('/')
          modelValue = new Date(data[2]+'/'+data[1]+'/'+data[0])
          v = modelValue >= fromDate && modelValue <= toDate
          return v

        return false

    }

  angular.module('app').factory 'CalendarData', CalendarData
  return
