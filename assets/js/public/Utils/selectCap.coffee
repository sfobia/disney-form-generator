do ->
  selectCap = ( $http, baseUrlAssets ) ->

    vm = this
    vm.province =         [
      {
        "idProvincia" : 1,
        "idRegione" : 1,
        "nomeProvincia" : "Chieti",
        "siglaProvincia" : "CH"
      },
      {
        "idProvincia" : 2,
        "idRegione" : 1,
        "nomeProvincia" : "L'Aquila",
        "siglaProvincia" : "AQ"
      },
      {
        "idProvincia" : 3,
        "idRegione" : 1,
        "nomeProvincia" : "Pescara",
        "siglaProvincia" : "PE"
      },
      {
        "idProvincia" : 4,
        "idRegione" : 1,
        "nomeProvincia" : "Teramo",
        "siglaProvincia" : "TE"
      },
      {
        "idProvincia" : 5,
        "idRegione" : 2,
        "nomeProvincia" : "Matera",
        "siglaProvincia" : "MT"
      },
      {
        "idProvincia" : 6,
        "idRegione" : 2,
        "nomeProvincia" : "Potenza",
        "siglaProvincia" : "PZ"
      },
      {
        "idProvincia" : 7,
        "idRegione" : 3,
        "nomeProvincia" : "Catanzaro",
        "siglaProvincia" : "CZ"
      },
      {
        "idProvincia" : 8,
        "idRegione" : 3,
        "nomeProvincia" : "Cosenza",
        "siglaProvincia" : "CS"
      },
      {
        "idProvincia" : 9,
        "idRegione" : 3,
        "nomeProvincia" : "Crotone",
        "siglaProvincia" : "KR"
      },
      {
        "idProvincia" : 10,
        "idRegione" : 3,
        "nomeProvincia" : "Reggio Calabria",
        "siglaProvincia" : "RC"
      },
      {
        "idProvincia" : 11,
        "idRegione" : 3,
        "nomeProvincia" : "Vibo Valentia",
        "siglaProvincia" : "VV"
      },
      {
        "idProvincia" : 12,
        "idRegione" : 4,
        "nomeProvincia" : "Avellino",
        "siglaProvincia" : "AV"
      },
      {
        "idProvincia" : 13,
        "idRegione" : 4,
        "nomeProvincia" : "Benevento",
        "siglaProvincia" : "BN"
      },
      {
        "idProvincia" : 14,
        "idRegione" : 4,
        "nomeProvincia" : "Caserta",
        "siglaProvincia" : "CE"
      },
      {
        "idProvincia" : 15,
        "idRegione" : 4,
        "nomeProvincia" : "Napoli",
        "siglaProvincia" : "NA"
      },
      {
        "idProvincia" : 16,
        "idRegione" : 4,
        "nomeProvincia" : "Salerno",
        "siglaProvincia" : "SA"
      },
      {
        "idProvincia" : 17,
        "idRegione" : 5,
        "nomeProvincia" : "Bologna",
        "siglaProvincia" : "BO"
      },
      {
        "idProvincia" : 18,
        "idRegione" : 5,
        "nomeProvincia" : "Ferrara",
        "siglaProvincia" : "FE"
      },
      {
        "idProvincia" : 19,
        "idRegione" : 5,
        "nomeProvincia" : "Forli Cesena",
        "siglaProvincia" : "FC"
      },
      {
        "idProvincia" : 20,
        "idRegione" : 5,
        "nomeProvincia" : "Modena",
        "siglaProvincia" : "MO"
      },
      {
        "idProvincia" : 21,
        "idRegione" : 5,
        "nomeProvincia" : "Parma",
        "siglaProvincia" : "PR"
      },
      {
        "idProvincia" : 22,
        "idRegione" : 5,
        "nomeProvincia" : "Piacenza",
        "siglaProvincia" : "PC"
      },
      {
        "idProvincia" : 23,
        "idRegione" : 5,
        "nomeProvincia" : "Ravenna",
        "siglaProvincia" : "RA"
      },
      {
        "idProvincia" : 24,
        "idRegione" : 5,
        "nomeProvincia" : "Reggio Emilia",
        "siglaProvincia" : "RE"
      },
      {
        "idProvincia" : 25,
        "idRegione" : 5,
        "nomeProvincia" : "Rimini",
        "siglaProvincia" : "RN"
      },
      {
        "idProvincia" : 26,
        "idRegione" : 6,
        "nomeProvincia" : "Gorizia",
        "siglaProvincia" : "GO"
      },
      {
        "idProvincia" : 27,
        "idRegione" : 6,
        "nomeProvincia" : "Pordenone",
        "siglaProvincia" : "PN"
      },
      {
        "idProvincia" : 28,
        "idRegione" : 6,
        "nomeProvincia" : "Trieste",
        "siglaProvincia" : "TS"
      },
      {
        "idProvincia" : 29,
        "idRegione" : 6,
        "nomeProvincia" : "Udine",
        "siglaProvincia" : "UD"
      },
      {
        "idProvincia" : 30,
        "idRegione" : 7,
        "nomeProvincia" : "Frosinone",
        "siglaProvincia" : "FR"
      },
      {
        "idProvincia" : 31,
        "idRegione" : 7,
        "nomeProvincia" : "Latina",
        "siglaProvincia" : "LT"
      },
      {
        "idProvincia" : 32,
        "idRegione" : 7,
        "nomeProvincia" : "Rieti",
        "siglaProvincia" : "RI"
      },
      {
        "idProvincia" : 33,
        "idRegione" : 7,
        "nomeProvincia" : "Roma",
        "siglaProvincia" : "RM"
      },
      {
        "idProvincia" : 34,
        "idRegione" : 7,
        "nomeProvincia" : "Viterbo",
        "siglaProvincia" : "VT"
      },
      {
        "idProvincia" : 35,
        "idRegione" : 8,
        "nomeProvincia" : "Genova",
        "siglaProvincia" : "GE"
      },
      {
        "idProvincia" : 36,
        "idRegione" : 8,
        "nomeProvincia" : "Imperia",
        "siglaProvincia" : "IM"
      },
      {
        "idProvincia" : 37,
        "idRegione" : 8,
        "nomeProvincia" : "La Spezia",
        "siglaProvincia" : "SP"
      },
      {
        "idProvincia" : 38,
        "idRegione" : 8,
        "nomeProvincia" : "Savona",
        "siglaProvincia" : "SV"
      },
      {
        "idProvincia" : 39,
        "idRegione" : 9,
        "nomeProvincia" : "Bergamo",
        "siglaProvincia" : "BG"
      },
      {
        "idProvincia" : 40,
        "idRegione" : 9,
        "nomeProvincia" : "Brescia",
        "siglaProvincia" : "BS"
      },
      {
        "idProvincia" : 41,
        "idRegione" : 9,
        "nomeProvincia" : "Como",
        "siglaProvincia" : "CO"
      },
      {
        "idProvincia" : 42,
        "idRegione" : 9,
        "nomeProvincia" : "Cremona",
        "siglaProvincia" : "CR"
      },
      {
        "idProvincia" : 43,
        "idRegione" : 9,
        "nomeProvincia" : "Lecco",
        "siglaProvincia" : "LC"
      },
      {
        "idProvincia" : 44,
        "idRegione" : 9,
        "nomeProvincia" : "Lodi",
        "siglaProvincia" : "LO"
      },
      {
        "idProvincia" : 45,
        "idRegione" : 9,
        "nomeProvincia" : "Mantova",
        "siglaProvincia" : "MN"
      },
      {
        "idProvincia" : 46,
        "idRegione" : 9,
        "nomeProvincia" : "Milano",
        "siglaProvincia" : "MI"
      },
      {
        "idProvincia" : 47,
        "idRegione" : 9,
        "nomeProvincia" : "Monza e della Brianza",
        "siglaProvincia" : "MB"
      },
      {
        "idProvincia" : 48,
        "idRegione" : 9,
        "nomeProvincia" : "Pavia",
        "siglaProvincia" : "PV"
      },
      {
        "idProvincia" : 49,
        "idRegione" : 9,
        "nomeProvincia" : "Sondrio",
        "siglaProvincia" : "SO"
      },
      {
        "idProvincia" : 50,
        "idRegione" : 9,
        "nomeProvincia" : "Varese",
        "siglaProvincia" : "VA"
      },
      {
        "idProvincia" : 51,
        "idRegione" : 10,
        "nomeProvincia" : "Ancona",
        "siglaProvincia" : "AN"
      },
      {
        "idProvincia" : 52,
        "idRegione" : 10,
        "nomeProvincia" : "Ascoli Piceno",
        "siglaProvincia" : "AP"
      },
      {
        "idProvincia" : 53,
        "idRegione" : 10,
        "nomeProvincia" : "Fermo",
        "siglaProvincia" : "FM"
      },
      {
        "idProvincia" : 54,
        "idRegione" : 10,
        "nomeProvincia" : "Macerata",
        "siglaProvincia" : "MC"
      },
      {
        "idProvincia" : 55,
        "idRegione" : 10,
        "nomeProvincia" : "Pesaro e Urbino",
        "siglaProvincia" : "PU"
      },
      {
        "idProvincia" : 56,
        "idRegione" : 11,
        "nomeProvincia" : "Campobasso",
        "siglaProvincia" : "CB"
      },
      {
        "idProvincia" : 57,
        "idRegione" : 11,
        "nomeProvincia" : "Isernia",
        "siglaProvincia" : "IS"
      },
      {
        "idProvincia" : 58,
        "idRegione" : 12,
        "nomeProvincia" : "Alessandria",
        "siglaProvincia" : "AL"
      },
      {
        "idProvincia" : 59,
        "idRegione" : 12,
        "nomeProvincia" : "Asti",
        "siglaProvincia" : "AT"
      },
      {
        "idProvincia" : 60,
        "idRegione" : 12,
        "nomeProvincia" : "Biella",
        "siglaProvincia" : "BI"
      },
      {
        "idProvincia" : 61,
        "idRegione" : 12,
        "nomeProvincia" : "Cuneo",
        "siglaProvincia" : "CN"
      },
      {
        "idProvincia" : 62,
        "idRegione" : 12,
        "nomeProvincia" : "Novara",
        "siglaProvincia" : "NO"
      },
      {
        "idProvincia" : 63,
        "idRegione" : 12,
        "nomeProvincia" : "Torino",
        "siglaProvincia" : "TO"
      },
      {
        "idProvincia" : 64,
        "idRegione" : 12,
        "nomeProvincia" : "Verbano-Cusio-Ossola",
        "siglaProvincia" : "VB"
      },
      {
        "idProvincia" : 65,
        "idRegione" : 12,
        "nomeProvincia" : "Vercelli",
        "siglaProvincia" : "VC"
      },
      {
        "idProvincia" : 66,
        "idRegione" : 13,
        "nomeProvincia" : "Bari",
        "siglaProvincia" : "BA"
      },
      {
        "idProvincia" : 67,
        "idRegione" : 13,
        "nomeProvincia" : "Barletta-Andria-Trani",
        "siglaProvincia" : "BT"
      },
      {
        "idProvincia" : 68,
        "idRegione" : 13,
        "nomeProvincia" : "Brindisi",
        "siglaProvincia" : "BR"
      },
      {
        "idProvincia" : 69,
        "idRegione" : 13,
        "nomeProvincia" : "Foggia",
        "siglaProvincia" : "FG"
      },
      {
        "idProvincia" : 70,
        "idRegione" : 13,
        "nomeProvincia" : "Lecce",
        "siglaProvincia" : "LE"
      },
      {
        "idProvincia" : 71,
        "idRegione" : 13,
        "nomeProvincia" : "Taranto",
        "siglaProvincia" : "TA"
      },
      {
        "idProvincia" : 72,
        "idRegione" : 14,
        "nomeProvincia" : "Cagliari",
        "siglaProvincia" : "CA"
      },
      {
        "idProvincia" : 73,
        "idRegione" : 14,
        "nomeProvincia" : "Carbonia-Iglesias",
        "siglaProvincia" : "CI"
      },
      {
        "idProvincia" : 74,
        "idRegione" : 14,
        "nomeProvincia" : "Medio Campidano",
        "siglaProvincia" : "VS"
      },
      {
        "idProvincia" : 75,
        "idRegione" : 14,
        "nomeProvincia" : "Nuoro",
        "siglaProvincia" : "NU"
      },
      {
        "idProvincia" : 76,
        "idRegione" : 14,
        "nomeProvincia" : "Ogliastra",
        "siglaProvincia" : "OG"
      },
      {
        "idProvincia" : 77,
        "idRegione" : 14,
        "nomeProvincia" : "Olbia-Tempio",
        "siglaProvincia" : "OT"
      },
      {
        "idProvincia" : 78,
        "idRegione" : 14,
        "nomeProvincia" : "Oristano",
        "siglaProvincia" : "OR"
      },
      {
        "idProvincia" : 79,
        "idRegione" : 14,
        "nomeProvincia" : "Sassari",
        "siglaProvincia" : "SS"
      },
      {
        "idProvincia" : 80,
        "idRegione" : 15,
        "nomeProvincia" : "Agrigento",
        "siglaProvincia" : "AG"
      },
      {
        "idProvincia" : 81,
        "idRegione" : 15,
        "nomeProvincia" : "Caltanissetta",
        "siglaProvincia" : "CL"
      },
      {
        "idProvincia" : 82,
        "idRegione" : 15,
        "nomeProvincia" : "Catania",
        "siglaProvincia" : "CT"
      },
      {
        "idProvincia" : 83,
        "idRegione" : 15,
        "nomeProvincia" : "Enna",
        "siglaProvincia" : "EN"
      },
      {
        "idProvincia" : 84,
        "idRegione" : 15,
        "nomeProvincia" : "Messina",
        "siglaProvincia" : "ME"
      },
      {
        "idProvincia" : 85,
        "idRegione" : 15,
        "nomeProvincia" : "Palermo",
        "siglaProvincia" : "PA"
      },
      {
        "idProvincia" : 86,
        "idRegione" : 15,
        "nomeProvincia" : "Ragusa",
        "siglaProvincia" : "RG"
      },
      {
        "idProvincia" : 87,
        "idRegione" : 15,
        "nomeProvincia" : "Siracusa",
        "siglaProvincia" : "SR"
      },
      {
        "idProvincia" : 88,
        "idRegione" : 15,
        "nomeProvincia" : "Trapani",
        "siglaProvincia" : "TP"
      },
      {
        "idProvincia" : 89,
        "idRegione" : 16,
        "nomeProvincia" : "Arezzo",
        "siglaProvincia" : "AR"
      },
      {
        "idProvincia" : 90,
        "idRegione" : 16,
        "nomeProvincia" : "Firenze",
        "siglaProvincia" : "FI"
      },
      {
        "idProvincia" : 91,
        "idRegione" : 16,
        "nomeProvincia" : "Grosseto",
        "siglaProvincia" : "GR"
      },
      {
        "idProvincia" : 92,
        "idRegione" : 16,
        "nomeProvincia" : "Livorno",
        "siglaProvincia" : "LI"
      },
      {
        "idProvincia" : 93,
        "idRegione" : 16,
        "nomeProvincia" : "Lucca",
        "siglaProvincia" : "LU"
      },
      {
        "idProvincia" : 94,
        "idRegione" : 16,
        "nomeProvincia" : "Massa-Carrara",
        "siglaProvincia" : "MS"
      },
      {
        "idProvincia" : 95,
        "idRegione" : 16,
        "nomeProvincia" : "Pisa",
        "siglaProvincia" : "PI"
      },
      {
        "idProvincia" : 96,
        "idRegione" : 16,
        "nomeProvincia" : "Pistoia",
        "siglaProvincia" : "PT"
      },
      {
        "idProvincia" : 97,
        "idRegione" : 16,
        "nomeProvincia" : "Prato",
        "siglaProvincia" : "PO"
      },
      {
        "idProvincia" : 98,
        "idRegione" : 16,
        "nomeProvincia" : "Siena",
        "siglaProvincia" : "SI"
      },
      {
        "idProvincia" : 99,
        "idRegione" : 17,
        "nomeProvincia" : "Bolzano",
        "siglaProvincia" : "BZ"
      },
      {
        "idProvincia" : 100,
        "idRegione" : 17,
        "nomeProvincia" : "Trento",
        "siglaProvincia" : "TN"
      },
      {
        "idProvincia" : 101,
        "idRegione" : 18,
        "nomeProvincia" : "Perugia",
        "siglaProvincia" : "PG"
      },
      {
        "idProvincia" : 102,
        "idRegione" : 18,
        "nomeProvincia" : "Terni",
        "siglaProvincia" : "TR"
      },
      {
        "idProvincia" : 103,
        "idRegione" : 19,
        "nomeProvincia" : "Aosta",
        "siglaProvincia" : "AO"
      },
      {
        "idProvincia" : 104,
        "idRegione" : 20,
        "nomeProvincia" : "Belluno",
        "siglaProvincia" : "BL"
      },
      {
        "idProvincia" : 105,
        "idRegione" : 20,
        "nomeProvincia" : "Padova",
        "siglaProvincia" : "PD"
      },
      {
        "idProvincia" : 106,
        "idRegione" : 20,
        "nomeProvincia" : "Rovigo",
        "siglaProvincia" : "RO"
      },
      {
        "idProvincia" : 107,
        "idRegione" : 20,
        "nomeProvincia" : "Treviso",
        "siglaProvincia" : "TV"
      },
      {
        "idProvincia" : 108,
        "idRegione" : 20,
        "nomeProvincia" : "Venezia",
        "siglaProvincia" : "VE"
      },
      {
        "idProvincia" : 109,
        "idRegione" : 20,
        "nomeProvincia" : "Verona",
        "siglaProvincia" : "VR"
      },
      {
        "idProvincia" : 110,
        "idRegione" : 20,
        "nomeProvincia" : "Vicenza",
        "siglaProvincia" : "VI"
      }
    ]

    return {
      'regioni': ( regione ) ->
        [
          {
            "idRegione" : 1,
            "abb" : "ABR",
            "nomeRegione" : "Abruzzo",
            "zona": "Sud e isole"
          },
          {
            "idRegione" : 2,
            "abb" : "BAS",
            "nomeRegione" : "Basilicata",
            "zona": "Sud e isole"
          },
          {
            "idRegione" : 3,
            "abb" : "CAL",
            "nomeRegione" : "Calabria",
            "zona": "Sud e isole"
          },
          {
            "idRegione" : 4,
            "abb" : "CAM",
            "nomeRegione" : "Campania",
            "zona": "Sud e isole"
          },
          {
            "idRegione" : 5,
            "abb" : "EMR",
            "nomeRegione" : "Emilia-Romagna",
            "zona": "Nord-est"
          },
          {
            "idRegione" : 6,
            "abb" : "FVG",
            "nomeRegione" : "Friuli-Venezia Giulia",
            "zona": "Nord-est"
          },
          {
            "idRegione" : 7,
            "abb" : "LAZ",
            "nomeRegione" : "Lazio",
            "zona": "Centro"
          },
          {
            "idRegione" : 8,
            "abb" : "LIG",
            "nomeRegione" : "Liguria",
            "zona": "Nord-ovest"
          },
          {
            "idRegione" : 9,
            "abb" : "LOM",
            "nomeRegione" : "Lombardia",
            "zona": "Nord-ovest"
          },
          {
            "idRegione" : 10,
            "abb" : "MAR",
            "nomeRegione" : "Marche",
            "zona": "Centro"
          },
          {
            "idRegione" : 11,
            "abb" : "MOL",
            "nomeRegione" : "Molise",
            "zona": "Sud e isole"
          },
          {
            "idRegione" : 12,
            "abb" : "PIE",
            "nomeRegione" : "Piemonte",
            "zona": "Nord-ovest"
          },
          {
            "idRegione" : 13,
            "abb" : "PUG",
            "nomeRegione" : "Puglia",
            "zona": "Sud e isole"
          },
          {
            "idRegione" : 14,
            "abb" : "SAR",
            "nomeRegione" : "Sardegna",
            "zona": "Sud e isole"
          },
          {
            "idRegione" : 15,
            "abb" : "SIC",
            "nomeRegione" : "Sicilia",
            "zona": "Sud e isole"
          },
          {
            "idRegione" : 16,
            "abb" : "TOS",
            "nomeRegione" : "Toscana",
            "zona": "Centro"
          },
          {
            "idRegione" : 17,
            "abb" : "TAA",
            "nomeRegione" : "Trentino-Alto Adige",
            "zona": "Nord-est"
          },
          {
            "idRegione" : 18,
            "abb" : "UMB",
            "nomeRegione" : "Umbria",
            "zona": "Centro"
          },
          {
            "idRegione" : 19,
            "abb" : "VDA",
            "nomeRegione" : "Valle d'Aosta",
            "zona": "Nord-ovest"
          },
          {
            "idRegione" : 20,
            "abb" : "VEN",
            "nomeRegione" : "Veneto",
            "zona": "Nord-est"
          }
        ].filter ( item ) ->
          return parseInt( item.idRegione ) == parseInt( regione )

      'province': () ->
#        s = vm.province.filter ( item ) ->
#          return item.idRegione = regione
        vm.province

      'promise_comuni': ->
        $http.get('/file/json/comuni.json')

      'comuni': ( province ) ->
        return province
    }

  angular.module('app').factory 'selectCap', selectCap
  selectCap.$inject = ['$http', 'baseUrlAssets' ]

  return
