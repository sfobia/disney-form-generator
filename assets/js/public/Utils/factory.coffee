do ->
  Utils = (baseUrl)->
    console.log 'start app '+baseUrl
    return {
      'BASE_URL': baseUrl
    }

  angular.module('app').factory 'Utils', Utils
  Utils.$inject = ['baseUrl']
  return

do ->
  compareTo = ->
    {
      require: 'ngModel'
      scope: otherModelValue: '=compareTo'
      link: (scope, element, attributes, ngModel) ->

        ngModel.$validators.compareTo = (modelValue) ->
          modelValue == scope.otherModelValue

        scope.$watch 'otherModelValue', ->
          ngModel.$validate()
          return
        return

    }

  'use strict'
  angular.module('app').directive 'compareTo', compareTo
  return

do ->
  overrideEmailValidator = ->
    {
      require: 'ngModel'
      restrict: ''
      priority: 2
      link: (scope, elm, attrs, ctrl) ->
        if ctrl and ctrl.$validators.email
        # this will overwrite the default Angular email validator

          ctrl.$validators.email = (modelValue) ->
            ctrl.$isEmpty(modelValue) or validateEmail(modelValue)

        return

    }

  'use strict'
  angular.module('app').directive 'overrideEmailValidator', overrideEmailValidator
  return



do ->

  dateValidate = ->
    {
      restrict: 'A'
      require: 'ngModel'
      link: (scope, element, attributes, ngModel) ->
        ngModel.$validators.dateValidate = (modelValue) ->
          re = /^(((0[1-9]|[12]\d|3[01])\/(0[13578]|1[02])\/((19|[2-9]\d)\d{2}))|((0[1-9]|[12]\d|30)\/(0[13456789]|1[012])\/((19|[2-9]\d)\d{2}))|((0[1-9]|1\d|2[0-8])\/02\/((19|[2-9]\d)\d{2}))|(29\/02\/((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))))$/
          m = undefined
          if (m = re.exec( modelValue )) != null

            fromDate = new Date()
            toDate = new Date()

            fromDate.setYear( fromDate.getFullYear() - 100 )
            toDate.setYear( toDate.getFullYear() - 18 )

            data = modelValue.split('/')
            modelValue = new Date(data[2]+'/'+data[1]+'/'+data[0])

            v = modelValue >= fromDate && modelValue <= toDate

            return v

          return false
        return
    }

  'use strict'
  angular.module('app').directive 'dateValidate', dateValidate
  return


do ->

  regexValidate = ->
    {
      restrict: 'A'
      require: 'ngModel'
      link: (scope, element, attributes, ngModel) ->

        ngModel.$validators.regexValidate = (modelValue) ->
          if typeof modelValue == 'undefined'
            modelValue = ''
          flags = attributes.regexValidateFlags or ''
          regex = new RegExp(attributes.regexValidate, flags)
          regex.test modelValue

        return

    }

  'use strict'
  angular.module('app').directive 'regexValidate', regexValidate
  return

do ->
  UtilsDom = () ->
    vm = {}
    vm.showPopup = (title,text,type,action) ->
      popup = $("#popup-1")
      popup.find("#modal-popup-title").html(title)
      popup.find("#modal-popup-description").html(text)
      popup.find("#modal-btn-dismiss").attr("href",action)
      popup.addClass "open"
      $('body').addClass('no-scroll')
      return
    vm
  angular.module('app').service('UtilsDom', UtilsDom)

do ->
  UtilsAuth = (Profile) ->
    vm = {}
    vm.profile = ->
      Profile.get id: 'me', (data) ->
        if data.statusCode == 200
          data
    vm
  angular.module('app').service('UtilsAuth', UtilsAuth)
  UtilsAuth.$inject = ['Profile']


do ->
  angular.module('app').directive('fileModel', [
    '$parse'
    ($parse) ->
      {
        restrict: 'A'
        link: (scope, element, attrs) ->
          model = $parse(attrs.fileModel)
          modelSetter = model.assign
          element.bind 'change', ->
            scope.$apply ->
              modelSetter scope, element[0].files
              return
            return
          return

      }
  ])

validateEmail = (email) ->
  re = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i
  re.test email