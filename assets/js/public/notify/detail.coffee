do ->
  notifyDetailController = (Notifies,Profile,$location) ->
    vm          = this
    vm.profilo  = {}
    vm.detail   = {}
    vm.read = 0

    vm.parseLocation = (location) ->
      i = undefined
      locationSubstring = undefined
      obj = undefined
      pair = undefined
      pairs = undefined
      locationSubstring = location.substring(1)
      obj = {}
      if locationSubstring
        pairs = locationSubstring.split('&')
        pair = undefined
        i = undefined
        for i of pairs
          `i = i`
          i = i
          if pairs[i] == ''
            continue
          pair = pairs[i].split('=')
          obj[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1])
      obj

    vm.getParams = ->
      location_parse = undefined
      params = undefined
      search = undefined
      search = $location.search()
      location_parse = vm.parseLocation(window.location.search)
      params = if Object.keys(search).length == 0 then location_parse else search


    Profile.get id: 'me', (data) ->
      vm.profilo = data
      Notifies.query (data,headers) ->
        headers   = headers()
        vm.read  = headers["x-total-count-read"]

        params    = vm.getParams()
        notifyId  = params.id
        if notifyId
          vm.setDetail null,notifyId
        else if data[0]
          vm.setDetail data[0]
        vm.lista = data
      return

    vm.setDetail = (item,id) ->
      if item
        id = item._id
      Notifies.single id: id, (data) ->
        vm.detail = data
      return

    return
  'use strict'
  angular.module('app').controller 'notifyDetailController', notifyDetailController
  notifyDetailController.$inject = ['Notifies','Profile','$location']
  return
