do ->
  notifyController = (Notifies,Profile) ->
    vm  = this
    vm.read = 0
    Notifies.query (notify,headers) ->
      headers   = headers()
      vm.read  = headers["x-total-count-read"]
      vm.lista = notify
      return
    return
  'use strict'
  angular.module('app').controller 'notifyController', notifyController
  notifyController.$inject = ['Notifies','Profile']
  return
