do ->
  Notifies = ($resource, Utils) ->
    BASE_URL = Utils.BASE_URL + '/V1/profile/notify'
    $resource BASE_URL + '/:id', {page:"@page",limit:"@limit"},
      {
        get: {method: 'GET', isArray: true}
        single: {method: 'GET', isArray: false}
        search: {method: 'POST', isArray : false }
      }
  angular.module('app').factory 'Notifies', Notifies
  Notifies.$inject = ['$resource', 'Utils' ]
  return