do ->
  campaignMinisiteController = ( Campaign, Profile, UtilsDom,CampaignData , CampaignReview, CampaignCheckReview) ->
    vm        = this
    vm.data   = []
    vm.page   = 1
    vm.limit  = 10
    vm.lista  = []
    vm.listaReview = []
    vm.now    = new Date().toISOString()
    vm.attive = []
    vm.logged = false
    vm.profile = {}
    vm.campaignOpen = false
    vm.urlCampaign = ''
    vm.privacy_accepted  = false
    vm.alreadyReviewed = false
    vm.acceptRules = false
    vm.reviewPublished = false
    vm.reviewUnpublished = false
    vm.reviewRejected = false
    vm.preview = false;
    vm.pubcomment = false;
    vm.voto = 0;


    Profile.get id: 'me', ( data ) ->
      vm.profile = data

      if data._id
        vm.logged = true

      if vm.profile.accepted and vm.profile.accepted.marketing != null
        vm.privacy_accepted  = true

      return

    vm.getNumber = (num) ->
      console.log "NUM: " + num
      new Array(num)


    vm.checkDates = (cId)->
      vm.dates = []

      CampaignData.get { id: cId }, (data) ->
        vm.dataCampaign = data
        vm.urlCampaign = '/it/campaign/'+data.slug

        angular.forEach vm.dataCampaign.dates, ( value, key ) ->
          vm.dates[key] = new Date(value).getTime()

        now = new Date().getTime()
        if (vm.dates.startCandidations<now) and (vm.dates.endCandidations>now)
          vm.campaignOpen = true

      return

    vm.candidate = (item) ->

      if vm.logged

        vm.shippingValues = 0
        vm.shippingData = vm.profile.form.filter( (item) ->
          return item.formType == 'shipping'
        )[0]
        if vm.shippingData && vm.shippingData.values
          vm.shippingValues = vm.shippingData.values.length

        if vm.profile.profileCompleted == false
          UtilsDom.showPopup("Info","Per poterti candidare devi aver completato il profilo", "redirect","/it/profilo/profilazione.html")
          return
        else if vm.privacy_accepted == false
          UtilsDom.showPopup("Attenzione","Per poterti candidare devi aver completato il profilo", "redirect","/it/profilo/modifica.html" )
          return
        else if vm.profile.shippingCompleted == false || vm.shippingValues == 0
          UtilsDom.showPopup("Info","Per poterti candidare devi completare il form con i dati per la spedizione", "redirect","/it/profilo/spedizione.html")
          return
        else

          formCheck = false
          if vm.profile.form
            for itemF in vm.profile.form
              if itemF.formType == 'campaign' && itemF.campaign == item
                  formCheck = true

          if !formCheck
            # vado direttamente alla pagina
            window.location.href = vm.urlCampaign+"/form"
            return

          if formCheck
            UtilsDom.showPopup("Attenzione","Sei già candidato", "redirect","/it/profilo.html#/listCampaignProfile" )

      else
        UtilsDom.showPopup("Attenzione","Per poterti candidare devi aver effettuato l'accesso", "redirect","/it/profilo/login.html" )
      return


    vm.checkReview = (cId, limitReviews = 100)->

      CampaignData.get { id: cId }, (data) ->
        if data.reviewable or vm.preview
          vm.loadReview(cId, limitReviews)
          CampaignCheckReview.get {id:cId},  (userReview) ->
            if userReview.dates.submitted
              vm.alreadyReviewed = true
            if userReview.status == 'published'
              vm.reviewPublished = true
            if userReview.status == 'unpublished'
              vm.reviewUnpublished = true
            if userReview.status == 'rejected'
              vm.reviewRejected = true
        else
          if limitReviews == 100 # 100 sarebbe il valore di default
            window.location.href = "/campaign/"+data.slug

    vm.sendReview = () ->

      if vm.acceptRules && vm.pubcomment
        data = {
          text: vm.text
          campaign: vm.campaign,
          pubcomment: vm.pubcomment,
          voto: vm.voto
        }
        CampaignReview.save data, (data) ->
          vm.alreadyReviewed = true
          vm.checkReview(vm.campaign)
      else
        alert('Devi accettare le condizioni e la pubblicazione del commento per poter inviare la tua opinione')

    vm.changePage = (page) ->
      vm.page = vm.page+page
      vm.loadReview(vm.campaign)

    vm.loadReview = (id, limitReviews = 100) ->
      vm.totalPages = 0
      vm.campaign = id
      vm.limit = limitReviews
      CampaignReview.get {id:id}, {page:vm.page, limit:vm.limit},  (data, headers) ->
        vm.listaReview = data
        headers   = headers()
        vm.total  = headers["x-total-count"]
        vm.totalPages = (vm.total - 1) / vm.limit

    vm.valueWithoutPercentage = ( percString, label ) ->

      if( (label == "") && ( percString != "" ) && ( percString.indexOf("%") <= 0 ) )
        newInt = parseInt( percString )
        newInt = newInt * 100 / 10;
        return newInt

      if( (percString == "") && ( label != "" ) )
        newInt = parseInt( label )
        newInt = newInt * 100 / 10;
        return newInt

      return percString.replace("%", percString.substr(0, percString.indexOf("") ))

    vm.getLabel = ( percString, label ) ->
      if( (label == "") && ( percString != "" ) )
        return percString;

      return label;


    return
  'use strict'
  angular.module('app').controller('campaignMinisiteController', campaignMinisiteController).directive 'myMaxlength', [
    '$compile'
    '$log'
    ($compile, $log) ->
      {
        restrict: 'A'
        require: 'ngModel'
        link: (scope, elem, attrs, ctrl) ->
          attrs.$set 'ngTrim', 'false'
          maxlength = parseInt(attrs.myMaxlength, 10)
          ctrl.$parsers.push (value) ->
#            $log.info 'In parser function value = [' + value + '].'
            if value.length > maxlength
#              $log.info 'The value [' + value + '] is too long!'
              value = value.substr(0, maxlength)
              ctrl.$setViewValue value
              ctrl.$render()
#              $log.info 'The value is now truncated as [' + value + '].'
            value
          return

      }
  ]
  campaignMinisiteController.$inject = ['Campaign','Profile','UtilsDom','CampaignData','CampaignReview', 'CampaignCheckReview']
  return
