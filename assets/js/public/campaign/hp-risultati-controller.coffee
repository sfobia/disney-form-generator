do ->
  campaignHPRisultatiController = ( UtilsDom, CampaignDataScroll, GetTotCampaigns ) ->
    vm        = this

    vm.now    = new Date().toISOString()
    vm.page   = 2
    vm.continue = true
    vm.wait = false
    vm.items = []
    vm.offset = 5

    vm.getTotRisultati = ->

      GetTotCampaigns.get { }, { type: "risultati"}, (data, header) ->
        totItems = data.length
        if totItems <= 5
          vm.continue = false
      return

    vm.getTotRisultati()


    vm.infiniteRisultatiCampaigns = ->
      if vm.continue && !vm.wait
        vm.wait = true
        CampaignDataScroll.get { }, { page: vm.page, limit: 3, offset: vm.offset, type: 'risultati' }, ( data, header ) ->

          header = header()
          vm.total_count = header['x-total-count']
          vm.count = vm.page*3

          if vm.count >= vm.total_count
            vm.continue = false

          vm.page++

          vm.wait = false

          if data.length == 0
            vm.continue = false
          else
            vm.offset += data.length
            angular.forEach data, (value, key) ->
              vm.items.push value

    return
  'use strict'
  angular.module('app').controller 'campaignHPRisultatiController', campaignHPRisultatiController
  campaignHPRisultatiController.$inject = [ 'UtilsDom', 'CampaignDataScroll', 'GetTotCampaigns' ]
  return
