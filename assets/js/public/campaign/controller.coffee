do ->
  campaignController = ( Campaign, Profile, UtilsDom, CampaignDataActive ) ->
    vm        = this
    vm.lista  = []
    vm.now    = new Date().toISOString()

    vm.statusfilter = 'Tutte'

    vm.ShowLinkQuestionari = false
    vm.ShowLink = []

    console.log( "campaignController" )

    Campaign.query (data) ->
      vm.lista = data

    vm.setFilter = ( filter ) ->
      vm.statusfilter = filter


    vm.CampaignQuery = ->
      CampaignDataActive.get (data) ->

        if( data.length > 0 )
          vm.ShowLinkQuestionari = true
        else
          vm.ShowLinkQuestionari = false


    vm.getCampaigns = ->

      vm.CampaignQuery()

      setInterval (->
        vm.CampaignQuery()

        return
      ), 30000

      return


    vm.CampaignSingleQuery = (cid) ->
      CampaignDataActive.get id: cid, (data) ->

        if( data.length > 0 )
          vm.ShowLink[cid] = true
        else
          vm.ShowLink[cid] = false


    vm.getSingleCampaignActive = (cid) ->

      vm.CampaignSingleQuery(cid)

      setInterval (->
        vm.CampaignSingleQuery(cid)

        return
      ), 30000

      return


    ###
    Profile.get id: 'me', (data) ->
      if data._id
        vm.logged   = true
        vm.profile  = data
        vm.attive   = data.campaigns
        console.log vm.attive
        Campaign.query (data) ->
          for item in data
            item.possocandidarmi = true
            for camp in vm.attive
              console.log camp.campaign,item._id
              if camp.campaign == item._id
                console.log camp
                item.possocandidarmi = false
                item.sonocandidato  = camp.active
                item.inattesa = true if camp.active == false && camp.rejected == false
                item.rejected = true if camp.rejected
          vm.lista = data
          return
    ###

    return
  'use strict'
  angular.module('app').controller 'campaignController', campaignController
  campaignController.$inject = ['Campaign','Profile','UtilsDom', 'CampaignDataActive']
  return
