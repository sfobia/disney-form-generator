do ->
  campaignHPTestController = ( UtilsDom, CampaignDataScroll, GetTotCampaigns ) ->
    vm        = this

    vm.now    = new Date().toISOString()
    vm.page   = 2
    vm.continue = true
    vm.wait = false
    vm.items = []
    vm.offset = 3

    vm.getTotInCorso = ->
      GetTotCampaigns.get { }, { type: "incorso"}, (data, header) ->
        totItems = data.length
        if totItems <= 3
          vm.continue = false
      return

    vm.getTotInCorso()


    vm.infiniteTestCampaigns = ->
      if vm.continue && !vm.wait
        vm.wait = true

        CampaignDataScroll.get { }, { page: vm.page, limit: 3, offset: vm.offset, type: 'incorso' }, ( data, header ) ->

          header = header()
          vm.total_count = header['x-total-count']
          vm.count = vm.page*3

          if vm.count >= vm.total_count
            vm.continue = false

          vm.page++

          vm.wait = false

          if data.length == 0
            vm.continue = false
          else
            vm.offset += data.length
            angular.forEach data, (value, key) ->
              vm.items.push value

    return
  'use strict'
  angular.module('app').controller 'campaignHPTestController', campaignHPTestController
  campaignHPTestController.$inject = [ 'UtilsDom', 'CampaignDataScroll', 'GetTotCampaigns' ]
  return
