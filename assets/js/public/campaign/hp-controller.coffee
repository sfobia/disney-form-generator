do ->
  campaignHPController = ( UtilsDom, CampaignDataScroll, CampaignCategories, CampaignByCategory, CampaignByType ) ->
    vm        = this

    vm.now    = new Date().toISOString()
    vm.page   = 2
    vm.continue = true
    vm.wait = false
    vm.items = []
    vm.categories = []
    vm.showdata = true
    vm.categorySelected = null
    vm.typeCampaignSelected = null

    #recupero categorie
    CampaignCategories.get {}, {}, (data, header) ->
      if data.length == 0
        console.log "not found"

      else
        console.log "found"
        angular.forEach data, (value, key) ->
          vm.categories.push value



    vm.getCampaignsByCategory = (idCategory) ->

      vm.items = []
      vm.showdata = false
      vm.categorySelected = idCategory

      #console.log("category: " + idCategory )
      #console.log("type: " + vm.typeCampaignSelected )

      if idCategory == null
        vm.page   = 1
        vm.continue = true
        vm.infiniteCampaigns()
      CampaignByCategory.get {}, { category: idCategory, type: vm.typeCampaignSelected }, (data, header) ->
        vm.continue = true
        angular.forEach data, (value, key) ->
          if data.length > 0
            vm.items.push value



    vm.getCampaignsByType = (idType) ->
      vm.items = []
      vm.showdata = false
      vm.typeCampaignSelected = idType
      #vm.infiniteCampaigns()

      if idType == null
        vm.page   = 1
        vm.continue = true
        vm.infiniteCampaigns()
      else
        CampaignByType.get {}, { type: idType, category: vm.categorySelected }, (data, header) ->
          vm.continue = true
          angular.forEach data, (value, key) ->
            if data.length > 0
              vm.items.push value


    vm.infiniteCampaigns = ->
      if vm.continue && !vm.wait
        vm.wait = true
        CampaignDataScroll.get { }, { page: vm.page, category: vm.categorySelected, type: vm.typeCampaignSelected }, ( data, header ) ->

          header = header()
          vm.total_count = header['x-total-count']
          vm.count = vm.page*4

          if vm.count >= vm.total_count
            vm.continue = false

          vm.page++

          vm.wait = false

          if data.length == 0
            vm.continue = false
          else
            angular.forEach data, (value, key) ->
              vm.items.push value

    return
  'use strict'
  angular.module('app').controller 'campaignHPController', campaignHPController
  campaignHPController.$inject = [ 'UtilsDom', 'CampaignDataScroll', 'CampaignCategories', 'CampaignByCategory', 'CampaignByType' ]
  return
