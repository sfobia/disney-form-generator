do ->
  Campaign = ($resource, Utils) ->
    BASE_URL = Utils.BASE_URL + '/V1/profile/campaigns'
    $resource BASE_URL + '/:action/:id', {page:"@page",limit:"@limit"},
      {
        get: {method: 'GET', isArray: true }
        getSingle: {method: 'GET', isArray: false }
        update: {method: 'PUT'}
        search: {method: 'POST', isArray : false }
      }

  angular.module('app').factory 'Campaign', Campaign
  Campaign.$inject = ['$resource', 'Utils']
  return

do ->
  Survey = ($resource, Utils) ->
    BASE_URL = Utils.BASE_URL + '/V1/profile/surveys'
    $resource BASE_URL + '/:action/:id', {page:"@page",limit:"@limit"},
      {
        get: {method: 'GET', isArray: true }
        getSingle: {method: 'GET', isArray: false }
        update: {method: 'PUT'}
        search: {method: 'POST', isArray : false }
      }

  angular.module('app').factory 'Survey', Survey
  Survey.$inject = ['$resource', 'Utils']
  return

do ->
  CampaignDataActive = ($resource, Utils) ->
    BASE_URL = Utils.BASE_URL + '/V1/campaign/active'
    $resource BASE_URL + '/:id', {page:"@page",limit:"@limit"},
      {
        get: {method: 'GET', isArray: true }
      }

  angular.module('app').factory 'CampaignDataActive', CampaignDataActive
  CampaignDataActive.$inject = ['$resource', 'Utils']
  return


do ->
  CampaignData = ($resource, Utils) ->
    BASE_URL = Utils.BASE_URL + '/V1/campaign'
    $resource BASE_URL + '/:id', {page:"@page",limit:"@limit"},{}

  angular.module('app').factory 'CampaignData', CampaignData
  CampaignData.$inject = ['$resource', 'Utils']
  return

do ->
  CampaignDataScroll = ($resource, Utils) ->
    BASE_URL = Utils.BASE_URL + '/V1/campaign'
    $resource BASE_URL + '/:id', {page:"@page",limit:"@limit", offset:"@offset", category: '@category', type: '@type'},
      {
        get: {method: 'GET', isArray: true }
      }

  angular.module('app').factory 'CampaignDataScroll', CampaignDataScroll
  CampaignDataScroll.$inject = ['$resource', 'Utils']
  return


do ->
  CampaignReview = ($resource, Utils) ->
    BASE_URL = Utils.BASE_URL + '/V1/review'
    $resource BASE_URL + '/:id', {page:"@page",limit:"@limit"},
      {
        get: {method: 'GET', isArray: true }
      }

  angular.module('app').factory 'CampaignReview', CampaignReview
  CampaignReview.$inject = ['$resource', 'Utils']
  return


do ->
  CampaignCheckReview = ($resource, Utils) ->
    BASE_URL = Utils.BASE_URL + '/V1/review/checkUserReview'
    $resource BASE_URL + '/:id', {},
      {
        get: {method: 'GET', isArray: false }
      }

  angular.module('app').factory 'CampaignCheckReview', CampaignCheckReview
  CampaignCheckReview.$inject = ['$resource', 'Utils']
  return

do ->
  CampaignCategories = ($resource, Utils) ->
    BASE_URL = Utils.BASE_URL + '/V1/campaign/categories'
    $resource BASE_URL + '/:id', {},
      {
        get: {method: 'GET', isArray: true }
      }

  angular.module('app').factory 'CampaignCategories', CampaignCategories
  CampaignCategories.$inject = ['$resource', 'Utils']
  return

do ->
  CampaignByCategory = ($resource, Utils) ->

    BASE_URL = Utils.BASE_URL + '/V1/campaign/category'

    $resource BASE_URL + '/:action/:id', { category: '@category', type: '@type' },
      {
        get: {method: 'GET', isArray: true }
      }

  angular.module('app').factory 'CampaignByCategory', CampaignByCategory
  CampaignByCategory.$inject = ['$resource', 'Utils']
  return

do ->
  CampaignByType = ($resource, Utils) ->

    BASE_URL = Utils.BASE_URL + '/V1/campaign/type'

    $resource BASE_URL + '/:action/:id', { type: '@type', category: '@category' },
      {
        get: {method: 'GET', isArray: true }
      }

  angular.module('app').factory 'CampaignByType', CampaignByType
  CampaignByType.$inject = ['$resource', 'Utils']
  return


do ->
  GetTotCampaigns = ($resource, Utils) ->
    BASE_URL = Utils.BASE_URL + '/V1/campaign/totcampaigns'

    $resource BASE_URL + '/:action/:id', { type: '@type' },
      {
        get: {method: 'GET', isArray: true }
      }

  angular.module('app').factory 'GetTotCampaigns', GetTotCampaigns
  GetTotCampaigns.$inject = ['$resource', 'Utils']
  return
