do ->
  passwordRetrieveController = (RetrievePassword,UtilsDom,$scope) ->
    vm = this

    vm.submit = ->
      valid = $scope.formRecuperaPassword.$valid
      if valid
        RetrievePassword.save {}, email:vm.email, (data) ->
          UtilsDom.showPopup("Ricorda",'Se ti sei correttamente registrato, riceverai un messaggio con le istruzioni per reimpostare la password' ,'redirect','/it/profilo/login.html')

    return

  'use strict'
  angular.module('app').controller 'passwordRetrieveController', passwordRetrieveController
  passwordRetrieveController.$inject = ['RetrievePassword','UtilsDom','$scope']
  return

do ->
  RetrievePassword = ($resource, Utils) ->

    BASE_URL = Utils.BASE_URL + '/account/forgot'
    $resource BASE_URL + '/:id', {},{}

  angular.module('app').factory 'RetrievePassword', RetrievePassword
  RetrievePassword.$inject = ['$resource', 'Utils']
  return







do ->
  passwordModifyController = (ModifyPassword,$location,UtilsDom,$scope) ->
    vm  = this

    vm.parseLocation = (location) ->
      i = undefined
      locationSubstring = undefined
      obj = undefined
      pair = undefined
      pairs = undefined
      locationSubstring = location.substring(1)
      obj = {}
      if locationSubstring
        pairs = locationSubstring.split('&')
        pair = undefined
        i = undefined
        for i of pairs
          `i = i`
          i = i
          if pairs[i] == ''
            continue
          pair = pairs[i].split('=')
          obj[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1])
      obj

    vm.getParams = ->
      location_parse = undefined
      params = undefined
      search = undefined
      search = $location.search()
      location_parse = vm.parseLocation(window.location.search)
      params = if Object.keys(search).length == 0 then location_parse else search

    params = vm.getParams()
    if !params.tk
      window.location.href = "/it/profilo/login.html"
      return

    vm.submit = ->
      valid = $scope.formModificaPassword.$valid
      if valid
        params = vm.getParams()
        if params.tk
          ModifyPassword.save { token: params.tk }, password:vm.password, (data) ->
            if data.status == 'ok'
              UtilsDom.showPopup("Complimenti!",'Hai modificato la password con successo. Ora puoi effettuare di nuovo il login.','redirect','/it/profilo/login.html' )
            else
              UtilsDom.showPopup("Attenzione",'Dati non corretti','redirect','/it/profilo/login.html' )
        else
          UtilsDom.showPopup("Attenzione",'Dati non corretti','redirect','/it/profilo/login.html' )
    return

  'use strict'
  angular.module('app').controller 'passwordModifyController', passwordModifyController
  passwordModifyController.$inject = ['ModifyPassword','$location','UtilsDom','$scope']
  return


do ->
  ModifyPassword = ($resource, Utils) ->

    BASE_URL = Utils.BASE_URL + '/account/changepassword'
    $resource BASE_URL + '/:token', {},{}

  angular.module('app').factory 'ModifyPassword', ModifyPassword
  ModifyPassword.$inject = ['$resource', 'Utils']
  return