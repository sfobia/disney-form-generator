do ->
  registrationController = (Profile, User, selectCap, $http, $scope ,UtilsDom,CalendarData) ->
    vm        = this

    vm.data = {}
    vm.data.profile = {}
    vm.province = selectCap.province()
    vm.sended = false

    vm.mesi   = CalendarData.mesi()
    vm.giorni = CalendarData.giorni()
    vm.anni   = CalendarData.anni()

    Profile.get id: 'me', ( data ) ->
      if !data.accepted and window.location.pathname == '/it/profilo.html'
        window.location.href = "/it/profilo/modifica.html"
        return

      if data._id
        window.location.href = "/it/profilo.html"
        return


    vm.objects = ->
      if !vm.data.profile.accepted
        vm.data.profile.accepted = {}

      return


    vm.submit = ->

      genderSel = document.getElementById("gender-selector")
      genderSel.className = ' col-md-6 sex-selector condition-box '

      marketingBox = document.getElementById("privacy-marketing")
      marketingBox.className = ' condition-box '

      demBox = document.getElementById("privacy-dem")
      demBox.className = ' condition-box '

      profilationBox = document.getElementById("privacy-profilation")
      profilationBox.className = ' condition-box '

      privacyBox = document.getElementById("privacy-main")
      privacyBox.className = ' condition-box '

      regulationBox = document.getElementById("conditions-box")
      regulationBox.className = ' condition-box '

      dateBox = document.getElementById("bdate-selector")
      dateBox.className = ' col-md-12 '

      valid = $scope.registrationForm.$valid

      if !vm.data.profile || !vm.data.profile.gender
        valid = false
        genderSel.className += ' validate-attention '

      #if !vm.data.profile || !vm.data.profile.accepted.privacy || vm.data.profile.accepted.privacy != true
      #  valid = false
      #  privacyBox.className += ' validate-attention '

      if !vm.data.profile || !vm.data.profile.accepted.marketing
        valid = false
        marketingBox.className += ' validate-attention '

      if !vm.data.profile || !vm.data.profile.accepted.dem
        valid = false
        demBox.className += ' validate-attention ';

      if !vm.data.profile || !vm.data.profile.accepted.profiling
        valid = false
        profilationBox.className += ' validate-attention ';

      if !CalendarData.validate( vm.giorno, vm.mese, vm.anno )
        valid = false
        dateBox.className += ' validate-attention ';

      #if !vm.data.profile || !vm.data.profile.accepted.conditions || vm.data.profile.accepted.conditions != true
      #  valid = false
      #  regulationBox.className += ' validate-attention ';

      if valid
        vm.data.profile.birthDate = CalendarData.toDb(vm.giorno,vm.mese,vm.anno)

        $('#BtnRegistration').button('loading')
        User.save action: 'register', vm.data, ( user ) ->
          $('#BtnRegistration').button('reset')
          if user.status == 200 or user.status == '200'
            UtilsDom.showPopup("Grazie!","Stiamo mandando una mail al tuo indirizzo. Per completare la registrazione clicca sul link di conferma che ti abbiamo inviato. ", "ok","/profilo/login.html" )
          else
            if user.message
              UtilsDom.showPopup("Attenzione", user.message, "error" )
            else
              UtilsDom.showPopup("Attenzione","C'è stato un errore, riprova più tardi", "error" )
        return

      else

        p = $('#registrationForm').offset().top
        pos = $('#registrationForm').offset().top

        if( jQuery('.validate-form.ng-submitted .ng-invalid').first().length > 0 )
          p = jQuery('.validate-form.ng-submitted .ng-invalid').first().offset().top

        if( $(".validate-attention").first().length > 0 )
          pos = $(".validate-attention").first().offset().top

        if p < pos
          pos = p

        $("html, body").animate(
          scrollTop: pos - 50
        ,300)

        return

    return

  'use strict'
  angular.module('app').controller 'registrationController', registrationController
  registrationController.$inject = ['Profile', 'User', 'selectCap', '$http', '$scope','UtilsDom','CalendarData']
  return