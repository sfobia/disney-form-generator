do ->
  Profilation = ($resource, Utils) ->

    BASE_URL = Utils.BASE_URL + '/V1/profile/profilation'
    $resource BASE_URL + '/:id', {},
      {
        getone: {method: 'GET', isArray: false }
      }

  angular.module('app').factory 'Profilation', Profilation
  Profilation.$inject = ['$resource', 'Utils']
  return

do ->
  PsicoLinguistica = ($resource, Utils) ->

    BASE_URL = Utils.BASE_URL + '/V1/profile/profilationservice'
    $resource BASE_URL + '/:id', { method:"@method", formdata:"@formdata" },
      {
        post: {method: 'POST', isArray: false }
      }

  angular.module('app').factory 'PsicoLinguistica', PsicoLinguistica
  PsicoLinguistica.$inject = ['$resource', 'Utils']
  return