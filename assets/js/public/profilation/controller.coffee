do ->
  profilationController = ( Profilation, Profile, PsicoLinguistica, $http, $scope, Form, $location, Campaign, Utils,UtilsDom, progress,CampaignData, $timeout, selectCap, Survey ) ->
    vm      = this
    vm.form = {}
    vm.sform = {}
    vm.isFormCampaign = false
    vm.isProfilation = false
    vm.shippingfields = []
    vm.steps = 0
    vm.steps = 0
    vm.dataUser = {}
    vm.address = {}
    vm.cap = {}
    vm.province = {}
    vm.comuni = {}
    vm.regione = {}


    vm.CompletedForm = false
    vm.Thanks = false
    vm.NotValid = false
    vm.Scaduto = false
    vm.NonAncoraAttivo = false
    vm.NotAuthenticated = false

    vm.choices = {}

    vm.tokenPsicoLinguistica    = null
    vm.responsePsicoLinguistica = null
    vm.psicoLinguisticaStep = -1
    vm.psicoLinguistica = null
    vm.pubcomment = false

    vm.parseLocation = (location) ->
      i = undefined
      locationSubstring = undefined
      obj = undefined
      pair = undefined
      pairs = undefined
      locationSubstring = location.substring(1)
      obj = {}
      if locationSubstring
        pairs = locationSubstring.split('&')
        pair = undefined
        i = undefined
        for i of pairs
          `i = i`
          i = i
          if pairs[i] == ''
            continue
          pair = pairs[i].split('=')
          obj[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1])
      obj

    vm.getParams = ->
      location_parse = undefined
      params = undefined
      search = undefined
      search = $location.search()
      location_parse = vm.parseLocation(window.location.search)
      params = if Object.keys(search).length == 0 then location_parse else search

    vm.setCurrentForm = ( data ) ->

      vm.data = data
      vm.form.ref = data._id
      vm.form.name = data.title
      vm.form.formType = data.formType
      vm.form.values = {}
      vm.form.campaign = vm.campaignId

      vm.required = 0
      vm.completed = 0

      vm.classnameform = document.getElementById("profilationForm").className

      if vm.steps == 0
        angular.forEach data.fields, ( value ) ->
          vm.steps = value.step if value.step > vm.steps
          vm.required++ if value.requiredField == true

        if vm.isProfilation
          vm.steps++

      Profile.get id: 'me', ( pro ) ->

#        console.log( pro )

        vm.dataUser = pro
        # questo dovrebbe servire  solo per lo shipping
#        filled = pro.form.filter (el) ->
#          return ( el.ref == data._id && el.campaign == vm.form.campaign )

#        if filled[0]
#          angular.forEach filled[0].values, ( value, key ) ->
#            vm.form.values[value.ref] = value
#            if value.type == 'cap'
#              vm.address[value.ref] = value.value
#
#          vm.calculateProgress()

      return

    completato = false
    vm.initProfilation = ->

      Profilation.getone id: 'registration', ( data ) ->

        vm.isProfilation = true


        warning = true
        window.onbeforeunload =  ->
          if warning and !completato
            return "Attenzione non hai completato la profilazione,  uscendo dalla pagina dovrai ricompilare il modulo"


        if data.statusCode == 400
          window.location.href = '/it/profilo.html'

        #console.log data.fields

        vm.tokenPsicoLinguistica = Date.now()

        for question in data.fields
          if question.type == "psicolinguistica"
            #console.log "verifico"


            formData = {}
            formData.Token            = vm.tokenPsicoLinguistica
            formData.Username         = ""
            formData.Password         = ""
            formData.Subject          = "Emotional test - Profilazione psicolinguistica - Disney"
            #formData.Subject          = vm.dataUser.user.userName
            formData.Source           = "Disney";
            formData.Channel          = "Sito";

            method = "start"
            PsicoLinguistica.post { }, { method, formData }, (data) ->
              if data.Questions.length > 0
                vm.psicoLinguistica = data.Questions

            ###
            $http.post( 'https://emolab.emotional-lab.it:443/start', formData,
              headers:
                'Content-Type': "application/json"
                transformRequest: angular.identity
            ).success (data, status) ->
              #console.log "ok"
              #console.log data.Questions
              if status == 200
                if data.Questions.length > 0
                  vm.psicoLinguistica = data.Questions
                  #vm.showNextStepPsicoLinguistica = true

              #console.log status
              return
            ###
        ###
        questions = [
          {
            img: "https://thumbs.dreamstime.com/b/planet-earth-sun-rising-space-night-original-image-nasa-57515432.jpg",
            _id: "57722abb6f670cababec8cbf"
          },
          {
            img: "http://www.ansa.it/webimages/ch_620x438/2017/6/26/0da631d3f82c7433c64451d415d528f7.jpg",
            _id: "123dsnkjv43ijcwkcnwecn23"
          }
        ]

        vm.psicoLinguistica = questions;
        ###


        Profilation.getone id: 'shipping', ( shippingdata ) ->

          vm.sform.ref = shippingdata._id
          vm.sform.name = shippingdata.title
          vm.sform.formType = shippingdata.formType
          vm.sform.values = {}

          if vm.steps == 0
            angular.forEach data.fields, ( value ) ->
              vm.steps = value.step if value.step > vm.steps
              vm.required++ if value.requiredField == true

            vm.steps++

          fieldslen = data.fields.length
          vm.shippingdata = shippingdata

          angular.forEach shippingdata.fields, ( shvalue, shkey ) ->
            shvalue.step = vm.steps
            data.fields[fieldslen++] = shvalue
            vm.shippingfields.push shvalue.identifier

          vm.setCurrentForm data
          vm.formtype = 'registration'
      return

    vm.submitPsicolinguistica = () ->

      formData = {}
      formData.Questions = []

      totQuestions = vm.psicoLinguistica.length
      zeroFound = 0 #conto le risposte che sono a 0 - se maggiore di una blocco
      for question in vm.psicoLinguistica
        #console.log question

        questionValue = if question.value then question.value else 0

        if questionValue == 0
          zeroFound += 1

        formData.Questions.push({id: question._id, value: questionValue})

      #console.log zeroFound + " - " + totQuestions

      if ( zeroFound >= totQuestions - 1)
        UtilsDom.showPopup("Attenzione","Non è possibile calcolare il tuo risultato, troppi zero non permettono un corretto assegnamento del colore del tuo cervello. Vuoi rivedere le tue risposte?
Clicca ok e scorri con la freccia indietro per modificare le tue risposte." )

        return
      #console.log resultQuestions

      formData.Token     = vm.tokenPsicoLinguistica
      formData.GetResult = true
      formData.GetWords = false
      #resultQuestions.Questions = vm.psicoLinguistica

      #console.log resultQuestions

      psicoLinguisticaCompleted = false

      method = "complete"
      PsicoLinguistica.post { }, { method, formData }, (data) ->
        psicoLinguisticaCompleted = false
        if  (data.Result)
          if (data.Result.IsCompleted == true)
            vm.responsePsicoLinguistica = data;
            psicoLinguisticaCompleted = true

        if (!psicoLinguisticaCompleted)
          UtilsDom.showPopup("Attenzione","Devi completare domande psicolinguistica" )


      #console.log resultQuestions
      ###
      $http.post( 'https://emolab.emotional-lab.it:443/complete', resultQuestions,
        headers:
          'Content-Type': "application/json"
          transformRequest: angular.identity
      ).success (data, status) ->

        if status == 200

          psicoLinguisticaCompleted = false
          if  (data.Result)
            if (data.Result.IsCompleted == true)
              vm.responsePsicoLinguistica = data;
              psicoLinguisticaCompleted = true
              #console.log vm.responsePsicoLinguistica

          if (!psicoLinguisticaCompleted)
            UtilsDom.showPopup("Attenzione","Devi completare domande psicolinguistica" )

        return
      ###


    vm.initShipping = () ->
      Profilation.getone id: 'shipping', ( data ) ->
        vm.setCurrentForm data
        vm.formtype = 'shipping'
        vm.isShipping = true
      return

#    vm.initSurvey = ( campaignId ) ->
#      vm.campaignId = campaignId
#      # se dovesse entrare nella pagina dei test
#      Profile.get id: 'me', ( pro ) ->
#        if pro.statusCode && pro.statusCode == 401
#          UtilsDom.showPopup("Info","Devi prima effettuare il login", "redirect","/it")
#          return
#        if !pro.profileCompleted
#          UtilsDom.showPopup("Info","Per poterti candidare devi aver completato il profilo", "redirect","/it/profilo/profilazione.html")
#          return
#        if pro.shippingCompleted == false
#          UtilsDom.showPopup("Info","Per poterti candidare devi completare il form con i dati per la spedizione", "redirect","/it/profilo/spedizione.html")
#          return
#
#        if campaignId
#
#          vm.form.campaign = campaignId
#          vm.dates = []
#          CampaignData.get { id: campaignId }, (data) ->
#            vm.dataCampaign = data
#            angular.forEach vm.dataCampaign.dates, ( value, key ) ->
#              vm.dates[key] = new Date(value).getTime()
#              return
#
#            now = new Date().getTime()
#            if vm.dates.startSurvey < now and  vm.dates.endSurvey  > now
#              Survey.getSingle id: campaignId, ( datac ) ->
#                if datac.statusCode == 400
#                  UtilsDom.showPopup("Info", datac.message, "redirect","/it/profilo.html")
#                  return
#                vm.setCurrentForm datac
#                vm.formtype = 'survey'
#                vm.isSurvey = true
#            else
#              window.location.href = '/it/profilo.html'
#
#          return
#      return

    vm.initFormCampaign = (campaignId)->
      vm.campaignId = campaignId
      Profile.get id: 'me', ( pro ) ->

#        console.log( pro )

        if(pro.statusCode > 399 )
          vm.NotAuthenticated = true


#        if !pro.profileCompleted
#          UtilsDom.showPopup("Info","Per poterti candidare devi aver completato il profilo", "redirect","/it/profilo/profilazione.html")
#          return
#        if pro.shippingCompleted == false
#          UtilsDom.showPopup("Info","Per poterti candidare devi completare il form con i dati per la spedizione", "redirect","/it/profilo/spedizione.html")
#          return


        if campaignId
          vm.form.campaign = campaignId
          vm.dates = []
          vm.dataUser = pro

          CampaignData.get { id: campaignId }, (data) ->

            vm.dataCampaign = data
            angular.forEach vm.dataCampaign.dates, ( value, key ) ->
              vm.dates[key] = new Date(value).getTime()

            now = new Date().getTime()

#            if ( 1 )
            if (vm.dates.startCampaign<now) and (vm.dates.endCampaign>now)
              Campaign.getSingle id: campaignId, (datac) ->

                if datac.statusCode == 400
                  vm.CompletedForm = true

                vm.isFormCampaign = true
                vm.setCurrentForm datac
                return
              return
            else
#              vm.Dates = true

              if( now < vm.dates.startCampaign)
                vm.NonAncoraAttivo = true

              if( now > vm.dates.endCampaign)
                vm.Scaduto = true

              console.log( "Verificare le date"  );
              # window.location.href = '/it/profilo.html'
          return
      return

    vm.init = ( page, campaignId ) ->

      if( page == 'profilation' )
        vm.initProfilation()
      else if( page == 'shipping' )
        vm.initShipping()
      else if( page == 'survey' )
        vm.initSurvey( campaignId )
      else

        if campaignId
          vm.initFormCampaign(campaignId)
        else
#          url = new URL(window.location.href)
#          campaignId = url.searchParams.get('id')
          campaignId = vm.getQueryString('id');

#          console.log( ">>" )
#          console.log( campaignId )

          vm.initFormCampaign(campaignId)

      return

    vm.getQueryString = ->

#      console.log("getQueryString")

      key = false
      res = {}
      itm = null
      # get the query string without the ?
      qs = location.search.substring(1)
      # check for the key as an argument
      if arguments.length > 0 and arguments[0].length > 1
        key = arguments[0]
      # make a regex pattern to grab key/value
      pattern = /([^&=]+)=([^&]*)/g
      # loop the items in the query string, either
      # find a match to the argument, or build an object
      # with key/value pairs
      while itm = pattern.exec(qs)
        if key != false and decodeURIComponent(itm[1]) == key
          return decodeURIComponent(itm[2])
        else if key == false
          res[decodeURIComponent(itm[1])] = decodeURIComponent(itm[2])
      if key == false then res else null

    vm.checkDependentField = ( field ) ->
      if field.depends && field.depends!='-'
        identifiers = field.depends.split ":"
        field_identifier = identifiers[0]
        value_identifier = identifiers[1]

        if !vm.form.values[ field_identifier ]
          return false

        if vm.form.values[ field_identifier ].refvalue == value_identifier
          vm.choices[ field_identifier ] = value_identifier
          return true

        return false

      return true

    vm.skipValidation = {}
    vm.checkD = ( e ) ->
      field_identifier = $(e.target).closest('.field-form-profilation').attr('data-id')
      value_identifier = $(e.target).attr('data-identifier')

      if vm.choices
        if vm.choices[ field_identifier ] != value_identifier

          $(e.target).closest('.field-form-profilation').nextAll('.field-form-profilation').each ( el ) ->
            sub_depends = $(this).data('depends')
            if !sub_depends
              delete vm.skipValidation[$(this).data("id")]
              $(this).removeClass("ng-hide")


          $(e.target).closest('.field-form-profilation').nextAll('.field-dependent').each ( el ) ->

            sub_depends = $(this).attr('data-depends')
            fid = $(this).attr('data-id')

            if sub_depends && sub_depends!='-'
              sub_identifiers = sub_depends.split ":"
              sub_field_identifier = sub_identifiers[0]
              $this = $(this)
              $this.find('input:radio').prop('checked', false )

              vm.form.values[ fid ].value = null
              # perche?
              #vm.form.values[ sub_field_identifier ].refvalue = null
        else
          # li nascondo tutti tranne quelli dipendenti
          # solo se clicco su una che ha un action dipendent
          existDep = $(".field-dependent[data-depends='"+field_identifier+':'+value_identifier+"']").length
          if existDep
            $(e.target).closest('.field-form-profilation').nextAll('.field-form-profilation').each ( el ) ->
              sub_depends = $(this).data('depends')
              if !sub_depends
                vm.skipValidation[$(this).data("id")] = true
                $(this).addClass("ng-hide")

        return
      return

    vm.nextStep = ->


      if vm.validate()
        if vm.continue
          if vm.currentStep < vm.steps
            vm.currentStep += 1
            document.getElementById("profilationForm").className = vm.classnameform


            angular.forEach vm.data.fields, (field) ->

              if field.type == "psicolinguistica"

                vm.psicoLinguisticaStep = parseInt(field.step);
                #console.log "STEP: " + field.step
                #vm.showNextStepPsicoLinguistica = false

                if parseInt(field.step)  == parseInt(vm.currentStep)

                  setTimeout ( ->

                    jQuery('.ssld').slick({
                      #dots: true,
                      swipe: false,
                      infinite: false,
                      fade: true
                    })
                    # range slider
                    jQuery('.valueRange').each( ->
                      new Slider(('#'+this.id), ->
                        formatter: (value) ->
                          return value
                      )
                    )

                  ), 500

            #profilationForm
            $("html, body").animate(
              scrollTop: $("#profilationForm").offset().top - 50
            ,300)
            vm.calculateProgress()
      else
        vm.NotValid = true

      return

    vm.prevStep = ->
      if vm.currentStep > 1
        vm.currentStep -= 1

        vm.calculateProgress()
      return

    vm.arrayComuni = []

    vm.caricaCitta = ( identifier, province ) ->

      vm.cap[identifier] = []
      c = vm.arrayComuni.filter ( item ) ->
        return item.provincia == province
      vm.comuni[identifier] = c
      regione = $("#"+identifier+"-provincia option:selected").attr('data-regione')
      vm.regione[identifier] = selectCap.regioni regione
      vm.regione[identifier] = vm.regione[identifier][0]
      vm.form.values[identifier].value.regione = vm.regione[identifier].nomeRegione
      vm.form.values[identifier].value.zona = vm.regione[identifier].zona
      return

    vm.caricaProvince = ( identifier ) ->
      selectCap.promise_comuni().then ( json ) ->
        vm.arrayComuni = json.data
        vm.province[identifier] = selectCap.province()
        $timeout ->
          prov = vm.form.values[identifier].value.provincia #$("#"+identifier+"-provincia").val()
          citta = vm.form.values[identifier].value.citta
          if prov
# carico i relativi comuni
            c = vm.arrayComuni.filter ( item ) ->
              return item.provincia == prov
            vm.comuni[identifier] = c

          if citta
            caps = vm.comuni[identifier].filter ( item ) ->
              return item.nome == citta
            vm.cap[identifier] = caps[0].codiciCap

          $timeout ->
            $("#"+identifier+"-provincia").val(vm.form.values[identifier].value.provincia)
            $("#"+identifier+"-citta").val(vm.form.values[identifier].value.citta)
            $("#"+identifier+"-cap").val(vm.form.values[identifier].value.cap)

      return

    vm.caricaCap = ( identifier ) ->
      $timeout ->
        cap = $("#"+identifier+"-citta option:selected").attr('data-cap')
        vm.cap[identifier] =  JSON.parse cap
      return

    vm.validate = ->
      document.getElementById("profilationForm").className += ' ng-submitted ';

      radiovalid = true
      rangevalid = true
      psicolinguisticavalid = true

      pubcommentApproved = true
      pubcommentValid = true

      vm.requiredInStep = 0
      vm.requiredCompleted = 0
      v = true

      angular.forEach vm.form.values, ( value, key ) ->
        f = vm.data.fields.filter ( field ) ->
          return field.identifier == value.ref


        if f[0] && !$(".field-form-profilation[data-id='"+f[0].identifier+"']").hasClass("ng-hide")

          if document.getElementById(f[0].identifier)
            el = document.getElementById(f[0].identifier)
            el.className = ' box-personal-question ng-scope ';

          if f[0].type == 'commento-voto'

            if vm.isSurvey
              $(".field-form-profilation[data-id='"+f[0].identifier+"']" ).each ->
                vm.requiredInStep++
                #console.log $(this).find("#pubcomment").is(":checked")
                if !$(this).find("#pubcomment").is(":checked") #se pub commento non approvato
                  #console.log "not checked"
                  pubcommentApproved = false
                  v = false
                  pubcommentValid = false
                else
                  vm.requiredCompleted++


          if f[0].type == 'ranges' || f[0].type == 'ranges-type2'
            re = false
            if parseInt(vm.currentStep) == parseInt(f[0].step)

              if f[0].requiredField == true
                vm.requiredInStep++
                re = true

              if vm.isSurvey
                $(".field-form-profilation[data-id='"+f[0].identifier+"']" ).each ->

                  if !$(this).find('input').hasClass('ng-dirty')
                    v = false
                    rangevalid = false
              else
                $("*[type=range]").each ->
                  if !$(this).hasClass('ng-dirty')
                    v = false
                    rangevalid = false

              if rangevalid && re
                vm.requiredCompleted++

          else if f[0].type == 'psicolinguistica'
            #console.log "verifica 0"
            if parseInt(vm.currentStep) == parseInt(f[0].step)
              #console.log "verifica 1"
              vm.requiredInStep++
              if vm.responsePsicoLinguistica == null || vm.responsePsicoLinguistica == undefined
                #console.log "verifica 2"
                psicolinguisticavalid = false
                v = false

              if psicolinguisticavalid
                vm.requiredCompleted++

          else if f[0].requiredField == true

            if parseInt(vm.currentStep) == parseInt(f[0].step)
              vm.requiredInStep++

              if f[0].type == 'cap'

                cap = $( '#' + f[0].identifier+ '-cap' ).val()
                if ( (cap != 'NULL') && (cap != '') && ( cap && cap.indexOf('?') == -1 ) )
                  vm.requiredCompleted++
                else
                  el.className += ' validate-attention '


              if f[0].type == 'multiradio'
                vm.requiredCompleted++

              else if angular.isDefined(value.value) && (value.value != '')

                if f[0].type == 'checkbox'
                  found = false
                  angular.forEach value.value, ( myval, key ) ->

                    if myval == true
                      found = true

                  if found
                    vm.requiredCompleted++
                  else
                    el.className += ' validate-attention '

                else

                  vm.requiredCompleted++

              else if f[0].type == 'checkbox' || f[0].type == 'radio' || f[0].type == 'multiradio'
                el.className += ' validate-attention '

          return


      if v
        $('*[data-permitted=true]').each ->
          if !$(this).is(':checked') && ( parseInt($(this).attr('data-step')) == parseInt(vm.currentStep) )
            $(this).closest('.box-personal-question').addClass('validate-attention')
            radiovalid = false
            v = false
          return

      if radiovalid and rangevalid and psicolinguisticavalid and pubcommentApproved
        v = vm.requiredInStep == vm.requiredCompleted

      if !v

        #console.log "v == false"
        if !rangevalid
#          UtilsDom.showPopup("Attenzione","Per poterti registrare devi compilare tutti i campi" )
        else if (vm.currentStep==1) and (vm.formtype=='registration')
          UtilsDom.showPopup("Attenzione","Per poterti registrare devi compilare tutti i campi del form" )

        if !psicolinguisticavalid
          UtilsDom.showPopup("Attenzione","Completare Psicolinguistica" )

        if !pubcommentApproved
          UtilsDom.showPopup("Attenzione","Accettare la pubblicazione commento" )

        p = jQuery('.validate-form.ng-submitted .ng-invalid.ng-untouched').first()

        setTimeout( ->
        #p.focus()
          return
        , 1000)

        #scrollo la pagina fino al punto dell'errore
        if $(".validate-attention, .ng-invalid-required").first().length > 0
          pos   = $(".validate-attention").first()
          if pos.length > 0
            pos = pos.offset().top

          pos2  = $(".ng-invalid-required").first()
          if pos2.length > 0
            pos2 = pos2.offset().top

          primo = Math.min(pos,pos2)
          $("html, body").animate(
            scrollTop: primo - 50
          ,300)

      return v

    vm.calculateProgress = ( last ) ->
      progress.calculate( vm, last )
      #spostare lato server
      return

    vm.sendPsicolinguisticaMail = () ->
      check1 = if jQuery('input[name=agree1]:checked').val() == "1" then true else false
      check2 = if jQuery('input[name=agree2]:checked').val() == "1" then true else false
      check3 = if jQuery('input[name=agree3]:checked').val() == "1" then true else false

      if (check1 != true)
        UtilsDom.showPopup("Attenzione", "Accettare Informativa per comunicazioni promozionali e di marketing")
        return

      #inviomail
      formData = {}
      formData.Token = vm.tokenPsicoLinguistica
      formData.Username         = ""
      formData.Password         = ""
      formData.FirstName        = vm.dataUser.name.first
      formData.LastName         = vm.dataUser.name.last
      formData.Email            = vm.dataUser.user.email
      formData.Privacy1         = check1
      formData.Privacy2         = check2
      formData.Privacy3         = check3
      formData.Source           = "Disney Test"
      formData.Channel          = "Sito"

      method = "sendMail"
      PsicoLinguistica.post { }, { method, formData }, (data) ->
        console.log "invio mail"
        UtilsDom.showPopup("Complimenti", "Mail psicolinguistica inviata")


      return

    vm.submitForm = ->

      if vm.validate()
#        vm.calculateProgress( true )

        vm.NotValid = false

        $("#BtnProfilationForm").button('loading')
        newarr = []
        shipping = []

        angular.forEach vm.form.values, ( value, key ) ->
          if vm.shippingfields.indexOf(value.ref) > -1
            shipping.push value
          else
            newarr.push value

        newform = jQuery.extend({}, vm.form );
        newform.values  = newarr

        vm.sform.values = shipping
        completato = true


        #todo verificare qua sotto
        Profile.formpost { action: 'form', id: vm.form.ref, campaignId: vm.form.campaign }, newform, ( res ) ->

          if res.status != 'ok'
            UtilsDom.showPopup("Attenzione!",res.message, "redirect","#" )
            return

          vm.Thanks = true

          $("#BtnProfilationForm").button('reset')

        return
      else
        vm.NotValid = true

    return

  'use strict'
  angular.module('app').controller 'profilationController', profilationController
  profilationController.$inject = ['Profilation', 'Profile', 'PsicoLinguistica', '$http', '$scope', 'Form', '$location', 'Campaign', 'Utils' ,'UtilsDom', 'progress','CampaignData', '$timeout', 'selectCap','Survey' ]
  return