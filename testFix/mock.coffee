ProfileGet = require('../dist/modules/profile/model').dao
UserProfile = require('../dist/modules/user/user').dao
Candidate = require('../dist/modules/candidate/model').dao
Campaign = require('../dist/modules/campaign/model').dao
Template = require('../dist/modules/template/model').dao
Form = require('../dist/modules/form/model').dao
Category = require('../dist/modules/category/model').dao
Notify = require('../dist/modules/notify/model').dao
Client = require('../dist/modules/client/model').dao

MomentGet = require 'moment'

module.exports.config =
  version: 'V1'

module.exports.profile = (owner) ->
  new ProfileGet(
    user: owner
    name:
      first: 'Mario'
      last: 'Rossi'
    gender: 'Male'
    birthDate: new MomentGet('2015-12-23')
    accepted:
      marketing: true
    profileCompleted: true
    newsletter:
      test: true
  )

module.exports.femaleProfile = (owner) ->
  new ProfileGet(
    user: owner
    name:
      first: 'Maria'
      last: 'Rossi'
    gender: 'Female'
    birthDate: new MomentGet('2015-12-23')
    accepted:
      marketing: true
      privacy: true
    profileCompleted: true
    newsletter:
      test: true
      generica: true
  )

module.exports.profileTestCarico = (owner) ->
  new ProfileGet(
    user: owner
    name:
      first: 'Luigi'
      last: 'Rossi'
    gender: 'Male'
    birthDate: new MomentGet('2015-12-23')
    accepted:
      marketing: true
      privacy: false
      dem: false
      profiling: false
      regulation: false
      clauses: false
    profileCompleted: true
  )

module.exports.adminProfile = (name) ->
  new UserProfile(
    email: Math.random()+'testAdmin@softfobia.com'
    password:'qwerty'
    scope: 'admin'
    userName:  name + Date.now()
    confirmed: true
  )

module.exports.userProfile = () ->
  new UserProfile(
    email: Math.random()+'@softfobia.com'
    password:'qwerty'
    scope: 'user'
    userName: 'test' + Date.now()
    confirmed: true
  )

#module.exports.surveyData = (campaignId) ->
#  {
#    title: 'Survey title'
#    type: 'test'
#    status:
#      actual: 'actual status'
#    questions: [
#      {
#        title: 'first question'
#        type: 'input'
#        description: 'question description'
#        answers: [
#          name: 'this is an answer'
#        ]
#      }
#    ]
#    campaign: campaignId
#    published: true
#  }
#
#module.exports.survey = (campaignId) ->
#  new Survey(
#    title: 'Survey title'
#    type: 'test'
#    status:
#      actual: 'actual status'
#    questions: [
#      {
#        title: 'first question'
#        type: 'input'
#        description: 'question description'
#        answers: [
#          name: 'this is an answer'
#        ]
#      }
#    ]
#    campaign: campaignId
#    published: true
#  )
#
#module.exports.surveyTwo = (campaignId) ->
#  new Survey(
#    title: 'Second survey title'
#    type: 'test'
#    status:
#      actual: 'a status'
#    questions: [
#      {
#        title: 'first question'
#        type: 'input'
#        description: 'question description'
#        answers: [
#          name: 'this is an answer'
#        ]
#      }
#    ]
#    campaign: campaignId
#    published: true
#  )

module.exports.client = () ->
  new Client(
    name: 'Client name'
    referent: 'name of the referent'
    email: 'info@unclick.it'
    phone:
      mobile: '3453453450'
  )

module.exports.candidate = (userId, campaignId) ->
  new Candidate(
    user: userId
    campaign: campaignId
    active: true
    rejected: false
    sentMail: false
    preconfirmed: true
  )

module.exports.candidateTwo = (userId, campaignId) ->
  new Candidate(
    user: userId
    campaign: campaignId
    active: false
    rejected: false
    sentMail: false
    preconfirmed: true
  )

module.exports.candidateThree = (userId, campaignId) ->
  new Candidate(
    user: userId
    campaign: campaignId
    active: false
    rejected: false
    sentMail: false
    preconfirmed: false
  )

module.exports.campaign = (formId,rnd) ->
  startCandidation = MomentGet().startOf('day')
  endCandidation = MomentGet().startOf('day')
  new Campaign(
    title: 'Campaign Test 1'
    type: 'test'
    form: formId
    link: 'http://campaigntest.com'
    published: true
    slug: 'test-campaign'+ rnd
    pieces: 100
    dates:
      startCandidations: startCandidation.add(-1, 'days')
      endCandidations: endCandidation.add(+2, 'days')
      startSurvey: startCandidation.add(-1, 'days')
      endSurvey: endCandidation.add(+2, 'days')
  )

module.exports.campaignTag = (formId,rnd) ->
  startCandidation = MomentGet().startOf('day')
  endCandidation = MomentGet().startOf('day')
  startCampaign = MomentGet().startOf('day')
  endCampaign = MomentGet().startOf('day')
  new Campaign(
    title: 'Campaign Test 2'
    type: 'test'
    form: formId
    link: 'http://campaigntest.com'
    published: true
    tag: [
      name: 'Nature'
    ]
    slug: 'test-campaigntag'+rnd
    pieces: 140
    dates:
      startCandidations: startCandidation.add(-1, 'days')
      endCandidations: endCandidation.add(+2, 'days')
      startCampaign: startCampaign.add(-2, 'days')
      endCampaign: endCampaign.add(+3, 'days')
  )

module.exports.campaignThree = (formId,rnd) ->
  startCandidation = MomentGet().startOf('day')
  endCandidation = MomentGet().startOf('day')
  new Campaign(
    title: 'Campaign Test 3'
    type: 'test'
    form: formId
    link: 'http://campaigntest.com'
    published: true
    tag: [
      name: 'Nature'
    ]
    slug: 'test-campaigntag'+rnd
    pieces: 200
    dates:
      startCandidations: startCandidation.add(-1, 'days')
      endCandidations: endCandidation.add(+2, 'days')
  )

module.exports.campaignFour = (formId,rnd) ->
  startCandidation = MomentGet().startOf('day')
  endCandidation = MomentGet().startOf('day')
  startSurvey = MomentGet().startOf('day')
  endSurvey = MomentGet().startOf('day')
  new Campaign(
    title: 'Campaign Test 4'
    type: 'test'
    form: formId
    link: 'http://campaigntest.com'
    published: true
    tag: [
      name: 'Nature'
    ]
    slug: 'test-campaigntag'+rnd
    pieces: 200
    dates:
      startCandidations: startCandidation.add(-1, 'days')
      endCandidations: endCandidation.add(+2, 'days')
      startSurvey: startSurvey.add(+2, 'days')
      endSurvey: endSurvey.add(+3, 'days')
  )

module.exports.campaignTagNotPublished = (formId) ->
  startCandidation = MomentGet().startOf('day')
  endCandidation = MomentGet().startOf('day')
  startSurvey = MomentGet().startOf('day')
  endSurvey = MomentGet().startOf('day')

  new Campaign(
    title: 'Unpublished campaign test'
    type: 'test'
    form: formId
    link: 'http://campaigntest.com'
    published: false
    tag: [
      name: 'Skies'
    ]
    slug: 'test-campaigntagnotpublished'
    dates:
      startCandidations: startCandidation.add(-1, 'days')
      endCandidations: endCandidation.add(+2, 'days')
      startSurvey: startSurvey.add(+2, 'days')
      endSurvey: endSurvey.add(+3, 'days')
  )

module.exports.form = ->
  new Form({
    title: 'form title 2'
    fields: [
      {
        name: 'form type'
        type: 'input'
        order: 1
        value: [
          {
            name: 'input name'
            value: 'my user input response'
            type: 'input'
            valid: true
          }

          {
            name: 'input name 2'
            value: 'sec response'
            type: 'input'
            valid: false
          }
        ]
      }
    ]
    predefined: true
    formType: 'campaign'
  })

module.exports.formCheckbox = ->
  new Form({
    title: 'form title 2'
    fields: [
      {
        name: 'form type'
        type: 'radio'
        step: '1'
        identifier: "1455796853553vM7JK8WhAVaL5EiS8HmsNUffyPbHy028"
        order: 1
        value: [
          {
            name: 'prima radio'
            identifier: 'aaa'
            valid: true
          }

          {
            name: 'seconda radio'
            identifier: 'ccc'
            valid: false
          }
        ]
      }
    ]
    predefined: true
    formType: 'campaign'
  })

module.exports.formRegistration = ->
  new Form({
    title: 'form registration'
    fields: [
      {
        name: 'form type'
        type: 'input'
        order: 1
        value: [
          name: 'input name'
        ]
      }
    ]
    predefined: true
    formType: 'registration'
  })

module.exports.formShipping = ->
  new Form({
    title: 'shipping form'
    fields: [
      {
        name: 'form type'
        type: 'input'
        order: 1
        value: [
          name: 'input name'
        ]
      }
    ]
    predefined: true
    formType: 'shipping'
  })

module.exports.formRegistrationTestCarico = ->
  new Form({
    title: 'form registration'
    fields: [
      {
        name: 'form type'
        type: 'input'
        order: 1
        value: [
          name: 'input name'
        ]
      }
      {
        name: 'form type 2'
        type: 'input'
        order: 1
        value: [
          name: 'input name 2'
        ]
      }
    ]
    predefined: true
    formType: 'registration'
  })

module.exports.formTwo = ->
  new Form({
    title: 'form title second'
    fields: [
      {
        name: 'form type'
        type: 'input'
        order: 1
        value: [
          name: 'input name'
        ]
      }
    ]
    predefined: true
    formType: 'campaign'
  })

module.exports.templateData = () ->
  {
    subject: 'Template subject'
    body: 'Body example of template {{ name.first }}'
    type: 'email'
    identifier: 'registration'
    info: 'info'
    published: true
  }

module.exports.templateUpdateData = () ->
  {
    subject: 'Template subject updated'
    body: 'Body example of template updated'
    type: 'email'
    identifier: 'ESEMPIO_BENVENUTO'
    info: 'info'
    published: true
  }

module.exports.template = () ->
  new Template(
    subject: 'Template subject mock'
    body: 'Body example of template mock'
    type: 'email'
    identifier: 'ESEMPIO_BENVENUTO'
    info: 'info'
    published: true
  )

module.exports.categoryData = () ->
  {
    title: 'First category'
    description: 'a category description'
    published: true
  }

module.exports.category = () ->
  new Category(
    title: 'First category'
    description: 'a category description'
    published: true
  )

module.exports.notify = (user) ->
  new Notify(
    user: user
    from: 'admin'
    subject: 'a notify subject'
    body: "testo"
  )

module.exports.formSurvey = ->
  new Form({
    title: 'survey 1'
    fields: [
      {
        name: 'form type'
        type: 'input'
        order: 1
        value: [
          name: 'input name'
        ]
      }
      {
        name: 'form type 2'
        type: 'input'
        order: 1
        value: [
          name: 'input name 2'
        ]
      }
    ]
    predefined: true
    formType: 'survey'
  })