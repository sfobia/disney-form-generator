Candidate = require('../dist/modules/candidate/model').dao
Campaign = require('../dist/modules/campaign/model').dao
User    = require '../dist/modules/user/user'
Profile    = require '../dist/modules/profile/model'
async = require 'async'
request = require('supertest')
_ = require "lodash"
version = 'V1'
fs = require('fs')
json2csv = require('json2csv')
fs = require('fs')
csv = require('fast-csv')
comuni = require('./comuni')
###
filenameOld = 'OM_mederma_2016.03.02.csv'
filename = 'new-mederma-crema-smagliature.csv'

describe 'XLS', ->
  arr = []
  usersFix = {}
  comuniGiusti = {}

  describe 'fix csv', ->
    before (done)->

      for com in comuni
        comune = com.split ';'
        comuniGiusti[comune[0]] = comune[1]

      options =
        headers: true
        delimiter: ';'
      csv.fromPath( 'testFix/'+filenameOld, options ).on('data', (data) ->

        usersFix[ data.candidate_id ] = {}
        usersFix[ data.candidate_id ][ 'user_id' ] = data.user_id
        usersFix[ data.candidate_id ][ 'campaign_id' ] = data.campaign_id

        return
      ).on 'end', ->
        done()

    it 'file 1', (done) ->

      options =
        headers: true
        delimiter: ';'

      csv.fromPath( 'testFix/'+filename, options ).on('data', (data) ->

        if comuniGiusti[data.city]
          data.city =  comuniGiusti[data.city]

        if usersFix[ data.candidate_id ]
          data.user_id = usersFix[ data.candidate_id ]['user_id']
          data.campaign_id = usersFix[ data.candidate_id ]['campaign_id']

        arr.push data

        return
      ).on 'end', ->
#        console.log arr
        done()

    it 'salvo', (done) ->

      fields = ['candidate_id', 'user_id', 'email', 'first_name', 'last_name',
        'campaign_id', 'campaign_title', 'address', 'address_spec',
        'house_number', 'city', 'province', 'region', 'zone', 'cap',
        'phone', 'tracking_code', 'tracking_info', 'tracking_url' ]

      csvfile = 'testFix/new-'+filenameOld

      json2csv {
        data: arr
        fields: fields
        quotes: ''
        del: ';'
      }, (err, csv) ->
        if err
          done()
          return
        fs.writeFile csvfile, csv, (err, s) ->
          if err
            throw err
          done()
          return
        return




###
###
describe 'Candidate', ->
  candidatiNonValidi                  = {}
  candidatiCheDovrebberoAvereIlPremio = {}
  tuttiIProfiliValidi                 = {}

  campaign1  = "56b78c55ab687dd27d6738fa"
  campaign2 = "56b4c3bb67e3e73e4fbb3a13"
  campaign3 = "56b78524ab687dd27d6738e1"
  campaign4 = "56b75473355954af16cc859d"

  profiliValidi1 = {}
  profiliValidi2 = {}
  profiliValidi3 = {}
  profiliValidi4 = {}

  conteggiMax = {}
  campaign = campaign1
  aggr = []
  utentidasaltare = {}

  totaleDaCercare = 0
  giaAggiunto = {}

  before (done)->
    done()


  describe 'Align candidate valid operations', ->
    it 'test lista candidati che hanno più candidature valide', (done)->
      Candidate.find({preconfirmed:true}).sort("dates.candidation": 1).lean().exec (err,data) ->
        for item in data
          candidatiNonValidi[item.user.toString()] = candidatiNonValidi[item.user]+1 || 1
        done()

    it 'lista di tutti gli utenti che hanno piu preconfirm', (done)->
      u = []
      for k,item of candidatiNonValidi
        if item > 1
          u.push(k)
      # li devo ragruppare x utente
      # qui stampo la lista dei candidati non validi, dove devo togliere la seconda preconfirmed
      totaleDaCercare = _.size u
      console.log "UserId da togliere una candidatura"
      console.log 'totale',totaleDaCercare

      async.waterfall [

        (cb) ->
          query =
            profileCompleted: true
            form:
              $elemMatch:
                campaign: campaign1
                prevalid: true

          Profile.dao.find(query).lean().exec (err,data) ->
            for item in data
              profiliValidi1[item.user.toString()]= item._id.toString()
            cb()

        (cb) ->
          query =
            profileCompleted: true
            form:
              $elemMatch:
                campaign: campaign2
                prevalid: true

          Profile.dao.find(query).lean().exec (err,data) ->
            for item in data
              profiliValidi2[item.user.toString()]= item._id.toString()
            cb()

        (cb) ->
          query =
            profileCompleted: true
            form:
              $elemMatch:
                campaign: campaign3
                prevalid: true

          Profile.dao.find(query).lean().exec (err,data) ->
            for item in data
              profiliValidi3[item.user.toString()]= item._id.toString()
            cb()

        (cb) ->
          query =
            profileCompleted: true
            form:
              $elemMatch:
                campaign: campaign4
                prevalid: true

          Profile.dao.find(query).lean().exec (err,data) ->
            for item in data
              profiliValidi4[item.user.toString()]= item._id.toString()
            cb()


        (cb) ->
          Candidate.aggregate [
            {
              $match: preconfirmed: true
            },
            {
              $sort: "dates.candidation": 1
            }
            { $group:
              _id: '$user'
              camps:
                $push:
                  id: '$_id'
                  campaign: '$campaign'
              count: $sum: 1
            },
            {
              $match: {'count': $gt: 1 }
            }
          ], (err, res) ->
            aggr = res
            totaleDaCercare = res.length
            cb()

        (cb) ->

          for rec in aggr
            console.log 'User:', rec._id.toString()
            last = _.last(rec.camps)
            console.log ' --- Camp:', last.campaign.toString(), 'id:', last.id.toString()
            console.log '--------------------------------------------------------------------'
            conteggiMax[last.campaign.toString()] = conteggiMax[last.campaign.toString()]+1 || 1
          console.log 'totale group ', aggr.length
          console.dir conteggiMax
          cb()

      ], (err,data) ->

        done()


    it 'cerco la persona che deve avere il premio camp1', (done) ->
      arr = []
      Candidate.find({ preconfirmed:false, campaign: campaign1 }).sort("dates.candidation": 1).limit(1000).lean().exec (err,data) ->
        i = 0
        for item in data
          id = item.user.toString()
          if profiliValidi1[id] and !giaAggiunto[id] and !candidatiNonValidi[id] and i < conteggiMax[campaign1.toString()]
            candidatiCheDovrebberoAvereIlPremio[id] = id: item._id.toString(),user: id, camp: item.campaign.toString()
            arr.push id: item._id.toString(),user: id, camp: item.campaign.toString()
            giaAggiunto[id] = id
            i++

        console.log " ------------- "
        console.log campaign1.toString(), "UserId da aggiungere una candidatura 1", arr.length
        console.log arr

        done()

    it 'cerco la persona che deve avere il premio camp 2', (done) ->
      arr = []
      Candidate.find({ preconfirmed:false, campaign: campaign2 }).sort("dates.candidation": 1).lean().exec (err,data) ->
        i = 0
        for item in data
          id = item.user.toString()
          # cerco il primo valido se ha un profilo corretto
          # deve essere presente nei profili con requisiti validi e non presente nella prima lista
          if profiliValidi2[id] and !giaAggiunto[id] and !candidatiNonValidi[id] and i <= conteggiMax[campaign2.toString()]
            candidatiCheDovrebberoAvereIlPremio[id] = id: item._id.toString(),user: id, camp: item.campaign.toString()
            arr.push id: item._id.toString(),user: id, camp: item.campaign.toString()
            giaAggiunto[id] = id
            i++

        console.log " ------------- "
        console.log campaign2.toString(), "UserId da aggiungere una candidatura 2", arr.length
        console.log arr
        done()


    it 'cerco la persona che deve avere il premio camp 3', (done) ->
      arr = []
      Candidate.find({ preconfirmed:false, campaign: campaign3 }).sort("dates.candidation": 1).limit(1000).lean().exec (err,data) ->
        i = 0
        for item in data
          id = item.user.toString()
          # cerco il primo valido se ha un profilo corretto
          # deve essere presente nei profili con requisiti validi e non presente nella prima lista
          if profiliValidi3[id] and !giaAggiunto[id] and !candidatiNonValidi[id] and i < conteggiMax[campaign3.toString()]
            candidatiCheDovrebberoAvereIlPremio[id] = id: item._id.toString(), camp: item.campaign.toString()
            arr.push id: item._id.toString(),user: id, camp: item.campaign.toString()
            giaAggiunto[id] = id
            i++

        console.log " ------------- "
        console.log campaign3.toString(), "UserId da aggiungere una candidatura 3", arr.length
        console.log arr
        done()

    it 'cerco la persona che deve avere il premio camp 4', (done) ->
      arr = []
      Candidate.find({ preconfirmed:false, campaign: campaign4 }).sort("dates.candidation": 1).limit(1000).lean().exec (err,data) ->
        i = 0
        for item in data
          id = item.user.toString()
          # cerco il primo valido se ha un profilo corretto
          # deve essere presente nei profili con requisiti validi e non presente nella prima lista
          if profiliValidi4[id] and !giaAggiunto[id] and !candidatiNonValidi[id] and i < conteggiMax[campaign4.toString()]
            candidatiCheDovrebberoAvereIlPremio[id] = id: item._id.toString(), camp: item.campaign.toString()
            arr.push id: item._id.toString(),user: id, camp: item.campaign.toString()
            giaAggiunto[id] = id
            i++

        console.log " ------------- "
        console.log campaign4.toString() , "UserId da aggiungere una candidatura 4", arr.length
        console.log arr
        done()
###