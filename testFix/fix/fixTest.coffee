Candidate = require('../../dist/modules/candidate/model').dao
Campaign  = require('../../dist/modules/campaign/model').dao
User      = require '../../dist/modules/user/user'
Profile   = require '../../dist/modules/profile/model'

Mock    = require '../mock'
version = Mock.config.version

async   = require 'async'
request = require 'supertest'
_       = require 'lodash'
moment  = require 'moment'


describe 'Various fixes to apply', ->
  url = 'http://localhost:8000/' + version
  adminToken = ''
  formId = ''
  campaignId = ''

  before (done)->
    Campaign.remove {}, (err, res) ->
      admin = Mock.adminProfile('lallo')
      admin.save (err, res) ->
        adminToken = res.token
        form = Mock.form()
        form.save (err, res) ->
          formId = res._id
          campaign = Mock.campaign(formId, Math.random())
          campaign.save (err, res) ->
            campaignId = res._id
            user = Mock.userProfile()
            user.save (err, res) ->
              token = res.token
              data = new Candidate(
                user: res._id
                campaign: campaignId
                active: false
                rejected: true
                preconfirmed: false
                requisite: true
                dates:
                  candidation: moment()
              )
              data.save (err, res) ->
                userTwo = Mock.userProfile()
                userTwo.save (err, res) ->
                  userTwoId = res._id
                  data = new Candidate(
                    user: userTwoId
                    campaign: campaignId
                    active: false
                    rejected: true
                    preconfirmed: false
                    requisite: true
                    dates:
                      candidation: moment()
                  )
                  data.save (err, res) ->
                    userThree = Mock.userProfile()
                    userThree.save (err, res) ->
                      userThreeId = res._id
                      data = new Candidate(
                        user: userThreeId
                        campaign: campaignId
                        active: true
                        rejected: false
                        preconfirmed: true
                        requisite: true
                        dates:
                          candidation: moment()
                      )
                      data.save (err, res) ->
                        userFour = Mock.userProfile()
                        userFour.save (err, res) ->
                          userFourId = res._id
                          data = new Candidate(
                            user: userFourId
                            campaign: campaignId
                            active: false
                            rejected: false
                            preconfirmed: false
                            requisite: false
                            dates:
                              candidation: moment()
                          )
                          data.save (err, res) ->
                            done()


  describe 'Fix operations', ->

    it 'Fix campagna Vitermine: numero utenti attivati 100, ne vanno attivati altri 400', (done) ->
      data = {
        campaign: campaignId
      }
      request(url).post('/fix/fixAcceptedCampaignVitermine').set('authorization', 'Bearer ' + adminToken).send(data).expect(200).end (err, res) ->
        if err
          throw err

        res.status.should.equal 200
        res.body.count.should.equal 2
        done()
      return


  return