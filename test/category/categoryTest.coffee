Category = require('../../dist/modules/category/model').dao
request = require('supertest')
Mock = require '../mock'

version = Mock.config.version

describe 'Category', ->
  url = 'http://localhost:8000/' + version
  categoryId = ''
  adminToken = ''

  before (done)->
    Category.remove {}, (err, res) ->
      admin = Mock.adminProfile()
      admin.save (err, res) ->
        adminToken = res.token
        done()

  describe 'CRUD operations', ->
    it 'POST /category with valid required data', (done) ->
      data = Mock.categoryData()
      request(url).post('/category').expect(200).set('authorization', 'Bearer ' + adminToken).send(data).end (err, res) ->
        if err
          throw err

        res.body.title.should.equal data.title
        res.body.description.should.equal data.description
        res.body.published.should.equal data.published
        done()
        return

    it 'POST /category with invalid required data', (done) ->
      data =
        title: ''
      request(url).post('/category').expect(400).set('authorization', 'Bearer ' + adminToken).send(data).end (err, res) ->
        if err
          throw err

        done()
        return

    it 'POST /category with valid required and optional data', (done) ->
      data =
        title: 'Second cat'
        description: 'this is a description of the category'
#        media:
#          logo:
#            ref:
#            path: 'http://blablabla'
#            name:
        order: 3
        class: 'testclass'
        published: true
      request(url).post('/category').expect(200).set('authorization', 'Bearer ' + adminToken).send(data).end (err, res) ->
        if err
          throw err

        res.body.title.should.equal data.title
        res.body.description.should.equal data.description
#        res.body.media.logo.should.equal data.images.logo
        res.body.order.should.equal data.order
        res.body.published.should.equal data.published
        res.body.class.should.equal 'testclass'
        categoryId = res.body._id
        done()
        return

    it 'GET /category with provided _id from previous test', (done) ->
      request(url).get('/category/'+categoryId).set('authorization', 'Bearer ' + adminToken).expect(200).end (err, res) ->
        if err
          throw err

        res.body.should.have.property('title')
        done()
        return

    it 'PUT /category with provided _id from previous test', (done) ->
      data =
        title: 'Second cat modified'
        description: 'this is a description of the category modified'
        published: false
        class: 'testclassmod'
      request(url).put('/category/'+categoryId).expect(200).set('authorization', 'Bearer ' + adminToken).send(data).end (err, res) ->
        if err
          throw err

        res.body.title.should.equal data.title
        res.body.description.should.equal data.description
        res.body.published.should.equal data.published
        res.body.class.should.equal 'testclassmod'
        done()
        return

    it 'GET /category list of all categories', (done) ->
      request(url).get('/category').set('authorization', 'Bearer ' + adminToken).expect(200).end (err, res) ->
        if err
          throw err

        done()
        return

  describe 'clean operations', ->
    it 'DELETE /category with provided _id', (done) ->
      request(url).delete('/category/'+categoryId).expect(400).set('authorization', 'Bearer ' + adminToken).send().end (err, res) ->
        if err
          throw err

        done()
        return

    it 'GET /category with provided _id after delete (expecting 404)', (done) ->
      request(url).get('/category/'+categoryId).set('authorization', 'Bearer ' + adminToken).expect(200).end (err, res) ->
        if err
          throw err

        done()
        return