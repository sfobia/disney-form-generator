Media   = require('../../dist/modules/media/model')
request = require('supertest')
Mock    = require '../mock'

version = Mock.config.version

describe 'Media', ->
  url = 'http://localhost:8000/' + version
  id = ''
  adminToken = ''

  before (done)->
    Media.remove {}, (err, res)->
      admin = Mock.adminProfile('lallo')
      admin.save (err, res) ->
        adminToken = res.token
        done()

  describe 'CRUD operations: ', ->
    it 'POST /media with valid required data', (done) ->
      request(url).post('/media')
      .set('Authorization', 'Bearer '+adminToken)
      .set('Content-Type', 'multipart/form-data')
      .field('name', 'moni')
      .field('tag', 'Nature,Pics')
      .attach('image', __dirname + '/file1.png')
      .expect(200)
      .end (err, res) ->
        id = res.body._id
        res.body.name.should.equal 'moni'
        res.body.tag[0].name.should.equal 'Nature'
        res.body.tag[1].name.should.equal 'Pics'
        res.body.image.original.should.have.property('path')
        done()
        return

    it 'POST /media with not valid data', (done) ->
      request(url).post('/media')
        #.set('Authorization', 'Token ')
      .set('Content-Type', 'multipart/form-data')
      .attach('image', __dirname + '/file1.png')
      .expect(400)
      .end (err, res) ->
        done()
        return

    it 'POST /media with not valid data', (done) ->
      request(url).post('/media')
      .set('authorization', 'Bearer ' + adminToken)
      .set('Content-Type', 'multipart/form-data')
      .field('name', 'moni')
      .expect(400)
      .end (err, res) ->
        done()
        return

  #it 'PUT /media update data with provided _id', (done) ->

  describe 'Query operations: ', ->
    it 'GET /media all media', (done) ->
      request(url).get('/media').set('authorization', 'Bearer ' + adminToken).expect(200).end (err, res) ->
        if err
          throw err
        res.body.length.should.equal = 1
        res.body[0].name.should.equal 'moni'
        res.body[0].tag[0].name.should.equal 'Nature'
        res.body[0].tag[1].name.should.equal 'Pics'
        res.body[0].image.original.should.have.property('path')
        done()
        return

    it 'GET /media search media with tags', (done) ->
      request(url).get('/media?q='+encodeURI('Nature')+'+'+encodeURI('Pics')).set('authorization', 'Bearer ' + adminToken).expect(200).end (err, res) ->
        if err
          throw err
        res.body.length.should.equal = 1
        res.body[0].tag[0].name.should.equal 'Nature'
        done()
        return

#    it 'GET /media search media with name', (done) ->
#      request(url).get('/media?q='+encodeURI('moni')).set('authorization', 'Bearer ' + adminToken).expect(200).end (err, res) ->
#        if err
#          throw err
#        res.body.length.should.equal = 1
#        res.body[0].name.should.equal 'moni'
#        done()
#        return


#  describe 'Delete', ->
#    it 'DELETE /media with provided _id', (done) ->
#      request(url).delete('/media/'+id).set('authorization', 'Bearer ' + adminToken).expect(200).end (err, res) ->
#        if err
#          throw err
#        done()
#        return
#
#    it 'GET /media after delete', (done) ->
#      request(url).get('/media').set('authorization', 'Bearer ' + adminToken).expect(200).end (err, res) ->
#        if err
#          throw err
#        res.body.length.should.equal = 0
#        done()
#        return
  return