Candidate = require('../../dist/modules/candidate/model').dao
Profile = require('../../dist/modules/profile/model').dao
Campaign = require('../../dist/modules/campaign/model').dao
Form = require('../../dist/modules/form/model').dao
User    = require '../../dist/modules/user/user'
Notify  = require '../../dist/modules/notify/model'
request = require 'supertest'
Mock    = require '../mock'
fs      = require 'fs'
async   = require 'async'

version = Mock.config.version

describe 'Candidate', ->
  url = 'http://localhost:8000/' + version
  campaignId = ''
  campaignTwoId = ''
  campaignThreeId = ''
  campaignFourId = ''
  formId = ''
  candidateId = ''
  userId = ''
  candidates = []
  tokenId =  ''
  email = Date.now() + 'test@softfobia.com'
  candidateWithProfile = ''

  csvFile = ''

  before (done)->
    Candidate.remove {}, (err, res) ->
      Campaign.remove {}, (err, res) ->
        User.dao.remove {}, (err, res) ->
          user = new User.dao({email: email, password:'qwerty', scope:'admin', confirmed: true})
          user.save (err, res) ->
            userId = res._id
            tokenId = res.token
            profile = Mock.profile(userId)
            profile.save (err, res) ->
              User.dao.update({_id: userId}, {profile: res._id}).exec (err, res) ->
                Candidate.remove {}, (err, res) ->
                  form = new Form({
                    title: 'form title'
                    type: [
                      {
                        name: 'form type'
                        type: 'input'
                        order: 1
                        value: [
                          name: 'input name'
                        ]
                      }
                    ]
                    predefined: true
                  })
                  form.save (err, res) ->
                    formId = res._id
                    campaign = new Campaign({
                      title: 'Campagna Test del menga'
                      type: 'test'
                      form: formId
                      link: 'http://campaigntest.com'
                      published: true
                      slug: 'test-campaigncandidate'
                      piecesInitial: 1
                      weekNumber: 7
                    })
                    campaign.save (err, res) ->
                      campaignId = res._id
                      formId = res._id
                      campaignTwo = new Campaign({
                        title: 'Campagna Test del menga 2'
                        type: 'test'
                        form: formId
                        link: 'http://campaigntest2.com'
                        published: true
                        slug: 'test-campaigncandidatetwo'
                        piecesInitial: 1
                        weekNumber: 7
                      })
                      campaignTwo.save (err, res) ->
                        campaignTwoId = res._id
                        done()

  describe 'CRUD operations', ->
    it 'POST /candidate with valid required data', (done) ->
      data =
        user: userId
        campaign: campaignId
        active: false
        rejected: false
      request(url).post('/candidate').set('authorization', 'Bearer ' + tokenId).expect(200).send(data).end (err, res) ->
        if err
          throw err

        res.body.user.should.equal userId.toString()
        res.body.campaign.should.equal campaignId.toString()
        res.body.active.should.equal false
        res.body.rejected.should.equal false

        candidateId = res.body._id
        done()
        return

    it 'POST /candidate with valid required data', (done) ->
      data =
        user: userId
        campaign: campaignTwoId
        active: false
        rejected: false
      request(url).post('/candidate').set('authorization', 'Bearer ' + tokenId).expect(200).send(data).end (err, res) ->
        if err
          throw err

        res.body.user.should.equal userId.toString()
        res.body.campaign.should.equal campaignTwoId.toString()
        res.body.active.should.equal false
        res.body.rejected.should.equal false

        done()
        return

    it 'POST /candidate with valid required data', (done) ->

      userTwo = new User.dao({ email: 'Second'+email, password:'qwerty', userName: 'usernam2934', scope:'user', confirmed: true })
      userTwo.save (err, res) ->


        userTwoId = res._id
        data =
          user: userTwoId
          campaign: campaignId
          active: false
          rejected: false
          preconfirmed: false

        request(url).post('/candidate').set('authorization', 'Bearer ' + tokenId).expect(200).send(data).end (err, res) ->
          if err
            throw err

          res.body.user.should.equal userTwoId.toString()
          res.body.campaign.should.equal campaignId.toString()
          res.body.active.should.equal false
          res.body.rejected.should.equal false
          res.body.preconfirmed.should.equal false
          done()
          return

    it 'POST /candidate with valid required data, but already signed in for the campaign', (done) ->
      data =
        user: userId
        campaign: campaignId
        active: false
        rejected: false
      request(url).post('/candidate').set('authorization', 'Bearer ' + tokenId).expect(403).send(data).end (err, res) ->
        if err
          throw err
        done()
        return

    it 'POST /candidate with valid empty data', (done) ->
      data =
        user: ''
        campaign: ''
        active: ''
        rejected: ''
      request(url).post('/candidate').set('authorization', 'Bearer ' + tokenId).expect(400).send(data).end (err, res) ->
        if err
          throw err

        done()
        return

    it 'GET /candidate with provided _id', (done) ->
      request(url).get('/candidate/'+candidateId).set('authorization', 'Bearer ' + tokenId).expect(200).end (err, res) ->
        if err
          throw err

        res.body._id.should.equal candidateId

        done()
        return

    it 'GET /candidate list', (done) ->
      request(url).get('/candidate').set('authorization', 'Bearer ' + tokenId).expect(200).end (err, res) ->
        if err
          throw err

        res.body.length.should.equal 3

        done()
        return

    it 'PUT /candidate with provided _id', (done) ->
      data =
        user: userId
        campaign: campaignId
        active: true
        rejected: true
        sentMail: false
        preconfirmed: true
      request(url).put('/candidate/'+candidateId).set('authorization', 'Bearer ' + tokenId).expect(200).send(data).end (err, res) ->
        if err
          throw err

        res.body.user.should.equal userId.toString()
        res.body.campaign.should.equal campaignId.toString()
        res.body.active.should.equal true
        res.body.rejected.should.equal true
        res.body.sentMail.should.equal false
        res.body.preconfirmed.should.equal true

        done()
        return
    return

  describe 'Queries client side', ->
    it 'POST /candidate of existing user', (done) ->
      data =
        'user': userId
      request(url).post('/candidate/query').set('authorization', 'Bearer ' + tokenId).expect(200).send(data).end (err, res) ->
        if err
          throw err
        res.body[0].should.have.property('user')

        done()
        return
      return

    it 'POST /candidate of existing user that is active', (done) ->
      data =
        'user': userId
        active: true
      request(url).post('/candidate/query').set('authorization', 'Bearer ' + tokenId).expect(200).send(data).end (err, res) ->
        if err
          throw err
        res.body[0].should.have.property('user')

        done()
        return
    return

  describe 'Queries', ->
    it 'POST /candidate with specified field active', (done) ->
      data =
        active: true
      request(url).post('/candidate/query').set('authorization', 'Bearer ' + tokenId).expect(200).send(data).end (err, res) ->
        if err
          throw err
        res.body[0].should.have.property('active')

        done()
        return

    return

  describe 'Complete campaign and select candidates', ->
    it 'POST /candidate/accepted', (done) ->
      candidateTwo = Mock.candidateTwo(userId, campaignId)
      candidateTwo.save (err, res) ->
        candidateTwoId = res._id
        candidates = [
          candidateId
          candidateTwoId
        ]

        request(url).post('/candidate/accepted/' + campaignId).set('authorization', 'Bearer ' + tokenId).send(candidates).expect(200).end (err, res) ->
          if err
            throw err
          res.body.nModified.should.equal candidates.length
          done()
          return

    return

  describe 'Check campaign for candidates', ->
    it 'get /candidate/query with provided _id', (done) ->

      data =
        campaign: campaignId

      request(url).post('/candidate/query').set('authorization', 'Bearer ' + tokenId).send(data).expect(200).end (err, res) ->
        if err
          throw err

        res.body.should.have.length 3

#        res.body[0]._id.should.equal candidates[0].toString()
#        res.body[1]._id.should.equal candidates[1].toString()
        done()
        return
    return

#  describe 'Complete campaign and select candidates', ->
#    it 'POST /candidate/reject', (done) ->
#      candidateTwo = Mock.candidateTwo(userId, campaignId)
#      candidateTwo.save (err, res) ->
#        candidateTwoId = res._id
#        candidates = [
#          candidateId
#          candidateTwoId
#        ]
#        request(url).post('/candidate/reject/' + campaignId).set('authorization', 'Bearer ' + tokenId).send(candidates).expect(200).end (err, res) ->
#          if err
#            throw err
#          res.body.nModified.should.equal candidates.length
#          done()
#          return

  describe 'Activation procedure', ->
    it 'POST /candidate/acceptPreconfirmed candidates (PASSO 1)', (done) ->
      request(url).post('/candidate/acceptPreconfirmed/' + campaignId).set('authorization', 'Bearer ' + tokenId).expect(200).end (err, res) ->
        if err
          throw err
        res.body.numberAccepted.should.equal 2
        done()
        return

    it 'POST /candidate/reject/all candidates of campaign (PASSO 2)', (done) ->
      request(url).post('/candidate/reject/all/' + campaignId).set('authorization', 'Bearer ' + tokenId).expect(200).end (err, res) ->
        if err
          throw err
        done()
        return

    it 'POST /candidate/sendMail to accepted candidates (PASSO 3)', (done) ->
      request(url).post('/candidate/sendMail/' + campaignId).set('authorization', 'Bearer ' + tokenId).expect(200).end (err, res) ->
        if err
          throw err
        res.body.numberSentMail.should.equal 2 #vedi sopra, un candidato
        done()
        return

    it 'get /candidate/export/ with provided campaign _id (PASSO 4)', (done) ->
      request(url).post('/candidate/export/'+campaignId).set('authorization', 'Bearer ' + tokenId).expect(200).end (err, res) ->
        if err
          throw err
        res.body.should.have.property('file')
        csvFile = __dirname + '/../../public/' + res.body.file
        txt = fs.readFileSync csvFile, 'utf8'
        lines = txt.split("\n")
        # 2 candidates più riga degli header
        lines.length.should.equal 2


        done()
        return
    return

    it 'get /candidate/import/csv with provided campaign _id (PASSO 5)', (done) ->

      csvFileTcode = __dirname + '/../../public/tcode.csv'
      txt = fs.readFileSync csvFile, 'utf8'
      txt = txt.split "\n"
      k = 0
      txtTtacking = ''

      txt.forEach ( item ) ->
        item = item.split ";"
        if k != 0
          item[18] = Math.random()

        item = item.join(";")
        txtTtacking += item + '\n'
        k++

        return

      candidatesTracking = k-1
      fs.writeFileSync( csvFileTcode, txtTtacking );

      request(url).post('/candidate/import/csv' )
      .set('authorization', 'Bearer ' + tokenId )
      .set('Content-Type', 'multipart/form-data')
      .attach('csv',  csvFileTcode )
      .expect(200).end ( err, res ) ->

        if err
          throw err

        txt = fs.readFileSync csvFileTcode, 'utf8'
        txt = txt.split "\n"

        count = 0


        k = 0
        txt.forEach ( item ) ->
          item = item.split ";"
          if k != 0
            if item[18]
              count++
          k++

        count.should.equal candidatesTracking


        fs.unlinkSync csvFile
        fs.unlinkSync csvFileTcode
        done()
        return
      return

    it 'POST /candidate/sendTracking to accepted candidates (PASSO 6)', (done) ->
      shipToken = ''
      candidate =   new Candidate(
        user: userId
        campaign: campaignId
        active: true
        rejected: false
        sentMail: true
        preconfirmed: false
        requisite: true
        ship:
          info: 'shipping infos'
          url: 'shipping url'
          number: '0012301002031ZA'
      )
      candidate.save (err, res) ->
        request(url).post('/candidate/sendTracking/' + campaignId).set('authorization', 'Bearer ' + tokenId).expect(200).end (err, res) ->
          if err
            throw err
          done()
          return

    it 'DB controllo se esiste la mail di notifica invio tracking', (done) ->
      Notify.dao.findOne({user:userId,subject:'Invio codice tracking'}).exec (err,items) ->
        items.subject.should.equal 'Invio codice tracking'
        items.body.should.containEql 'pacco'
        done()
      return


    it 'POST /candidate/sendSurvey to accepted candidates (PASSO 7)', (done) ->
      shipToken = ''
      candidate =   new Candidate(
        user: userId
        campaign: campaignId
        active: true
        rejected: false
        sentMail: true
        preconfirmed: false
        requisite: true
        ship:
          info: 'shipping infos'
          url: 'shipping url'
          number: '0012301002031ZA'
      )
      candidate.save (err, res) ->
        request(url).post('/candidate/sendSurvey/' + campaignId).set('authorization', 'Bearer ' + tokenId).expect(200).end (err, res) ->
          if err
            throw err
          done()
          return

    it 'DB controllo se esiste la mail di notifica invio survey', (done) ->
      Notify.dao.findOne({user:userId,subject:'Invio survey'}).exec (err,items) ->
        items.subject.should.equal 'Invio survey'
        items.body.should.containEql 'compila il survey'
        done()
      return



  describe 'Deletion', ->
    it 'DELETE /candidate with provided _id', (done) ->
      request(url).delete('/candidate/'+candidateId).set('authorization', 'Bearer ' + tokenId).expect(400).send().end (err, res) ->
        if err
          throw err

        done()
        return

    it 'GET /candidate with provided _id after delete (expecting 404)', (done) ->
      request(url).get('/candidate/'+candidateId).set('authorization', 'Bearer ' + tokenId).expect(200).end (err, res) ->
        if err
          throw err

        done()
        return


  describe 'Procedura attivazione', ->

    it 'POST /campaign/filterCandidates/{id} (use default data) ', (done) ->
      data = {}

      async.waterfall [
        (cb) ->
          campaignTre = new Campaign({
            title: 'Campagna Test del menga 3'
            type: 'test'
            form: formId
            link: 'http://campaigntest2.com'
            published: true
            slug: 'test-campaigncandidatetreee'
            piecesInitial: 1
            weekNumber: 7
          })
          campaignTre.save (err, res) ->
            campaignThreeId = res._id
            cb()
        (cb) ->
          campaignFour = new Campaign({
            title: 'Campagna Test del menga 4'
            type: 'test'
            form: formId
            link: 'http://campaigntest4.com'
            published: true
            slug: 'test-campaigncandidatefoooour'
            piecesInitial: 1
            weekNumber: 23
          })
          campaignFour.save (err, res) ->
            campaignFourId = res._id
            cb()
        (cb) ->
          Candidate.remove {}, (err, res) ->
            cb()
        (cb) ->
          promise = Mock.createCampaignCandidates(campaignId, campaignTwoId, campaignThreeId, campaignFourId, true, true, true, false, false, false, true, false)
          promise.then (data) ->
            cb()
        (cb) ->
          promise = Mock.createCampaignCandidates(campaignId, campaignTwoId, campaignThreeId, campaignFourId, true, true, true, true, false, false, true, false)
          promise.then ->
            cb()
        (cb) ->
          promise = Mock.createCampaignCandidates(campaignId, campaignTwoId, campaignThreeId, campaignFourId, true, true, true, true, true, true, false, false)
          promise.then ->
            cb()
        (cb) ->
          promise = Mock.createCampaignCandidates(campaignId, campaignTwoId, campaignThreeId, campaignFourId, true, true, true, true, true, true, false, false)
          promise.then ->
            cb()
        (cb) ->
          promise = Mock.createCampaignCandidates(campaignId, campaignTwoId, campaignThreeId, campaignFourId, true, true, true, true, false, false, false, false)
          promise.then ->
            cb()
        (cb) ->
          promise = Mock.createCampaignCandidates(campaignId, campaignTwoId, campaignThreeId, campaignFourId, true, true, true, true, true, true, true, false)
          promise.then ->
            cb()
        (cb) ->
          request(url).post('/campaign/filterCandidates/'+campaignId).set('authorization', 'Bearer ' + tokenId).send(data).expect(200).end (err, res) ->
            res.body.count.should.equal 1
            if err
              throw err
            cb()
      ], (err, res) ->
        done()
      return

    it 'POST /campaign/filterCandidates/{id} (simulate, sex male, age 20-50, zone Sud e isole) ', (done) ->
      data = {
        sex: 'Male'
        age: {min: 20, max: 50}
        simulate: true
        zone: 'Sud e isole'
      }
      request(url).post('/campaign/filterCandidates/'+campaignId).set('authorization', 'Bearer ' + tokenId).send(data).expect(200).end (err, res) ->
        res.body.count.should.equal 1
        if err
          throw err
        done()
        return

    it 'POST /campaign/filterCandidates/{id} (simulate, sex female, age 20-50, zone Sud e isole) ', (done) ->
      data = {
        sex: 'Female'
        age: {min: 20, max: 50}
        simulate: true
        zone: 'Sud e isole'
      }
      request(url).post('/campaign/filterCandidates/'+campaignId).set('authorization', 'Bearer ' + tokenId).send(data).expect(400).end (err, res) ->
        if err
          throw err
        done()
        return

    it 'POST /campaign/filterCandidates/{id} (no simulate, sex male, age 20-50, zone Sud e isole) ', (done) ->
      data = {
        sex: 'Male'
        age: {min: 20, max: 50}
        simulate: false
        zone: 'Sud e isole'
      }
      request(url).post('/campaign/filterCandidates/'+campaignId).set('authorization', 'Bearer ' + tokenId).send(data).expect(200).end (err, res) ->
        res.body.count.should.equal 1
        if err
          throw err
        done()
        return

    it 'POST /campaign/filterCandidates/{id} (simulate, sex male, age 20-50, zone Sud e isole) ', (done) ->
      data = {
        sex: 'Male'
        age: {min: 20, max: 50}
        simulate: true
        zone: 'Sud e isole'
      }
      request(url).post('/campaign/filterCandidates/'+campaignId).set('authorization', 'Bearer ' + tokenId).send(data).expect(400).end (err, res) ->
        if err
          throw err
        done()
        return

    it 'POST /campaign/filterCandidates/{id} (simulate, sex male, age 20-50, zone Sud e isole) ', (done) ->
      data = {
        sex: 'Male'
        age: {min: 20, max: 50}
        simulate: true
        zone: 'Sud e isole'
      }
      request(url).post('/campaign/filterCandidates/'+campaignFourId).set('authorization', 'Bearer ' + tokenId).send(data).expect(200).end (err, res) ->
        res.body.count.should.equal 1
        if err
          throw err
        done()
        return

    it 'POST /campaign/filterCandidates/{id} (simulate, sex male, age 20-50, zone Sud e isole) ', (done) ->
      data = {
        sex: 'Male'
        age: {min: 200, max: 500}
        simulate: true
        zone: 'Sud e isole'
      }
      request(url).post('/campaign/filterCandidates/'+campaignFourId).set('authorization', 'Bearer ' + tokenId).send(data).expect(400).end (err, res) ->
        if err
          throw err
        done()
        return

    it 'POST /report/query List of users that match the query on info on their profile', (done) ->
      setTimeout( ->
        data =
          reportType: 'candidates-'+campaignId+'-accepted'
          campaign: campaignId
          active: true
          accepted:
            marketing: true
            dem: true
            profiling: true
        request(url).post('/report/queryProfile').set('authorization', 'Bearer ' + tokenId).send(data).expect(200).end (err, res) ->
          if err
            throw err
          done()
        ,4000)
      return

#
#    it 'POST /report/surveyNotSent List of users that did not send the survey', (done) ->
#      setTimeout( ->
#        data =
#          reportType: 'survey-not-sent-'
#          campaign: campaignId
#          typeForm: 'survey'
#
#        request(url).post('/report/surveyNotSent').set('authorization', 'Bearer ' + tokenId).send(data).expect(200).end (err, res) ->
#          console.log res
#          console.log err
#          if err
#            throw err
#          done()
#        ,3000)
#      return
  return