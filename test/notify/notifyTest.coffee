request = require('supertest')
Mock = require '../mock'
User = require('../../dist/modules/user/user').dao
Notify = require('../../dist/modules/notify/model').dao

version = Mock.config.version

describe 'Notify', ->
  urlAdmin = 'http://localhost:8000/' + version
  url = 'http://localhost:8100/' + version
  userId = ''
  tokenId = ''
  dataNotify = {}

  before (done)->
    user = new User({email:'test3@softfobia.com', password:'qwerty', scope:'user', userName: 'test3'+Date.now(), confirmed: true })
    user.save (err, item) ->
      userId = item._id
      tokenId = item.token

      notifyPromise = Notify.remove {}
      notifyPromise.then ->
        notify = Mock.notify(userId)
        notify.save (err,res)->
          dataNotify = res
          done()
    return

  describe 'Test notifications: ', ->
    it 'GET /notify with valid required data', (done) ->
      request(url).get('/profile/notify').set('authorization', 'Bearer ' + tokenId).expect(200).end (err, res) ->
        if err
          throw err
        res.body[0].user.should.equal userId.toString()
        res.body[0].subject.should.equal dataNotify.subject
        res.body[0].body.should.equal dataNotify.body
        res.body[0].read.should.equal false
        done()
        return
      return

    it 'GET /notify/id with valid required data', (done) ->
      request(url).get('/profile/notify/'+dataNotify._id).set('authorization', 'Bearer ' + tokenId).expect(200).end (err, res) ->
        if err
          throw err
        res.body.user.should.equal userId.toString()
        res.body.subject.should.equal dataNotify.subject
        res.body.body.should.equal dataNotify.body
        res.body.read.should.equal false

        done()
        return
      return

    it 'GET /notify/id letta', (done) ->
      request(url).get('/profile/notify/'+dataNotify._id).set('authorization', 'Bearer ' + tokenId).expect(200).end (err, res) ->
        if err
          throw err
        res.body.user.should.equal userId.toString()
        res.body.subject.should.equal dataNotify.subject
        res.body.body.should.equal dataNotify.body
        res.body.read.should.equal true
        done()
        return
      return
  return