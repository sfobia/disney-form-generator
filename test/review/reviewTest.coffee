#Review = require(process.cwd()+'/build/backend/review/model').dao()
Review = require('../../dist/modules/review/model').dao
request = require('supertest')
Mock = require '../mock'

version = Mock.config.version

describe 'Review', ->
  url = 'http://localhost:8100/' + version
  token = ''
  campaignId = ''
  userId = ''

  before (done)->
    Review.remove {}, (err, res) ->
      user = Mock.userProfile()
      user.save (err1, res1) ->
        userId = res1._id
        token = res1.token
        form = Mock.form()
        form.save (err2, res2) ->
          campaign = Mock.campaign(res2._id, 'reviewFrontend')
          campaign.save (err3, res3) ->
            campaignId = res3._id
            done()

  describe 'frontend operations', ->

    it 'POST /review with valid required data', (done) ->
      data = Mock.reviewData(campaignId)
      request(url).post('/review').expect(200).set('authorization', 'Bearer ' + token).send(data).end (err, res) ->
        if err
          throw err
#        res.body.text.should.equal data.text
#        res.body.campaign.should.equal data.campaign
        done()
        return

    it 'POST /review with invalid required data', (done) ->
      data = {
        campaign: ''
        text: ''
      }
      request(url).post('/review').expect(400).set('authorization', 'Bearer ' + token).send(data).end (err, res) ->
        if err
          throw err
        done()
        return

    it 'GET all /review of a campaign', (done) ->
      request(url).get('/review/'+campaignId).set('authorization', 'Bearer ' + token).expect(200).end (err, res) ->
        if err
          throw err

        res.body.length.should.equal = 1
        done()
        return
#
#    it 'GET /review of a user - Check doubles (one)', (done) ->
#      request(url).get('/review/checkUserReview/'+campaignId).set('authorization', 'Bearer ' + token).expect(400).end (err, res) ->
#        if err
#          throw err
#
#        done()
#        return

  return
return