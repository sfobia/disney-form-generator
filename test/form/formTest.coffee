Form = require('../../dist/modules/form/model').dao
request = require('supertest')
Mock = require '../mock'

version = Mock.config.version

describe 'Form', ->
  url = 'http://localhost:8000/' + version
  formId = ''
  form = ''
  adminToken = ''

  before (done)->
    Form.remove {}, (err, res) ->
      admin = Mock.adminProfile('formino')
      admin.save (err, res) ->
        adminToken = res.token
        done()

  describe 'Creation', ->
    it 'POST /form with valid required data', (done) ->
      data =
        title: 'form title'
        fields: [
          {
            name: 'form type'
            type: 'input'
            order: 1
            value: [
              name: 'input name'
            ]
          }
        ]
        predefined: true
        formType: 'registration'
      request(url).post('/form').set('authorization', 'Bearer ' + adminToken).expect(200).send(data).end (err, res) ->
        if err
          throw err
        res.body.should.have.property('title')
        formId = res.body._id
        done()
        return

    it 'POST /form with valid required data, but already posted formType registration', (done) ->
      data =
        title: 'form title bis'
        fields: [
          {
            name: 'form type'
            type: 'input'
            order: 1
            value: [
              name: 'input name'
            ]
          }
        ]
        predefined: true
        formType: 'registration'
      request(url).post('/form').set('authorization', 'Bearer ' + adminToken).expect(400).send(data).end (err, res) ->
        if err
          throw err
        done()
        return

    it 'POST /form with valid required data, radio and ranges', (done) ->
      data =
        title: 'Test form ranges'
        fields: [
          {
            name: 'form type'
            type: 'ranges'
            order: 2
            value: [
              left: 'io sono a sinistra'
              right: 'io sono a destra'
            ]
          }
          {
            name: 'form type'
            type: 'radio'
            order: 3
            value: [
              name: 'io sono una checkbox'
            ]
          }
        ]
        predefined: true
        formType: 'campaign'
      request(url).post('/form').expect(200).set('authorization', 'Bearer ' + adminToken).send(data).end (err, res) ->
        if err
          throw err

        formId = res.body._id

        res.body.title.should.equal 'Test form ranges'

        done()
        return

    it 'POST /form with empty required data', (done) ->
      data =
        title: ''
        fields: [
          {
            name: ''
            type: ''
            order: 1
          }
        ]
        predefined: true
      request(url).post('/form').set('authorization', 'Bearer ' + adminToken).expect(400).send(data).end (err, res) ->
        formType: ""
        if err
          throw err

        done()
        return

    it 'GET /form with provided _id', (done) ->
      request(url).get('/form/'+formId).set('authorization', 'Bearer ' + adminToken).expect(200).end (err, res) ->
        if err
          throw err

        res.body._id.should.equal formId.toString()

        done()
        return

    it 'GET /form all data ', (done) ->
      request(url).get('/form').set('authorization', 'Bearer ' + adminToken).expect(200).end (err, res) ->
        if err
          throw err

        res.body.length.should.equal 2

        done()
        return

    it 'PUT /form with provided _id', (done) ->
      data =
        title: 'Modified form'
        fields: [
          {
            name: 'Form name changed'
            type: 'checkbox'
            order: 2
          }
        ]
        predefined: true
        formType: "campaign"
      request(url).put('/form/'+formId).set('authorization', 'Bearer ' + adminToken).expect(200).send(data).end (err, res) ->
        if err
          throw err
        res.body.title.should.equal 'Modified form'
        res.body.fields[0].name.should.equal 'Form name changed'
        res.body.fields[0].type.should.equal 'checkbox'
        res.body.fields[0].order.should.equal 2
        res.body.predefined.should.equal true

        done()
        return

    it 'POST /form with given title', (done) ->
      data =
        title: 'Modified form'
      request(url).post('/form/query').set('authorization', 'Bearer ' + adminToken).expect(200).send(data).end (err, res) ->
        if err
          throw err
        res.body[0].should.have.property('title')

        done()
        return
      return

    it 'DELETE /form with provided _id', (done) ->
      request(url).delete('/form/'+formId).set('authorization', 'Bearer ' + adminToken).expect(400).send().end (err, res) ->
        if err
          throw err

        done()
        return

    it 'GET /form with provided _id after delete (expecting 404)', (done) ->
      request(url).get('/form/'+formId).set('authorization', 'Bearer ' + adminToken).expect(200).end (err, res) ->
        if err
          throw err

        done()
        return

    it 'GET /form init', (done) ->
      request(url).get('/form/init' ).set('authorization', 'Bearer ' + adminToken).expect(200).end (err, res) ->
        if err
          throw err
        done()
        return

    it 'POST /form with valid required data, radio, MULTIPRODUCT', (done) ->
      data =
        title: 'Test form multiproduct'
        multiProduct: true
        products: [
          {
            name: 'first'
            quantity: 50
          }
          {
            name: 'secondo'
            quantity: 60
          }
        ]
        fields: [
          {
            name: 'form type'
            type: 'radio'
            order: 3
            assignProduct: true
            value: [
              {
                name: 'io sono una radio'
                product: 'first'
              }
              {
                name: 'anche io sono una radio'
                product: 'secondo'
              }
              {
                name: 'vorrei essere utile'
                product: '-'
              }
            ]
          }
        ]
        predefined: true
        formType: 'campaign'
      request(url).post('/form').expect(200).set('authorization', 'Bearer ' + adminToken).send(data).end (err, res) ->
        console.log err
        console.log res.body
        if err
          throw err

        formId = res.body._id

        res.body.title.should.equal 'Test form multiproduct'
        res.body.multiProduct.should.equal true
        res.body.products.length.should.equal 2


        done()
        return


    return
  return