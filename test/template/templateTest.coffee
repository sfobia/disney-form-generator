Template = require('../../dist/modules/template/model').dao
#CampaignSurvey = require('../../dist/modules/campaign/model').dao
#FormTemplate = require('../../dist/modules/form/model').dao

request = require('supertest')
Mock = require '../mock'

version = Mock.config.version

describe 'Template', ->
  url = 'http://localhost:8000/' + version
  templateId = ''
  adminToken = ''

  before (done)->
    Template.remove {}, (err, res)->
      admin = Mock.adminProfile('templatino')
      admin.save (err, res) ->
        adminToken = res.token
        done()

  describe 'CRUD operations: ', ->
    it 'POST /template with valid required data', (done) ->
      data = Mock.templateData()
      request(url).post('/template').set('authorization', 'Bearer ' + adminToken).expect(200).send(data).end (err, res) ->
        if err
          throw err
        templateId = res.body._id
        done()
        return
      return

    it 'POST /template with empty required data', (done) ->
      data =
        subject: ''
        body: ''
        type: 'email'
        identifier: ''
        published: true
      request(url).post('/template').set('authorization', 'Bearer ' + adminToken).expect(400).send(data).end (err, res) ->
        if err
          throw err
        done()
        return

    it 'POST /template with invalid required data', (done) ->
      data =
        subject: 'subject'
        body: 'body test'
        type: ''
        identifier: ''
        published: true
      request(url).post('/template').set('authorization', 'Bearer ' + adminToken).expect(400).send(data).end (err, res) ->
        if err
          throw err
        done()
        return

    it 'GET /template with provided _id', (done) ->
      request(url).get('/template/'+templateId).set('authorization', 'Bearer ' + adminToken).expect(200).end (err, res) ->
        if err
          throw err
        done()
        return

    it 'GET /template list', (done) ->
      request(url).get('/template').set('authorization', 'Bearer ' + adminToken).expect(200).end (err, res) ->
        if err
          throw err
        done()
        return

    it 'PUT /template with provided _id', (done) ->
      data = Mock.templateUpdateData()
      request(url).put('/template/' + templateId ).set('authorization', 'Bearer ' + adminToken).expect(200).send(data).end (err, res) ->
        if err
          throw err
        done()
        return

    it 'GET /template init', (done) ->
      request(url).get('/template/init' ).set('authorization', 'Bearer ' + adminToken).expect(200).end (err, res) ->
        if err
          throw err
        done()
        return

  describe 'Delete', ->
    createdTemplateId = ''

    it 'DELETE /template with provided _id', (done) ->

      template = Mock.template()
      template.save (err, res) ->
        createdTemplateId = res._id
        request(url).delete('/template/'+ createdTemplateId ).set('authorization', 'Bearer ' + adminToken).expect(400).send().end (err, res) ->
          if err
            throw err
          done()
          return
        return

    it 'GET /template with provided _id after delete (expecting 404)', (done) ->
      request(url).get('/template/'+ createdTemplateId ).set('authorization', 'Bearer ' + adminToken).expect(200).end (err, res) ->
        if err
          throw err
        done()
        return


  return