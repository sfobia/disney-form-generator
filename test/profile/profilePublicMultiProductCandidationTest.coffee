Profile = require('../../dist/modules/profile/model').dao
UserProfile    = require '../../dist/modules/user/user'
Candidate = require('../../dist/modules/candidate/model').dao
Moment = require 'moment'
request = require('supertest')
Mock = require '../mock'

version = Mock.config.version

describe 'Profile public', ->
  url = 'http://localhost:8100/' + version
  owner = ''
  ownerTwo = ''
  token = ''
  tokenTwo = ''
  formId = ''
  campaignId = ''
  formCampaignId = ''
  campaignTwoId = ''
  campaignThreeId = ''
  campaignFourId = ''
  email= ''
  before (done)->
    Profile.remove {}, (err, res) ->
      UserProfile.dao.remove {}, (err, res) ->
        email = 'publicprofiletest2@softfobia.com'
        user = new UserProfile.dao({email:'publicprofiletest2@softfobia.com', password:'qwerty','userName':'andreazucca', scope: 'user', confirmed: true })
        user.save (err, res) ->
          owner = res._id
          token = res.token
          profile = Mock.profile(owner)
          profile.save (err, res) ->
            form = Mock.formMultiProduct()
            form.save (err, res) ->
              formId = res._id
              campaign = Mock.campaign(formId,'new')
              campaign.save (err, resC) ->
                campaignId = resC._id
                formCampaign = Mock.formTwo()
                formCampaign.save (err, resD) ->
                  formCampaignId = resD._id
                  campaignTwo = Mock.campaignTag(formId)
                  campaignTwo.save (err, resE) ->
                    campaignTwoId = resE._id
                    campaignThree = Mock.campaignThree(formId, 'new')
                    campaignThree.save (err, resF) ->
                      campaignThreeId = resF._id
                      campaignFour = Mock.campaignFour(formId, 'neww')
                      campaignFour.save (err, resG) ->
                        campaignFourId = resG._id
                        user = new UserProfile.dao({email:'publicprofiletest3@softfobia.com', password:'qwerty','userName':'andreazucca2', scope: 'user', confirmed: true })
                        user.save (err, res) ->
                          ownerTwo = res._id
                          tokenTwo = res.token
                          profile = Mock.profile(ownerTwo)
                          profile.save (err, res) ->
                            done()

  describe 'Profile public multiproduct candidation operations', ->

    it 'POST /profile/form for submitting a form ( formId + campaignId ) [campaign candidation]', (done) ->
      form = Mock.formMultiProduct()
      form.save (err, res) ->
        formId = res._id
        data =
          ref: formId
          campaign: campaignId
          name: 'form campaign'
          values: [
            {
              name: 'first name'
              value: 'first answer'
              type: 'radio'
              product: 'first'
            }
          ]
          formType: 'campaign'
          prevalid: true
        request(url).post( '/profile/form/' + formId + '/' + campaignId ).set('authorization', 'Bearer ' + token).expect(200).send(data).end (err, res) ->
          if err
            throw err
          done()
          return

#    it 'POST /profile/form for submitting a form ( formId + campaignId ) [campaign candidation 2]', (done) ->
#      form = Mock.form()
#      form.save (err, res) ->
#        formId = res._id
#        data =
#          ref: formId
#          campaign: campaignTwoId
#          name: 'form campaign'
#          values: [
#            name: 'input string'
#            value: 'my user input response'
#            type: 'input'
#          ]
#          formType: 'campaign'
#          prevalid: true
#        request(url).post( '/profile/form/' + formId + '/' + campaignId ).set('authorization', 'Bearer ' + token).expect(200).send(data).end (err, res) ->
#          if err
#            throw err
#
#          done()
#          return
#
#    it 'POST /candidate candidate for a campaign', (done) ->
#      data =
#        campaign: campaignId
#      request(url).post('/profile/candidate').set('authorization', 'Bearer ' + token).expect(200).send(data).end (err, res) ->
#        if err
#          throw err
#        res.body.active.should.equal false
#        res.body.rejected.should.equal false
#        done()
#        return

#
#
#    it 'POST /profile/form for submitting a form ( formId + campaignId ) [campaign candidation 3]', (done) ->
#      form = Mock.form()
#      form.save (err, res) ->
#        formId = res._id
#        data =
#          ref: formId
#          campaign: campaignThreeId
#          name: 'form campaign'
#          values: [
#            name: 'input string'
#            value: 'my user input response'
#            type: 'input'
#          ]
#          formType: 'campaign'
#          prevalid: true
#        request(url).post( '/profile/form/' + formId + '/' + campaignThreeId ).set('authorization', 'Bearer ' + token).expect(200).send(data).end (err, res) ->
#          if err
#            throw err
#
#          done()
#          return
#
#    it 'POST /profile/form for submitting a form ( formId + campaignId ) [campaign fail survey out of dates 2]', (done) ->
#      formSurvey = Mock.formSurvey()
#      formSurvey.save (err, res) ->
#        formSurveyId = res._id
#        data =
#          ref: formSurveyId
#          campaign: campaignFourId
#          name: 'survey 1'
#          values: [
#            {
#              name: 'form type'
#              type: 'input'
#              order: 1
#              value: [
#                name: 'input name'
#              ]
#            }
#            {
#              name: 'form type 2'
#              type: 'input'
#              order: 1
#              value: [
#                name: 'input name 2'
#              ]
#            }
#          ]
#          formType: 'survey'
#        request(url).post( '/profile/form/' + formSurveyId + '/' + campaignFourId ).set('authorization', 'Bearer ' + token).expect(400).send(data).end (err, res) ->
#          if err
#            throw err
#
#          done()
#          return
#
#    it 'POST /candidate candidate for a third campaign', (done) ->
#      data =
#        campaign: campaignThreeId
#      request(url).post('/profile/candidate').set('authorization', 'Bearer ' + token).expect(200).send(data).end (err, res) ->
#        if err
#          throw err
#        res.body.active.should.equal false
#        res.body.rejected.should.equal false
#        done()
#        return
#
#    it 'POST /profile/form for submitting a form ( formId + campaignId ) [campaign candidation 4]', (done) ->
#      form = Mock.form()
#      form.save (err, res) ->
#        formId = res._id
#        data =
#          ref: formId
#          campaign: campaignFourId
#          name: 'form campaign'
#          values: [
#            name: 'input string'
#            value: 'my user input response'
#            type: 'input'
#          ]
#          formType: 'campaign'
#          prevalid: true
#        request(url).post( '/profile/form/' + formId + '/' + campaignFourId ).set('authorization', 'Bearer ' + token).expect(200).send(data).end (err, res) ->
#          if err
#            throw err
#
#          done()
#          return
#
#    it 'POST /profile/form for submitting a form survey [survey campaign 1]', (done) ->
#      form = Mock.formSurvey()
#      form.save (err, res) ->
#        formId = res._id
#        data =
#          ref: formId
#          campaign: campaignId
#          name: 'survey 1'
#          values: [
#            {
#              name: 'form type'
#              type: 'input'
#              order: 1
#              value: [
#                name: 'input name'
#              ]
#            }
#            {
#              name: 'form type 2'
#              type: 'input'
#              order: 1
#              value: [
#                name: 'input name 2'
#              ]
#            }
#          ]
#          formType: 'survey'
#        request(url).post( '/profile/form/' + formId + '/' + campaignId ).set('authorization', 'Bearer ' + token).expect(200).send(data).end (err, res) ->
#          if err
#            throw err
#
#          done()
#          return
#
#    it 'POST /candidate candidate for a fourth campaign', (done) ->
#      data =
#        campaign: campaignFourId
#      request(url).post('/profile/candidate').set('authorization', 'Bearer ' + token).expect(200).send(data).end (err, res) ->
#        if err
#          throw err
#        res.body.active.should.equal false
#        res.body.rejected.should.equal false
#        done()
#        return