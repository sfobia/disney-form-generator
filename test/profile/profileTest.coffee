Profile = require('../../dist/modules/profile/model').dao
Template = require('../../dist/modules/template/model').dao
UserProfile    = require '../../dist/modules/user/user'
Moment = require 'moment'
request = require('supertest')
Mock = require '../mock'

version = Mock.config.version

describe 'Profile', ->
  url = 'http://localhost:8000/' + version
  urlPublic = 'http://localhost:8100/' + version
  owner = ''
  token = ''
  adminToken = ''
  profileId = ''
  firstProfileId = ''
  tokenConfirm = ''
  formId = ''
  formTwoId = ''
  campaignId = ''
  campaignTwoId = ''
  surveyId = ''
  userId = ''
  before (done)->
    profileRemove = Profile.remove {}
    profileRemove.then ->
      userRemove = UserProfile.dao.remove {}
      userRemove.then ->
        user = new UserProfile.dao({email:'test1test@softfobia.com', password:'qwerty', userName: 'xxx'+Date.now(), confirmed: true })
        user.save (err, res) ->
          owner = res._id
          token = res.token
#          tokenConfirm = res.tokenConfirm
          admin = Mock.adminProfile()
          admin.save (err, res) ->
            adminToken = res.token
            template = new Template(Mock.templateData())
            template.save (err, res) ->
              form = Mock.form()
              form.save (err, res) ->
                formId = res._id
                formTwo = Mock.formTwo()
                formTwo.save (err, res) ->
                  formTwoId = res._id
                  campaign = Mock.campaign(formId,'aaa')
                  campaign.save (err, resC) ->
                    campaignId = resC._id
                    campaignTwo = Mock.campaignTag(formTwoId,'xxxxx')
                    campaignTwo.save (err, resT) ->
                      campaignTwoId = resT._id
                      done()

  describe 'CRUD operations', ->
#    it 'GET /account/activate to activate account', (done) ->
#      request(urlPublic).get('/account/activate/' + tokenConfirm ).expect(200).end (err, res) ->
#        if err
#          throw err
#        done()
#        return

    it 'POST /profile with valid required data', (done) ->
      data =
        user: owner
        name:
          first: 'Mario2'
          last: 'Rossi2'
        gender: 'Male'
        birthDate: new Moment('2015-12-23')
        accepted:
          marketing: true
      request(url).post('/profile').set('authorization', 'Bearer ' + adminToken).send(data).end (err, res) ->
        if err
          throw err

        res.body.accepted.marketing.should.equal true

        res.body.should.have.property('user')
        res.body.should.have.property('gender')
        res.body.should.have.property('birthDate')
        res.body.should.have.property('accepted')
        res.body.should.have.property('name')
        res.status.should.equal 200
        profileId = res.body._id
        firstProfileId = res.body._id
        done()
        return

    it 'POST /profile with empty required data', (done) ->
      data =
        user: owner
        name:
          first: ''
          last: ''
        gender: ''
        birthDate: ''
        accepted:
          marketing: ''
      request(url).post('/profile').set('authorization', 'Bearer ' + adminToken).send(data).end (err, res) ->
        if err
          throw err
        res.status.should.equal 400
        done()
        return

    it 'POST /profile with invalid required data', (done) ->
      data =
        user: owner
        name:
          first: ''
          last: ''
        gender: 'Any'
        birthDate: ''
        accepted:
          marketing: ''
      request(url).post('/profile').set('authorization', 'Bearer ' + adminToken).send(data).end (err, res) ->
        if err
          throw err
        res.status.should.equal 400
        done()
        return

    it 'POST /profile with valid optional data', (done) ->
      user = new UserProfile.dao({email: 'test2test2@softfobia.com', password: 'qwerty'})
      user.save (err, res) ->
        userId = res._id
        data =
          user: res._id
          name:
            first: 'Mario3'
            last: 'Rossi3'
          gender: 'Male'
          birthDate: new Moment('2015-12-23')
          phone:
            mobile: '3401234567'
            home: '02012345'
            office: '0298765'
          accepted:
            marketing: true
            dem: true
        request(url).post('/profile').set('authorization', 'Bearer ' + adminToken).send(data).end (err, res) ->
          if err
            throw err

          res.body.accepted.marketing.should.equal true
          res.body.accepted.dem.should.equal true

          res.body.should.have.property('user')
          res.body.should.have.property('gender')
          res.body.should.have.property('accepted')
          res.body.should.have.property('name')
          res.body.should.have.property('phone')
          #res.body.should.have.property('photo')
          res.body.should.have.property('birthDate')
          res.status.should.equal 200
          profileId = res.body._id
          done()
          return

    it 'POST /profile with valid required data but already existing', (done) ->
      data =
        user: owner
        name:
          first: 'Mario'
          last: 'Rossi'
        gender: 'Male'
        birthDate: new Moment('2015-12-23')
        accepted:
          marketing: true
      request(url).post('/profile').set('authorization', 'Bearer ' + adminToken).send(data).end (err, res) ->
        if err
          throw err
        res.status.should.equal 403
        done()
        return

    it 'POST /profile with invalid optional data', (done) ->
      data =
        user: owner
        name:
          first: 'Mario'
          last: 'Rossi'
        gender: 'Male'
        birthDate: new Moment('2015-12-23')
        phone:
          mobile: ''
          home: ''
          office: ''
        accepted:
          marketing: true
          dem: ''
        photo: ''
      request(url).post('/profile').set('authorization', 'Bearer ' + adminToken).send(data).end (err, res) ->
        if err
          throw err
        res.status.should.equal 400
        done()
        return

    it 'GET /profile with provided _id', (done) ->
      request(url).get('/profile/'+profileId).set('authorization', 'Bearer ' + adminToken).end (err, res) ->
        if err
          throw err
        res.status.should.equal 200
        done()
        return

    it 'GET /profile with provided _id and false token', (done) ->
      request(url).get('/profile/'+profileId).set('authorization', 'Bearer ' + adminToken + 'ehrhraorer').end (err, res) ->
        if err
          throw err
        res.status.should.equal 401
        done()
        return

    it 'GET /profile ', (done) ->
      request(url).get('/profile').set('authorization', 'Bearer ' + adminToken).end (err, res) ->
        if err
          throw err
        res.status.should.equal 200
        done()
        return

    it 'PUT /profile with provided _id', (done) ->
      data =
        status: 'unreliable'
      request(url).put('/profile/'+firstProfileId).set('authorization', 'Bearer ' + adminToken).send(data).end (err, res) ->
        if err
          throw err
        res.status.should.equal 200
        done()
        return

    it 'POST /profile/form for submitting a form', (done) ->
      form = Mock.form()
      form.save (err, res) ->
        formId = res._id
        data =
          ref: formId
          name: 'form title 2'
          values: [
            name: 'input string'
            value: 'my user input response'
            type: 'input'
          ]
          formType: 'campaign'
        request(url).post('/profile/form/' + formId).set('authorization', 'Bearer ' + token).send(data).end (err, res) ->
          if err
            throw err
          res.status.should.equal 200
          done()
          return

    it 'POST /profile/form for submitting a second form', (done) ->
      form = Mock.form()
      form.save (err, res) ->
        secondFormId = res._id
        data =
          ref: secondFormId
          name: 'Second form title'
          values: [
            name: 'input string'
            value: 'my user input response'
            type: 'input'
          ]
          formType: 'campaign'
        request(url).post('/profile/form/' + secondFormId).set('authorization', 'Bearer ' + token).send(data).end (err, res) ->
          if err
            throw err
          res.status.should.equal 200
          done()
          return

    it 'POST /profile/form for submitting a form already submitted (expecting update)', (done) ->
        data =
          ref: formId
          name: 'form title 2 but now updated'
          values: [
            name: 'input string'
            value: 'my user input response updated'
            type: 'input'
          ]
          formType: 'campaign'
        request(url).post('/profile/form/' + formId).set('authorization', 'Bearer ' + token).send(data).end (err, res) ->
          if err
            throw err
          res.status.should.equal 200
          done()
          return

    it 'DELETE /profile with provided _id', (done) ->
      request(url).delete('/profile/'+userId).set('authorization', 'Bearer ' + adminToken).send().end (err, res) ->
        if err
          throw err
        # todo aggiungere controlli maggiori
        res.body.status.should.equal 'ok'
        res.status.should.equal 200
        done()
        return

    it 'DELETE /profile with provided _id and token of a user not authorized to delete', (done) ->
      request(url).delete('/profile/'+userId).set('authorization', 'Bearer ' + token).send().end (err, res) ->
        if err
          throw err
        res.status.should.equal 403
        done()
        return

    it 'GET /profile with provided _id after delete (expecting 404)', (done) ->
      request(url).get('/profile/'+profileId).set('authorization', 'Bearer ' + adminToken).end (err, res) ->
        if err
          throw err
        res.status.should.equal 404
        done()
        return

  describe 'Get profile and campaign data', ->

    it 'GET /profile with provided _id and provided campaign', (done) ->
      request(url).get('/profile/'+firstProfileId + "/" + campaignId).set('authorization', 'Bearer ' + adminToken).end (err, res) ->
        if err
          throw err
        res.status.should.equal 200
        done()
        return

    it 'GET /profile user info', (done) ->
      request(url).get('/profile/info').set('authorization', 'Bearer ' + adminToken).end (err, res) ->
        if err
          throw err
        res.status.should.equal 200
        done()
        return

    return


  return