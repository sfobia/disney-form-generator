ProfileGet = require('../../dist/modules/profile/model').dao
UserProfileGet    = require '../../dist/modules/user/user'
MomentGet = require 'moment'
request = require('supertest')
Mock = require '../mock'

version = Mock.config.version

describe 'ProfileGet', ->
  url = 'http://localhost:8000/' + version
  token = ''
  owner = ''
  profileId = ''
  adminToken = ''

  before (done)->
    ProfileGet.remove {}, (err, res) ->
      UserProfileGet.dao.remove {}, (err, res) ->
        user = new UserProfileGet.dao({email:'test@softfobia.com', password:'qwerty'})
        user.save (err, res) ->
          owner = res._id
          token = res.token
          profile = Mock.profile owner
          profile.save (err, res) ->
            admin = Mock.adminProfile('profilino')
            admin.save (err, res) ->
              adminToken = res.token
              done()

  describe 'Queries', ->
    it 'POST /profile with existing user', (done) ->
      data =
        'user': owner
      request(url).post('/profile/query').set('authorization', 'Bearer ' + adminToken).expect(200).send(data).end (err, res) ->
        if err
          throw err

        res.body[0].should.have.property('user')

        done()
        return
      return

    it 'POST /profile with not existing user', (done) ->
      data =
        'user': '1231231'
      request(url).post('/profile/query').set('authorization', 'Bearer ' + adminToken).expect(404).send(data).end (err, res) ->
        if err
          throw err

        done()
        return
      return

    it 'POST /profile with given first name', (done) ->
      data =
        'name.first': 'Mario'
      request(url).post('/profile/query').set('authorization', 'Bearer ' + adminToken).expect(200).send(data).end (err, res) ->
        if err
          throw err

        res.body[0].should.have.property('name')
        res.body[0].name.first.should.equal 'Mario'

        done()
        return
      return

    it 'POST /profile with given last name', (done) ->
      data =
        'name.last': 'Rossi'
      request(url).post('/profile/query').set('authorization', 'Bearer ' + adminToken).expect(200).send(data).end (err, res) ->
        if err
          throw err
        res.body[0].should.have.property('name')
        res.body[0].name.last.should.equal 'Rossi'
        done()
        return
      return

    it 'POST /profile search with given last name', (done) ->
      data =
        'name.last': 'ossi'
      request(url).post('/profile/search').set('authorization', 'Bearer ' + adminToken).expect(200).send(data).end (err, res) ->
        if err
          throw err

#        console.log res.body[0]

        res.body[0].should.have.property('name')
        res.body[0].name.last.should.equal 'Rossi'
        done()
        return
      return

    it 'POST /profile with given gender', (done) ->
      data =
        gender: 'Male'
      request(url).post('/profile/query').set('authorization', 'Bearer ' + adminToken).expect(200).send(data).end (err, res) ->
        if err
          throw err
        res.body[0].should.have.property('gender')
        res.body[0].gender.should.equal 'Male'
        done()
        return
      return

    it 'POST /profile with given birth date', (done) ->
      data =
        birthDate: new MomentGet('1984-12-23').toISOString()
      request(url).post('/profile/query').set('authorization', 'Bearer ' + adminToken).expect(200).send(data).end (err, res) ->
        if err
          throw err
        #TODO Controllo su data
        res.body[0].should.have.property('birthDate')

        done()
        return
      return

    it 'POST /profile with given accepted marketing (false)', (done) ->
      data =
        'marketing': false
      request(url).post('/profile/query/accepted').set('authorization', 'Bearer ' + adminToken).expect(200).send(data).end (err, res) ->
        if err
          throw err

        res.body.should.have.length 1

        done()
        return
      return

    it 'POST /profile with given accepted marketing (true)', (done) ->
      data =
        'marketing': true
      request(url).post('/profile/query/accepted').set('authorization', 'Bearer ' + adminToken).expect(200).send(data).end (err, res) ->
        if err
          throw err
        res.body.should.have.length 1

        done()
        return
      return

    it 'POST /profile with given accepted dem (true)', (done) ->
      data =
        'dem': true
      request(url).post('/profile/query/accepted').set('authorization', 'Bearer ' + adminToken).expect(200).send(data).end (err, res) ->
        if err
          throw err
        res.body.should.have.length 0

        done()
        return
      return

    it 'POST /profile with given accepted dem (false)', (done) ->
      data =
        'dem': false
      request(url).post('/profile/query/accepted').set('authorization', 'Bearer ' + adminToken).expect(200).send(data).end (err, res) ->
        if err
          throw err
        res.body.should.have.length 0

        done()
        return
      return

    it 'POST /profile with not existing birth date', (done) ->
      data =
        birthDate: new MomentGet('1998-12-23').toISOString()
      request(url).post('/profile/query').set('authorization', 'Bearer ' + adminToken).expect(200).send(data).end (err, res) ->
        if err
          throw err

        res.body.should.have.length 0
        done()
        return
      return

    it 'POST /profile with not existing key', (done) ->
      data =
        'name.wrongKey': new MomentGet('1998-12-23').toISOString()
      request(url).post('/profile/query').set('authorization', 'Bearer ' + adminToken).expect(400).send(data).end (err, res) ->
        if err
          throw err

        done()
        return
      return

  return