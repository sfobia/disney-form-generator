Profile = require('../../dist/modules/profile/model').dao
UserProfile = require '../../dist/modules/user/user'
Candidate = require('../../dist/modules/candidate/model').dao
Survey = require('../../dist/modules/survey/model').dao
Moment = require 'moment'
request = require('supertest')
Mock = require '../mock'

version = Mock.config.version

describe 'Profile details for campaigns and surveys', ->
  url = 'http://localhost:8100/' + version
  token = ''
  owner = ''
  profileId = ''
  campaignId = ''

  before (done)->
    Profile.remove {}, (err, res) ->
      UserProfile.dao.remove {}, (err, res) ->
        user = new UserProfile.dao({email:'test@softfobia.com', password:'qwerty', userName: 'uesksd', confirmed: true})
        user.save (err, res) ->
          owner = res._id
          token = res.token
          profile = Mock.profile owner
          profile.save (err, res) ->
            form = Mock.form()
            form.save (err, res) ->
              formId = res._id
              campaign = Mock.campaign(formId)
              campaign.save (err,res) ->
                campaignId = res._id
                candidate = new Candidate(
                  user: owner
                  campaign: campaignId
                  active: false
                  rejected: false
                )
                candidate.save (err, res) ->
                  done()

  describe 'Queries', ->
    it 'GET /profile/campaigns of existing user', (done) ->
      request(url).get('/profile/campaigns').set('authorization', 'Bearer ' + token).end (err, res) ->
        if err
          throw err
        res.body[0].campaign.should.have.property('title')
        res.body[0].campaign.should.have.property('type')
        campaignId = res.body[0]._id
        res.status.should.equal 200
        done()
        return
      return
  return