Profile = require('../../dist/modules/profile/model').dao
UserProfile    = require '../../dist/modules/user/user'
Candidate = require('../../dist/modules/candidate/model').dao
Campaign = require('../../dist/modules/campaign/model').dao
Moment = require 'moment'
request = require('supertest')
Mock = require '../mock'

version = Mock.config.version

describe 'Profile public', ->
  url = 'http://localhost:8100/' + version
  owner = ''
  ownerTwo = ''
  token = ''
  tokenTwo = ''
  formId = ''
  campaignId = ''
  formCampaignId = ''
  campaignTwoId = ''
  campaignThreeId = ''
  campaignFourId = ''
  email= ''
  before (done)->
    Campaign.remove {}, (errCamp, resCamp) ->
      Profile.remove {}, (err, res) ->
        UserProfile.dao.remove {}, (err, res) ->
          email = 'publicprofiletest2@softfobia.com'
          user = new UserProfile.dao({email:'publicprofiletest2@softfobia.com', password:'qwerty','userName':'andreazucca', scope: 'user', confirmed: true })
          user.save (err, res) ->
            owner = res._id
            token = res.token
            profile = Mock.profile(owner)
            profile.save (err, res) ->
              form = Mock.form()
              form.save (err, res) ->
                formId = res._id
                campaign = Mock.campaign(formId,'new')
                campaign.save (err, resC) ->
                  campaignId = resC._id
                  formCampaign = Mock.formTwo()
                  formCampaign.save (err, resD) ->
                    formCampaignId = resD._id
                    campaignTwo = Mock.campaignTag(formId)
                    campaignTwo.save (err, resE) ->
                      campaignTwoId = resE._id
                      campaignThree = Mock.campaignThree(formId, 'new')
                      campaignThree.save (err, resF) ->
                        campaignThreeId = resF._id
                        campaignFour = Mock.campaignFour(formId, 'neww')
                        campaignFour.save (err, resG) ->
                          campaignFourId = resG._id
                          user = new UserProfile.dao({email:'publicprofiletest3@softfobia.com', password:'qwerty','userName':'andreazucca2', scope: 'user', confirmed: true })
                          user.save (err, res) ->
                            ownerTwo = res._id
                            tokenTwo = res.token
                            profile = Mock.profile(ownerTwo)
                            profile.save (err, res) ->
                              done()

  describe 'Profile public operations', ->
    it 'GET /profile my profile', (done) ->
      request(url).get('/profile/me').set('authorization', 'Bearer ' + token).expect(200).end (err, res) ->
        if err
          throw err
        done()
        return

    it 'GET /profile/form (test esclusione valid dai values)', (done) ->
      form = Mock.form()
      form.save (err, res) ->
        formId = res._id
        request(url).get( '/profile/form/' + formId  ).set('authorization', 'Bearer ' + token).expect(200).end (err, res) ->
          if err
            throw err

          done()
          return

    it 'POST /candidate candidate for a campaign (rejected because not posted a registration form)', (done) ->
      data =
        campaign: campaignId
      request(url).post('/profile/candidate').set('authorization', 'Bearer ' + token).expect(403).send(data).end (err, res) ->
        if err
          throw err
        done()
        return

    formId = ''
    resFormId = ''

    it 'POST /profile/form for submitting a form ( only formId  ) ', (done) ->
      form = Mock.formRegistration()
      form.save (err, res) ->
        resFormId = res._id
        data =
          ref: resFormId
          name: 'form registration b'
          values: [
            name: 'input string'
            value: 'my user input response'
            type: 'input'
          ]
          formType: 'registration'
        request(url).post( '/profile/form/' + resFormId  ).set('authorization', 'Bearer ' + token).expect(200).send(data).end (err, res) ->
          if err
            throw err

          done()
          return

    it 'POST /profile/form for submitting a form ( formId + campaignId ) [campaign candidation]', (done) ->
      form = Mock.form()
      form.save (err, res) ->
        formId = res._id
        data =
          ref: formId
          campaign: campaignId
          name: 'form campaign'
          values: [
            name: 'input string'
            value: 'my user input response'
            type: 'input'
          ]
          formType: 'campaign'
          prevalid: true
        request(url).post( '/profile/form/' + formId + '/' + campaignId ).set('authorization', 'Bearer ' + token).expect(200).send(data).end (err, res) ->
          if err
            throw err
          done()
          return

    it 'POST /profile/form for submitting a form ( formId + campaignId ) [campaign candidation 2]', (done) ->
      form = Mock.form()
      form.save (err, res) ->
        formId = res._id
        data =
          ref: formId
          campaign: campaignTwoId
          name: 'form campaign'
          values: [
            name: 'input string'
            value: 'my user input response'
            type: 'input'
          ]
          formType: 'campaign'
          prevalid: true
        request(url).post( '/profile/form/' + formId + '/' + campaignId ).set('authorization', 'Bearer ' + token).expect(200).send(data).end (err, res) ->
          if err
            throw err

          done()
          return

    it 'POST /candidate candidate for a campaign', (done) ->
      data =
        campaign: campaignId
      request(url).post('/profile/candidate').set('authorization', 'Bearer ' + token).expect(200).send(data).end (err, res) ->
        if err
          throw err
        res.body.active.should.equal false
        res.body.rejected.should.equal false
        done()
        return

    it 'POST /candidate candidate for a campaign of the same week', (done) ->
      data =
        campaign: campaignTwoId
      request(url).post('/profile/candidate').set('authorization', 'Bearer ' + token).expect(200).send(data).end (err, res) ->
        if err
          throw err
        res.body.active.should.equal false
        res.body.rejected.should.equal false
        done()
        return

    it 'POST /profile/form for submitting a form ( formId + campaignId ) [campaign candidation 3]', (done) ->
      form = Mock.form()
      form.save (err, res) ->
        formId = res._id
        data =
          ref: formId
          campaign: campaignThreeId
          name: 'form campaign'
          values: [
            name: 'input string'
            value: 'my user input response'
            type: 'input'
          ]
          formType: 'campaign'
          prevalid: true
        request(url).post( '/profile/form/' + formId + '/' + campaignThreeId ).set('authorization', 'Bearer ' + token).expect(200).send(data).end (err, res) ->
          if err
            throw err

          done()
          return

    it 'POST /profile/form for submitting a form ( formId + campaignId ) [campaign fail survey out of dates 2]', (done) ->
      formSurvey = Mock.formSurvey()
      formSurvey.save (err, res) ->
        formSurveyId = res._id
        data =
          ref: formSurveyId
          campaign: campaignFourId
          name: 'survey 1'
          values: [
            {
              name: 'form type'
              type: 'input'
              order: 1
              value: [
                name: 'input name'
              ]
            }
            {
              name: 'form type 2'
              type: 'input'
              order: 1
              value: [
                name: 'input name 2'
              ]
            }
          ]
          formType: 'survey'
        request(url).post( '/profile/form/' + formSurveyId + '/' + campaignFourId ).set('authorization', 'Bearer ' + token).expect(400).send(data).end (err, res) ->
          if err
            throw err

          done()
          return

    it 'POST /candidate candidate for a third campaign', (done) ->
      data =
        campaign: campaignThreeId
      request(url).post('/profile/candidate').set('authorization', 'Bearer ' + token).expect(200).send(data).end (err, res) ->
        if err
          throw err
        res.body.active.should.equal false
        res.body.rejected.should.equal false
        done()
        return

    it 'POST /profile/form for submitting a form ( formId + campaignId ) [campaign candidation 4]', (done) ->
      form = Mock.form()
      form.save (err, res) ->
        formId = res._id
        data =
          ref: formId
          campaign: campaignFourId
          name: 'form campaign'
          values: [
            name: 'input string'
            value: 'my user input response'
            type: 'input'
          ]
          formType: 'campaign'
          prevalid: true
        request(url).post( '/profile/form/' + formId + '/' + campaignFourId ).set('authorization', 'Bearer ' + token).expect(200).send(data).end (err, res) ->
          if err
            throw err

          done()
          return

    it 'POST /profile/form for submitting a form survey [survey campaign 1]', (done) ->
      form = Mock.formSurvey()
      form.save (err, res) ->
        formId = res._id
        data =
          ref: formId
          campaign: campaignId
          name: 'survey 1'
          values: [
            {
              name: 'form type'
              type: 'input'
              order: 1
              value: [
                name: 'input name'
              ]
            }
            {
              name: 'form type 2'
              type: 'input'
              order: 1
              value: [
                name: 'input name 2'
              ]
            }
          ]
          formType: 'survey'
        request(url).post( '/profile/form/' + formId + '/' + campaignId ).set('authorization', 'Bearer ' + token).expect(200).send(data).end (err, res) ->
          if err
            throw err

          done()
          return

    it 'POST /candidate candidate for a fourth campaign', (done) ->
      data =
        campaign: campaignFourId
      request(url).post('/profile/candidate').set('authorization', 'Bearer ' + token).expect(200).send(data).end (err, res) ->
        if err
          throw err
        res.body.active.should.equal false
        res.body.rejected.should.equal false
        done()
        return


    it 'PUT /profile/me update my profile con dati errati', (done) ->
      data =
        user:
          email: 'infounclick.it'
          userName: 'zucska'
        profile:
          name:
            first: 'Andrea'
            last: 'Zucca'
          gender: 'Male'
          birthDate: new Moment('2015-12-23')
          province: 'CA'
      request(url).put('/profile/me').set('authorization', 'Bearer ' + token).send(data).expect(400).end (err, res) ->
        if err
          throw err
        done()
        return

    it 'PUT /profile/me update my profile', (done) ->
      data =
        user:
          email: email
          userName: 'zucska'
        profile:
          name:
            first: 'AndreaZ'
            last: 'Zucca'
          gender: 'Male'
          birthDate: new Moment('2015-12-23')
          province: 'CA'
      request(url).put('/profile/me').set('authorization', 'Bearer ' + token).send(data).expect(200).end (err, res) ->
        if err
          throw err
        done()
        return

    it 'PUT /profile/me update my profile con cambio password', (done) ->
      data =
        user:
          email: email
          userName: 'zucska'
          password: 'ciaociao'
      request(url).put('/profile/me').set('authorization', 'Bearer ' + token).send(data).expect(200).end (err, res) ->
        if err
          throw err
        done()
        return

    it 'PUT /profile/me update my profile con cambio email, ma non posso accedere, perchè ho cambiato password', (done) ->
      data =
        user:
          email: 'info@unclick.it'
          userName: 'zucska'
      request(url).put('/profile/me').set('authorization', 'Bearer ' + token).send(data).expect(401).end (err, res) ->
        if err
          throw err
        done()
        return
      return

    # rifaccio un nuovo login perchè ho cambiato la password
    it 'POST /account/login with email/password con nuova password', (done) ->
      data =
        email: email
        password: 'ciaociao'
      request('http://localhost:8000').post('/account/login').send(data).end (err, res) ->
        if err
          throw err
        token = res.body.token
        res.status.should.equal 200
        res.body.should.have.property('token')
        done()
        return


    it 'PUT /profile/me update my profile con cambio email', (done) ->
      data =
        user:
          email: 'info@unclick.it'
          userName: 'zucska'
      request(url).put('/profile/me').set('authorization', 'Bearer ' + token).send(data).expect(200).end (err, res) ->
        if err
          throw err
        res.body.should.have.property('email')
        res.body.email.should.equal true
        done()
        return
      return

  describe 'Test candidations of the same week, first try gone not preconfirmed, second gone preconfirmed', ->

    it 'POST /profile/form for submitting a form ( formId + campaignId ) [campaign candidation]', (done) ->
      form = Mock.formCheckbox()
      form.save (err, res) ->
        formId = res._id
        data =
          ref: formId
          campaign: campaignId
          name: 'form title 2'
          values: [
            {
              name: 'form type'
              ref: '1455796853553vM7JK8WhAVaL5EiS8HmsNUffyPbHy028'
              refvalue: 'ccc'
              type: 'radio'
              value: 'seconda radio'
            }
          ]
          formType: 'campaign'
        request(url).post( '/profile/form/' + formId + '/' + campaignId ).set('authorization', 'Bearer ' + tokenTwo).expect(200).send(data).end (err, res) ->
          if err
            throw err
          done()
          return

    it 'POST /profile/form for submitting a form ( formId + campaignId ) [campaign candidation 2]', (done) ->
      form = Mock.formCheckbox()
      form.save (err, res) ->
        formId = res._id
        data =
          ref: formId
          campaign: campaignTwoId
          name: 'form title 2'
          values: [
            {
              name: 'form type'
              ref: '1455796853553vM7JK8WhAVaL5EiS8HmsNUffyPbHy028'
              refvalue: 'aaa'
              type: 'radio'
              value: 'prima radio'
            }
          ]
          formType: 'campaign'
        request(url).post( '/profile/form/' + formId + '/' + campaignTwoId ).set('authorization', 'Bearer ' + tokenTwo).expect(200).send(data).end (err, res) ->
          if err
            throw err

          done()
          return

    it 'POST /profile/form for submitting a form ( only formId  ) ', (done) ->
      form = Mock.formRegistration()
      form.save (err, res) ->
        resFormId = res._id
        data =
          ref: resFormId
          name: 'form registration b'
          values: [
            name: 'input string'
            value: 'my user input response'
            type: 'input'
          ]
          formType: 'registration'
        request(url).post( '/profile/form/' + resFormId  ).set('authorization', 'Bearer ' + tokenTwo).expect(200).send(data).end (err, res) ->
          if err
            throw err

          done()
          return

    it 'POST /candidate candidate for a campaign', (done) ->
      data =
        campaign: campaignId
      request(url).post('/profile/candidate').set('authorization', 'Bearer ' + tokenTwo).expect(200).send(data).end (err, res) ->
        if err
          throw err
        res.body.active.should.equal false
        res.body.rejected.should.equal false
        res.body.preconfirmed.should.equal false
        res.body.requisite.should.equal false
        done()
        return

    it 'POST /candidate candidate for a campaign of the same week', (done) ->
      data =
        campaign: campaignTwoId
      request(url).post('/profile/candidate').set('authorization', 'Bearer ' + tokenTwo).expect(200).send(data).end (err, res) ->

        if err
          throw err

        res.body.active.should.equal false
        res.body.rejected.should.equal false
        res.body.preconfirmed.should.equal true
        res.body.requisite.should.equal true
        done()
        return





    return