User    = require('../../dist/modules/user/user').dao
Report  = require('../../dist/modules/report/model').dao
Candidate  = require('../../dist/modules/candidate/model').dao
Profile  = require('../../dist/modules/profile/model').dao
request = require 'supertest'
Mock    = require '../mock'
fs      = require 'fs'

version = Mock.config.version

describe 'Report', ->
  url = 'http://localhost:8000/' + version
  adminToken = ''
  userToken = ''

  before (done)->
    admin = Mock.adminProfile('formino')
    admin.save (err, res) ->
      adminToken = res.token
      userOne = Mock.userProfile()
      userOne.save (err, res) ->
        userToken = res.token
        profileOne = Mock.profile(res._id)
        profileOne.save (err, res) ->
          userTwo = Mock.userProfile()
          userTwo.save (err, res) ->
            profileTwo = Mock.femaleProfile(res._id)
            profileTwo.save (err, res) ->
              Report.remove (err,res) ->
                done()

  describe 'Reports about User', ->

    it 'GET /report with count of Users ', (done) ->
      request(url).get('/report/getUsersCount').set('authorization', 'Bearer ' + adminToken).expect(200).end (err, res) ->
        if err
          throw err
        done()
        return

    it 'GET /report with count of active Users ', (done) ->
      request(url).get('/report/getActiveUsersCount').set('authorization', 'Bearer ' + adminToken).expect(200).end (err, res) ->
        if err
          throw err
        done()
        return

    it 'GET /report with count of Users that have completed their profile', (done) ->
      request(url).get('/report/getProfileCompletedCount').set('authorization', 'Bearer ' + adminToken).expect(200).end (err, res) ->
        if err
          throw err
        done()
        return

    it 'GET /report with counts of users that subscribed a newsletter', (done) ->
      request(url).get('/report/getNewsletterUsersCount').set('authorization', 'Bearer ' + adminToken).expect(200).end (err, res) ->
        if err
          throw err
        #res.body[0][0]._id.should.equal 'test'
        #res.body[1][0]._id.should.equal 'generica'
#        res.body[0][0].count.should.equal 2
#        res.body[1][0].count.should.equal 1
        done()
        return

    it 'POST /profile/form for submitting a shipping form (empty values) ', (done) ->
      form = Mock.formShipping()
      form.save (err, res) ->
        resFormId = res._id
        data =
          ref: resFormId
          name: 'shipping form'
          values: []
          formType: 'shipping'
        request(url).post( '/profile/form/' + resFormId  ).set('authorization', 'Bearer ' + userToken).expect(200).send(data).end (err, res) ->
          if err
            throw err
          done()
          return

    it 'GET /report list of users that have not compiled the shipping form', (done) ->
      request(url).get('/report/getUsersWithoutShippingForm').set('authorization', 'Bearer ' + adminToken).expect(200).end (err, res) ->
        if err
          throw err
        done()
        return

    it 'POST /profile/form for submitting a form (registration) ', (done) ->
      form = Mock.formRegistration()
      form.save (err, res) ->
        resFormId = res._id
        data =
          ref: resFormId
          name: 'form registration b'
          values: [
            name: 'input string'
            value: 'my user input response'
            type: 'input'
          ]
          formType: 'registration'
        request(url).post( '/profile/form/' + resFormId  ).set('authorization', 'Bearer ' + userToken).expect(200).send(data).end (err, res) ->
          if err
            throw err

          done()
          return

    it 'GET /report list of users that have never got an active status for their candidation on any Campaign', (done) ->
      promise = Mock.createCampaignAndCandidateNotActive()
      promise.then ->
        setTimeout( ->
          request(url).get('/report/getNeverActivedUsers').set('authorization', 'Bearer ' + adminToken).expect(200).end (err, res) ->
            if err
              throw err
            done()
          ,5000)
        return

#    it 'POST /report/query List of users that match the query on info on their profile', (done) ->
#      setTimeout( ->
#        data =
#          reportType: 'profileFemaleNewsletterGenericaAndAcceptedProfiling'
#          gender: 'Female'
#          newsletter:
#            generica: true
#          accepted:
#            profiling: true
#        request(url).post('/report/queryProfile').set('authorization', 'Bearer ' + adminToken).send(data).expect(200).end (err, res) ->
#          console.log res.body
#          if err
#            throw err
#          done()
#        ,6000)
#      return



###
    it 'GET /report/getUsersProfilation list of users profilation ', (done) ->
      request(url).get('/report/getUsersProfilation').set('authorization', 'Bearer ' + adminToken).expect(200).end (err, res) ->
        if err
          throw err
        res.body.should.have.property('file')
        csvFile = __dirname + '/../../public/' + res.body.file
        txt = fs.readFileSync csvFile, 'utf8'
        lines = txt.split("\n")
        # 1 user più riga degli header più riga riassunto legenda
        lines.length.should.equal 3

        done()
        return
###