Client = require('../../dist/modules/client/model').dao
request = require('supertest')
Mock = require '../mock'

version = Mock.config.version

describe 'Client', ->
  url = 'http://localhost:8000/' + version
  clientId = ''
  adminToken = ''
  adminId = ''

  before (done)->
    Client.remove {}, (err, res) ->
      admin = Mock.adminProfile('clientino')
      admin.save (err, res) ->
        adminToken = res.token
        adminId = res._id
        client = Mock.client()
        client.save (err, res) ->
          clientId = res._id
          done()


  describe 'CRU operations', ->
    it 'POST /client with valid required data', (done) ->
      data =
        name: 'test client'
        email: 'test@test.te'
      request(url).post('/client').set('authorization', 'Bearer ' + adminToken).expect(200).send(data).end (err, res) ->
        if err
          throw err
        res.body.name.should.equal data.name
        res.body.email.should.equal data.email

        done()
        return

    it 'POST /client with invalid required data', (done) ->
      data =
        name: ''
        email: ''
      request(url).post('/client').set('authorization', 'Bearer ' + adminToken).expect(400).send(data).end (err, res) ->
        if err
          throw err
        done()
        return

    it 'GET /client list', (done) ->
      request(url).get('/client').set('authorization', 'Bearer ' + adminToken).expect(200).end (err, res) ->
        if err
          throw err
        done()
        return

    it 'PUT /client with provided _id', (done) ->
      data =
        name: 'lello'
        email: 'lello@lello.net'
        phone:
          mobile: '1234567890'
      request(url).put('/client/' + clientId).set('authorization', 'Bearer ' + adminToken).expect(200).send(data).end (err, res) ->
        if err
          throw err
        done()
        return

  describe 'Queries', ->
    it 'POST /client with specified name', (done) ->
      data =
        name: 'lello'
      request(url).post('/client/query').set('authorization', 'Bearer ' + adminToken).expect(200).send(data).end (err, res) ->
        if err
          throw err
        res.body[0].should.have.property('name')
        done()
        return
      return

    it 'POST /client with specified mobile phone', (done) ->
      data =
        phone:
          mobile: '1234567890'
      request(url).post('/client/query').set('authorization', 'Bearer ' + adminToken).expect(200).send(data).end (err, res) ->
        if err
          throw err
        res.body[0].should.have.property('name')
        res.body[0].should.have.property('phone')
        done()
        return
      return


  describe 'Delete', ->
    it 'DELETE /client with provided _id', (done) ->
      request(url).delete('/client/'+clientId).set('authorization', 'Bearer ' + adminToken).expect(400).send().end (err, res) ->
        if err
          throw err
        done()
        return
      return

    it 'GET /client with provided _id after delete (expecting 404)', (done) ->
      request(url).get('/client/'+clientId).set('authorization', 'Bearer ' + adminToken).expect(200).end (err, res) ->
        if err
          throw err
        done()
        return

    return