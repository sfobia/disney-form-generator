#Survey = require('../../dist/modules/survey/model').dao
#CampaignSurvey = require('../../dist/modules/campaign/model').dao
#FormSurvey = require('../../dist/modules/form/model').dao
#
#request = require('supertest')
#Mock = require '../mock'
#
#version = Mock.config.version
#
#describe 'Survey', ->
#  url = 'http://localhost:8000/' + version
#  surveyId = ''
#  campaignId = ''
#  survey = ''
#  formId = ''
#  userId = ''
#  adminToken = ''
#
#  before (done)->
#    Survey.remove {}, (err, res) ->
#      CampaignSurvey.remove {}, (err, res) ->
#        FormSurvey.remove {}, (err, res) ->
#          form = new FormSurvey({
#            title: 'form title'
#            fields: [
#              {
#                name: 'form type'
#                type: 'input'
#                order: 1
#                value: [
#                  name: 'input name'
#                ]
#              }
#            ]
#            predefined: true
#          })
#          form.save (err, res) ->
#            formId = res._id
#            campaignSurvey = new CampaignSurvey({
#              title: 'Campagna Test per il survey'
#              type: 'test'
#              form: formId
#              link: 'http://campaigntest.com'
#              published: true
#              slug: 'test-campaignsurvey'
#            })
#            campaignSurvey.save (err, res) ->
#              campaignId = res._id
#              user = Mock.userProfile()
#              user.save (err, res) ->
#                userId = res._id
#                admin = Mock.adminProfile('aaa')
#                admin.save (err, res) ->
#                  adminToken = res.token
#                  done()
#
#  describe 'CRUD operations', ->
#    it 'POST /survey with valid required data', (done) ->
#      data = Mock.surveyData campaignId
#      request(url).post('/survey').set('authorization', 'Bearer ' + adminToken).expect(200).send(data).end (err, res) ->
#        if err
#          throw err
#        surveyId = res.body._id
#        done()
#        return
#
#    it 'POST /survey with empty required data', (done) ->
#      data =
#        title: ''
#        type: ''
#        status:
#          actual: true
#        questions: [
#          {
#            title: ''
#            type: ''
#            description: ''
#            answers: [
#              name: ''
#            ]
#          }
#        ]
#        campaign: campaignId
#        published: true
#      request(url).post('/survey').set('authorization', 'Bearer ' + adminToken).send(data).end (err, res) ->
#        if err
#          throw err
#        res.status.should.equal 400
#        done()
#        return
#
#    it 'POST /survey with invalid required data', (done) ->
#      data =
#        title: 'ble'
#        type: 'any'
#        status:
#          actual: ''
#        questions: [
#          {
#            title: ''
#            type: ''
#            description: ''
#            answers: [
#              name: ''
#            ]
#          }
#        ]
#        campaign: campaignId
#        published: true
#      request(url).post('/campaign').set('authorization', 'Bearer ' + adminToken).send(data).end (err, res) ->
#        if err
#          throw err
#        res.status.should.equal 400
#        done()
#        return
#
#    it 'GET /survey with provided _id', (done) ->
#      request(url).get('/survey/'+surveyId).set('authorization', 'Bearer ' + adminToken).expect(200).end (err, res) ->
#        if err
#          throw err
#        done()
#        return
#
#    it 'GET /survey list', (done) ->
#      request(url).get('/survey').set('authorization', 'Bearer ' + adminToken).expect(200).end (err, res) ->
#        if err
#          throw err
#        done()
#        return
#
#    it 'PUT /survey with provided _id', (done) ->
#      data =
#        title: 'Survey title modified'
#        type: 'test'
#        status:
#          actual: 'updated status'
#        questions: [
#          {
#            title: 'first question modified'
#            type: 'input'
#            description: 'question description'
#            answers: [
#              name: 'this is an answer'
#            ]
#          }
#        ]
#        campaign: campaignId
#        published: true
#      request(url).put('/survey/' + surveyId).set('authorization', 'Bearer ' + adminToken).expect(200).send(data).end (err, res) ->
#        if err
#          throw err
#        done()
#        return
#
#
#    return
#
#  describe 'Queries', ->
#    it 'POST /survey with specified title', (done) ->
#      data =
#        title: 'Survey title modified'
#      request(url).post('/survey/query').set('authorization', 'Bearer ' + adminToken).expect(200).send(data).end (err, res) ->
#        if err
#          throw err
#        res.body[0].should.have.property('title')
#        done()
#        return
#      return
#
#    return
#
#
#  describe 'Delete', ->
#    surveyId = ''
#    it 'DELETE /survey with provided _id', (done) ->
#      form = Mock.form();
#      form.save (err, res) ->
#        formId = res._id
#        campaign = Mock.campaign(formId)
#        campaign.save (err, res) ->
#          campaignId = res._id
#          survey = Mock.survey(campaignId)
#          survey.save (err, res) ->
#            surveyId = res._id
#            request(url).delete('/survey/'+surveyId).set('authorization', 'Bearer ' + adminToken).expect(200).send().end (err, res) ->
#              if err
#                throw err
#              done()
#              return
#            return
#
#    it 'GET /survey with provided _id after delete (expecting 404)', (done) ->
#      request(url).get('/survey/'+surveyId).set('authorization', 'Bearer ' + adminToken).expect(404).end (err, res) ->
#        if err
#          throw err
#        done()
#        return
#
#
#  return