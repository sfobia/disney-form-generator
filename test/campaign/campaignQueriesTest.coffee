Campaign = require('../../dist/modules/campaign/model').dao
Form = require('../../dist/modules/form/model').dao
Moment = require 'moment'
request = require('supertest')
Mock = require '../mock'

version = Mock.config.version

describe 'Campaign queries', ->
  url = 'http://localhost:8000/' + version
  campaignId = ''
  formId = ''
  adminToken = ''
  campaign = ''

  before (done)->
    Campaign.remove {}, (err, res) ->
      form = Mock.form()
      form.save (err, res) ->
        formId = res._id
        campaign = Mock.campaignTag(formId)
        campaign.save (err, res) ->
          adminUser = Mock.adminProfile('lello')
          adminUser.save (err, res) ->
            adminToken = res.token
            done()

  describe 'Campaigns by tags', ->
    it 'GET /campaign by tags', (done) ->
      request(url).get('/campaign/tags').set('authorization', 'Bearer ' + adminToken).expect(200).end (err, res) ->
        if err
          throw err
        res.body.length.should.equal = 1
        done()
        return