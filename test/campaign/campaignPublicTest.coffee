Campaign = require('../../dist/modules/campaign/model').dao
CampaignForm = require('../../dist/modules/form/model').dao
Moment = require 'moment'
request = require('supertest')
Mock = require '../mock'

version = Mock.config.version

describe 'Campaign', ->
  url = 'http://localhost:8100/' + version
  campaignId = ''
  formId = ''
  token = ''
  campaignUnpublishedId = ''

  before (done)->
    Campaign.remove {}, (err, res) ->
      form = new CampaignForm({
        title: 'form title'
        type: [
          {
            name: 'form type'
            type: 'input'
            order: 1
            value: [
              name: 'input name'
            ]
          }
        ]
        predefined: true
      })
      form.save (err, res) ->
        formId = res._id
        admin = Mock.adminProfile('spongebob')
        admin.save (err, res) ->
          adminToken = res.token
          campaign = Mock.campaignTag(formId)
          campaign.save (err, res) ->
            campaignId = res._id
            user = Mock.userProfile()
            user.save (err, res) ->
              token = res.token
              campaignUnpublished = Mock.campaignTagNotPublished(formId)
              campaignUnpublished.save (err, res) ->
                campaignUnpublishedId = res._id
                done()

  describe 'Public operations', ->
    it 'GET /campaign by tag', (done) ->
      request(url).get('/campaign/tags/'+encodeURI('Nature')).expect(200).end (err, res) ->
        console.log res
        if err
          throw err
        res.body.length.should.equal = 1
        res.body[0].tag[0].name.should.equal 'Nature'
        done()
        return

    it 'GET /campaign by inexistent tag', (done) ->
      request(url).get('/campaign/tags/'+encodeURI('Fish')).expect(200).end (err, res) ->
        if err
          throw err
        res.body.length.should.equal = 0
        done()
        return

    it 'GET /campaign ', (done) ->
      request(url).get('/campaign').expect(200).end (err, res) ->
        if err
          throw err
        res.body.length.should.equal = 1
        res.body[0].title.should.equal 'Campaign Test 2'
        res.body[0].type.should.equal 'test'
        res.body[0].form.should.equal formId.toString()
        res.body[0].link.should.equal 'http://campaigntest.com'
        res.body[0].published.should.equal true

        done()
        return

    it 'GET /campaign with provided _id', (done) ->
      request(url).get('/campaign/'+campaignId).set('authorization', 'Bearer ' + token).expect(200).send().end (err, res) ->
        if err
          throw err
        res.body._id.should.equal = campaignId
        done()
        return

    it 'GET /campaign with provided _id', (done) ->
      request(url).get('/campaign/'+campaignUnpublishedId).set('authorization', 'Bearer ' + token).expect(404).send().end (err, res) ->
        if err
          throw err
        done()
        return

    it 'GET /campaign with provided week number', (done) ->
      camp = new Campaign(
        title: 'Week Campaign Test'
        type: 'test'
        form: formId
        dates:
          startCampaign: new Moment("2016-01-08")
          endCampaign: new Moment("2015-10-23")
          startCandidations: new Moment()
          endCandidations: new Moment("2016-01-29")
        link: 'http://campaigntest.com'
        published: true
      )
      camp.save (err, res) ->
        request(url).get('/campaign/week').end (err, res) ->
          if err
            throw err
          res.status.should.equal 200
          done()
          return
      return
    return
  return