Campaign = require('../../dist/modules/campaign/model').dao
Candidate = require('../../dist/modules/candidate/model').dao
CampaignForm = require('../../dist/modules/form/model').dao
Client = require('../../dist/modules/client/model').dao
Moment = require 'moment'
request = require 'supertest'
Mock = require '../mock'
fs = require 'fs'
moment = require 'moment'


version = Mock.config.version

describe 'Campaign', ->
  url = 'http://localhost:8000/' + version
  campaignId = ''
  formId = ''
  adminToken = ''
  clientId = ''
  secondCampaignId = ''

  before (done)->
    Campaign.remove {}, (err, res) ->
      form = new CampaignForm({
        title: 'form title'
        type: [
          {
            name: 'form type'
            type: 'input'
            order: 1
            value: [
              name: 'input name'
            ]
          }
        ]
        predefined: true
      })
      form.save (err, res) ->
        formId = res._id
        admin = Mock.adminProfile('lallo')
        admin.save (err, res) ->
          adminToken = res.token
          client = Mock.client()
          client.save (err, res) ->
            clientId = res._id
            done()

  describe 'CRUD operations', ->
    it 'POST /campaign with valid required data', (done) ->
      category = Mock.category()
      category.save (err, cat) ->
        data =
          pieces: 10
          piecesInitial: 10
          title: 'Campagna Test'
          type: 'test'
          form: formId
          dates:
            startCampaign: new Moment("2016-01-08")
            endCampaign: new Moment("2015-10-23")
            startCandidations: new Moment("2016-01-12")
            endCandidations: new Moment("2016-01-29")
            releaseFeedback: new Moment("2016-01-30")
          published: true
          categories: [
            cat._id
          ]
          slug: 'test-campaign'
        request(url).post('/campaign').set('authorization', 'Bearer ' + adminToken).send(data).end (err, res) ->
          if err
            throw err
          res.status.should.equal 200
          campaignId = res.body._id

          res.body.title.should.equal 'Campagna Test'
          res.body.type.should.equal 'test'
          res.body.form.should.equal formId.toString()
          res.body.published.should.equal true

          res.body.dates.startCampaign.should.equal  new Moment("2016-01-08").toISOString()
          res.body.dates.endCampaign.should.equal  new Moment("2015-10-23").toISOString()
          res.body.dates.startCandidations.should.equal  new Moment("2016-01-12").toISOString()
          res.body.dates.endCandidations.should.equal  new Moment("2016-01-29").toISOString()
          res.body.dates.releaseFeedback.should.equal  new Moment("2016-01-30").toISOString()

          done()
          return

    it 'POST /campaign with slug not unique', (done) ->
      category = Mock.category()
      category.save (err, cat) ->
        data =
          pieces: 10
          title: 'Campagna Test'
          type: 'test'
          form: formId
          dates:
            startCampaign: new Moment("2016-01-08")
            endCampaign: new Moment("2015-10-23")
            startCandidations: new Moment("2016-01-12")
            endCandidations: new Moment("2016-01-29")
            releaseFeedback: new Moment("2016-01-30")
          published: true
          categories: [
            cat._id
          ]
          slug: 'test-campaign'
        request(url).post('/campaign').set('authorization', 'Bearer ' + adminToken).send(data).end (err, res) ->
          if err
            throw err
          res.status.should.equal 400
          done()
          return

    it 'POST /campaign new slug', (done) ->
      category = Mock.category()
      category.save (err, cat) ->
        data =
          pieces: 10
          piecesInitial: 10
          title: 'Campagna Test'
          type: 'test'
          dates:
            startCampaign: new Moment("2016-01-08")
            endCampaign: new Moment("2015-10-23")
            startCandidations: new Moment("2016-01-12")
            endCandidations: new Moment("2016-01-29")
            releaseFeedback: new Moment("2016-01-30")
          published: true
          categories: [
            cat._id
          ]
          slug: 'test-campaign-new'
        request(url).post('/campaign').set('authorization', 'Bearer ' + adminToken).send(data).end (err, res) ->
          secondCampaignId = res._id
          if err
            throw err
          res.status.should.equal 200
          done()
          return

    it 'POST /campaign new slug but with formId already used', (done) ->
      category = Mock.category()
      category.save (err, cat) ->
        data =
          pieces: 10
          title: 'Campagna Test'
          type: 'test'
          form: formId
          dates:
            startCampaign: new Moment("2016-01-08")
            endCampaign: new Moment("2015-10-23")
            startCandidations: new Moment("2016-01-12")
            endCandidations: new Moment("2016-01-29")
            releaseFeedback: new Moment("2016-01-30")
          published: true
          categories: [
            cat._id
          ]
          slug: 'test-campaign-new'
        request(url).post('/campaign').set('authorization', 'Bearer ' + adminToken).send(data).end (err, res) ->
          if err
            throw err
          res.status.should.equal 400
          done()
          return

    it 'POST /campaign with empty required data', (done) ->
      data =
        title: ''
        type: ''
        form: ''
        published: ''
      request(url).post('/campaign').set('authorization', 'Bearer ' + adminToken).expect(400).send(data).end (err, res) ->
        if err
          throw err
        done()
        return

    it 'POST /campaign with invalid required data', (done) ->
      data =
        title: ''
        type: 'anyTypeYouWant'
        form: formId
        published: true
      request(url).post('/campaign').set('authorization', 'Bearer ' + adminToken).expect(400).send(data).end (err, res) ->
        if err
          throw err
        done()
        return

    it 'POST /campaign with invalid type', (done) ->
      data =
        title: 'Campaign invalid type'
        type: 'anyTypeYouWant'
        form: formId
        published: true
      request(url).post('/campaign').set('authorization', 'Bearer ' + adminToken).expect(400).send(data).end (err, res) ->
        if err
          throw err
        done()
        return

    it 'GET /campaign ', (done) ->
      request(url).get('/campaign').set('authorization', 'Bearer ' + adminToken).expect(200).end (err, res) ->
        if err
          throw err
        res.body.length.should.equal = 1
        res.body[0].title.should.equal 'Campagna Test'
        res.body[0].type.should.equal 'test'
        res.body[0].form.should.equal formId.toString()
        res.body[0].published.should.equal true

        done()
        return

    it 'GET /campaign/query by payload parameters', (done) ->
      data =
        q:'ampagn'
      request(url).post('/campaign/query').set('authorization', 'Bearer ' + adminToken).send(data).expect(200).end (err, res) ->
        if err
          throw err

        res.body[0].title.should.equal 'Campagna Test'
        #        res.body.length.should.equal = 1
        done()
        return

    it 'GET /campaign with provided _id', (done) ->
      request(url).get('/campaign/'+campaignId).set('authorization', 'Bearer ' + adminToken).expect(200).send().end (err, res) ->
        if err
          throw err
        res.body._id.should.equal = campaignId
        done()
        return

    it 'PUT /campaign with provided _id', (done) ->
      category = Mock.category()
      category.save (err, cat) ->
        data =
          pieces: 90
          piecesInitial: 90
          title: 'Modified Campaign Test'
          type: 'test'
          form: formId
          dates:
            startCampaign: new Moment("2016-02-08")
            endCampaign: new Moment("2016-03-23")
            startCandidations: new Moment("2016-01-27")
            endCandidations: new Moment("2016-01-28")
            releaseFeedback: new Moment("2016-03-29")
          published: true
          categories: [
            cat._id
          ]
          slug: 'test-campaign'
          client: clientId
        request(url).put('/campaign/'+campaignId).set('authorization', 'Bearer ' + adminToken).expect(200).send(data).end (err, res) ->
          if err
            throw err

          isoString = new Moment("2016-01-08").toISOString()
          res.body._id.should.equals = campaignId
          res.body.title.should.equal 'Modified Campaign Test'
          res.body.type.should.equal 'test'
          res.body.form.should.equal formId.toString()
          res.body.published.should.equal true

          res.body.dates.startCampaign.should.equal  new Moment("2016-02-08").toISOString()
          res.body.dates.endCampaign.should.equal  new Moment("2016-03-23").toISOString()
          res.body.dates.startCandidations.should.equal  new Moment("2016-01-27").toISOString()
          res.body.dates.endCandidations.should.equal  new Moment("2016-01-28").toISOString()
          res.body.dates.releaseFeedback.should.equal  new Moment("2016-03-29").toISOString()

          done()
          return

    it 'PUT /campaign change not unique slug', (done) ->
      category = Mock.category()
      category.save (err, cat) ->
        data =
          pieces: 90
          title: 'Modified Campaign Test'
          type: 'test'
          form: formId
          dates:
            startCampaign: new Moment("2016-02-08")
            endCampaign: new Moment("2016-03-23")
            startCandidations: new Moment("2016-01-27")
            endCandidations: new Moment("2016-01-28")
            releaseFeedback: new Moment("2016-03-29")
          published: true
          categories: [
            cat._id
          ]
          slug: 'test-campaign-new'
          client: clientId
        request(url).put('/campaign/'+campaignId).set('authorization', 'Bearer ' + adminToken).expect(400).send(data).end (err, res) ->
          if err
            throw err
          done()
          return

    it 'PUT /campaign change with same form of another campaign', (done) ->
      category = Mock.category()
      category.save (err, cat) ->
        data =
          pieces: 90
          title: 'Modified Second Campaign Test'
          type: 'test'
          form: formId
          dates:
            startCampaign: new Moment("2016-02-08")
            endCampaign: new Moment("2016-03-23")
            startCandidations: new Moment("2016-01-27")
            endCandidations: new Moment("2016-01-28")
            releaseFeedback: new Moment("2016-03-29")
          published: true
          categories: [
            cat._id
          ]
          client: clientId
        request(url).put('/campaign/'+secondCampaignId).set('authorization', 'Bearer ' + adminToken).expect(400).send(data).end (err, res) ->
          if err
            throw err
          done()
          return


    it 'GET /campaign ', (done) ->
      campaignTwo = Mock.campaign(formId)
      campaignTwo.save (err, res) ->
        campaignThree = Mock.campaignTag(formId)
        campaignThree.save (err, res) ->
          request(url).get('/campaign').set('authorization', 'Bearer ' + adminToken).end (err, res) ->
            if err
              throw err
            res.status.should.equal 200
            done()
            return

    it 'GET /campaign with provided _id', (done) ->
      request(url).get('/campaign/'+campaignId).set('authorization', 'Bearer ' + adminToken).send().end (err, res) ->
        if err
          throw err
        res.status.should.equal 200
        done()
        return

    it 'DELETE /campaign with provided _id', (done) ->
      request(url).delete('/campaign/'+campaignId).set('authorization', 'Bearer ' + adminToken).expect(400).send().end (err, res) ->
        if err
          throw err
        done()
        return

    it 'GET /campaign with provided _id after delete (expecting 404)', (done) ->
      request(url).get('/campaign/'+campaignId).set('authorization', 'Bearer ' + adminToken).expect(200).end (err, res) ->
        if err
          throw err
        done()
        return

    it 'GET /campaign/info', (done) ->
      request(url).get('/campaign/info').set('authorization', 'Bearer ' + adminToken).expect(200).end (err, res) ->
        if err
          throw err
        done()
        return
###
    it 'GET /campaign/responses/{id} responses of users to campaign form ', (done) ->
      user = Mock.userProfile()
      user.save (err, res) ->
        userId = res._id
        profile = Mock.profile(userId)
        profile.form = [{
          formType: 'campaign'
          name: 'test'
          ref: formId
          campaign: campaignId
          values: [
            {
              name: 'nome della domanda'
              type: 'input'
              value: "yes"
              refvalue: "xxxx"
              ref: "xxxxyyyy"
            }
          ]
          prevalid: true
        }]
        profile.save (err, res) ->
          request(url).get('/campaign/responses/'+campaignId).set('authorization', 'Bearer ' + adminToken).expect(200).end (err, res) ->
            if err
              throw err



            done()
            return


  return
    return
###
