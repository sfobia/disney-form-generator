UserRegister  = require '../../dist/modules/user/user'
Profile       = require '../../dist/modules/profile/model'
Notify        = require '../../dist/modules/notify/model'
request       = require('supertest')
Moment        = require 'moment'
Mock = require '../mock'

describe 'User Registration', ->
  url = 'http://localhost:8000'
  token = ''
  email = Date.now() + 'test@softfobia.com'
  userId = ''
  tokenConfirm = ''
  before (done) ->
    UserRegister.dao.remove {}, (err, res) ->
      Notify.dao.remove {}, (err, res) ->
        done()

  describe 'Registration', ->
    it 'POST /account/register trying to create a new user account', (done) ->
      user =
        email: email
        password: 'qwerty'
        userName: 'tstUser'

      profile =
        name:
          first: "Jon"
          last: "Snow"
        gender: 'Male'
        birthDate: new Moment('2015-12-23')
        province: "CA"
        accepted:
          marketing: true
          dem: true
          profiling: true

      payload =
        user: user
        profile: profile

      request(url).post('/account/register').send( payload ).end (err, res) ->
        if err
          throw err
#        token = res.body.token
#        email = profile.email
        res.status.should.equal 200
#        res.body. tokenLink
        done()
        return
      return

    it 'Verifico che sia abbinato user/profile _id', (done) ->
      UserRegister.dao.findOne({}).exec (err,user) ->
        user = user.toObject()
        Profile.dao.findOne({ user: user._id}).exec (err,profilo) ->
          profilo = profilo.toObject()
          user.profile.toString().should.equal profilo._id.toString()
          profilo.user.toString().should.equal user._id.toString()
          done()
        return
      return

    it 'POST /account/register trying to create a new user account with wrong data', (done) ->
      profile =
        email: 'testt.it'
        password: 'qwerty'
      request(url).post('/account/register').send(profile).end (err, res) ->
        if err
          throw err
        res.status.should.equal 400
        done()
        return
      return

    it 'POST /account/register trying to create a new user account with empty email and password', (done) ->
      profile =
        email: ''
        password: ''
      request(url).post('/account/register').send(profile).end (err, res) ->
        if err
          throw err
        res.status.should.equal 400
        done()
        return
      return

    it 'POST /account/register trying to create a new user account with empty password', (done) ->
      profile =
        email: 'test@softfobia.com'
        password: ''
      request(url).post('/account/register').send(profile).end (err, res) ->
        if err
          throw err
        res.status.should.equal 400
        done()
        return
      return

    it 'POST /account/register trying to create a new user account with empty email', (done) ->
      profile =
        email: ''
        password: 'qwerty'
      request(url).post('/account/register').send(profile).end (err, res) ->
        if err
          throw err
        res.status.should.equal 400
        done()
        return
      return

    it 'POST /account/register trying registration with duplicate email', (done) ->
      profile =
        email: email
        password: 'qwerty2'
      request(url).post('/account/register').send(profile).end (err, res) ->
        if err
          throw err
        res.status.should.equal 400
        done()
        return
      return

  describe "Confirm user", ->
    it 'GET /account/activate/token confermo utente dopo la registrazione', (done) ->
      UserRegister.dao.findOne({}).exec (err,user) ->
        userId = user._id
        tokenConfirm = user.tokenConfirm
        request(url).get('/account/activate/'+user.tokenConfirm).send().end (err, res) ->
          if err
            throw err
          res.headers.location.should.containEql user.token
          res.status.should.equal 302
          done()
          return
        return
      return

    it 'DB controllo se esiste la mail di notifica conferma mail', (done) ->
      setTimeout( ->
        Notify.dao.findOne({user:userId,subject:'Registration'}).exec (err,items) ->
          items.subject.should.equal 'Registration'
          items.body.should.containEql 'Jon'
          done()
      ,300)
      return

    it 'DB controllo se esiste la mail di notifica conferma registrazione', (done) ->
      Notify.dao.findOne({user:userId,subject:'Confirm email'}).exec (err,items) ->
        items.subject.should.equal 'Confirm email'
        items.body.should.containEql 'Jon'
        items.body.should.containEql tokenConfirm
        done()
      return

    it 'POST /account/forgot forgot user password', (done) ->
      profile =
        email: email
      request(url).post('/account/forgot').send(profile).end (err, res) ->
        if err
          throw err
        res.status.should.equal 200
        done()
        return
      return

    it 'POST /account/changepassword change user password after requesting a reset', (done) ->
      UserRegister.dao.findOne({}).exec (err,user) ->
        tokenConfirm = user.tokenConfirm
        userId = user._id
        password = {'password': 'pippofranco'}
        request(url).post('/account/changepassword/' + user.tokenConfirm).send(password).end (err, res) ->
          if err
            throw err
          res.status.should.equal 200
          done()
          return
      return

    it 'GET /profile/me/notify controllo se esiste la mail di notifica forgot password', (done) ->
      Notify.dao.findOne({user:userId, subject: 'Password dimenticata'}).exec (err,items) ->
        items.subject.should.equal 'Password dimenticata'
        items.body.should.containEql 'modifica-password.html?tk'
        items.body.should.containEql tokenConfirm
        done()
      return

    it 'POST /account/login with email/password after change', (done) ->
      UserRegister.dao.find({}).exec (err,items) ->
      data =
        email: email
        password: 'pippofranco'
      request(url).post('/account/login').send(data).end (err, res) ->
        if err
          throw err
        token = res.body.token
        res.status.should.equal 200
        res.body.should.have.property('token')
        done()
      return


    it 'DELETE /account with provided _id', (done) ->
      adminToken = ''
      admin = Mock.adminProfile('adminUser')
      admin.save (err, res) ->
        adminToken = res.token

        Profile.dao.findOne({user: userId}).exec (err, res) ->
          UserRegister.dao.update({_id: userId}, {$set: profile: res._id}).exec (err, res) ->
            request(url).delete('/V1/profile/'+userId).set('authorization', 'Bearer ' + adminToken).expect(200).send().end (err, res) ->
              if err
                throw err
              done()
              return