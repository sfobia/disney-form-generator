UserLogin    = require '../../dist/modules/user/user'
request = require('supertest')

describe 'User Login', ->
  url = 'http://localhost:8000';
  token = ''
  email = ''


  before (done)->
    UserLogin.dao.remove {}, (err,res)->
      user = new UserLogin.dao({email:'test@softfobia.com', password:'qwerty',confirmed:true})
      user.save (err, res) ->
        done()

  describe 'Login', ->
    it 'POST /account/login with email/password', (done) ->
      data =
        email: 'test@softfobia.com'
        password: 'qwerty'
      request(url).post('/account/login').send(data).end (err, res) ->
        if err
          throw err

        token = res.body.token
        res.status.should.equal 200
        res.body.should.have.property('token')
        done()
        return

    it 'POST /account/login with wrong email/password', (done) ->
      data =
        email: 'wrong@softfobia.co.uk'
        password: 'ytrewq'
      request(url).post('/account/login').send(data).end (err, res) ->
        if err
          throw err
        res.status.should.equal 400
        done()
        return

    it 'POST /account/login with wrong email', (done) ->
      data =
        email: 'andrea@zucca.it'
        password: 'qwe'
      request(url).post('/account/login').send(data).end (err, res) ->
        if err
          throw err
        res.status.should.equal 400
        done()
        return

    it 'POST /account/login with wrong password', (done) ->
      data =
        email: 'test@softfobia.com'
        password: 'qwe\''
      request(url).post('/account/login').send(data).end (err, res) ->
        if err
          throw err
        res.status.should.equal 400
        done()
        return
    return
  return