'use strict'

Glue = require('glue')
config = require('./../manifest-fix.json')

options =
  relativeTo: __dirname+'/modules'

Glue.compose config, options, (err, server) ->
  if err
    throw err
  server.start ->
    console.log 'START APP FIX !!! PORT: '+server.connections[0].info.port
    return
  return