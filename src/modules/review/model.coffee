Mongoose        = require('mongoose')
Schema          = Mongoose.Schema
mongoosePaginate = require('mongoose-paginate')

ReviewSchema  = new Schema(
  text:
    type: String
    required: true
  dates:
    submitted:
      type: Date
      required: true
      default: Date.now
  campaign:
    type: Schema.ObjectId
    ref: 'Campaign'
    required: true
  user:
    type: Schema.ObjectId
    ref: 'User'
    required: true
  status:
    type: String,
    enum: ['published', 'unpublished', 'rejected']
    required: true
    default: "unpublished"
  voto:
    type: Number
    default: 0
  pubcomment:
    type: Boolean,
    default: false
  tester:
    type: Boolean,
    defalut: false
)

ReviewSchema.plugin(mongoosePaginate)

module.exports.dao = Mongoose.model('Review', ReviewSchema)