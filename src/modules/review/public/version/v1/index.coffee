Joi         = require 'joi'
Handlers    = require './handlers'
Schema      = require './schema'
Version     = "V1"
Base        = "review"

module.exports = [
  {
    method: 'POST'
    path: "/#{Version}/#{Base}"
    config:
      description: 'Post a review'
      notes: 'Post a review'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.save
      auth:
        strategy: 'token'
        scope: ['user']
      validate:
        headers: Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
  }
  {
    method: 'GET'
    path: "/#{Version}/#{Base}/{campaign}"
    config :
      description: 'All reviews of a campaign'
      notes: 'List of all reviews of a campaign'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.getCampaignReviews
      response:
        schema: Schema.listModel
      validate:
        query:
          page: Joi.number().optional()
          limit: Joi.number().optional()
        params:
          campaign: Joi.string().required()
  }
  {
    method: 'GET'
    path: "/#{Version}/#{Base}/checkUserReview/{campaign}"
    config :
      description: 'Get user review'
      notes: 'Get a user review'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.getUserReview
      auth:
        strategy: 'token'
        scope: ['user']
      validate:
        params:
          campaign: Joi.string().required()
        headers: Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
  }
]