Mongoose = require('mongoose')

#Campaign = Mongoose.model('Campaign')
#User = Mongoose.model('User')
#Profile = Mongoose.model('Profile')
#Review = Mongoose.model('Review')
dist = process.env.NODE_FOLDER || 'dist'

Review = require(process.cwd() + '/'+dist+'/modules/review/model').dao

Boom    = require 'boom'
async   = require 'async'


methods =
  save: (request, reply) ->
    user = request.auth.credentials._id
    campaign = request.payload.campaign

    review = ''

    async.series [
      (cb) ->
        Review.find({user: user, campaign: campaign}).exec (err, items) ->
          if !err and items
            if items.length > 0
              cb('Review already existing')
              return
            cb()
          else
            cb('Error')
      (cb) ->
        request.payload.user = user
        review = new Review(request.payload)
        review.save (err, review) ->
          if err
            cb(err)
          else
            cb()
    ], (errAsync) ->
      if !errAsync
        reply review
      else
        reply Boom.badRequest(errAsync)
    return

  getCampaignReviews: (request, reply) ->

    page = request.query.page || 1
    limit = request.query.limit || 100

    populate = [
      {
        path: 'user', select:"userName"
      }
    ]

    Review.paginate({campaign: request.params.campaign, status: 'published'}, {sort: {'dates.submitted': -1} , page: page, limit: limit, lean: true, populate: populate}).then (item) ->
      if item.docs
        reply(item.docs).header('X-Total-Count', item.total);
      else
        reply []
      return
    return

  getUserReview: (request, reply) ->

    user = request.auth.credentials._id
    campaign = request.params.campaign

    Review.findOne({user: user, campaign: campaign}).exec (err, item) ->
      if !err and item
        reply item
      else
        reply [{error: 'Error checking user review'}]
    return

module.exports = methods