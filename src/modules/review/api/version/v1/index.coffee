Joi         = require 'joi'
Handlers    = require './handlers'
Schema      = require './schema'
Version     = "V1"
Base        = "review"

module.exports = [
  {
    method: 'GET'
    path: "/#{Version}/#{Base}/{id}"
    config :
      description: 'Get Review by id'
      notes: 'Detail of a review providing a valid _id'
      tags: ['api',"#{Version}","#{Base}"]
      response:
        schema: Schema.reviewModel
      validate:
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        params:
          id: Joi.string().required().description('review id')
      handler: Handlers.getById
      auth:
        strategy: 'token'
        scope: ['admin']
  }
  {
    method: 'DELETE'
    path: "/#{Version}/#{Base}/{id}"
    config:
      description: 'Delete a review'
      notes: 'Delete the review with given _id'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.deleteById
      auth:
        strategy: 'token'
        scope: ['admin']
      validate:
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        params:
          id: Joi.string().required().description('campaign id')
  }
  {
    method: 'GET'
    path: "/#{Version}/#{Base}"
    config :
      description: 'All reviews'
      notes: 'List of all reviews without any filter'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.getAll
      auth:
        strategy: 'token'
        scope: ['admin']
      validate:
        query:
          page: Joi.number().optional()
          limit: Joi.number().optional()
          order: Joi.string().optional()
          sort: Joi.string().optional()
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
      response:
        schema: Schema.listModel
  }
  {
    method: 'PUT'
    path: "/#{Version}/#{Base}/{id}"
    config :
      description: 'Put review'
      notes: 'Modify a review providing a valid _id'
      tags: ['api',"#{Version}","#{Base}"]
      response:
        schema: Schema.reviewModel
      validate:
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        params:
          id: Joi.string().required().description('campaign id')
        payload: Schema.payload
      handler: Handlers.putById
      auth:
        strategy: 'token'
        scope: ['admin']
  }
  {
    method: 'POST'
    path: "/#{Version}/#{Base}/query"
    config:
      description: 'Query reviews'
      notes: 'Search reviews with given parameters'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.query
      auth:
        strategy: 'token'
        scope: ['admin']
      validate:
        query:
          page: Joi.number().optional()
          limit: Joi.number().optional()
          sort: Joi.string().optional()
          order: Joi.string().optional()
          published: Joi.boolean().optional()
          campaign: Joi.string().optional()
          status: Joi.string().optional()
          tester: Joi.boolean().optional()
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        payload:
          Schema.payloadQuery
  }
]