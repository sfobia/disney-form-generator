Mongoose = require('mongoose')

dist = process.env.NODE_FOLDER || 'dist'

Review = require(process.cwd() + '/'+dist+'/modules/review/model').dao

#Campaign = Mongoose.model('Campaign')
#User = Mongoose.model('User')
#Profile = Mongoose.model('Profile')
#Review = Mongoose.model('Review')

Moment  = require 'moment'
Boom    = require 'boom'
async   = require 'async'
json2csv = require 'json2csv'
_ = require 'lodash'
fs = require 'fs'
csv = require 'fast-csv'


methods =
  getById: (request, reply) ->

    populate = [
      {
        path: 'user', select:"email profile", populate: { path: 'profile', model: 'Profile', select: 'name'}
      }
      {
        path: 'campaign', select:"title"
      }
    ]
    Review.findById(request.params.id).populate(populate).exec (err, item) ->
      if !err and item
        reply item
      else
        reply Boom.notFound(err)
      return
    return

  deleteById: (request, reply) ->
    Review.remove _id: request.params.id, (err, item) ->
      if !err and item
        reply('ok')
      else
        reply Boom.notFound(err)
      return
    return

  getAll: (request, reply) ->
    page = request.query.page || 1
    limit = request.query.limit || 10
    order = request.query.order || "dates.submitted"
    sort = request.query.sort || ''
    sortOrder = sort + order
    query = {}

    Review.paginate(query, { page: page, limit: limit, lean: true, populate: 'user campaign', sort: sortOrder }).then (item) ->
      if item.docs
        reply(item.docs).header('X-Total-Count', item.total)
      else
        reply Boom.notFound()
      return
    return

  query: (request, reply) ->

    reg = []
    if request.query.campaign
      reg.push {
        "campaign": request.query.campaign
      }

    query = {}
    if reg.length > 0
      query = $and: reg

    page  = request.query.page  || 1
    limit = request.query.limit || 10
    order = "dates.submitted"
    sort = '-'
    sortOrder = sort + order


    #if request.query.tester == true
      #query.tester = request.query.tester
    #else
    query.tester = request.query.tester

    query.published = true if request.query.published
    query.status = request.query.status if request.query.status

    populate = [
      {
        path: 'user', select:"email profile", populate: { path: 'profile', model: 'Profile', select: 'name'}
      }
      {
        path: 'campaign', select:"title"
      }
    ]
    Review.paginate(query, { page: page, limit: limit, lean: true, sort: sortOrder, populate: populate }).then (item) ->
      reply(item.docs).header('X-Total-Count', item.total)
      return
    return


  putById: (request, reply) ->
    Review.findByIdAndUpdate(_id: request.params.id, {$set: request.payload}).exec (err, item) ->
        if !err and item
          reply item
        else
          reply Boom.notFound(err)
          return
    return


module.exports = methods