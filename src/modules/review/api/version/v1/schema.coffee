Joi = require 'joi'
Mongoose = require('mongoose')
Review   = Mongoose.model('Review')

module.exports.reviewModel = Joi.object(
  _id: Joi.any().required()
  campaign: Joi.any().required()
  user: Joi.any().required()
  text: Joi.string().required().description('review text')
  dates:
    submitted: Joi.date().required().description('start date of the review')
  status: Joi.string().optional().description('indicate if the review is published')
).meta(className: 'Review').options(allowUnknown: true, abortEarly: false)

module.exports.payload = Joi.object(
  status: Joi.string().optional().description('indicate if the review is published')
)

module.exports.payloadQuery = Joi.object(
  campaign: Joi.any().optional()
  user: Joi.any().optional()
  dates:
    submitted: Joi.date().optional().description('start date of the review')
  status: Joi.string().optional().description('indicate if the review is published')
)

module.exports.listModel = Joi.array().items(module.exports.reviewModel).meta(className: 'List')