Joi         = require 'joi'
Handlers    = require './handlers'
Version     = "V1"
Base        = "report"

queryProfilePayload =
  active: Joi.boolean().optional()
  rejected: Joi.boolean().optional()
  campaign: Joi.any().optional().description('campaign id')
  reportType: Joi.string().optional()
  limit: Joi.number().optional()
  gender: Joi.string().optional()
  province: Joi.string().optional()
  accepted:
    marketing: Joi.boolean().optional().description('accepted marketing policy')
    dem: Joi.boolean().optional().description('accepted dem policy')
    profiling: Joi.boolean().optional().description('accepted profiling policy')
    privacy: Joi.boolean().optional().description('accepted privacy policy')
    conditions: Joi.boolean().optional().description('accepted conditions policy')
  newsletter: Joi.any().optional().description('newsletter')
  birthDate: Joi.date().optional().description('birth date')

module.exports = [
  {
    method: 'GET'
    path: "/#{Version}/#{Base}/getUsersCount"
    config :
      description: 'Get Users Count'
      notes: 'Details of the number of male and female users and profiles have been registered'
      tags: ['api',"#{Version}","#{Base}"]
      validate:
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
      handler: Handlers.getUsersCount
      auth:
        strategy: 'token'
        scope: ['admin']
  }
  {
    method: 'GET'
    path: "/#{Version}/#{Base}/getActiveUsersCount"
    config :
      description: 'Get active Users count'
      notes: 'Details of the number of male and female registered and active users'
      tags: ['api',"#{Version}","#{Base}"]
      validate:
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
      handler: Handlers.getActiveUsersCount
      auth:
        strategy: 'token'
        scope: ['admin']
  }
  {
    method: 'GET'
    path: "/#{Version}/#{Base}/getProfileCompletedCount"
    config :
      description: 'Get completed profile Users count'
      notes: 'Details of the number of male and female users that have completed their profile'
      tags: ['api',"#{Version}","#{Base}"]
      validate:
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
      handler: Handlers.getProfileCompletedCount
      auth:
        strategy: 'token'
        scope: ['admin']
  }
  {
    method: 'GET'
    path: "/#{Version}/#{Base}/getNewsletterUsersCount"
    config :
      description: 'Get counts of users that subscribed a newsletter'
      notes: 'Details of the number of male and female users that have subscribed a newsletter'
      tags: ['api',"#{Version}","#{Base}"]
      validate:
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
      handler: Handlers.getNewsletterUsersCount
      auth:
        strategy: 'token'
        scope: ['admin']
  }
  {
    method: 'GET'
    path: "/#{Version}/#{Base}/getUsersWithoutShippingForm"
    config :
      description: 'Get list of users that have not compiled the shipping form'
      notes: 'Details of the number of users that have not compiled the shipping form'
      tags: ['api',"#{Version}","#{Base}"]
      validate:
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
      handler: Handlers.getUsersWithoutShippingForm
      auth:
        strategy: 'token'
        scope: ['admin']
  }
  {
    method: 'POST'
    path: "/#{Version}/#{Base}/getUsersExport"
    config :
      description: 'Get list of users export'
      notes: 'Details of the users export'
      tags: ['api',"#{Version}","#{Base}"]
      validate:
        query:
          page: Joi.number().optional()
          limit: Joi.number().optional()
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        payload:
          typeForm: Joi.string().required().description('form type')
          formId: Joi.string().required().description('form id')
          limit: Joi.number().optional().description('limit row')
          campaignId: Joi.string().optional().description('campaign id')
      handler: Handlers.getUsersExport
      auth:
        strategy: 'token'
        scope: ['admin']
  }
  {
    method: 'GET'
    path: "/#{Version}/#{Base}/stats"
    config:
      description: 'All stats db'
      notes: 'all count db'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.getAllStats
      validate:
        query:
          month: Joi.string().optional()
          year: Joi.string().optional()
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
      auth:
        strategy: 'token'
        scope: ['admin']
  }
  {
    method: 'GET'
    path: "/#{Version}/#{Base}/messages"
    config:
      description: 'All messages report'
      notes: 'all messages reports'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.getAllMessages
      validate:
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
      auth:
        strategy: 'token'
        scope: ['admin']
  }
  {
    method: 'GET'
    path: "/#{Version}/#{Base}/download/{id}"
    config:
      description: 'Rotta per download file csv'
      notes: 'all messages reports'
      tags: ['api',"#{Version}","#{Base}"]
      validate:
        headers:
          Joi.object('authorization': Joi.string().required()).options( allowUnknown: true )
        params:
          id: Joi.string().required().description('id report')
      auth:
        strategy: 'token'
        scope: ['admin']
      handler: Handlers.downloadReport
  }
  {
    method: 'POST'
    path: "/#{Version}/#{Base}/pending"
    config:
      description: 'Ricerca se esistono report in pending per type'
      notes: 'all pending reports by type'
      tags: ['api',"#{Version}","#{Base}"]
      validate:
        headers:
          Joi.object('authorization': Joi.string().required()).options( allowUnknown: true )
        payload:
          type: Joi.string().required().description('report type')
      auth:
        strategy: 'token'
        scope: ['admin']
      handler: Handlers.getPendingReports
  }
  {
    method: 'DELETE'
    path: "/#{Version}/#{Base}/{id}"
    config:
      description: 'Delete a report'
      notes: 'Delete the report with given _id'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.deleteById
      auth:
        strategy: 'token'
        scope: ['admin']
      validate:
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        params:
          id: Joi.string().required().description('report id')
  }
  {
    method: 'GET'
    path: "/#{Version}/#{Base}/getNeverActivedUsers"
    config :
      description: 'Never actived users on any campaign'
      notes: 'List of users that have never got an active status for their candidation on any Campaign'
      tags: ['api',"#{Version}","#{Base}"]
      validate:
        query:
          page: Joi.number().optional()
          limit: Joi.number().optional()
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
      handler: Handlers.getNeverActivedUsers
      auth:
        strategy: 'token'
        scope: ['admin']
  }
  {
    method: 'POST'
    path: "/#{Version}/#{Base}/queryProfile"
    config:
      description: 'All stats db'
      notes: 'all count db'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.queryProfile
      validate:
        query:
          limit: Joi.number().optional()
        payload:
          queryProfilePayload
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
      auth:
        strategy: 'token'
        scope: ['admin']
  }
  {
    method: 'POST'
    path: "/#{Version}/#{Base}/surveyNotSent"
    config :
      description: 'Users that never sent their survey for a product they tested'
      notes: 'List of users that never sent their survey for a product they tested'
      tags: ['api',"#{Version}","#{Base}"]
      validate:
        query:
          page: Joi.number().optional()
          limit: Joi.number().optional()
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
      handler: Handlers.surveyNotSent
      auth:
        strategy: 'token'
        scope: ['admin']
  }
]