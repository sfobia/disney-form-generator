dist = process.env.NODE_FOLDER || 'dist'

Candidate = require process.cwd() + '/'+dist+'/modules/candidate/model'
Campaign = require process.cwd() + '/'+dist+'/modules/campaign/model'
Profile = require process.cwd() + '/'+dist+'/modules/profile/model'
Form = require process.cwd() + '/'+dist+'/modules/form/model'
Report = (require (process.cwd() + '/'+dist+'/modules/report/model')).dao
User = require process.cwd() + '/'+dist+'/modules/user/user'

Boom      = require 'boom'
async = require 'async'
json2csv = require 'json2csv'
fs = require 'fs'
csv = require 'fast-csv'
Moment = require 'moment'
mongoose = require 'mongoose'
_ = require 'lodash'

path = require 'path'
mime = require 'mime'
zone = require('./zone')

ageRanges = [
  { min: 18, max: 24, title: '18-24', num: 1 }
  { min: 25, max: 34, title: '25-34', num: 2 }
  { min: 35, max: 44, title: '35-44', num: 3 }
  { min: 45, max: 54, title: '45-54', num: 4 }
  { min: 55, max: 64, title: '55-64', num: 5 }
  { min: 65, max: 200, title: '65 e oltre', num: 6 }
]

zoneArray = []
zoneArray['Nord Ovest'] = 1
zoneArray['Nord Est'] = 2
zoneArray['Centro'] = 3
zoneArray['Sud e Isole'] = 4

methods =

  getUsersExport: (request, reply) ->

    console.log( "#getUsersExport" )

    usersResponses = []
    fields = [ "email" ]
    limit = request.payload.limit || 100000

    typeForm  = request.payload.typeForm
    formId    = request.payload.formId

    campaignId    = undefined
    if request.payload.campaignId
      campaignId = request.payload.campaignId

    questionFlat  = {}
    answerFlat    = {}

    reportName = "Report"
    slug = ''
    counter = 0
    offset = 0
    lastReport = {}
    reportId = ''
    csvfile = ''

    regForm = ''

    async.waterfall [
      (cb) ->

        console.log( "#getCampaign" );
        Campaign.dao.findOne({'_id': campaignId}).exec (err, campaign) ->

#Premium in questo caso equivale ad anonima
          if( campaign.premium )
            fields = []

          cb()


      (cb) ->

        Report.count({ 'dates.end' : $exists: false }).lean().exec (err, items) ->
          if err
#cb 'err pending'
            reply Boom.badRequest('err pending')
            return

          if items > 0
            reply Boom.badRequest("Un altro export è in attesa, devi attendere che sia concluso")
          else
            reply { msg: "ok" }
            cb()

          return

      (cb) ->

# Devo trovare l'offset
        Report.findOne({ form: formId }).sort({ 'dates.end': -1}).lean().exec (err, last) ->
          if err
            reply Boom.badRequest()
            return
          if last
            lastReport  = last
            offset      = last.counters.total
            if typeForm != 'profilation'
              offset = 0
          cb()

      (cb) ->
        Form.dao.findOne({ formType: typeForm , _id : formId }).lean().exec (err, form) ->
          if err
            reply Boom.badRequest(err)
            return

          num = 0
          if form

            # Riordinamento campi come definiti su form
            formFields = _.sortByOrder(form.fields, ['order'], 'asc')
            #            form.fields.sort (a, b) ->
            #              a.order > b.order

            slug = form.title.toLowerCase().replace(new RegExp(' ', 'g'), '-')

            report = new Report()
            filename = typeForm
            # campaign
            if campaignId
              filename = filename+'-'+slug
            else
              filename = filename+'-all'

            # offset
            if offset == 0
              labelOffset = '0-'+limit
            else
              sum = offset+limit
              labelOffset = offset+'-'+sum
            filename = filename+'-'+labelOffset


            reportId = report._id
            csvfile = 'download/'+filename+'.csv'
            report.name     = typeForm+"-"+slug+'-'+labelOffset
            report.url      = csvfile
            report.form     = formId
            report.reportType       = typeForm+"-report"
            report.save (err,item)->
              return


#            console.log( "#formFields" )
#            console.log( formFields )

            for item in formFields
              num2 = 0
              domanda = ''

              # fix per mettere in elenco i CAP pre-modifica e post-modifica struttura indirizzi
              #              if (item.type == 'input' && item.name == 'Il CAP della città in cui vivi?') || (item.type == 'cap')
              #                #questionFlat['cap'] = label: 'CAP', num: 'CAP'
              #              else
              # fix domanda su numero figli ed età


#              console.log( "#Form Field" )
#              console.log( item )
#              console.log( num )


              if item.type == "onlydescription"

                ## NON FARE NULLA


              else if item.type == "ranges-type2" || item.type == "ranges"
                for r in item.value
                  num++
                  nome = item.name+' ---- '
                  nome += r.name || r.left+'/'+r.right
                  domanda = nome
                  questionFlat[r.identifier] = label: nome, num: num

              else if item.type == "checkbox"


#                console.log( item )

                num++

                for r in item.value

                  nome = item.name
#                  nome += r.name
                  domanda = nome
                  questionFlat[item.identifier] = label: nome, num: num


              else if item.type == "multiradio"

                for r in item.radiovalue
                  num++
                  nome = item.name + ': '
                  nome += r.name
                  domanda = nome
                  questionFlat[r.identifier] = label: nome, num: num

              else
                num++
                domanda = item.name
                questionFlat[item.identifier] = label: item.name, num: num
                questionFlat[item.identifier].children = []
                numChildren = 0

                for x in item.value
                  numChildren++
                  nome = numChildren+') '
                  nome += x.name || x.left+'/'+x.right
                  questionFlat[item.identifier].children.push nome


              #                console.log( item.value )

              if item.type == "multiradio"
                for valore in item.radiovalue

#                    console.log( valore )

                  num2++
                  nome = valore.name
                  answerFlat[valore.identifier] =label: nome, num: num2
              else

#                console.log( "#######################" )
#                console.log( item )

                for valore in item.value
                  num2++
                  nome = valore.name || valore.left+'/'+valore.right

                  #                    console.log( "#0#0#0" )
                  #                    console.log( valore )

                  answerFlat[valore.identifier] =label: nome, num: num2


            for k,q of questionFlat
              if q.label != 'Zona' and q.label != 'Sigla Provincia'
                fields.push 'Domanda:'+q.num


#            console.log( "QUESTIONS" )
#            console.log( questionFlat )

            cb()

      (cb) ->
        flag = false

        payloadMatch = { 'formType': typeForm }

        if campaignId
          payloadMatch.campaign = campaignId

        stream = Profile.dao.find( {"form": { $elemMatch: payloadMatch }})
          .skip(offset)
          .limit(limit)
          .populate('user')
          .select('name form user gender birthDate accepted province')
          .lean()
          .stream()

        stream.on 'data', (item) ->

          counter++

          forms = item.form

          #          console.log( "#1. FORMS" )

          for form in forms

#            console.log( "### FORM" )

            if ( campaignId and form.formType == typeForm and form.campaign.toString() == campaignId ) || ( !campaignId and form.formType == typeForm )

              # Age calculation and range
              age = Moment().diff(item.birthDate, 'years')
              for ageRange in ageRanges
                if age >= ageRange.min and age <= ageRange.max
                  ageText = ageRange.num
                  break

              # Email retrieve
              if item.user and typeof item.user is 'object'
                if item.user.email
                  if item.user.email.indexOf('@mailinator') == -1
                    email = item.user.email
                  else
                    continue
                else
                  continue

              # fixed and always present values
              obj =
#                name: item.username
                email: email
              #                sex: item.gender
              #                age: ageText
              #                zone: ''

              #              console.log( "---- " )
              #              console.log( obj)

              #              if item.province
              #                obj.zone = zoneArray[zone[item.province.toUpperCase()]]


              # accepted checkboxes
              #              if item.accepted
              #                obj['accepted.marketing'] = if item.accepted.marketing then '1' else '0'
              #                obj['accepted.dem'] = if item.accepted.dem then '1' else '0'
              #                obj['accepted.profiling'] = if item.accepted.profiling then '1' else '0'

              reportName = slug

              for val in form.values

                if !val.value and !val.multi
                  continue

#                console.log( "###############################" )
#                console.log( val.type )

                if typeof val.value is 'object'

                  if val.type == "checkbox"

#                    console.log( val )

                    keyColumn = 'Domanda:'+questionFlat[val.ref].num

                    answers = []

                    for key, value of val.value
                      if value == true
                        answers.push(val.info[key])

                    obj[keyColumn] = answers.join(" - ")

                  else


                    for key, value of val.value

                      if !questionFlat[key]
                        continue

                      keyColumn = 'Domanda:'+questionFlat[key].num

                      if val.type == "checkbox" and value == true
                        obj[keyColumn] = 1
                      else
                        if isNaN(value.value)
                          obj[keyColumn] = 0
                        else
                          obj[keyColumn] = value.value



                else

                  if val.type == "onlydescription"

                    ## NIENTE

                  else if val.type == 'input'

                    keyColumn = 'Domanda:'+questionFlat[val.ref].num
                    obj[keyColumn] = val.value

#                  if val.type == 'checkbox'
#
#                    keyColumn = 'Domanda:'+questionFlat[val.ref].num
#
#                    cval = []
#
#                    for checkboxkey, checkboxval of val.value
#                      cval.push( val.info[checkboxkey] )
#
#                    obj[keyColumn] = cval.join(", ")


                  else if val.type == 'textarea'

                    keyColumn = 'Domanda:'+questionFlat[val.ref].num

#                    console.log( keyColumn )

                    obj[keyColumn] = val.value

                  else if val.type == 'multiradio'

                    for multikey, multivalue of val.multi

                      keyColumn = 'Domanda:'+questionFlat[multivalue.ref].num

#                      console.log( keyColumn )

                      obj[keyColumn] = multivalue.radiovalue

                  else if val.type == 'radio'

                    keyColumn = 'Domanda:'+questionFlat[val.ref].num
                    obj[keyColumn] = val.value

                  else

                    if !questionFlat[val.ref]
                      continue

                    if !answerFlat[val.refvalue]
                      continue

                    keyColumn = 'Domanda:'+questionFlat[val.ref].num
                    obj[keyColumn] = answerFlat[val.refvalue].num.toString()


              flag = true
              #
              #              console.log( "###### FINAL OBJ" )
              #              console.log( obj );




              #unset cap se campaignId
              usersResponses.push obj

              break
              return




        stream.on 'error', (err) ->
          cb Boom.badRequest(err)
          return

        stream.on 'close', ->

          ##### GENERAZIONE LEGENDA #####
          obj = {}
          obj["Domanda:1"] = ""
          usersResponses.push(obj)

#          obj = {}
#          obj["name"] = "Range di età"
#          usersResponses.push(obj)
#
#          for ageRange in ageRanges
#            obj = {}
#            obj["name"] = '----    '+ageRange.num+") "+ageRange.title
#            usersResponses.push(obj)
#
#          obj = {}
#          obj["name"] = "Zona"
#          usersResponses.push(obj)
#
#          for k, v of zoneArray
#            obj = {}
#            obj["name"] = '----    '+v+") "+k
#            usersResponses.push(obj)

          obj = {}
          obj["Domanda:1"] = "DOMANDE ---------------------------------------"
          usersResponses.push(obj)

          for k,domanda of questionFlat
            if domanda.label != 'Zona' and domanda.label != 'Sigla Provincia'
              obj = {}
              obj["Domanda:1"] = domanda.num+") "+domanda.label
              usersResponses.push(obj)


          cb()

      (cb) ->

        json2csv {
          data: usersResponses
          fields: fields
          quotes: ''
          del: ';'
        }, (err, csv) ->
          if err
            cb({ msg: err})
            return
          fs.writeFile csvfile, csv, (err, s) ->
            if err
              throw err
              cb()
              return

            total    = counter+offset
            Report.findByIdAndUpdate( reportId,{ $set: {'dates.end': new Moment(), 'counters.exported' : counter, 'counters.total' : total } }).exec (err,res)->
              cb(null,csvfile)
            return
          return

    ], (err, res) ->
      return

    return

  getUsersWithoutShippingForm: (request, reply) ->
    usersResponse = []

    stream = Profile.dao.find({"form": {$elemMatch: {'formType': 'shipping'}}}, 'form user').populate("user","email").lean().stream()
    stream.on 'data', (profile) ->
      if profile
        if profile.form
          for form in profile.form
            if form.formType == 'shipping'
              if form.values.length == 0
                usersResponse.push { id: profile._id, email:profile.user.email, form: form }

    stream.on 'error', (err) ->
      Boom.badRequest(err)
      return

    stream.on 'close', ->
      reply usersResponse
      return
    return


  getUsersCount: (request, reply) ->

    Profile.dao.aggregate(
      {
        $group: {
          _id: '$gender'
          total: {$sum: 1}
        }
      }
    ).exec (err, res) ->
      if err
        reply Boom.badRequest(err)
        return
      if res and !err
        reply res
        return
    return

  getActiveUsersCount: (request, reply) ->
    User.dao.aggregate(
      {
        $match: {'confirmed': true}
      }
      {
        $group: {
          _id: 0
          total: {$sum: 1}
        }
      }
    ).exec (err, res) ->
      if err
        reply Boom.badRequest(err)
        return
      if res and !err
        reply res
        return
    return

  getProfileCompletedCount: (request, reply) ->

    Profile.dao.aggregate(
      {
        $match: {'profileCompleted': true}
      }
      {
        $group: {
          _id: '$gender'
          total: {$sum: 1}
        }
      }
    ).exec (err, res) ->
      if err
        reply Boom.badRequest(err)
        return
      if res and !err
        reply res
        return
    return

  getNewsletterUsersCount: (request, reply) ->
    response = []

    async.parallel [
      (cb) ->
        Profile.dao.aggregate(
          {
            $match: {'newsletter.test': true}
          }
          {
            "$group": {
              "_id": "test",
              "count": {
                "$sum": {
                  "$cond": [ "$newsletter.test", 1, 0 ]
                }
              }
            }
          }
        ).exec (err, res) ->
          if err
            reply Boom.badRequest(err)
            return
          if res and !err
            response.push res
            cb()

      (cb) ->
        Profile.dao.aggregate(
          {
            $match: {'newsletter.generica': true}
          }
          {
            "$group": {
              "_id": "generica",
              "count": {
                "$sum": {
                  "$cond": [ "$newsletter.generica", 1, 0 ]
                }
              }
            }
          }
        ).exec (err, res) ->
          if err
            reply Boom.badRequest(err)
            return
          if res and !err
            response.push res
            cb()
    ], (err) ->
      if err
        reply err
        return
      reply response
      return
    return

  getAllStats: (request ,reply) ->
    response = {}

    if request.query.month and request.query.year
      startDate = Moment(request.query.year + ' ' + request.query.month, "YYYY MM")
      startDate = startDate.startOf('month')
      endDate = Moment(request.query.year + ' ' + request.query.month, "YYYY MM")
      endDate = endDate.endOf('month')

    start = startDate || Moment.utc().startOf('month')
    end = endDate || Moment.utc().endOf('month')

    async.parallel [
      (cb) ->
        User.dao.count({ creationDate: { $gte: new Date(start.toISOString()) , $lte: new Date(end.toISOString()) } }).exec (err, items) ->
          response.users = items
          cb()
      (cb) ->
        User.dao.count({ creationDate: { $gte: new Date(start.toISOString()) , $lte: new Date(end.toISOString()) } }).exec (err, items) ->
          if items
            User.dao.aggregate [
              { $match:
                creationDate: { $gte: new Date(start.toISOString()) , $lte: new Date(end.toISOString()) }
              },
              { $group:
                _id:
                  year: $year: '$creationDate'
                  month: $month: '$creationDate'
                  day: $dayOfMonth: '$creationDate'
                count: $sum: 1
              },
              { $group:
                _id:
                  year: '$_id.year'
                  month: '$_id.month'
                dailyusage:
                  $push:
                    day: '$_id.day'
                    count: '$count'
                    "percentage":
                      "$concat": [ { "$substr": [ { "$multiply": [ { "$divide": [ "$count", {"$literal": items }] }, 100 ] }, 0,2 ] }, "", "%" ]
              },
              { $group:
                _id: year: '$_id.year'
                monthlyusage: $push:
                  month: '$_id.month'
                  dailyusage: '$dailyusage'
              }
            ], (err, res) ->
              if err
                cb(err)
                return
              response.usersStats = res
              cb()

          if !items
            cb()
    ],
      (err) ->
        if err
          reply err
          return
        reply response
    return

  getPendingReports: ( request, reply ) ->

    Report.count({ reportType: request.payload.type, 'dates.end' : undefined }).lean().exec (err, items) ->
      if err
        reply Boom.badRequest()
        return
      if items >= 0
        reply({ count: items })

      return
    return

  downloadReport: ( request ,reply) ->

    Report.findByIdAndUpdate( request.params.id, { $set: { read: true } } ).exec (err, item) ->
      if !err and item
        reply.file(process.cwd()+'/'+item.url)
      else
        reply Boom.notFound(err)
      return
    return

    #
    return

  getAllMessages: (request ,reply) ->
    Report.find({}).lean().exec (err, items) ->
      if err
        reply Boom.badRequest()
        return
      if items
        reply(items)

      return
    return

  deleteById: (request, reply) ->
    Report.remove _id: request.params.id, (err, item) ->
      if !err and item
        reply('ok')
      else
        reply Boom.notFound(err)
      return
    return


  surveyNotSent: (request, reply) ->

#    console.log( "#surveyNotSent" )

    fields = [ "firstName", "lastName", "email" ]
    limit = request.query.limit || 100000

    reportName = "survey-not-sent-report"
    reportType = "survey-not-sent-"
    counter = 0
    offset = 0
    csvfile = ''
    report = ''
    reportId = ''

    campaignId = request.payload.campaignId

    usersActive = []
    notSentSurveyUsers = []
    delendaUsers = []

    async.waterfall [
      (cb) ->
        Report.count({ 'dates.end' : $exists: false }).lean().exec (err, items) ->
          if err
            cb 'err pending'
          if items > 0
            cb "Un altro export è in attesa, devi attendere che sia concluso "
          else
            reply { msg: "ok" }
            cb()
          return

      (cb) ->
        Campaign.dao.findOne({'_id': campaignId}).exec (err, campaign) ->
          slug = campaign.slug
          reportType = reportType+slug
          cb()

      (cb) ->
# Devo trovare l'offset
        Report.findOne({ reportType: reportType }).sort({ 'dates.end': -1}).lean().exec (err, last) ->
          if err
            reply Boom.badRequest()
            return
          if last
            lastReport  = last
            offset      = 0 #last.counters.total
          cb()

      (cb) ->
        report = new Report
        filename = reportType

        # offset
        if offset == 0
          labelOffset = '0-'+limit
        else
          sum = offset+limit
          labelOffset = offset+'-'+sum
        filename = filename+'-'+labelOffset

        reportId = report._id
        csvfile = 'download/'+filename+'.csv'
        report.name       = filename
        report.url        = csvfile
        report.reportType = reportType
        report.save (err,item)->
          cb()

      (cb) ->
# ottengo i candidati che hanno avuto un'attivazione
        Candidate.dao.aggregate [
          {
            $match: {
              active: true
              campaign: mongoose.Types.ObjectId(campaignId)
            }
          }
        ], (err, res) ->
          for user in res
# conservo gli id degli utenti che hanno avuto un'attivazione in questo array
            usersActive.push user.user.toString()
          cb()

      (cb) ->
# ottengo i profili dei candidati della campagna che hanno inviato il survey
#payloadMatch = { 'formType': 'survey',  'campaign': campaignId }
        Profile.dao.find({"user": { $in: usersActive }})
          .lean()
          .exec (err, items) ->
        console.log items.length
        for item in items
          for form in item.form
            if form.formType == 'survey' and form.campaign.toString() == campaignId
# rimuovo gli utenti che hanno inviato il survey
              delendaUsers.push item.user.toString()
        cb()

      (cb) ->
        goodUsers = _.difference(usersActive,delendaUsers)
        stream = Profile.dao.find( {"user": { $in: goodUsers }})
          .populate({path: "user", select: "email"})
          .skip(offset)
          .limit(limit)
          .lean()
          .stream()

        stream.on 'data', (item) ->
          counter++
          age = Moment().diff(item.birthDate, 'years')
          email = "nomail"
          if item.user and typeof item.user is 'object'
            if item.user.email
              email = item.user.email

          obj =
            firstName: item.name.first
            lastName: item.name.last
            email: email
            sex: item.gender
            age: age
          if item.accepted
            obj['accepted.marketing'] = if item.accepted.marketing then '1' else '0'
            obj['accepted.dem'] = if item.accepted.dem then '1' else '0'
            obj['accepted.profiling'] = if item.accepted.profiling then '1' else '0'

          notSentSurveyUsers.push obj

        stream.on 'error', (err) ->
          cb Boom.badRequest(err)
          return

        stream.on 'close', ->
          cb()

      (cb) ->
        json2csv {
          data: notSentSurveyUsers
          fields: fields
          quotes: ''
          del: ';'
        }, (err, csv) ->
          if err
            reply { msg: err}
            return
          fs.writeFile csvfile, csv, (err, s) ->
            if err
              throw err
              return

          total = counter+offset
          Report.findByIdAndUpdate( reportId,{ $set: {'dates.end': new Moment(), 'counters.exported' : counter, 'counters.total' : total } }).exec (err,res)->
            cb(null,csvfile)
            return
        return

    ], (err, res) ->
      if err
        reply Boom.badRequest(err)
        return
      return

    return


  getNeverActivedUsers: (request, reply) ->

    fields = [ "firstName", "lastName", "email", "sex", "age", 'accepted.marketing', 'accepted.dem', 'accepted.profiling']
    limit = request.query.limit || 100000

    reportName = "never-actived-users-report"
    reportType = "never-actived-users"
    counter = 0
    offset = 0
    csvfile = ''
    report = ''
    reportId = ''

    userExclude = []
    neverActivedUsers = []
    neverActived = []

    async.waterfall [
      (cb) ->
        Report.count({ 'dates.end' : $exists: false }).lean().exec (err, items) ->
          if err
            cb 'err pending'
          if items > 0
            cb "Un altro export è in attesa, devi attendere che sia concluso "
          else
            reply { msg: "ok" }
            cb()
          return

      (cb) ->
# Devo trovare l'offset
        Report.findOne({ reportType: reportType }).sort({ 'dates.end': -1}).lean().exec (err, last) ->
          if err
            reply Boom.badRequest()
            return
          if last
            lastReport  = last
            offset      = last.counters.total
          cb()

      (cb) ->
        report = new Report
        filename = reportType

        # offset
        if offset == 0
          labelOffset = '0-'+limit
        else
          sum = offset+limit
          labelOffset = offset+'-'+sum
        filename = filename+'-'+labelOffset

        reportId = report._id
        csvfile = 'download/'+filename+'.csv'
        report.name       = filename
        report.url        = csvfile
        report.reportType = reportType
        report.save (err,item)->
          cb()

      (cb) ->
# ottengo i candidati che hanno avuto un'attivazione
        Candidate.dao.aggregate [
          {
            $match: {
              active: true
            }
          }
          { $group:
            _id: '$user'
          }
        ], (err, res) ->
          for user in res
# conservo gli id degli utenti che hanno avuto un'attivazione in questo array
            userExclude.push user._id
          cb()

      (cb) ->
# ottengo i candidati che hanno active false e mai avuto un active true
        Candidate.dao.find( { "active": false, "rejected": true, "user": { $nin: userExclude }})
          .populate({path: 'user'})
          .lean()
          .exec (err, items) ->
        for item in items
          if item.user
            neverActivedUsers.push item.user.profile
        cb()

      (cb) ->
        stream = Profile.dao.find( {"_id": { $in: neverActivedUsers }})
          .populate({path: "user", select: "email"})
          .skip(offset)
          .limit(limit)
          .lean()
          .stream()

        stream.on 'data', (item) ->
          counter++
          age = Moment().diff(item.birthDate, 'years')
          for ageRange in ageRanges
            if age >= ageRange.min and age <= ageRange.max
              ageText = ageRange.num
              break

          # Email retrieve
          if item.user and typeof item.user is 'object'
            if item.user.email
              if item.user.email.indexOf('@mailinator') == -1
                email = item.user.email

          obj =
            firstName: item.name.first
            lastName: item.name.last
            email: email
            sex: item.gender
            age: ageText
          if item.accepted
            obj['accepted.marketing'] = if item.accepted.marketing then '1' else '0'
            obj['accepted.dem'] = if item.accepted.dem then '1' else '0'
            obj['accepted.profiling'] = if item.accepted.profiling then '1' else '0'

          neverActived.push obj

        stream.on 'error', (err) ->
          cb Boom.badRequest(err)
          return

        stream.on 'close', ->
          cb()

      (cb) ->
        json2csv {
          data: neverActived
          fields: fields
          quotes: ''
          del: ';'
        }, (err, csv) ->
          if err
            reply { msg: err}
            return
          fs.writeFile csvfile, csv, (err, s) ->
            if err
              throw err
              return

          total = counter+offset
          Report.findByIdAndUpdate( reportId,{ $set: {'dates.end': new Moment(), 'counters.exported' : counter, 'counters.total' : total } }).exec (err,res)->
            cb(null,csvfile)
            return
        return

    ], (err, res) ->
      if err
        reply Boom.badRequest(err)
        return
      return

    return

  queryProfile: (request, reply) ->

    console.log( "#queryProfile" )

    fields = [ "name", "email", "sex", "age", "zone"]
    limit = request.payload.limit || 100000

    query = {}

    reportType = request.payload.reportType
    query.gender = request.payload.gender if request.payload.gender
    query.birthDate = request.payload.birthDate if request.payload.birthDate

    # solo se bisogna filtrare per campagna
    if request.payload.campaign
      campaign = request.payload.campaign
      slug = ''
      usersToInclude = []
      if request.payload.rejected
        rejected = request.payload.rejected
        fields.push 'rejected'
      if request.payload.active
        active = request.payload.active
        fields.push 'active'

    if request.payload.province
      query.province = request.payload.province
      fields.push 'province'

    if request.payload.newsletter
      if request.payload.newsletter.generica
        query['newsletter.generica'] = true
        fields.push 'newsletter.generica'
      if request.payload.newsletter.test
        query['newsletter.test'] = true
        fields.push 'newsletter.test'

    # campi di accettazione regole dall'utente. 3 campi al momento sempre presenti nel report
    fields.push 'accepted.marketing'
    fields.push 'accepted.dem'
    fields.push 'accepted.profiling'

    reportName = "profiles-query-report"
    counter = 0
    offset = 0
    csvfile = ''
    report = ''
    reportId = ''

    list = []

    async.waterfall [
      (cb) ->
# verifica concorrenza di altri Report in generazione
        Report.count({ 'dates.end' : $exists: false }).lean().exec (err, items) ->
          if err
            cb 'err pending'
          if items > 0
            cb "Un altro export è in attesa, devi attendere che sia concluso "
          else
            reply { msg: "ok" }
            cb()
          return

      (cb) ->
        if request.payload.campaign
          Campaign.dao.findOne({'_id': request.payload.campaign}).exec (err, campaign) ->
            slug = campaign.slug
            reportType = reportType+slug
            cb()
        else
          cb()

      (cb) ->
# Devo trovare l'offset da cui ripartire a scaricare i dati se eventualmente erano stati scaricati in precedenza
        Report.findOne({ reportType: reportType }).sort({ 'dates.end': -1}).lean().exec (err, last) ->
          if err
            reply Boom.badRequest()
            return
          if last
            lastReport  = last
            offset      = last.counters.total
          cb()
      (cb) ->
        if campaign
          match = {
            campaign: mongoose.Types.ObjectId(campaign)
          }

          if rejected
            match.rejected = true
          if active
            match.active = true

          # ottengo i candidati della campagna specificata
          Candidate.dao.aggregate [
            {
              $match: match
            }
            { $group:
              _id: '$user'
            }
          ], (err, res) ->
            for user in res
# conservo gli id degli utenti di questa campagna
              usersToInclude.push user._id
            query.user = {$in: usersToInclude}
            cb()

        else
          cb()

      (cb) ->
# Inizializzo nuovo dato Report a db
        report = new Report
        reportId = report._id
        filename = reportType

        # campaign
        if !campaign
          filename = filename+'-all'

        # offset
        if offset == 0
          labelOffset = '0-'+limit
        else
          sum = offset+limit
          labelOffset = offset+'-'+sum
        filename = filename+'-'+labelOffset

        csvfile = 'download/'+filename+'.csv'

        report.name       = filename
        report.url        = csvfile
        report.reportType = reportType
        report.save (err,item)->
          cb()

      (cb) ->
# applico i filtri ai profili ed estraggo i dati corrispondenti
        stream = Profile.dao.find(query)
          .populate({path: "user", select: "email"})
          .skip(offset)
          .limit(limit)
          .lean()
          .stream()

        stream.on 'data', (item) ->

          counter++
          # Age calculation and range
          age = Moment().diff(item.birthDate, 'years')
          for ageRange in ageRanges
            if age >= ageRange.min and age <= ageRange.max
              ageText = ageRange.num
              break

          # Email retrieve
          if item.user and typeof item.user is 'object'
            if item.user.email
              if item.user.email.indexOf('@mailinator') == -1
                email = item.user.email

                if item.name and item.name.first and item.name.last

# costruisco la riga del dato csv
                  obj =
                    name: item.name.first+' '+item.name.last
                    email: email
                    sex: item.gender
                    age: ageText
                    zone: ''


                  if item.province
                    userZone = zone[item.province.toUpperCase()]
                    obj['zone'] = zoneArray[userZone]

                  for form in item.form
                    if form.formType == 'registration'
                      for val in form.values
                        if val.name == 'Zona'
                          obj['zone'] = zoneArray[val.value]
                      break

                  # recupero i dati dinamici
                  if query['province']
                    obj['province'] = item.province

                  # condizioni accettate dall'utente
                  if item.accepted
                    obj['accepted.marketing'] = if item.accepted.marketing then '1' else '0'
                    obj['accepted.dem'] = if item.accepted.dem then '1' else '0'
                    obj['accepted.profiling'] = if item.accepted.profiling then '1' else '0'
                  #obj['accepted.privacy'] = if item.accepted.privacy then '1' else '0'
                  #obj['accepted.conditions'] = if item.accepted.conditions then '1' else '0'

                  #newsletter
                  if query['newsletter.generica']
                    obj['newsletter.generica'] = '1'
                  if query['newsletter.test']
                    obj['newsletter.test'] = '1'

                  # status candidato
                  if rejected
                    obj['rejected'] = '1'
                  if active
                    obj['active'] = '1'

                  list.push obj

        stream.on 'error', (err) ->
          cb Boom.badRequest(err)
          return

        stream.on 'close', ->

##### GENERAZIONE LEGENDA #####
          obj = {}
          obj["name"] = ""
          list.push(obj)

          obj = {}
          obj["name"] = "Range di età"
          list.push(obj)

          for ageRange in ageRanges
            obj = {}
            obj["name"] = '----    '+ageRange.num+") "+ageRange.title
            list.push(obj)

          obj = {}
          obj["name"] = "Zona"
          list.push(obj)

          for k, v of zoneArray
            obj = {}
            obj["name"] = '----    '+v+") "+k
            list.push(obj)

          cb()
      (cb) ->

        total = counter+offset


        json2csv {
          data: list
          fields: fields
          quotes: ''
          del: ';'
        }, (err, csv) ->
          if err
            reply { msg: err}
            return
          fs.writeFile csvfile, csv, (err, s) ->
            if err
              throw err
              return

          total = counter+offset
          Report.findByIdAndUpdate( reportId,{ $set: {'dates.end': new Moment(), 'counters.exported' : counter, 'counters.total' : total } }).exec (err,res)->
            cb(null,csvfile)
            return
        return

    ], (err, res) ->
      if err
        reply Boom.badRequest(err)
        return
      return
    return

module.exports = methods