Mongoose        = require('mongoose')
Schema          = Mongoose.Schema

ReportSchema = new Schema(
  name:
    type: String
  url:
    type: String
  read:
    type: Boolean
    default: false
  reportType:
    type: String
  form:
    type: Schema.ObjectId
    ref: 'Form'
  counters:
    exported:
      type: Number
      default: 0
    total:
      type: Number
      default: 0
  dates:
    start:
      type: Date
      required: false
      default: Date.now
    end:
      type: Date
      required: false
)

module.exports.dao = Mongoose.model('Report', ReportSchema)