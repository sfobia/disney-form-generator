Survey   = require('./../../model')
Boom      = require 'boom'

methods =
  save: (request, reply) ->
    survey = new Survey.dao(
      title: request.payload.title
      type: request.payload.type
      status: request.payload.status
      questions: request.payload.questions
      campaign: request.payload.campaign
      published:  request.payload.published
    )
    survey.save (err, item) ->
      if err
        reply Boom.forbidden(err)
      else
        reply item
      return
    return

  getById: (request, reply) ->
    Survey.dao.findById(request.params.id).exec (err, item) ->
      if !err and item
        reply item
      else
        reply Boom.notFound(err)
      return
    return

  getAll: (request, reply) ->
    page = request.query.page || 1
    limit = request.query.limit || 10
    Survey.dao.paginate({}, {select: "title type questions campaign status published _id", page: page, limit: limit, lean: true}).then (item) ->
      if item.docs
        reply(item.docs).header('X-Total-Count', item.total)
      else
        reply Boom.notFound()
      return
    return

  putById: (request, reply) ->
    Survey.dao.findByIdAndUpdate(request.params.id, { $set: request.payload} ).exec (err, item) ->
      if !err and item
        reply item
      else
        reply Boom.notFound(err)
      return
    return

  deleteById: (request, reply) ->
    Survey.dao.remove _id: request.params.id, (err, item) ->
      if !err and item
        reply('ok')
      else
        reply Boom.notFound(err)
      return
    return

  query: (request, reply) ->
    page = request.query.page || 1
    limit = request.query.limit || 10
    Survey.dao.paginate(request.payload).then (item) ->
      if item.docs
        reply(item.docs).header('X-Total-Count', item.total)
      else
        reply Boom.notFound()
      return
    return

  vote: (request, reply) ->
# solo se i test/campagna è ancora attivo
    Survey.dao.findOne({"questions.answers._id": request.params.qid}).exec (err, item) ->
      if !err and item
        item.questions.id(request.params.id).answers.id(request.params.qid).responses.push(request.payload)
        item.save (err, res) ->
          reply res
      else
        reply Boom.notFound(err)
      return
    return

  vote333: (request, reply) ->
    Survey.dao.update({"questions.answers._id": request.params.qid}, {$addToSet: {"questions.0.answers.0.responses": request.payload}}).exec (err, item) ->
      if !err and item
        reply item
      else
        reply Boom.notFound(err)
      return
    return

module.exports = methods