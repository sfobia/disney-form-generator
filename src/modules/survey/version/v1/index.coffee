Joi         = require 'joi'
Handlers    = require './handlers'
Version     = "V1"
Base        = "survey"

answerModel = Joi.object(
  _id: Joi.any().optional()
  __v: Joi.any().optional()
  name: Joi.string().required().description('answer name')
  percent: Joi.string().optional().description('answer percent')
  image: Joi.string().optional().description('answer image')
)

questionModel = Joi.object(
  _id: Joi.any().optional()
  __v: Joi.any().optional()
  title: Joi.string().required().description('question title')
  type: Joi.string().required().description('question type')
  description: Joi.string().required().description('question description')
  answers: Joi.array().items(answerModel).required().description('answers')
)

surveyModel = Joi.object(
  _id: Joi.any().optional()
  __v: Joi.any().optional()
  id: Joi.string().required()
  title: Joi.string().required().description('title')
  type: Joi.string().required().description('type')
  status:
    actual: Joi.string().optional().description('actual status')
    afterClose: Joi.string().optional().description('after conclusion status')
  questions: Joi.array().items(questionModel).optional().description('questions')
  campaign: Joi.any().optional().description('campaign id')
  published: Joi.boolean().optional().description('published or not')
).meta(className: 'Survey').options(allowUnknown: true,abortEarly: false )

listModel = Joi.array().items(surveyModel).meta(className: 'List')

payload = Joi.object(
  _id: Joi.any().optional()
  __v: Joi.any().optional()
  title: Joi.string().required().description('title')
  type: Joi.string().required().description('type')
  status:
    actual: Joi.string().optional().description('actual status')
    afterClose: Joi.string().optional().description('after conclusion status')
  questions: Joi.array().items(questionModel).optional().description('questions')
  campaign: Joi.any().optional().description('campaign id')
  published: Joi.boolean().optional().description('published or not')
)

payloadQuery = Joi.object(
  title: Joi.string().optional().description('title')
  type: Joi.string().optional().description('type')
  status:
    actual: Joi.string().optional().description('actual status')
    afterClose: Joi.string().optional().description('after conclusion status')
  campaign: Joi.any().optional().description('campaign id')
  published: Joi.boolean().optional().description('published or not')
)

module.exports = [
  {
    method: 'POST'
    path: "/#{Version}/#{Base}"
    config:
      description: 'Create new survey'
      notes: 'Creates a new kind of Survey'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.save
      auth:
        strategy: 'token'
        scope: ['admin']
      validate:
        payload:
          payload
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
  }
  {
    method: 'GET'
    path: "/#{Version}/#{Base}"
    config :
      description: 'All surveys'
      notes: 'List of all surveys without any filter'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.getAll
      auth:
        strategy: 'token'
        scope: ['admin']
      response:
        schema: listModel
      validate:
        query:
          page: Joi.number().optional()
          limit: Joi.number().optional()
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
  }
  {
    method: 'GET'
    path: "/#{Version}/#{Base}/{id}"
    config :
      description: 'Get Survey by id'
      notes: 'Detail of a survey providing a valid _id'
      tags: ['api',"#{Version}","#{Base}"]
      response:
        schema: surveyModel
      validate:
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        params:
          id: Joi.string().required().description('survey id')
      handler: Handlers.getById
      auth:
        strategy: 'token'
        scope: ['admin']
  }
  {
    method: 'PUT'
    path: "/#{Version}/#{Base}/{id}"
    config :
      description: 'Put survey'
      notes: 'Modify a survey providing a valid _id'
      tags: ['api',"#{Version}","#{Base}"]
      response:
        schema: surveyModel
      validate:
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        params:
          id: Joi.string().required().description('survey id')
        payload: payload
      handler: Handlers.putById
      auth:
        strategy: 'token'
        scope: ['admin']
  }
  {
    method: 'DELETE'
    path: "/#{Version}/#{Base}/{id}"
    config:
      description: 'Delete a survey'
      notes: 'Delete the survey with given _id'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.deleteById
      auth:
        strategy: 'token'
        scope: ['admin']
      validate:
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        params:
          id: Joi.string().required().description('survey id')
  }
  {
    method: 'POST'
    path: "/#{Version}/#{Base}/query"
    config:
      description: 'Query surveys'
      notes: 'Search surveys with given parameters'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.query
      auth:
        strategy: 'token'
        scope: ['admin']
      validate:
        query:
          page: Joi.number().optional()
          limit: Joi.number().optional()
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        payload:
          payloadQuery
  }
]