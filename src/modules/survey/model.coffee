Mongoose        = require('mongoose')
Schema          = Mongoose.Schema
mongoosePaginate = require('mongoose-paginate')

ResponseSchema = new Schema(
  user:
    type: String
    ref: 'User'
  value:
    type: String
)

AnswerSchema = new Schema(
  name:
    type: String
    required: true
  percent:
    type: String
  image:
    type: String
  responses: [ResponseSchema]
)

QuestionSchema = new Schema(
  title:
    type: String
    required: true
  type:
    type: String
    enum: ['radio', 'checkbox', 'input', 'textarea', 'select']
    required: true
  description:
    type: String
    required: true
  answers: [AnswerSchema]
)

SurveySchema  = new Schema(
  title:
    type: String
    required: true
  type:
    type: String,
    required: true
  status:
    actual:
      type: String
      required: true
    afterClose:
      type: String
      required: false
  questions: [QuestionSchema]
  campaign:
    type: Schema.ObjectId
    ref: 'Campaign'
  published:
    type: Boolean
)

SurveySchema.plugin(mongoosePaginate)

module.exports.dao = Mongoose.model('Survey', SurveySchema)