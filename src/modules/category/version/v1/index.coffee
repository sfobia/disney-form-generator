Joi         = require 'joi'
Handlers    = require './handlers'
Schema = require './schema'
Version     = "V1"
Base        = "category"

module.exports = [
  {
    method: 'GET'
    path: "/#{Version}/#{Base}/{id}"
    config :
      description: 'Get category by id'
      notes: 'Detail of a category providing a valid _id'
      tags: ['api',"#{Version}","#{Base}"]
      response:
        schema: Schema.categoryModel
      validate:
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        params:
          id: Joi.string().required().description('category id')
      handler: Handlers.getById
      auth:
        strategy: 'token'
        scope: ['admin']
  }
  {
    method: 'GET'
    path: "/#{Version}/#{Base}"
    config :
      description: 'All categories'
      notes: 'List of all categories without any filter'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.getAll
      auth:
        strategy: 'token'
        scope: ['admin']
      response:
        schema: Schema.listModel
      validate:
        query:
          page: Joi.number().optional()
          limit: Joi.number().optional()
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
  }
]