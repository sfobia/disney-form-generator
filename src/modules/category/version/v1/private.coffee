Joi         = require 'joi'
Handlers    = require './handlers'
Schema      = require './schema'
Version     = "V1"
Base        = "category"

module.exports = [
  {
    method: 'POST'
    path: "/#{Version}/#{Base}"
    config:
      description: 'Create new category'
      notes: 'Creates a new category'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.save
      auth:
        strategy: 'token'
        scope: ['admin']
      validate:
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        payload: Schema.payload
  }
  {
    method: 'PUT'
    path: "/#{Version}/#{Base}/{id}"
    config :
      description: 'Put category'
      notes: 'Modify a category providing a valid _id'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.putById
      response:
        schema: Schema.categoryModel
      auth:
        strategy: 'token'
        scope: ['admin']
      validate:
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        params:
          id: Joi.string().required().description('category id')
        payload: Schema.payload
  }
  {
    method: 'DELETE'
    path: "/#{Version}/#{Base}/{id}"
    config:
      description: 'Delete a category'
      notes: 'Delete the category with given _id'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.deleteById
      auth:
        strategy: 'token'
        scope: ['admin']
      validate:
        headers: Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        params:
          id: Joi.string().required().description('category id')
  }
]