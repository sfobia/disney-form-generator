Category   = require('./../../model')
Boom = require 'boom'

methods =
  save: (request, reply) ->
    category = new Category.dao(request.payload)
    category.save (err, item) ->
      if err
        reply Boom.forbidden(err)
      else
        reply item
      return
    return

  getById: (request, reply) ->
    Category.dao.findById(request.params.id).exec (err, item) ->
      if !err and item
        reply item
      else
        reply Boom.notFound(err)
      return
    return

  putById: (request, reply) ->
    Category.dao.findByIdAndUpdate(_id: request.params.id, {$set: request.payload}, {new: true}).exec (err, item) ->
      if !err and item
        reply item
      else
        reply Boom.notFound(err)
      return
    return

  getAll: (request, reply) ->

    page = request.query.page || 1
    limit = request.query.limit || 10

    Category.dao.paginate({}, {page: page, limit: limit, lean: true}).then (item) ->
      if item.docs
        reply(item.docs).header('X-Total-Count', item.total);
      else
        reply Boom.notFound()
      return
    return

  deleteById: (request, reply) ->
    reply Boom.badRequest('remove disabled')
    return
    Category.dao.remove _id: request.params.id, (err, item) ->
      if !err and item
        reply('ok')
      else
        reply Boom.notFound(err)
      return
    return

module.exports = methods