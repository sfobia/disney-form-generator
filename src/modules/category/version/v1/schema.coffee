Joi = require 'joi'
Category = require('./../../model')

module.exports.categoryImageModel = Joi.object(
  _id: Joi.any().optional()
  __v: Joi.any().optional()
  name: Joi.string().optional()
  path: Joi.string().optional()
  ref: Joi.any().optional()
)

module.exports.categoryModel = Joi.object(
  _id: Joi.any().required()
  __v: Joi.any().optional()
  title: Joi.string().required().description('category title')
  description: Joi.string().optional().description('category description')
  media:
    logo: Joi.array().items(module.exports.categoryImageModel).optional()
    images: Joi.array().items(module.exports.categoryImageModel).optional()
  order: Joi.number().optional().description('category order')
  published: Joi.boolean().optional().description('indicate if the category is published')
  class: Joi.string().optional().description('category style')
).meta(className: 'Category').options(allowUnknown: true, abortEarly: false)

module.exports.payload = Joi.object(
  _id: Joi.any().optional()
  __v: Joi.any().optional()
  title: Joi.string().required().description('category title')
  description: Joi.string().optional().description('category description')
  media:
    logo: Joi.array().items(module.exports.categoryImageModel).optional()
    images: Joi.array().items(module.exports.categoryImageModel).optional()
  order: Joi.number().optional().description('category order')
  published: Joi.boolean().optional().description('indicate if the category is published')
  class: Joi.string().optional().description('category style')
)

module.exports.listModel = Joi.array().items(module.exports.categoryModel).meta(className: 'List')
