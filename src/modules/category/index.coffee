exports.register = (server, options, next) ->
  server.route require('./version/v1')
  server.route require('./version/v1/private')
  next()
  return

exports.register.attributes =
  name: 'category'
  version: '1.0.0'