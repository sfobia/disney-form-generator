Mongoose        = require('mongoose')
Schema          = Mongoose.Schema
mongoosePaginate = require('mongoose-paginate')

Image = [
  name:
    type: String
    required: false
  path:
    type: String
    required: false
  ref:
    type: Schema.ObjectId
    ref: 'Media'
]

CategorySchema  = new Schema(
  title:
    type: String
    required: true
  description:
    type: String
  media:
    logo: Image
    images: Image
  order:
    type: Number
    default: 0
  published:
    type: Boolean
  class:
    type: String
)

CategorySchema.plugin(mongoosePaginate)

module.exports.dao = Mongoose.model('Category', CategorySchema)