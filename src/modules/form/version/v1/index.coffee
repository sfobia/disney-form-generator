Joi         = require 'joi'
Handlers    = require './handlers'
Version     = "V1"
Base        = "form"
Form = require('./../../model')

valueModel = Joi.object(
  _id: Joi.any().optional()
  __v: Joi.any().optional()
  name: Joi.string().optional().description('name value')
  left: Joi.string().optional().description('left value')
  right: Joi.string().optional().description('right value')
  identifier: Joi.string().optional().description('identifier')
  valid: Joi.boolean().optional().description('valore valida per essere pre accettato')
  permitted: Joi.boolean().optional().description('valore obbligatorio')
  product: Joi.string().optional().description('valore obbligatorio')
)

radiovalueModel = Joi.object(
  _id: Joi.any().optional()
  __v: Joi.any().optional()
  identifier: Joi.string().optional().description('identifier')
  name: Joi.string().optional().description('name value')
  pos: Joi.number().optional().description('pos value')
  one: Joi.string().optional().description('one value')
  two: Joi.string().optional().description('two value')
  three: Joi.string().optional().description('three value')
  four: Joi.string().optional().description('four value')
)

fieldModel = Joi.object(
  _id: Joi.any().optional()
  __v: Joi.any().optional()
  name: Joi.string().required().description('type name')
  description: Joi.string().optional().allow('').description('description')
  type: Joi.string().required().description('type')
  order: Joi.number().integer().required().description('type order')
  value: Joi.array().items(valueModel).required()
  radiovalue: Joi.array().items(radiovalueModel).required()
  permitted: Joi.any().optional().description('permitted values for radio button')
  valid: Joi.any().optional().description('valid values to become tester')
  identifier: Joi.string().optional().description('identifier')
  depends: Joi.string().optional().description('depends')
  requiredField: Joi.boolean().optional().description('is the field optional or required')
  step: Joi.string().optional()
  info: Joi.object(
    title: Joi.string().allow('').optional().description('info of the group, title')
    abstract: Joi.string().allow('').optional().description('info of the group, abstract')
    linkname: Joi.string().allow('').optional().description('info of the group, link name')
    linkurl: Joi.string().allow('').optional().description('info of the group, link url')
  ).optional().options(allowUnknown: true, abortEarly: false)
  assignProduct: Joi.boolean().optional()
  altro: Joi.boolean().optional()
  altrolabel: Joi.string().optional().description('altroLabel')
)

productModel = Joi.object(
  _id: Joi.any().optional()
  __v: Joi.any().optional()
  name: Joi.string().required().description('product name')
  quantity: Joi.number().integer().required().description('product availability')
)

formModel = Joi.object(
  _id: Joi.any().optional()
  __v: Joi.any().optional()
  id: Joi.string().required()
  title: Joi.string().required().description('title')
  fields: Joi.array().items(fieldModel).optional()
  predefined: Joi.boolean().optional().description('indicate if the form is default')
  formType: Joi.string().valid(Form.enumType).required()
  step: Joi.string().optional().description('step')
  multiProduct: Joi.boolean().optional()
  products: Joi.array().items(productModel).optional()
).meta(className: 'Form').options(allowUnknown: true,abortEarly: false )

listModel = Joi.array().items(formModel).meta(className: 'List')

payloadType = Joi.object(
  _id: Joi.any().optional()
  __v: Joi.any().optional()
  name: Joi.string().required()
  type: Joi.string().required()
  description: Joi.string().optional().allow('').description('description')
  identifier: Joi.string().optional().description('identifier')
  depends: Joi.string().optional().description('depends')
  order: Joi.number().integer().required()
  value: Joi.array().items(valueModel).optional()
  radiovalue: Joi.array().items(radiovalueModel).required()
  permitted: Joi.any().optional().description('permitted values for radio button')
  valid: Joi.any().optional().description('valid values to become tester')
  requiredField: Joi.boolean().optional()
  step: Joi.string().optional()
  info: Joi.object(
    title: Joi.string().allow('').optional().description('info of the group, title')
    abstract: Joi.string().allow('').optional().description('info of the group, abstract')
    linkname: Joi.string().allow('').optional().description('info of the group, link name')
    linkurl: Joi.string().allow('').optional().description('info of the group, link url')
  ).optional().options(allowUnknown: true, abortEarly: false)
  assignProduct: Joi.boolean().optional()
  altro: Joi.boolean().optional()
  altroLabel: Joi.string().optional().description('altroLabel')
)

payload = Joi.object(
  _id: Joi.any().optional()
  __v: Joi.any().optional()
  title: Joi.string().required()
  fields: Joi.array().items(payloadType).optional()
  predefined: Joi.boolean().optional()
  formType: Joi.string().required()
  multiProduct: Joi.boolean().optional()
  products: Joi.array().items(productModel).optional()
#  step: Joi.string().optional()
)

payloadQuery =
  title: Joi.string().optional()
  fields: Joi.array().items(payloadType).optional()
  predefined: Joi.boolean().optional()
  formType: Joi.string().optional()
#  step: Joi.string().optional()

module.exports = [
  {
    method: 'POST'
    path: "/#{Version}/#{Base}"
    config:
      description: 'Create new form'
      notes: 'Creates a new kind of Form'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.save
      auth:
        strategy: 'token'
        scope: ['admin']
      validate:
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        payload: payload
  }
  {
    method: 'GET'
    path: "/#{Version}/#{Base}/{id}"
    config :
      description: 'Get Form id'
      notes: 'Detail of a form providing a valid _id'
      tags: ['api',"#{Version}","#{Base}"]
      response:
        schema: formModel
      validate:
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        params:
          id: Joi.string().required().description('form id')
      handler: Handlers.getById
      auth:
        strategy: 'token'
        scope: ['admin']
  }
  {
    method: 'GET'
    path: "/#{Version}/#{Base}"
    config :
      description: 'Get Form'
      notes: 'Detail of forms'
      tags: ['api',"#{Version}","#{Base}"]
      response:
        schema: listModel
      handler: Handlers.getAll
      auth:
        strategy: 'token'
        scope: ['admin']
      validate:
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
  }
  {
    method: 'PUT'
    path: "/#{Version}/#{Base}/{id}"
    config :
      description: 'Put form'
      notes: 'Modify a form providing a valid _id'
      tags: ['api',"#{Version}","#{Base}"]
      response:
        schema: formModel
      validate:
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        params:
          id: Joi.string().required().description('form id')
        payload: payload
      handler: Handlers.putById
      auth:
        strategy: 'token'
        scope: ['admin']
  }
  {
    method: 'POST'
    path: "/#{Version}/#{Base}/query"
    config:
      description: 'Query forms'
      notes: 'Search forms with given parameters'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.query
      auth:
        strategy: 'token'
        scope: ['admin']
      validate:
        payload: payloadQuery
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
  }
  {
    method: 'DELETE'
    path: "/#{Version}/#{Base}/{id}"
    config:
      description: 'Delete a form'
      notes: 'Delete the form with given _id'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.deleteById
      auth:
        strategy: 'token'
        scope: ['admin']
      validate:
        params:
          id: Joi.string().required().description('form id')
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
  }
  {
    method: 'GET'
    path: "/#{Version}/#{Base}/init"
    config :
      description: 'Creates basic system forms'
      notes: 'Creates the system default templates: registration and shipping'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.initForms
  }
]