Form   = require('./../../model')
Boom      = require 'boom'

methods =
  save: (request, reply) ->
    Form.dao.find({'formType': 'registration'}).exec (err, item) ->
      if item.length > 0 && request.payload.formType == 'registration'
        reply Boom.badRequest('form registration already inserted')
      else
        form = new Form.dao(request.payload)
        form.save (err, item) ->
          if err
            reply Boom.forbidden(err)
          else
            reply item
          return
        return

  putById: (request, reply) ->

    console.log( "#putById" )

    Form.dao.findByIdAndUpdate(request.params.id, { $set: request.payload },{ new: true } ).exec (err, item) ->
      if !err and item
        reply item
      else
        reply Boom.notFound(err)
      return
    return

  getAll: (request, reply) ->
    Form.dao.find({}, "title predefined formType _id").exec (err, item) ->
      if !err and item
        reply item
      else
        reply Boom.notFound(err)
      return
    return

  getById: (request, reply) ->
    Form.dao.findById(request.params.id).exec (err, item) ->
      if !err and item
        reply item
      else
        reply Boom.notFound(err)
      return
    return

  query: (request, reply) ->
    Form.dao.find(request.payload).exec (err, item) ->
      if !err and item
        reply item
      else
        reply Boom.notFound(err)
      return
    return

  deleteById: (request, reply) ->
    reply Boom.badRequest('remove disabled')
    return
    Form.dao.remove _id: request.params.id, (err, item) ->
      if !err and item
        reply('ok')
      else
        reply Boom.notFound(err)
      return
    return

  initForms: (request, reply) ->
    Form.dao.findOne({'formType': 'registration'}).exec (err, item) ->
      if !item
        registrationForm =
          new Form.dao({
            title: 'Registration'
            fields: [
              {
                name: 'sample field'
                type: 'input'
                order: 1
                value: [
                  name: 'sample value'
                ]
              }
            ]
            predefined: true
            formType: 'registration'
          })
        registrationForm.save (err, res) ->
          return

    Form.dao.findOne({'formType': 'shipping'}).exec (err, item) ->
      if !item
        shippingForm =
          new Form.dao({
            title: 'Shipping'
            fields: [
              {
                name: 'sample field'
                type: 'input'
                order: 1
                value: [
                  name: 'sample value'
                ]
              }
            ]
            predefined: true
            formType: 'shipping'
          })
        shippingForm.save (err, res) ->
          return

      reply 'ok'
    return

module.exports = methods