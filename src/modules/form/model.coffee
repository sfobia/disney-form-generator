Mongoose        = require('mongoose')
Schema          = Mongoose.Schema
mongoosePaginate = require('mongoose-paginate')

module.exports.enumType = ['registration', 'campaign', 'shipping', 'survey']

FieldSchema = new Schema(
  identifier:
    type: String
  name:
    type: String
    required: true
  type:
    type: String
    enum: ['input', 'radio', 'select', 'textarea', 'checkbox', 'ranges', 'cap', 'telefono', 'number', 'commento-voto', 'onlydescription', 'multiradio' ]
    required: true
  description:
    type: String
  order:
    type: Number
    default: 0
    required: true
  depends:
    type: String
  value: [
    identifier: String
    name: String
    left: String
    right: String
    valid: Boolean
    permitted: Boolean
    product: String
  ]
  radiovalue: [
    identifier: String
    name: String
    pos: Number
    one: String
    two: String
    three: String
    four: String
  ]
  permitted:
    type: Schema.Types.Mixed
  valid:
    type: Schema.Types.Mixed
  requiredField:
    type: Boolean
  info:
    title: String
    abstract: String
    linkname: String
    linkurl: String
  step:
    type: String
  assignProduct:
    type: Boolean
    default: false
  altro:
    type: Boolean
  altroLabel:
    type: String
)

FormSchema  = new Schema(
  title:
    type: String
    required: true
  fields: [FieldSchema]
  predefined:
    type: Boolean
  formType:
    type: String
    enum: module.exports.enumType
    default: "campaign"
    required: true
  multiProduct:
    type: Boolean
    default: false
  products: [
    name:
      type: String
    quantity:
      type: String
  ]
)

FormSchema.plugin(mongoosePaginate)

module.exports.dao = Mongoose.model('Form', FormSchema)