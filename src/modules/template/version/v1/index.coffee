Joi         = require 'joi'
Handlers    = require './handlers'
Version     = "V1"
Base        = "template"


templateModel = Joi.object(
  _id: Joi.any().optional()
  __v: Joi.any().optional()
  subject: Joi.string().required().description('subject')
  body: Joi.string().required().description('body')
  type: Joi.string().required().description('type')
  identifier: Joi.string().required().description('identifier')
  info: Joi.string().optional().description('info')
  published: Joi.boolean().optional().description('published or not')
).meta(className: 'Template').options(allowUnknown: true,abortEarly: false )

listModel = Joi.array().items(templateModel).meta(className: 'List')

payload = Joi.object(
  _id: Joi.any().optional()
  __v: Joi.any().optional()
  subject: Joi.string().required().description('subject')
  body: Joi.string().required().description('body')
  type: Joi.string().required().description('type')
  identifier: Joi.string().required().description('identifier')
  info: Joi.string().optional().description('info')
  published: Joi.boolean().optional().description('published or not')
)


module.exports = [
  {
    method: 'POST'
    path: "/#{Version}/#{Base}"
    config:
      description: 'Create new template'
      notes: 'Creates a new kind of template'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.save
      auth:
        strategy: 'token'
        scope: ['admin']
      validate:
        payload:
          payload
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
  }
  {
    method: 'GET'
    path: "/#{Version}/#{Base}"
    config :
      description: 'All templates'
      notes: 'List of all templates without any filter'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.getAll
      auth:
        strategy: 'token'
        scope: ['admin']
      response:
        schema: listModel
      validate:
        query:
          page: Joi.number().optional()
          limit: Joi.number().optional()
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
  }
  {
    method: 'GET'
    path: "/#{Version}/#{Base}/{id}"
    config :
      description: 'Get template by id'
      notes: 'Detail of a template providing a valid Id'
      tags: ['api',"#{Version}","#{Base}"]
      response:
        schema: templateModel
      validate:
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        params:
          id: Joi.string().required().description('template id')
      handler: Handlers.getById
      auth:
        strategy: 'token'
        scope: ['admin']
  }
  {
    method: 'PUT'
    path: "/#{Version}/#{Base}/{id}"
    config :
      description: 'Put template'
      notes: 'Modify a template providing a valid _id'
      tags: ['api',"#{Version}","#{Base}"]
      response:
        schema: templateModel
      validate:
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        params:
          id: Joi.string().required().description('template id')
        payload: payload
      handler: Handlers.putById
      auth:
        strategy: 'token'
        scope: ['admin']
  }
  {
    method: 'DELETE'
    path: "/#{Version}/#{Base}/{id}"
    config:
      description: 'Delete a template'
      notes: 'Delete the template with given _id'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.deleteById
      auth:
        strategy: 'token'
        scope: ['admin']
      validate:
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        params:
          id: Joi.string().required().description('template id')
  }
  {
    method: 'GET'
    path: "/#{Version}/#{Base}/init"
    config :
      description: 'Creates basic system templates'
      notes: 'Creates the system default templates'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.initTemplates
  }
]