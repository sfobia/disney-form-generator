Template   = require('./../../model')
Boom      = require 'boom'

methods =
  save: (request, reply) ->
    template = new Template.dao(request.payload)
    template.save (err, item) ->
      if err
        reply Boom.forbidden(err)
      else
        reply item
      return
    return

  getById: (request, reply) ->
    Template.dao.findById(request.params.id).exec (err, item) ->
      if !err and item
        reply item
      else
        reply Boom.notFound(err)
      return
    return

  getAll: (request, reply) ->
    page = request.query.page || 1
    limit = request.query.limit || 10
    Template.dao.paginate({}, {select: 'subject body type identifier info _id', page: page, limit: limit, lean: true}).then (item) ->
      if item.docs
        reply(item.docs).header('X-Total-Count', item.total)
      else
        reply Boom.notFound()
      return
    return

  putById: (request, reply) ->
    Template.dao.findByIdAndUpdate(request.params.id, { $set: request.payload } ).exec (err, item) ->
      if !err and item
        reply item
      else
        reply Boom.notFound(err)
      return
    return

  deleteById: (request, reply) ->
    reply Boom.badRequest('remove disabled')
    return
    Template.dao.remove _id: request.params.id, (err, item) ->
      if !err and item
        reply('ok')
      else
        reply Boom.notFound(err)
      return
    return

  initTemplates: (request, reply) ->

    Template.dao.findOne({'identifier': 'REGISTRATION_OK'}).exec (err, item) ->
      if !item
        registrationOkTemplate = new Template.dao(
          {
            subject: 'Registration'
            body: 'Ciao {{ firstName }}'
            type: 'site+email'
            identifier: 'REGISTRATION_OK'
            published: true
          }
        )
        registrationOkTemplate.save (err, res) ->
          return


    Template.dao.findOne({'identifier': 'CONFIRM_EMAIL'}).exec (err, item) ->
      if !item
        confirmEmailTemplate = new Template.dao(
          {
            subject: 'Confirm email'
            body: 'Ciao {{ firstName }}, <a href="{{ tokenLink }}">link</a>'
            type: 'email'
            identifier: 'CONFIRM_MAIL'
            published: true
          }
        )
        confirmEmailTemplate.save (err, res) ->
          return

    Template.dao.findOne({'identifier': 'CANDIDATE_ACCEPTED'}).exec (err, item) ->
      if !item
        candidateAcceptedTemplate = new Template.dao(
          {
            subject: 'Candidate accepted'
            body: ' '
            type: 'site+email'
            identifier: 'CANDIDATE_ACCEPTED'
            published: true
          }
        )
        candidateAcceptedTemplate.save (err, res) ->
          return

    Template.dao.findOne({'identifier': 'SURVEY_START'}).exec (err, item) ->
      if !item
        surveyStartTemplate = new Template.dao(
          {
            subject: 'Survey start'
            body: ' '
            type: 'site+email'
            identifier: 'SURVEY_START'
            published: true
          }
        )
        surveyStartTemplate.save (err, res) ->
          return

    Template.dao.findOne({'identifier': 'FORGOT_PASSWORD_MAIL'}).exec (err, item) ->
      if !item
        forgotPasswordTemplate = new Template.dao(
          {
            subject: 'Password dimenticata'
            body: 'Clicca per resettare la tua password, <a href="{{ tokenLink }}">link</a>'
            type: 'site+email'
            identifier: 'FORGOT_PASSWORD_MAIL'
            published: true
          }
        )
        forgotPasswordTemplate.save (err, res) ->
          return

    Template.dao.findOne({'identifier': 'SEND_TRACKING'}).exec (err, item) ->
      if !item
        sendTrackingTemplate = new Template.dao(
          {
            subject: 'Invio codice tracking'
            body: 'Ecco il link per monitorare il pacco con il tuo prodotto, <a href="{{ tokenLink }}">link</a>'
            type: 'site+email'
            identifier: 'SEND_TRACKING'
            published: true
          }
        )
        sendTrackingTemplate.save (err, res) ->
          return

    Template.dao.findOne({'identifier': 'SEND_SURVEY'}).exec (err, item) ->
      if !item
        sendTrackingTemplate = new Template.dao(
          {
            subject: 'Invio survey'
            body: 'compila il survey'
            type: 'site+email'
            identifier: 'SEND_SURVEY'
            published: true
          }
        )
        sendTrackingTemplate.save (err, res) ->
          return


    reply "ok"
    return

module.exports = methods