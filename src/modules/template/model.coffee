Mongoose        = require('mongoose')
Schema          = Mongoose.Schema
mongoosePaginate = require('mongoose-paginate')

TemplateSchema  = new Schema(
  subject:
    type: String
    required: true
  body:
    type: String,
    required: true
  type:
    type: String
    enum: ['email', 'site', 'site+email']
    required: true
  identifier:
    type: String,
    required: true
  info:
    type: String,
    required: false
  published:
    type: Boolean
)
TemplateSchema.plugin(mongoosePaginate)

module.exports.dao = Mongoose.model('Template', TemplateSchema)