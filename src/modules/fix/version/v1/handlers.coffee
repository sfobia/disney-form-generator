
dist = process.env.NODE_FOLDER || 'dist'

Campaign  = require process.cwd() + '/'+dist+'/modules/campaign/model'
Candidate = require process.cwd() + '/'+dist+'/modules/candidate/model'
Boom      = require 'boom'
Moment    = require 'moment'

methods =

  # accetta i candidati della campagna vitermine, inizialmente fissata a 100 pezzi mentre con questo si corregge
  # accettando altri 400 utenti
  fixAcceptedCampaignVitermine: (request, reply) ->
    reply()
    return
    counter = 0
    candidateIds = []

    query = {
      requisite: true
      rejected: true
      active: false
      preconfirmed: false
      campaign: request.payload.campaign
    }

    set = {
      rejected: false
      active: true
      preconfirmed: true
      'dates.accepted': new Moment()
    }

    stream = Candidate.dao.find(query).sort('dates.candidation').lean().limit(400).stream()
    stream.on 'data', (item) ->
      counter++
      candidateIds.push item._id
      return

    stream.on 'error', (err) ->
      reply Boom.badRequest(err)
      return

    stream.on 'close', ->
      Candidate.dao.update({_id: {"$in": candidateIds}}, $set: set, {multi: true}).exec (err, res) ->
        Campaign.dao.update(_id: request.payload.campaign, {
          $addToSet: {
            "candidates":
              $each: candidateIds
          }
        }).exec (err, itemCampaign) ->
          reply { status: 'Modificati '+counter+' candidati', campaign: request.payload.campaign, count: counter, ids:candidateIds}
          return

    return

module.exports = methods