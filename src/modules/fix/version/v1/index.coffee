Joi         = require 'joi'
Handlers    = require './handlers'
Version     = "V1"
Base        = "fix"


module.exports = [
  {
    method: 'POST'
    path: "/#{Version}/#{Base}/fixAcceptedCampaignVitermine"
    config:
      description: 'Fix wrong number accepted'
      notes: 'Fix wrong number accepted users for the campaign Vitermine'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.fixAcceptedCampaignVitermine
      auth:
        strategy: 'token'
        scope: ['admin']
      validate:
        payload:
          campaign: Joi.any().required()
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
  }
]