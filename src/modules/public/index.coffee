exports.register = (server, options, next) ->
  server.route require('./routes')
  next()
  return

exports.register.attributes =
  name: 'public'
  version: '1.0.0'