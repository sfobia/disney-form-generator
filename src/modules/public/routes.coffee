module.exports = [
  {
    method: '*'
    path: '/{p*}'
    handler: (request, reply) ->
      reply.view('error-404').code(404)
  }
  {
    method: 'GET'
    path: '/assets/{param*}'
    handler:
      directory:
        path: 'public'
  }
  {
    method: 'GET'
    path: '/'
    handler: (request, reply) ->
      reply.view 'public'
  }
  {
    method: 'GET'
    path: '/frontend/partials/{files*}'
    handler: (request, reply) ->
      reply.view 'frontend/partials/' + request.params.files.replace(".html", '')
  }
  {
    method: 'GET'
    path: '/profile'
    config:
      description: 'get a profile'
      notes: 'the profile with given _id'
      tags: ['public', "V1"]
      handler: (request, reply) ->
        reply.view 'frontend/profile'
  }
]