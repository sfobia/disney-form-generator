request   = require('supertest')

JoguApikey    = process.env.NODE_ENV_JOGU_API_KEY
JoguApiSecret = process.env.NODE_ENV_JOGU_API_SECRET
JoguApiUrl    = process.env.NODE_ENV_JOGU_API_URL

methods =
  playerCreate: (valori) ->
    #console.log 'playerCreate',valori
    if !JoguApikey
      return

    data =
      "nickname": valori.nickname
      "metadati": valori.metadati || {}
      "utente": valori.utente
      "avatar": valori.avatar || ""

    request(JoguApiUrl).post('/service/rest/Player/create')
    .set('xx-jogu-api-key', JoguApikey)
    .set('xx-jogu-api-password', JoguApiSecret)
    .set('content-type','application/json')
    .send(data).end (err, res) ->
      if err
        throw err
    return


  sendAttivita: (attivita,valori) ->
    #console.log attivita,valori
    if !JoguApikey
      return

    data =
      "utente": valori.utente
      "canale": valori.canale || attivita
      "riferimento": valori.riferimento || valori.utente
      "tipoAttivita": attivita
      "metadati": valori.metadati || {}
    request(JoguApiUrl).post('/service/rest/Attivita/create')
    .set('xx-jogu-api-key', JoguApikey)
    .set('xx-jogu-api-password', JoguApiSecret)
    .set('content-type','application/json').send(data).end (err, res) ->
      if err
        throw err
    return

module.exports = methods