Joi         = require 'joi'

module.exports = [
  {
    method: 'GET'
    path: '/assets/{param*}'
    handler:
      directory:
        path: 'public/assets'
  }
  {
    method: 'GET'
    path: '/components/{param*}'
    handler:
      directory:
        path: 'public/components'
  }
  {
    method: 'GET'
    path: '/media/{param*}'
    handler:
      directory:
        path: 'public/media'
  }
  {
    method: 'GET'
    path: '/'
    handler:  (request, reply) ->
      reply.view 'index'
  }
  {
    method: 'GET'
    path: '/admin'
    config:
      handler: (request, reply) ->
        reply.view 'admin'
  }
  {
    method: 'GET'
    path: '/partials/{files*}'
    handler:  (request, reply) ->
      reply.view 'partials/'+request.params.files.replace(".html",'')
  }
]