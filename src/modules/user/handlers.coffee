User      = require('./user')
Profile   = require('../profile/model')
Boom      = require 'boom'
https     = require('https')
crypto    = require('crypto')

dist = process.env.NODE_FOLDER || 'dist'

NotifyHandlers  = require(process.cwd()+ '/'+dist+'/modules/notify/version/v1/handlers')
JoguHandlers  = require(process.cwd()+ '/'+dist+'/modules/jogu/version/v1/handlers')
Candidate      = require process.cwd() + '/'+dist+'/modules/candidate/model'

urlBack   = process.env.NODE_ENV_DOMAIN_PRIMARY || ""
urlBackFE = process.env.NODE_ENV_DOMAIN_PRIMARY || ""


internals = {}

exports.validate = (token, callback) ->
  User.dao.findOne { token: token, confirmed: true }, (err, user) ->
    callback(null, null) if err
    callback(null, null) if !user
    callback(err, true, user) if user
  return


exports.simpleValidate = (request, username, password, callback) ->
  if username == userSwagger.username
    callback(false, true, userSwagger)
  else
    callback(false, false)
  return

# login base user + pass
exports.login = (request, reply) ->
  User.dao.getAuthenticated request.payload.email, request.payload.password, (err, user, reason) ->
    txt = "Troppi tentativi, aspetta qualche minuto"  if reason == 2
    txt = "Dati non validi"                    if reason != 2
    if err
      reply Boom.badRequest(txt)
      return
    if user
      reply token: user.token
      return

    reply Boom.badRequest(txt)
    return
  return

exports.resendConfirmationEmail = (request, reply) ->

  User.dao.findOne({ _id: request.payload.user}).exec (err, item) ->
    if err
      reply Boom.badRequest(err)
      return

    if item

      crypto.randomBytes 40, (ex, buf) ->
        tokenConfirm = buf.toString('hex')

        User.dao.findOneAndUpdate({_id: item._id}, {tokenConfirm: tokenConfirm}, {new: true}).exec (err, user) ->
          url = process.env.NODE_ENV_DOMAIN_PUBLIC || "http://localhost:8100"
          user.tokenLink  = url+'/account/activate/'+user.tokenConfirm
          request.server.plugins.notify.service.send( 'CONFIRM_MAIL', user._id, user )

          reply {status: 'ok'}
          return
    if !item
      reply { status: 'ko' }
    return
  return

exports.forgot = (request, reply) ->
  url = process.env.NODE_ENV_DOMAIN_PRIMARY || "http://localhost:8100"
  email = request.payload.email.toLowerCase()
  User.dao.findOne({email: email }).exec (err, item) ->
    if err
      reply Boom.badRequest(err)
    if item
      crypto.randomBytes 40, (ex, buf) ->
        tokenConfirm = buf.toString('hex')
        User.dao.findOneAndUpdate({_id: item._id}, {tokenConfirm: tokenConfirm}).exec (err, user) ->
          dataMail = {}
          dataMail.tokenLink  = url+'/it/profilo/modifica-password.html?tk='+tokenConfirm
          dataMail.email      = user.email
          request.server.plugins.notify.service.send( 'FORGOT_PASSWORD_MAIL', user._id, dataMail )
          reply {status: 'ok'}
          return
    if !item
      reply {status: 'ok'}
    return
  return

exports.setFalseConversion = (request, reply) ->
  User.dao.findOneAndUpdate( {_id: request.auth.credentials._id }, { $set: { sendConversion: false } }, { new: true } ).exec (err, user) ->
    if err
      reply Boom.badRequest(err)
    else
      reply {status: 'ok'}
    return
  return

# registrazione classica
exports.registration = (request, reply) ->
  crypto.randomBytes 40, (ex, buf) ->
    request.payload.user.tokenConfirm = buf.toString('hex')

    user = new User.dao( request.payload.user )
    user.save (err, item ) ->
      if err
        msg = internals.getErrorMessage err
        reply Boom.methodNotAllowed msg
        return

      if item
        profile = request.payload.profile
        profile.user = item._id
        userprofile = new Profile.dao( profile )
        userprofile.save (errp, itempro ) ->
          if errp
            msg = internals.getErrorMessage errp
            reply Boom.methodNotAllowed msg
            return

          if itempro
            #item = item.toObject()
            internals.sendToken(item._id, itempro)
            reply status:200
            return
      return
    return
  return

internals.sendToken = ( userId, itempro ) ->

  User.dao.findOneAndUpdate({_id: userId}, { profile: itempro._id }  ).exec ( err, user ) ->

    # jogu
    jogu =
      utente: user._id
      nickname: user.userName

    JoguHandlers.playerCreate( jogu )

    url                 = process.env.NODE_ENV_DOMAIN_PUBLIC || "http://localhost:8100"
    dataMail            = {}
    dataMail.email      = user.email
    dataMail.firstName  = itempro.name.first
    dataMail.lastName   = itempro.name.last
    dataMail.tokenLink  = url+'/account/activate/'+user.tokenConfirm

    NotifyHandlers.send( 'CONFIRM_MAIL', user._id, dataMail  )

    return
  return



exports.confirmUser = ( request, reply ) ->
  url = process.env.NODE_ENV_DOMAIN_PRIMARY || ""
  User.dao.findOneAndUpdate({tokenConfirm: request.params.token }, {$set: { confirmed: true, tokenConfirm: '' } }).populate("profile","name").exec ( err, user) ->
    if err
      reply().redirect( url + '?error=Token+non+valido+o+gia+usato')
      return

    if user
      jogu = "utente": user._id
      JoguHandlers.sendAttivita('registrazione', jogu)
      # todo se ho già madato la mail non la devo rinviare, controllare nella collezione notifiche

      if user.sendConversion && user.sendConversion == true

        dataMail            = {}
        dataMail.email      = user.email
        dataMail.firstName  = user.profile.name.first
        dataMail.lastName   = user.profile.name.last
#        document.cookie = 'tokenom' + '=' + request.params.token  + '; expires=Fri, 31 Dec 2030 23:59:59 GMT; path=/;'
        NotifyHandlers.send('REGISTRATION_OK', user._id, dataMail)

      reply().redirect( url + '/?token='+user.token )
      return

    else
      reply().redirect( url + '?error=Token+non+valido+o+gia+usato')
      return
  return

exports.changePassword = (request, reply ) ->
  User.dao.findOne({tokenConfirm: request.params.token }).populate("profile","name").exec ( err, user) ->
    if err or !user
      reply Boom.badRequest()
      return

    crypto.randomBytes 40, (ex, buf) ->
      user.tokenConfirm = buf.toString('hex')
      user.password     = request.payload.password
      user.confirmed    = true
      user.save (err2, item) ->

        if err2
          reply Boom.badRequest()

        if !item
          reply Boom.badRequest()

        if item
          reply {'status': 'ok'}
  return

# registrazione classica
exports.retrieve = () ->
  return

exports.logout = (request, reply) ->
  crypto.randomBytes 50, (ex, buf) ->
    token = buf.toString('hex')
    User.dao.findOneAndUpdate({_id: request.auth.credentials._id}, { $set: { token: token } }).exec (err, user) ->
      if !err and user
        reply 200
      else
        reply Boom.badRequest(err)
  return

# Registrazione / login tramite social
exports.registrationProvider = (request, reply) ->
  User.dao.findOne({
    $or: [
      { email: request.profile.email },
      { 'provider.facebook.id': request.profile.id  },
      { 'provider.twitter.id': request.profile.id  },
    ]
  }).populate("profile").exec ( err, user) ->
    if err
      reply Boom.badRequest()
      return

    if user
      internals.updateUser( user, request )
      internals.redirectUser( user, reply )
      return

    profile = internals.profileByFacebook request  if request.provider == 'facebook'
    profile = internals.profileByTwitter request   if request.provider == 'twitter'

    internals.createUser request, profile, reply

  return

internals.generatePassword = () ->
  length = 25
  charset = "abcdefghijklnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
  retVal = ""
  i = 0
  n = charset.length
  while i < length
    retVal += charset.charAt(Math.floor(Math.random() * n))
    ++i

  return retVal

internals.getErrorMessage = (err) ->
  message = ''
  if err.code
    switch err.code
      when 11000
        message = 'Email o username già presente nel sistema'
      when 11001
        message = 'Email o username già presente nel sistema'
      else
        message = 'Errore riprova'
  else
    for errName of err.errors
      if err.errors[errName].message
        message = err.errors[errName].message
  message

internals.profileByFacebook = (request) ->
  profile =
    'name.first': request.profile.raw.first_name
    'name.last': request.profile.raw.last_name

internals.profileByTwitter = (request) ->
  profile =
    cover: request.profile.raw.profile_image_url.replace( '_normal', '' )
    'name.first': request.profile.raw.name

internals.createUser = (request, profile, reply ) ->
  datiUser =
    email: request.profile.email || request.profile.id+'@'+request.provider+'.com'
    password: internals.generatePassword()
    provider: {}
    userName: request.profile.username || request.profile.id
    confirmed: false

  crypto.randomBytes 40, (ex, buf) ->
    datiUser.tokenConfirm = buf.toString('hex')

    datiUser.provider[request.provider] = id: request.profile.id

    if request.provider == 'twitter'
      datiUser.confirmed = true

    user = new User.dao(datiUser)
    user.save (err, item) ->
      if err
        reply Boom.methodNotAllowed(err)
        return

      if item

        if request.provider == 'facebook'

          url = process.env.NODE_ENV_DOMAIN_PUBLIC || "http://localhost:8100"
          dataMail            = {}
          dataMail.email      = item.email
          dataMail.firstName  = profile['name.first'] || ""
          dataMail.tokenLink = url+'/account/activate/'+item.tokenConfirm
          NotifyHandlers.send( 'CONFIRM_MAIL',null, dataMail )


        profile.user = item._id
        userProfile = new Profile.dao( profile )
        userProfile.save (err, profileitem ) ->
          # aggiorno user con id profilo
          user.profile = profileitem._id
          user.save (e,u) ->
            if request.provider == 'facebook'
              internals.getImageFromFacebook( user._id, request.token )

            internals.actionsForAutoLogin(item,profileitem,request.provider)
            internals.redirectUser( item, reply )

        return
      return
    return

internals.updateUserImage = ( user_id, image_url ) ->
  Profile.dao.findOneAndUpdate({'user': user_id }, {$set: { cover: image_url} }, {upsert: true}).exec ( err, item) ->
    return

internals.getImageFromFacebook = ( user_id, token ) ->
  https.get 'https://graph.facebook.com/v2.5/me/picture?width=300&redirect=false&access_token='+token, ( res ) ->
    res.on 'data', (d) ->
      response = JSON.parse( d.toString() )
      internals.updateUserImage( user_id, response.data.url )

  return


internals.redirectUser = ( user, reply ) ->
  urlDest = urlBack + '/admin?token='+user.token
  if user.scope.indexOf "user" > -1

    if user.confirmed == false
      urlDest = urlBackFE + '?confirmMail=true'
      reply().redirect(urlDest)
      return

    urlDest = urlBackFE + '/it/save-token.html?token='+user.token
    reply().redirect(urlDest)
  return


internals.updateUser = ( user, request ) ->
  if request.provider == 'facebook'
    internals.getImageFromFacebook( user._id, request.token )
  else if request.provider == 'twitter'
    internals.updateUserImage( user._id, request.profile.raw.profile_image_url.replace( '_normal', '' ) )

internals.actionsForAutoLogin = (user,profile,provider) ->
  jogu =
    utente: user._id
    nickname: user.userName
  JoguHandlers.playerCreate( jogu )




# profile user logged
exports.profile = (request, reply) ->
  User.dao.findOne(request.auth.credentials._id).populate(User.pop()).exec (err, user) ->
    if !err and user
      reply user
    else
      reply Boom.notFound(err)
  return