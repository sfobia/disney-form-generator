Handlers  = require('./handlers')
Joi       = require 'joi'
Boom      = require 'boom'

exports.register = (server, options, next) ->
  server.register require('hapi-auth-bearer-simple'), (err) ->
    server.auth.strategy 'token', 'bearerAuth', validateFunction: Handlers.validate
    return

  ###
  server.register require('hapi-auth-basic'), (err) ->
    server.auth.strategy 'simple', 'basic', validateFunc: Handlers.simpleValidate
    return
  ###

  server.auth.strategy 'twitter', 'bell',
    provider: 'twitter'
    password: 'password'
    isSecure: false
    clientId: process.env.NODE_ENV_TWITTER_ID || '9pXKwQ8Iv3dXRalJLAg3Wq7JW'
    clientSecret: process.env.NODE_ENV_TWITTER_SECRET ||  'NhJWqOmSU1sSqEz0BFoCKGdP0qB4gKRgvCmbRLa30KhojIq3ve'

  server.auth.strategy 'facebook', 'bell',
    provider: 'facebook'
    password: 'password'
    isSecure: false
    clientId: process.env.NODE_ENV_FB_ID || '1630541600491297'
    clientSecret: process.env.NODE_ENV_FB_SECRET || 'e4340239712f42a8901518eb636976e9'
    location: process.env.NODE_ENV_DOMAIN_PUBLIC || "http://localhost:8000"

  server.route([
    {
      method: 'POST'
      path: '/account/register'
      config:
        description: 'Register user classica'
        notes: 'Api per la registrazione utente classica'
        tags: ['api']
        handler: Handlers.registration
        validate:
          payload:
            user:
              email: Joi.string().email().required()
              password: Joi.string().min(6).required()
              userName: Joi.string().min(6).required()
            profile:
              name:
                first: Joi.string().required()
                last: Joi.string().required()
              gender: Joi.string().allow('Male', 'Female')
              birthDate: Joi.date().required()
              province: Joi.string().required()
              accepted:
                marketing: Joi.boolean().required()
                dem: Joi.boolean().required()
                profiling: Joi.boolean().required()
                #privacy: Joi.boolean().required().allow(true)
                #conditions: Joi.boolean().required().allow(true)
              newsletter: Joi.any().optional()
    }
    {
      method: 'POST'
      path: '/account/login'
      config:
        description: 'Login classico'
        notes: 'Login tramite email / password'
        tags: ['api']
        handler: Handlers.login
        validate:
          payload:
            email: Joi.string().email().required()
            password: Joi.string().required()
    }
    {
      method: 'POST'
      path: '/account/forgot'
      config:
        description: 'Password dimenticata'
        notes: 'Procedura di recupero password'
        tags: ['api']
        handler: Handlers.forgot
        validate:
          payload:
            email: Joi.string().email().required()
    }
    {
      method: 'POST'
      path: '/account/conversion'
      config:
        description: 'Imposta il send conversion'
        notes: 'Imposta il send conversion a false'
        tags: ['api']
        auth:
          strategy: 'token'
          scope: ['user']
        handler: Handlers.setFalseConversion
        validate:
          headers:
            Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
    }
    {
      method: 'POST'
      path: '/account/resend-token'
      config:
        description: 'Reinvio token'
        notes: 'Procedura di reinvio token'
        tags: ['api']
        handler: Handlers.resendConfirmationEmail
        auth:
          strategy: 'token'
          scope: ['admin']
        validate:
          payload:
            user: Joi.string().required()
    }
    {
      method: 'GET'
      path: '/account/logout'
      config:
        description: 'Logout utente'
        notes: 'Logout utente'
        tags: ['api', 'V1']
        auth:
          strategy: 'token'
          scope: ['user']
        validate:
          headers:
            Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        handler: Handlers.logout
    }
    {
      method: '*'
      path: '/auth/twitter'
      config:
        auth: 'twitter'
        handler: (request, reply) ->
          if !request.auth.isAuthenticated
            return reply(Boom.unauthorized('Authentication failed: ' + request.auth.error.message))
          Handlers.registrationProvider(request.auth.credentials,reply)
    }
    {
      method: '*'
      path: '/auth/facebook'
      config:
        auth:
          strategy: 'facebook'
          mode: 'try'
        handler: (request, reply) ->
          if !request.auth.isAuthenticated
            return reply(Boom.unauthorized('Authentication failed: ' + request.auth.error.message))
          Handlers.registrationProvider(request.auth.credentials,reply)
    }
    {
      method: 'GET'
      path: '/account/activate/{token}'
      config:
        handler: Handlers.confirmUser
        validate:
          params:
            token: Joi.string().required().description('token')
    }
    {
      method: 'POST'
      path: '/account/changepassword/{token}'
      config:
        handler: Handlers.changePassword
        validate:
          params:
            token: Joi.string().min(20).required().description('token')
          payload:
            password: Joi.string().min(6).required().description('password')
    }
  ])

  next()
  return

exports.register.attributes =
  name: 'auth'