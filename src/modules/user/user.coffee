Mongoose    = require 'mongoose'
crypto      = require('crypto')
credential  = require('credential')
mongoosePaginate = require('mongoose-paginate')

SALT_WORK_FACTOR = 10
MAX_LOGIN_ATTEMPTS = 5
LOCK_TIME = 2 * 60 * 60 * 1000


unique = (modelName, field, caseSensitive) ->
  (value, respond) ->
    if value and value.length
      query = Mongoose.model(modelName).where(field, new RegExp('^' + value + '$', if caseSensitive then 'i' else undefined))
      if !@isNew
        query = query.where('_id').ne(@_id)
      query.count (err, n) ->
        respond n < 1
        return
    else
      respond false
    return


module.exports.enumStatus = ['reliable', 'unreliable']

Schema = Mongoose.Schema
UserSchema = new Schema(
  profile:
    type: Schema.ObjectId
    ref: 'Profile'
  userName:
    type: String
    index: true
    lowercase: true
    trim: true
    validate: [unique('User', 'userName',true), 'Questa username è già presente']
  email:
    type: String
    required: true
    index: true
    lowercase: true
    trim: true
    validate: [unique('User', 'email',true), 'Questa email è già presente']
  password:
    type: String
    required: true
  confirmed:
    type: Boolean
    default: false
  tokenConfirm:
    type: String
  token:
    type: String
    required: false
  sendConversion:
    type: Boolean
    default: true
  scope:
    type: Array
    required: true
    default: ['user']
  status:
    type: String
    enum: module.exports.enumStatus
    required: true
    default: 'reliable'
  provider:
    type: Array
  creationDate:
    type: Date
    required: true
    default: Date.now
  loginAttempts:
    type: Number
    required: true
    default: 0
  lockUntil:
    type: Number
  )





UserSchema.virtual('isLocked').get ->
# check for a future lockUntil timestamp
  ! !(@lockUntil and @lockUntil > Date.now())


UserSchema.pre 'save', (next) ->
  user = this

  # only hash the password if it has been modified (or is new)
  if !user.isModified('password')
    return next()

  pw = credential()
  pw.hash user.password, (err, hash) ->
    if (err)
      return next(err)

    user.password = hash
    crypto.randomBytes 50, (ex, buf) ->
#      user.token = buf.toString('hex')
      next()
      return
    return
  return


UserSchema.methods.comparePassword = (candidatePassword, cb) ->
  pw = credential()
  pw.verify @password, candidatePassword, (err, isValid) ->
    if (err)
      return cb(err)
    if isValid
      cb null, isValid
    else
      return cb(err)
  return


UserSchema.methods.incLoginAttempts = (cb) ->
# if we have a previous lock that has expired, restart at 1
  if @lockUntil and @lockUntil < Date.now()
    return @update({
      $set: loginAttempts: 1
      $unset: lockUntil: 1
    }, cb)
  # otherwise we're incrementing
  updates = $inc: loginAttempts: 1
  # lock the account if we've reached max attempts and it's not locked already
  if @loginAttempts + 1 >= MAX_LOGIN_ATTEMPTS and !@isLocked
    updates.$set = lockUntil: Date.now() + LOCK_TIME
  @update updates, cb

# expose enum on the model, and provide an internal convenience reference
reasons = UserSchema.statics.failedLogin =
  NOT_FOUND: 0
  PASSWORD_INCORRECT: 1
  MAX_ATTEMPTS: 2

UserSchema.statics.getAuthenticated = (username, password, cb) ->
  @findOne { email: username, confirmed : true }, (err, user) ->
    if err
      return cb(err)
    # make sure the user exists
    if !user
      return cb(null, null, reasons.NOT_FOUND)
    # check if the account is currently locked
    if user.isLocked
      # just increment login attempts if account is already locked
      return user.incLoginAttempts((err) ->
        if err
          return cb(err)
        cb null, null, reasons.MAX_ATTEMPTS
      )
    # test for a matching password
    user.comparePassword password, (err, isMatch) ->
      if err
        return cb(err)
      # check if the password was a match
      if isMatch
        # if there's no lock or failed attempts, just return the user
        if !user.loginAttempts and !user.lockUntil
          return cb(null, user)
        # reset attempts and lock info
        updates =
          $set: loginAttempts: 0
          $unset: lockUntil: 1
        return user.update(updates, (err) ->
          if err
            return cb(err)
          cb null, user
        )
      # password is incorrect, so increment login attempts before responding
      user.incLoginAttempts (err) ->
        if err
          return cb(err)
        cb null, null, reasons.PASSWORD_INCORRECT
      return
    return
  return


module.exports.blacklist = ["password","token","scope", "loginAttempts", "typology","tokenConfirm",'status',"provider"]
module.exports.pop = ->
  if ! module.exports.blacklist
    return ''
  else
    '-'+module.exports.blacklist.join(" -")


UserSchema.plugin(mongoosePaginate)
module.exports.dao = Mongoose.model('User', UserSchema)