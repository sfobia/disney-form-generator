Joi         = require 'joi'

exports.register = (server, options, next) ->
  server.route require('./version/v1')

  server.route(
    {
      method: 'GET'
      path: "/campaign/guide"
      handler: (request,reply) ->
        fields = []
        for r in server.connections[0].table()
          if r.settings.tags and r.settings.tags.indexOf("campaign") != -1
            data = Joi.compile(r.settings.validate.payload).describe()
            fields.push(data)
        reply fields
    }
  )


  next()
  return

exports.register.attributes =
  name: 'campaign'
  version: '1.0.0'