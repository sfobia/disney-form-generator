Joi         = require 'joi'
Handlers    = require './handlers'
Schema      = require './schema'
Version     = "V1"
Base        = "campaign"

module.exports = [
  {
    method: 'GET'
    path: "/#{Version}/admin/stats"
    config:
      description: 'All stats db'
      notes: 'all count db'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.getAllStats
      validate:
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
      auth:
        strategy: 'token'
        scope: ['admin']
  }
  {
    method: 'POST'
    path: "/#{Version}/#{Base}"
    config:
      description: 'Create new campaign'
      notes: 'Creates a new campaign'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.save
      validate:
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        payload: Schema.payload
      auth:
        strategy: 'token'
        scope: ['admin']
  }
  {
    method: 'GET'
    path: "/#{Version}/#{Base}/{id}"
    config :
      description: 'Get Campaign by id'
      notes: 'Detail of a campaign providing a valid _id'
      tags: ['api',"#{Version}","#{Base}"]
      response:
        schema: Schema.campaignModel
      validate:
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        params:
          id: Joi.string().required().description('campaign id')
      handler: Handlers.getById
#      auth:
#        strategy: 'token'
#        scope: ['admin']
  }
  {
    method: 'PUT'
    path: "/#{Version}/#{Base}/{id}"
    config :
      description: 'Put campaign'
      notes: 'Modify a campaign providing a valid _id'
      tags: ['api',"#{Version}","#{Base}"]
      response:
        schema: Schema.campaignModel
      validate:
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        params:
          id: Joi.string().required().description('campaign id')
        payload: Schema.payload
      handler: Handlers.putById
      auth:
        strategy: 'token'
        scope: ['admin']
  }
  {
    method: 'PUT'
    path: "/#{Version}/#{Base}/status/{id}"
    config :
      description: 'Put status campaign'
      notes: 'Modify a status campaign providing a valid _id'
      tags: ['api',"#{Version}","#{Base}"]
      validate:
        headers: Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        params:
          id: Joi.string().required().description('campaign id')
        payload:
          status: Joi.string().required()
      handler: Handlers.putStatusById
      auth:
        strategy: 'token'
        scope: ['admin']
  }
  {
    method: 'DELETE'
    path: "/#{Version}/#{Base}/{id}"
    config:
      description: 'Delete a campaign'
      notes: 'Delete the campaign with given _id'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.deleteById
      auth:
        strategy: 'token'
        scope: ['admin']
      validate:
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        params:
          id: Joi.string().required().description('campaign id')
  }
  {
    method: 'GET'
    path: "/#{Version}/#{Base}"
    config :
      description: 'All campaigns'
      notes: 'List of all campaigns without any filter'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.getAll
      auth:
        strategy: 'token'
        scope: ['admin']
      validate:
        query:
          page: Joi.number().optional()
          limit: Joi.number().optional()
          order: Joi.string().optional()
          sort: Joi.string().optional()
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
      response:
        schema: Schema.listModel
  }
  {
    method: 'GET'
    path: "/#{Version}/#{Base}/info"
    config:
      description: 'Info about campaigns'
      notes: 'List of info about campaigns'
      tags: ['api', "#{Version}", "#{Base}"]
      handler: Handlers.getInfo
      auth:
        strategy: 'token'
        scope: ['admin']
      validate:
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
  }
  {
    method: "GET"
    path: "/#{Version}/#{Base}/tags"
    config:
      description: 'All campaigns'
      notes: 'List all campaign tags'
      tags: ['api', "#{Version}", "#{Base}"]
      handler: Handlers.getTags
      auth:
        strategy: 'token'
        scope: ['admin']
      validate:
        query:
          page: Joi.number().optional()
          limit: Joi.number().optional()
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
  }
  {
    method: 'POST'
    path: "/#{Version}/#{Base}/query"
    config:
      description: 'Query campaigns'
      notes: 'Search campaigns with given parameters'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.query
      auth:
        strategy: 'token'
        scope: ['admin']
      validate:
        query:
          page: Joi.number().optional()
          limit: Joi.number().optional()
          sort: Joi.string().optional()
          order: Joi.string().optional()
          published: Joi.boolean().optional()
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        payload:
          Schema.payloadQuery
  }
  {
    method: 'GET'
    path: "/#{Version}/#{Base}/responses/{id}"
    config :
      description: 'Get Campaign user responses by id'
      notes: 'Detail of a campaign user responses providing a valid _id'
      tags: ['api',"#{Version}","#{Base}"]
      validate:
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        params:
          id: Joi.string().required().description('campaign id')
      handler: Handlers.getUserResponses
      auth:
        strategy: 'token'
        scope: ['admin']
  }
  {
    method: 'POST'
    path: "/#{Version}/#{Base}/filterCandidates/{id}"
    config :
      description: 'Get list of candidates to confirm'
      notes: 'List of candidates to confirm, filtered'
      tags: ['api',"#{Version}","#{Base}"]
      validate:
        payload:
          sex: Joi.string().optional()
          age: Joi.object().optional()
          zone: Joi.string().optional()
          simulate: Joi.boolean().optional()
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        params:
          id: Joi.string().required().description('campaign id')
      handler: Handlers.filterCandidates
      auth:
        strategy: 'token'
        scope: ['admin']
  }
]