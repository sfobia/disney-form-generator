Campaign   = require('./../../../model')


dist = process.env.NODE_FOLDER || 'dist'

User      = require process.cwd() + '/'+dist+'/modules/user/user'
Form      = require process.cwd() + '/'+dist+'/modules/form/model'
Media      = require process.cwd() + '/'+dist+'/modules/media/model'
Category      = require process.cwd() + '/'+dist+'/modules/category/model'
Client      = require process.cwd() + '/'+dist+'/modules/client/model'
Template      = require process.cwd() + '/'+dist+'/modules/template/model'
Profile      = require process.cwd() + '/'+dist+'/modules/profile/model'
Report      = require process.cwd() + '/'+dist+'/modules/report/model'
Candidate      = require process.cwd() + '/'+dist+'/modules/candidate/model'

Moment  = require 'moment'
Boom    = require 'boom'
async   = require 'async'
json2csv = require 'json2csv'
_ = require 'lodash'
fs = require 'fs'
csv = require 'fast-csv'

province = require('./province')

methods =
  getAllStats: (req,reply) ->
    response = {}

    reply 200
    return

    start = Moment.utc().startOf('month')
    end = Moment.utc().endOf('month')

    async.parallel [
      (cb) ->
        Campaign.dao.count().exec (err, items) ->
          response.campaigns = items
          cb()

      (cb) ->
        User.dao.count().exec (err, items) ->
          response.users = items
          cb()
      (cb) ->
        User.dao.count({ creationDate: { $gte: new Date(start.toISOString()) , $lte: new Date(end.toISOString()) } }).exec (err, items) ->
          if items
            User.dao.aggregate [
              { $match:
                  creationDate: { $gte: new Date(start.toISOString()) , $lte: new Date(end.toISOString()) }
              },
              { $group:
                  _id:
                    year: $year: '$creationDate'
                    month: $month: '$creationDate'
                    day: $dayOfMonth: '$creationDate'
                  count: $sum: 1
              },
              { $group:
                  _id:
                    year: '$_id.year'
                    month: '$_id.month'
                  dailyusage:
                    $push:
                      day: '$_id.day'
                      count: '$count'
                      "percentage":
                        "$concat": [ { "$substr": [ { "$multiply": [ { "$divide": [ "$count", {"$literal": items }] }, 100 ] }, 0,2 ] }, "", "%" ]
              },
              { $group:
                _id: year: '$_id.year'
                monthlyusage: $push:
                  month: '$_id.month'
                  dailyusage: '$dailyusage'
              }
            ], (err, res) ->
              if err
                cb(err)
                return
              response.usersStats = res
              cb()

          if !items
            cb()

      (cb) ->
        Media.count().exec (err, items) ->
          response.media = items
          Media.find({}).sort({'creationDate': -1}).limit(30).lean().exec (err, items) ->
            items.forEach (item) ->
              for k,img of item.image
                if img != null
                  img.url = img.defaultUrl.replace(process.cwd() + '/public', '')
            response.mediaImages = items
            cb()

      (cb) ->
        Form.dao.count().exec (err, items) ->
          response.forms = items
          cb()

      (cb) ->
        Category.dao.count().exec (err, items) ->
          response.categories = items
          cb()

      (cb) ->
        Client.dao.count().exec (err, items) ->
          response.clients = items
          cb()
      (cb) ->
        Template.dao.count().exec (err, items) ->
          response.templates = items
          cb()
    ],
      (err) ->
        if err
          reply err
          return
        reply response
    return

  save: (request, reply) ->
    slug = request.payload.slug
    form = request.payload.form if request.payload.form
    survey = request.payload.survey if request.payload.survey



    async.series [
      (cb) ->
        Campaign.dao.find({slug: slug}).exec (err, items) ->
          if items.length > 0
            reply Boom.badRequest('slug already existing')
            return
          else
            cb()

#      (cb) ->
#
#        if form #il form non può essere ripetuto
#          Campaign.dao.find({form: form}).exec (err, items) ->
#            if items.length > 0
#              reply Boom.badRequest('form already used in another campaign')
#              return
#            else
#              cb()
#        else
#          cb()
#
#      (cb) ->
#
#        if survey #il survey non può essere ripetuto
#          Campaign.dao.find({survey: survey}).exec (err, items) ->
#            if items.length > 0
#              reply Boom.badRequest('survey already used in another campaign')
#              return
#            else
#              cb()
#        else
#          cb()

    ], (err) ->

      campaign  = new Campaign.dao(request.payload)
      date      = new Moment(request.payload.dates.startCandidations)
      campaign.weekNumber = date.week()
      campaign.save (err, item) ->
        if err
          reply Boom.badRequest(err)
          return
        else
          reply item
        return
    return

  getById: (request, reply) ->
    Campaign.dao.findById(request.params.id,"-candidates -responses").populate("client").exec (err, item) ->
      if !err and item
        reply item
      else
        reply Boom.notFound(err)
      return
    return

  putById: (request, reply) ->

    if request.payload.dates.startCandidations
      date = new Moment(request.payload.dates.startCandidations)
      request.payload.weekNumber = date.week()

    slug = request.payload.slug
    form = request.payload.form
    survey = request.payload.survey

    async.series [
      (cb) ->
        Campaign.dao.find({slug: slug, _id: {$nin: request.params.id}}).exec (err, items) ->
          if items.length > 0
            reply Boom.badRequest('slug already existing')
            return
          cb()

#      (cb) ->
#        if form # il form non può essere ripetuto
#          Campaign.dao.find({form: form, _id: {$nin: request.params.id}}).exec (err, items) ->
#            if items.length > 0
#              reply Boom.badRequest('form already used in another campaign')
#              return
#            else
#              cb()
#        else
#          cb()
#      (cb) ->
#        if survey # il form non può essere ripetuto
#          Campaign.dao.find({ survey: survey , _id: {$nin: request.params.id}}).exec (err, items) ->
#            if items.length > 0
#              reply Boom.badRequest('survey already used in another campaign')
#              return
#            else
#              cb()
#        else
#          cb()

    ], (err) ->
      Campaign.dao.findByIdAndUpdate(_id: request.params.id, {$set: request.payload}, {new: true} ).exec (err, item) ->
        if !err and item
          reply item
        else
          reply Boom.notFound(err)
          return
    return


  putStatusById: (request, reply) ->
    status = {}
    status[request.payload.status] = true
    Campaign.dao.findByIdAndUpdate(_id: request.params.id, { $set: "status.#{request.payload.status}": true } ).exec (err, item) ->
      if !err and item
        reply status
      else
        reply Boom.notFound(err)
      return
    return

  deleteById: (request, reply) ->
    reply Boom.badRequest('remove disabled')
    return
    Campaign.dao.remove _id: request.params.id, (err, item) ->
      if !err and item
        reply('ok')
      else
        reply Boom.notFound(err)
      return
    return

  getAll: (request, reply) ->
    page = request.query.page || 1
    limit = request.query.limit || 10
    order = request.query.order || "dates.startCampaign"
    sort = request.query.sort || ''
    sortOrder = sort + order
    query = {}

    Campaign.dao.paginate(query, { page: page, limit: limit, lean: true, populate: 'categories client', sort: sortOrder }).then (item) ->
      if item.docs
        reply(item.docs).header('X-Total-Count', item.total)
      else
        reply Boom.notFound()
      return
    return

  getByTag: (request, reply) ->
    q = false
    q = decodeURI(request.params.q).toString().split(' ') if request.params.q
    query = {}
    if q
      reg = []
      for x in q
        reg.push {
          "tag.name":
            $regex: x
        }
      query =
        $or: reg
    page = request.query.page || 1
    limit = request.query.limit || 10
    Campaign.dao.paginate({}, {page: page, limit: limit, lean: true}).then (items) ->
      if items.docs
        # fix url
        items.docs.forEach (item) ->
          for k,img of item.image
            if img.defaultUrl
              img.url = img.defaultUrl.replace(process.cwd() + '/public', '')
        reply(items.docs).header('X-Total-Count', items.total)
      else
        reply Boom.notFound()
      return
    return

  query: (request, reply) ->

    reg = []
    if request.payload.hasOwnProperty('q')
      reg.push {
        "title":
          $regex: request.payload.q
          $options: 'i'
      }
      reg.push {
        "description":
          $regex: request.payload.q
          $options: 'i'
      }
      reg.push {
        "type": request.payload.q
      }

    query = {}
    if reg.length > 0
      query = $or: reg

    page  = request.query.page  || 1
    limit = request.query.limit || 10
    order = "dates.startCampaign"
    sort = '-'
    sortOrder = sort + order

    query.published = true if request.query.published


    Campaign.dao.paginate(query, { page: page, limit: limit, lean: true, sort: sortOrder }).then (item) ->
      reply(item.docs).header('X-Total-Count', item.total)
      return
    return

  getByWeek: (request, reply) ->
    Moment.locale('it')
    start = Moment.utc().startOf('week')
    end = Moment.utc().endOf('week')
    page = request.query.page || 1
    limit = request.query.limit || 10
    Campaign.dao.paginate({"dates.startCandidations": {"$gte": start.toISOString(), "$lt": end.toISOString()}}, {page: page, limit: limit, lean: true, populate: 'categories'}).then (item) ->
      if item.docs
        reply item.docs
      else
        reply Boom.notFound()
      return
    return

  getInfo: ( request, reply ) ->

    item =
      enumType: Campaign.enumType
      domain: process.env.NODE_ENV_DOMAIN_PRIMARY

    reply item
    return

  getTags: (request, reply) ->
    Campaign.dao.distinct("tag.name").lean().exec (err, items) ->
      reply items
      return

    return

  getUserResponses: (request, reply) ->
    campaignId = request.params.id
    usersResponses = []
    #fieldNames = [ "First name", "Last name", "Email"]
    fields = [ "firstName", "lastName", "email", 'accepted.marketing', 'accepted.dem', 'accepted.profiling']
    filename = campaignId
    campaignName = ''
    answers = {}
    lastIdMsg = ''
    csvfile = ''
    typeForm = "survey"

    async.waterfall [
      (cb) ->
        modulo = ''
        Campaign.dao.findById(campaignId).populate('form survey').exec (err, res) ->
          if err
            cb(Boom.badRequest(err))
            return

          if res
            campaignName = res.slug

            if typeForm == "campaign"
              modulo      = res.form

            if typeForm == "survey"
              modulo    = res.survey

            csvfile   = 'download/'+typeForm+'-'+campaignName+'.csv'
            report = new Report(
              name: campaignName
              url: csvfile
              reportType: typeForm+"-report-"+campaignName
            )
            report.save (err,item)->
              lastIdMsg = item._id

          cb(null, modulo)


      (modulo, cb) ->
        for field in modulo.fields
          i = 0
          for item in field.value
            if item.refvalue
              i++
              answers[item.identifier] = num:i, answer:item.name, question:field.name
            else
              psicoValues = field.value
              for psico in psicoValues
                i++
                if !item.right
                  item.right = ''
                answers[item.identifier] = num:i, answer: item.name, question:item.left+' '+item.right
        cb()


      (cb) ->
        flag = false

        stream = Profile.dao.find({'form.campaign': campaignId, 'form.formType': typeForm}).populate('user').select('name form user accepted').lean().stream()
        stream.on 'data', (item) ->

          forms = item.form

          for form in forms
            if form.formType == typeForm and form.campaign
              if form.campaign.toString() == campaignId.toString()
                obj =
                  firstName: item.name.first
                  lastName: item.name.last
                  email: item.user.email
                if item.accepted
                  obj['accepted.marketing'] = if item.accepted.marketing then '1' else '0'
                  obj['accepted.dem'] = if item.accepted.dem then '1' else '0'
                  obj['accepted.profiling'] = if item.accepted.profiling then '1' else '0'

                for val in form.values
                  if val.refvalue
                    if(answers[val.refvalue])
                      obj[val.name] = answers[val.refvalue].num.toString()
                    if !flag
                      fields.push(val.name)
                  else
                    for key, value of val.value
                      psicoName = value.left + ' ' + value.right
                      obj[psicoName] = value.value
                      if !flag
                        fields.push(psicoName)
                flag = true
                usersResponses.push obj

        stream.on 'error', (err) ->
          cb Boom.badRequest(err)
          return

        stream.on 'close', ->

          obj = {}
          for k, answer of answers
            if !obj[answer.question]
              obj[answer.question] = ''
            obj[answer.question] += answer.num+':'+answer.answer+" "

          usersResponses.push({})
          usersResponses.push({})
          usersResponses.push(obj)

          json2csv {
            data: usersResponses
            fields: fields
            quotes: ''
            del: ';'
          }, (err, csv) ->
            if err
              return
            fs.writeFile csvfile, csv, (err, s) ->
              if err
                throw err

              Report.findByIdAndUpdate(lastIdMsg,{ $set: 'dates.end': new Moment() }).exec (err,res)->
                return

              return
            return
          return
        cb()


    ], (err, res) ->
      if err
        reply Boom.badRequest(err)
        return

      reply { msg: 'file saved', file: '' }
      return

    return


  resetCandidation: (request, reply) ->
    return
    #todo metodo per cancellare le candidature in caso di errore filtraggio
    # devo rimettere active:false a tutti gli utenti candidati ad una campagna specifica
    # per sicurezza mi salvo la lista degli utenti che sto modificando
    #todo facciamo un metodo che scrive i risultati in un file di log che mettiamo dentro /download
    # inoltre questa operazione deve essere consentita solo una volta, oppure non deve essere richiamata per sbaglio
    # query { campaign: campaignId, active: false, rejected: false }
    # csv = []
    # fs.writeFile 'log-resetCandidation.csv', csv, (err, s) ->
    #  if err
    #     throw err


  filterCandidates: (request, reply) ->

    campaignId = request.params.id

    age = request.payload.age || { min: 18, max: 120 }
    sex = request.payload.sex || undefined
    zone = request.payload.zone || undefined
    simulate = true
    if request.payload.simulate == false
      simulate = false
    if request.payload.simulate == undefined
      simulate = true
    minAge = Moment().subtract(age.min, 'year')
    maxAge = Moment().subtract(age.max, 'year')


    profileIdList = []
    userIdFilteredList = []
    candidatesFilteredList = []

    campaign = ''
    piecesInitial = ''
    weekNumber = ''
    sameWeekCampaigns = []
    arrayProductsCounts = []
    formCampaign = ''
    countPiecesAssigned = 0

    candidateExclude = {}

    paths = [
      {path: 'campaign', select: 'weekNumber'}
      {path: 'user', select: 'profile'}
    ]

    async.waterfall [
      (cb) ->
        # recupero il numero di pezzi e il numero di settimana della campagna
        Campaign.dao.findById(campaignId).lean().exec (err, item) ->
          campaign = item
          piecesInitial = campaign.piecesInitial
          weekNumber = campaign.weekNumber
          cb()

      (cb) ->
        #recupero il form della campagna per verificare se è multiprodotto
        Form.dao.findById(campaign.form).lean().exec (err, form) ->
          formCampaign = form

        # nel caso di multiProduct, il piecesInitial è dato dalla somma delle disponibilità dei singoli prodotti
        if formCampaign.multiProduct
          piecesInitial = 0
          for product in products
            piecesInitial += product.quantity
          cb()
        else
          cb()

      (cb) ->
        #recupero le altre campagne della stessa settimana (e stesso anno) e le metto in un array
        thisYear = Moment().year().toString()
        Campaign.dao.find({weekNumber: weekNumber}).lean().exec (err, campaigns) ->
          for campaign in campaigns
            if campaign._id.toString() != campaignId
              campaignYear = Moment(campaign.dates.startCampaign).format('YYYY')
              if(campaignYear == thisYear)
                sameWeekCampaigns.push campaign._id
          cb()
      (cb) ->
        # controllo per settimana: un utente non può essere attivo in più di una campagna della stessa settimana
        # creo array di utenti da escludere
        Candidate.dao.aggregate [
          {
            $match: {
              active: true
              campaign: {$in: sameWeekCampaigns}
            }
          },
          {
            $sort: "dates.candidation": 1
          }
          { $group:
              _id: '$user'
              camps:
                $push:
                  id: '$_id'
                  campaign: '$campaign'
              count: $sum: 1
          },
          {
            $match: {'count': $gte: 1 }
          }
        ], (err, res) ->
          for user in res
            candidateExclude[user._id] = true
          cb()
      (cb) ->
        # controllo per "mese": un utente non può avere più di due attivazioni nelle campagne
        # inserisco gli utenti da escludere in un array
        # TODO: controllo mensile
        #aMonthAgo = Moment().subtract(1, 'month')

        Candidate.dao.aggregate [
          {
            $match: {
              active: true
              #'dates.accepted': {$lte: aMonthAgo}
            }
          }
          { $group:
            _id: '$user'
            count: $sum: 1
          },
          {
            $match: {'count': $gte: 2 }
          }
        ], (err, res) ->
          for user in res
            candidateExclude[user._id] = true
          cb()

      (cb) ->
        # creo un array di id profilo di utenti con i requisiti e non attivi sulla campagna, e non da escludere
        stream = Candidate.dao.find({campaign: campaignId, requisite: true, active: false, rejected: false}).sort("dates.candidation").populate(paths).lean().stream()
        stream.on 'data', (item) ->
          # cerco tutti i candidati validi che non sono esclusi per altri fattori
          if !candidateExclude[item.user._id]
            profileIdList.push item.user.profile

        stream.on 'error', (err) ->
          cb Boom.badRequest(err)
          return

        stream.on 'close', ->
          cb()

      (cb) ->
        # Query per filtrare gli utenti. Basata su range di età, sesso e zona
        query = {
          _id: {$in: profileIdList}
          birthDate: {$gte: maxAge.toISOString(), $lte: minAge.toISOString()}
        }
        if sex
          query.gender = sex
        if zone
          query.province = {$in: province[zone]}

        # Seleziono i profili con la query
        stream = Profile.dao.find(query).populate({ path: "user",select:"email status"}).lean().stream()
        stream.on 'data', (item) ->
          # Escludo gli utenti "non affidabili" e gli utenti con le email di test
          if (item.user.status == 'reliable' and item.user.email.indexOf('@mailinator') == -1)
            userIdFilteredList.push item.user._id

        stream.on 'error', (err) ->
          cb Boom.badRequest(err)
          return

        stream.on 'close', ->
          cb()

      (cb) ->
        avoidDuplicates = []

        #conta ed esclusioni per le campagne a prodotto singolo
        if !formCampaign.multiProduct
          stream = Candidate.dao.find({campaign: campaignId, user: {$in: userIdFilteredList}}).sort("dates.candidation").lean().stream()
          stream.on 'data', (item) ->
            if candidatesFilteredList.length < piecesInitial
              if avoidDuplicates.indexOf(item.user.toString()) == -1
                candidatesFilteredList.push item._id
                avoidDuplicates.push item.user.toString()

          stream.on 'error', (err) ->
            cb Boom.badRequest(err)
            return

          stream.on 'close', ->
            cb()

        else
          #conta ed esclusioni per le campagne multiprodotto
          for product, k in formCampaign.products
            arrayProductsCounts[k] = {}
            arrayProductsCounts[k].product = product.name
            arrayProductsCounts[k].count = 0
            arrayProductsCounts[k].max = product.quantity


          stream = Candidate.dao.find({campaign: campaignId, user: {$in: userIdFilteredList}}).sort("dates.candidation").lean().stream()
          stream.on 'data', (item) ->
            for row, k in arrayProductsCounts
              if row.product == item.product #cerco il prodotto del candidato nell'array dei prodotti presenti nel Form della campagna
                if arrayProductsCounts[k].count < arrayProductsCounts[k].max   # se la conta non ha superato il massimo della disponibilità, procede
                  if candidatesFilteredList.length < piecesInitial
                    if avoidDuplicates.indexOf(item.user.toString()) == -1
                      candidatesFilteredList.push item._id
                      avoidDuplicates.push item.user.toString()
                      arrayProductsCounts[k].count++                # incremento la quantità di oggetti assegnati

          stream.on 'error', (err) ->
            cb Boom.badRequest(err)
            return

          stream.on 'close', ->
            for k, row of arrayProductsCounts
              countPiecesAssigned = parseInt(arrayProductsCounts[k].count)+countPiecesAssigned
            cb()

      (cb) ->
        if formCampaign.multiProduct
          if countPiecesAssigned != piecesInitial
            text = ''
            for product in arrayProductsCounts
              text += product.product + ': ' + product.count + ' pezzi; '
            cb 'La distribuzione dei diversi prodotti non eguaglia il totale necessario. '+text
            return
        if piecesInitial > candidatesFilteredList.length
          cb 'Troppi pochi utenti per procedere. Sono solamente '+candidatesFilteredList.length+' rispetto a '+piecesInitial+' pezzi iniziali'
          return
        else
          if piecesInitial != candidatesFilteredList.length
            cb 'Non tornano i conti, ci son più candidati del previsto'
            return
          else
            cb()

    ], (err, res) ->
      if err
        reply Boom.badRequest(err)
        return

      if simulate
        reply {count: candidatesFilteredList.length, candidates: candidatesFilteredList}
      else
        ## eseguire attivazione dei candidates del candidatesFilteredList
        # aggiorna candidati con true su active e false su rejected
        # controllo uguale su campaign/api/handlers
        Candidate.dao.update({_id: {"$in": candidatesFilteredList}}, $set: {
          active: true
          rejected: false
          'dates.accepted': new Moment()
        }, {multi: true}).exec (err, res) ->
          Campaign.dao.update(_id: campaignId, {
            $addToSet: {
              "candidates":
                $each: candidatesFilteredList
            }
          }).exec (err, itemCampaign) ->
            reply {count: candidatesFilteredList.length}

      return

    return

module.exports = methods