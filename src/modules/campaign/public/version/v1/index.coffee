Joi         = require 'joi'
Handlers    = require './handlers'
Schema      = require './schema'
Version     = "V1"
Base        = "campaign"

module.exports = [
  {
    method: 'GET'
    path: "/#{Version}/#{Base}/{id}"
    config :
      description: 'Get Campaign by id'
      notes: 'Detail of a campaign providing a valid _id'
      tags: ['api',"#{Version}","#{Base}"]
      response:
        schema: Schema.campaignModel
      validate:
        params:
          id: Joi.string().required().description('campaign id')
      handler: Handlers.getById
  }
  {
    method: 'GET'
    path: "/#{Version}/#{Base}/slug/{slug}"
    config :
      description: 'Get Campaign by slug'
      notes: 'Detail of a campaign providing a valid _id'
      tags: ['api',"#{Version}","#{Base}"]
      response:
        schema: Schema.campaignModel
      validate:
        params:
          slug: Joi.string().required().description('campaign slug')
      handler: Handlers.getBySlug
  }
  {
    method: 'GET'
    path: "/#{Version}/#{Base}/slug/preview/{slug}"
    config :
      description: 'Get Campaign by slug offline'
      notes: 'Detail of a campaign providing a valid _id'
      tags: ['api',"#{Version}","#{Base}"]
      response:
        schema: Schema.campaignModel
      validate:
        params:
          slug: Joi.string().required().description('campaign slug')
      handler: Handlers.getBySlugPreview
  }
  {
    method: 'GET'
    path: "/#{Version}/#{Base}/premium/{slug?}"
    config :
      description: 'Get Campaign by premium'
      notes: 'Detail of a campaign providing a premium'
      tags: ['api',"#{Version}","#{Base}"]
      response:
        schema: Schema.listModel
      validate:
        params:
          slug: Joi.string().optional().description('campaign slug')
      handler: Handlers.getByPremium
  }
  {
    method: 'GET'
    path: "/#{Version}/#{Base}/premium/preview/{slug?}"
    config :
      description: 'Get Campaign by premium'
      notes: 'Detail of a campaign providing a premium'
      tags: ['api',"#{Version}","#{Base}"]
      response:
        schema: Schema.listModel
      validate:
        params:
          slug: Joi.string().optional().description('campaign slug')
      handler: Handlers.getByPremiumPreview
  }
  {
    method: 'GET'
    path: "/#{Version}/#{Base}/week"
    config :
      description: 'Get Campaign by actual week'
      notes: 'Detail of a campaign of the week'
      tags: ['api',"#{Version}","#{Base}"]
      response:
        schema: Schema.listModel
      handler: Handlers.getByWeek
      validate:
        query:
          page: Joi.number().optional()
          limit: Joi.number().optional()
  }
  {
    method: 'GET'
    path: "/#{Version}/#{Base}"
    config :
      description: 'All campaigns frontend'
      notes: 'List of all campaigns without any filter'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.getAllPublic
      response:
        schema: Schema.listModel
      validate:
        query:
          page: Joi.number().allow('').optional()
          limit: Joi.number().allow('').optional()
          offset: Joi.number().allow('').optional()
          order: Joi.string().optional()
          sort: Joi.string().optional()
          category: Joi.string().allow('').optional()
          type: Joi.string().allow('').optional()
  }
  {
    method: 'GET'
    path: "/#{Version}/#{Base}/active/{id?}"
    config :
      description: 'All campaigns frontend'
      notes: 'List of all campaigns without any filter'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.getInDates
      response:
        schema: Schema.listModel
      validate:
        params:
          id: Joi.string().optional().description('campaign id')
  }
  {
    method: 'GET'
    path: "/#{Version}/#{Base}/preview"
    config :
      description: 'All campaigns frontend ancorda offline'
      notes: 'List of all campaigns without any filter'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.getAllPublicPreview
      response:
        schema: Schema.listModel
      validate:
        query:
          page: Joi.number().allow('').optional()
          limit: Joi.number().allow('').optional()
          order: Joi.string().optional()
          sort: Joi.string().optional()
  }
  {
    method: "GET"
    path: "/#{Version}/#{Base}/tags"
    config:
      description: 'All campaigns'
      notes: 'List all campaign tags'
      tags: ['api', "#{Version}", "#{Base}"]
      handler: Handlers.getTags
  }
  {
    method: 'GET'
    path: "/#{Version}/#{Base}/tags/{q}"
    config:
      description: 'All campaigns with tags'
      notes: 'List all campaigns with tags'
      tags: ['api', "#{Version}", "#{Base}"]
      handler: Handlers.getByTag
      validate:
        query:
          page: Joi.number().optional()
          limit: Joi.number().optional()
  }
  {
    method: 'GET'
    path: "/#{Version}/#{Base}/category"
    config:
      description: 'All campaigns by id category'
      notes: 'List all campaigns by category'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.getByCategory
  }
  {
    method: 'GET'
    path: "/#{Version}/#{Base}/type"
    config:
      description: 'All campaigns by '
      notes: 'List all campaigns by '
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.getByType
  }
  {
    method: 'GET'
    path: "/#{Version}/#{Base}/categories"
    config :
      description: 'All categories'
      notes: 'List of all categories without any filter'
      tags: ['api',"#{Version}","#{Base}"]
      validate:
        query:
          page: Joi.number().optional()
          limit: Joi.number().optional()
      handler: Handlers.getAllCategories
  }
  {
    method: 'GET'
    path: "/#{Version}/#{Base}/release"
    config:
      description: 'All campaigns for hp where release is true'
      notes: 'List all campaigns for hp where release is true '
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.getReleased
  }
  {
    method: 'GET'
    path: "/#{Version}/#{Base}/result"
    config:
      description: 'All campaigns with result'
      notes: 'List all campaigns with result '
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.getResult
  }
  {
    method: 'GET'
    path:"/#{Version}/#{Base}/totcampaigns"
    config:
      description: 'Campaigns by type'
      notes: 'TOT campaigns by'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.getTotCampaigns
      validate:
        query:
          type: Joi.string().allow('').optional()
  }
  {
    method: 'GET'
    path: "/#{Version}/#{Base}/sitemap"
    config:
      description: 'All campaigns with result for sitemap'
      notes: 'List all campaigns with result for sitemap'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.getSitemapResult
  }

]