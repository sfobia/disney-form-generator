Joi = require 'joi'
Campaign = require('./../../../model')

module.exports.campaignImageModel = Joi.object(
  _id: Joi.any().optional()
  __v: Joi.any().optional()
  name: Joi.string().optional()
  path: Joi.string().optional()
  ref: Joi.any().optional()
)

module.exports.campaignTagModel = Joi.object(
  _id: Joi.any().optional()
  __v: Joi.any().optional()
  name: Joi.string().optional()
)

module.exports.categoryModel = Joi.object(
  _id: Joi.any().optional()
  __v: Joi.any().optional()
  title: Joi.string().required().description('category title')
  description: Joi.string().optional().description('category description')
  media:
    logo: Joi.array().items(module.exports.campaignImageModel).optional()
    images: Joi.array().items(module.exports.campaignImageModel).optional()
  order: Joi.number().optional().description('category order')
  published: Joi.boolean().optional().description('indicate if the category is published')
)

module.exports.categoryGetModel = Joi.object(
  _id: Joi.any().optional()
  __v: Joi.any().optional()
  title: Joi.string().optional().description('category title')
  description: Joi.string().optional().description('category description')
  media:
    logo: Joi.array().items(module.exports.campaignImageModel).optional()
    images: Joi.array().items(module.exports.campaignImageModel).optional()
  order: Joi.number().optional().description('category order')
  published: Joi.boolean().optional().description('indicate if the category is published')
)

module.exports.campaignModel = Joi.object(
  _id: Joi.any().required()
  form: Joi.any().optional() # qui dovrei prendere il model form facendo export dal model form
  title: Joi.string().required().description('campaign title')
  description: Joi.string().optional().description('campaign description')
  type: Joi.string().required().description('campaign type')
  date:
    startCampaign: Joi.date().required().description('start date of the campaign')
    endCampaign: Joi.date().required().description('end date of the campaign')
    startCandidations: Joi.date().required().description('start date for candidations')
    endCandidations: Joi.date().required().description('end date for candidations')
    releaseFeedback: Joi.date().optional().description('release date of user feedbacks')
    startSurvey: Joi.date().optional().description('start showing links for survey')
    endSurvey: Joi.date().optional().description('stop showing links for survey')
  weekNumber: Joi.number().optional().description('weekNumber')
  media:
    logo: Joi.array().items(module.exports.campaignImageModel).optional()
    images: Joi.array().items(module.exports.campaignImageModel).optional()
    cover: Joi.array().items(module.exports.campaignImageModel).optional()
    skin: Joi.array().items(module.exports.campaignImageModel).optional()
    footer: Joi.array().items(module.exports.campaignImageModel).optional()
  tag: Joi.array().items(module.exports.campaignTagModel).optional()
  link: Joi.string().optional().description('campaign link')
  published: Joi.boolean().optional().description('indicate if the campaign is published')
  categories: Joi.array().items(module.exports.categoryGetModel).optional().description('categories associated to the campaign')
  content: Joi.any().optional()
  slug: Joi.any().optional()
  column: Joi.number().optional().description('column order')
  premium: Joi.boolean().optional().description('premium campaign')
  expired: Joi.boolean().optional()
  release: Joi.boolean().optional()
  publishResults: Joi.boolean().optional().description('campaign results show')
  reviewable: Joi.boolean().optional().description('campagna recensibile')
  additionalJs: Joi.boolean().optional().description('adds additional js on frontend')
).meta(className: 'Campaign').options(allowUnknown: true, abortEarly: false)

module.exports.campaignListModel = Joi.object(
  _id: Joi.any().required()
  form: Joi.any().optional() # qui dovrei prendere il model form facendo export dal model form
  title: Joi.string().required().description('campaign title')
  description: Joi.string().optional().description('campaign description')
  type: Joi.string().required().description('campaign type')
  date:
    startCampaign: Joi.date().optional().description('start date of the campaign')
    endCampaign: Joi.date().optional().description('end date of the campaign')
    startCandidations: Joi.date().optional().description('start date for candidations')
    endCandidations: Joi.date().optional().description('end date for candidations')
    startSurvey: Joi.date().optional().description('start showing links for survey')
    endSurvey: Joi.date().optional().description('stop showing links for survey')
  weekNumber: Joi.number().optional().description('weekNumber')
  column: Joi.number().optional().description('column order')
  premium: Joi.boolean().optional().description('premium campaign')
  expired: Joi.boolean().optional()
  release: Joi.boolean().optional()
  publishResults: Joi.boolean().optional().description('campaign results show')
  reviewable: Joi.boolean().optional().description('campagna recensibile')
  additionalJs: Joi.boolean().optional().description('adds additional js on frontend')
).meta(className: 'Campaign').options(allowUnknown: true, abortEarly: false)


module.exports.payloadQuery = Joi.object(
  _id: Joi.any().optional()
  __v: Joi.any().optional()
  title: Joi.string().optional().description('campaign title')
  description: Joi.string().optional().description('campaign description')
  type: Joi.string().optional().description('campaign type')
  q: Joi.string().optional().description('query string')
)

module.exports.listModel = Joi.array().items(module.exports.campaignListModel).meta(className: 'List')
