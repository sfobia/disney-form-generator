Campaign   = require('./../../../model')
Moment     = require 'moment'
Boom       = require 'boom'
moment     = require 'moment'

Category   = require('./../../../../category/model')

internals = {}
internals.buildContentHtml = (item )->
  section = {}
  for rec in item
    section['section'+rec.section] = section['section'+rec.section] || []
    section['section'+rec.section].push rec

  for k,rec of section
    rec.sort (a,b) ->
      parseInt(a.order) - parseInt(b.order)
  section

internals.sortBy = (key, a, b, r) ->
  r = if r then 1 else -1
  return -1*r if a[key] > b[key]
  return +1*r if a[key] < b[key]
  return 0

methods =
  getById: (request, reply) ->
    Campaign.dao.findOne({_id: request.params.id},"-candidates -responses -client -pieces -piecesInitial -status").lean().exec (err, item) ->
      if !err and item
        reply item
      else
        reply Boom.notFound(err)
      return
    return

  getBySlug: (request, reply) ->
    #console.log "Qjuiiii"
    Campaign.dao.findOne({slug: request.params.slug, published: true},"-candidates -responses -client -pieces -piecesInitial -status", populate: 'categories').lean().exec (err, item) ->
      if !err and item
        reply item
      else
        reply Boom.notFound(err)
      return
    return

  getBySlugPreview: (request, reply) ->
    Campaign.dao.findOne({slug: request.params.slug},"-candidates -responses -client -pieces -piecesInitial -status", populate: 'categories').lean().exec (err, item) ->
      if !err and item
        reply item
      else
        reply Boom.notFound(err)
      return
    return

  getByPremium: (request, reply) ->
    now = moment()
    query = { published: true, premium: true, 'dates.startCampaign': {$lt: now.toDate()}, 'dates.endCampaign': {$gte: now.toDate()} }

    if request.params.slug
      query.slug = request.params.slug

    Campaign.dao.find(query,"-candidates -responses -client -pieces -piecesInitial -status", populate: 'categories', sort: "-dates.startCampaign").lean().exec (err, items) ->
      if !err and items
        for campaign in items
          if campaign.publishResults
            campaign.release = true
          else if campaign.dates.endCandidations < now
            campaign.expired = true
        reply items
      else
        reply Boom.notFound(err)
      return
    return

  getByPremiumPreview: (request, reply) ->
    now = moment()
    query = premium: true

    if request.params.slug
      query.slug = request.params.slug

    Campaign.dao.find(query,"-candidates -responses -client -pieces -piecesInitial -status", populate: 'categories', sort: "-dates.startCampaign").lean().exec (err, items) ->
      if !err and items
        for campaign in items
          if campaign.publishResults
            campaign.release = true
          else if campaign.dates.endCandidations < now
            campaign.expired = true
        reply items
      else
        reply Boom.notFound(err)
      return
    return

  getInDates: (request, reply) ->
    now = moment()

    if( request.params.id )
      query = {_id: request.params.id, 'dates.startCampaign': {$lt: now.toDate()}, 'dates.endCampaign': {$gte: now.toDate()}}
    else
      query = {'dates.startCampaign': {$lt: now.toDate()}, 'dates.endCampaign': {$gte: now.toDate()}}

    Campaign.dao.find(query,"-candidates -responses -client -pieces -piecesInitial -status", sort: "-dates.startCampaign").lean().exec (err, items) ->

      console.log( items )

      if items
        reply(items).header('X-Total-Count', items.total);
      else
        reply Boom.notFound()
      return
    return

  getAllPublic: (request, reply) ->
    today = moment()
    page = request.query.page || 1
    limit = request.query.limit || 6
    order = request.query.order || "order"
    sort = request.query.sort || ''
    sortOrder = sort + order
    now = moment()

    if request.query.type
      if request.query.type == 'incorso'
        #query = { published: true, 'dates.endCandidations': {$gte: today.toDate()}, publishResults: false }
        query = { published: true, publishResults: false }
      else
        query = { published: true, publishResults: true }
    else
      query = { published: true }

    #query.premium = false

    if  request.query.category && request.query.category != null
      query.categories =  request.query.category

    options = { select: "-candidates -client -pieces -piecesInitial -status", page: page, limit: limit, lean: true, populate: 'categories', sort: "-dates.startCampaign"}

    if request.query.offset
      options.offset = request.query.offset

    Campaign.dao.paginate(query, options).then (item) ->
      if item.docs
        for campaign in item.docs
          if campaign.publishResults
            campaign.release = true
          else if campaign.dates.endCandidations < now
            campaign.expired = true
        reply(item.docs).header('X-Total-Count', item.total);
      else
        reply Boom.notFound()
      return
    return

  getAllPublicPreview: (request, reply) ->
    today = moment()
    page = request.query.page || 1
    limit = request.query.limit || 100
    order = request.query.order || "order"
    sort = request.query.sort || ''
    sortOrder = sort + order
    now = moment()

    query = {'dates.startCampaign': {$lt: today.toDate()}, 'dates.endCampaign': {$gte: today.toDate()}}
    options = { select: "-candidates -client -pieces -piecesInitial -status", page: page, limit: limit, lean: true, populate: 'categories', sort: sortOrder}

    Campaign.dao.paginate(query, options).then (item) ->
      if item.docs
        for campaign in item.docs
          if campaign.publishResults
            campaign.release = true
          else if campaign.dates.endCandidations < now
            campaign.expired = true
        reply(item.docs).header('X-Total-Count', item.total);
      else
        reply Boom.notFound()
      return
    return

  getByTag: (request, reply) ->
    q = false
    q = decodeURI(request.params.q).toString().split(' ') if request.params.q
    query = {}
    if q
      reg = []
      for x in q
        reg.push {
          "tag.name":
            $regex: x
        }
      query =
        $or: reg
    page = request.query.page || 1
    limit = request.query.limit || 10
    Campaign.dao.paginate({published: true}, {page: page, limit: limit, lean: true}).then (items) ->
      if items.docs
# fix url
        items.docs.forEach (item) ->
          for k,img of item.image
            try
              img.url = img.defaultUrl.replace(process.cwd() + '/public', '')
        reply(items.docs).header('X-Total-Count', items.total)
      else
        reply Boom.notFound()
      return
    return

  getByWeek: (request, reply) ->
    Moment.locale('it')
    start = Moment.utc().startOf('week')
    end = Moment.utc().endOf('week')
    page = request.query.page || 1
    limit = request.query.limit || 10
    Campaign.dao.paginate({"dates.startCandidations": {"$gte": start.toISOString(), "$lt": end.toISOString()}, published: true}, {select: '-candidates -dates -client -pieces -piecesInitial -status', page: page, limit: limit, lean: true, populate: 'categories'}).then (item) ->
      if item.docs
        reply item.docs
      else
        reply Boom.notFound()
      return
    return

  getTags: (request, reply) ->
    Campaign.dao.distinct("tag.name").lean().exec (err, items) ->
      reply items
      return

    return

  getByCategory: (request, reply) ->

    today = moment()
    id_category = request.query.category
    page = request.query.page || 1
    limit = request.query.limit || 6
    now = moment()


    if request.query.type
      if request.query.type == 'incorso'
        #query = { published: true, categories: id_category,  'dates.endCandidations': {$gte: today.toDate()}, publishResults: false }
        query = { published: true, categories: id_category, publishResults: false }
      else
        query = { published: true, categories: id_category, publishResults: true }
    else
      query = { published: true, categories: id_category }


    options = { lean: true, populate: 'categories', page: page, limit: limit, sort: "-dates.startCampaign" }
    Campaign.dao.paginate(query, options).then (items) ->
      if items.docs
        for campaign in items.docs
          if campaign.publishResults
            campaign.release = true
          else if campaign.dates.endCandidations < now
            campaign.expired = true
        reply(items.docs).header('X-Total-Count', items.total);
      else
        reply Boom.notFound()
      return
    return


  getByType: (request, reply) ->

    today = moment()
    id_type = request.query.type
    page = request.query.page || 1
    limit = request.query.limit || 6
    now = moment()

    if id_type == 'incorso'
      #query = { published: true, 'dates.endCandidations': {$gte: today.toDate()}, publishResults: false }
      query = { published: true, publishResults: false }
    else
#query = { published: true, 'dates.endCandidations': {$lt: today.toDate()}, publishResult: true }
      query = { published: true, publishResults: true }

    if request.query.category
      query.categories = request.query.category
    options = {  select: "-candidates -client -pieces -piecesInitial -status", page: page, limit: limit, lean: true, populate: 'categories', sort: "-dates.startCampaign" }

    Campaign.dao.paginate(query, options).then (items) ->
      if items.docs
        for campaign in items.docs
          if campaign.publishResults
            campaign.release = true
          else if campaign.dates.endCandidations < now
            campaign.expired = true
        reply(items.docs).header('X-Total-Count', items.total);
      else
        reply Boom.notFound()
      return
    return

  getAllCategories: (request, reply) ->

    Category.dao.find().sort({"order":1}).lean().exec (err, items) ->
      reply items
      return
    return

  getReleased: (request, reply) ->

    today = moment()
    limit = request.query.limit || 3
    page = request.query.page || 1
    now = moment()

    #query = { published: true, 'dates.endCandidations': {$gte: today.toDate()}, publishResults: false }
    query = { published: true, publishResults: false }
    options = {  select: "-candidates -client -pieces -piecesInitial -status", page: page, limit: limit, lean: true, populate: 'categories', sort: "-dates.startCampaign" }

    Campaign.dao.paginate(query, options).then (items) ->
      if items.docs
        for campaign in items.docs
          if campaign.publishResults
            campaign.release = true
          else if campaign.dates.endCandidations < now
            campaign.expired = true
        reply(items.docs).header('X-Total-Count', items.total);
      else
        reply Boom.notFound()
      return
    return

  getResult: (request, reply) ->

    today = moment()
    limit = request.query.limit || 5
    page = request.query.page || 1
    now = moment()

    #query = { published: true, 'dates.endCandidations': {$lt: today.toDate()}, publishResult: true }
    query = { published: true, publishResults: true }
    options = {  select: "-candidates -client -pieces -piecesInitial -status", page: page, limit: limit, lean: true, populate: 'categories', sort: "-dates.startCampaign" }

    Campaign.dao.paginate(query, options).then (items) ->
      if items.docs
        for campaign in items.docs
          if campaign.publishResults
            campaign.release = true
          else if campaign.dates.endCandidations < now
            campaign.expired = true
        reply(items.docs).header('X-Total-Count', items.total);
      else
        reply Boom.notFound()
      return
    return


  getSitemapResult: (request, reply) ->
    Campaign.dao.find( { published: true, publishResults: true } ).lean().exec (err, items) ->
      reply items
      return
    return

  getTotCampaigns: (request, reply) ->

    today = moment()

    if request.query.type
      if request.query.type == 'incorso'
        #query = { published: true,  'dates.endCandidations': {$gte: today.toDate()}, publishResults: false }
        query = { published: true, publishResults: false }
      else
        query = { published: true, publishResults: true }
    else # di default in corso
      #query = { published: true,  'dates.endCandidations': {$gte: today.toDate()}, publishResults: false }
      query = { published: true, publishResults: false }

    Campaign.dao.find( query ).lean().exec (err, items) ->

      if err
        reply Boom.notFound()
        return
      else
        reply items
        return
    return


module.exports = methods