Mongoose        = require('mongoose')
Schema          = Mongoose.Schema
mongoosePaginate = require('mongoose-paginate')

module.exports.enumType = ['test'] #, 'wom', 'co-creation', 'contest', 'coupon', 'mystery-shopping']

CategorySchema =
  type: Schema.ObjectId
  ref: 'Category'
  unique: true

Image = [
  name:
    type: String
    required: false
  path:
    type: String
    required: false
  ref:
    type: Schema.ObjectId
    ref: 'Media'
]


CampaignSchema  = new Schema(
  title:
    type: String
    required: true
  magazine:
    type: String
    required: false
    default: ""
  description:
    type: String
  type:
    type: String
    enum: module.exports.enumType
    required: true
  slug:
    type: String
    required: true
    lowercase: true
    trim: true
    index: unique: true
  dates:
    startCampaign:
      type: Date
      required: true
      default: Date.now
    endCampaign:
      type: Date
      required: true
      default: Date.now
    startCandidations:
      type: Date
      required: true
      default: Date.now
    endCandidations:
      type: Date
      required: true
      default: Date.now
    releaseFeedback:
      type: Date
    startSurvey:
      type: Date
    endSurvey:
      type: Date
  weekNumber:
    type: Number
  media:
    logo: Image
    images: Image
    skin: Image
    cover: Image
    footer: Image
  form:
    type: Schema.ObjectId
    ref: 'Form'
  survey:
    type: Schema.ObjectId
    ref: 'Form'
  tag: [
    name: String
  ]
  link:
    type: String
    required: false
  premium:
    type: Boolean
    required: false
    default: false
  published:
    type: Boolean
    required: false
    default: false
  publishResults:
    type: Boolean
    default: false
  candidates:[
    type: Schema.ObjectId
    ref: 'Candidate'
    unique: true
  ]
  categories: [CategorySchema]
  client:
    type: Schema.ObjectId
    ref: 'Client'
  pieces:
    type: Number
    default: 0
  piecesInitial:
    type: Number
    default: 0
  mail:
    info: String
  content: Schema.Types.Mixed
  order:
    type: Number
    default: 100
  column:
    type: Number
    default: 6
  status:
    accepted:
      type: Boolean
      default: false
    rejected:
      type: Boolean
      default: false
    mailAccepted:
      type: Boolean
      default: false
    mailTracking:
      type: Boolean
      default: false
    mailSurvey:
      type: Boolean
      default: false
    exportXls:
      type: Boolean
      default: false
    importXls:
      type: Boolean
      default: false
  reviewable:
    type: Boolean
    default: false
  additionalJs:
    type: Boolean
    default: false
)

CampaignSchema.plugin(mongoosePaginate)
#CampaignSchema.index({ slug: 1 }, { unique: true })

module.exports.dao = Mongoose.model('Campaign', CampaignSchema)