Mongoose = require('mongoose')
os = require('os')
path = require('path')
attachments = require('mongoose-attachments-localfs')
Schema = Mongoose.Schema
mongoosePaginate = require('mongoose-paginate')


MediaSchema = new Schema(
  name:
    type: String
    required: true
  tag: [
    name: String
  ]
  creationDate:
    type: Date
    required: true
    default: Date.now
)

tmpDirectory = os.tmpdir() || process.cwd() + '/public/media'
if process.env.NODE_ENV_FOLDER_MEDIA
  tmpDirectory = process.cwd() + process.env.NODE_ENV_FOLDER_MEDIA

MediaSchema.plugin attachments,
  directory: tmpDirectory
  storage:
    providerName: 'localfs'
    options:
      directory: process.cwd() + '/public/media'
  properties:
    image:
      styles:
        original:
          '$format': 'png'
        thumb:
          thumbnail: '40x40^'
          gravity: 'center'
          extent: '40x40'
          '$format': 'png'
        abstract:
          thumbnail: '240x240^'
          gravity: 'center'
          extent: '240x240'
          '$format': 'png'
        medium:
          thumbnail: '340x340^'
          gravity: 'center'
          extent: '240x240'
          '$format': 'png'
        large:
          thumbnail: '640x640^'
          gravity: 'center'
          extent: '640x640'
          '$format': 'png'

MediaSchema.plugin(mongoosePaginate)

module.exports = Mongoose.model('Media', MediaSchema)
