exports.register = (server, options, next) ->
  server.route require('./version/v1')
  next()
  return

exports.register.attributes =
  name: 'media'
  version: '1.0.0'