Media = require('./../../model')
Boom = require 'boom'
fs = require('fs')
async = require('async')
_ = require "lodash"

dist = process.env.NODE_FOLDER || 'dist'


Campaign  = require(process.cwd() + '/'+dist+'/modules/campaign/model').dao
Category  = require(process.cwd() + '/'+dist+'/modules/category/model').dao
methods =
  save: (request, reply) ->
    tags = []
    for tag in request.payload.tag.toString().split(',')
      tags.push
        name: tag
    media = new Media(
      name: request.payload.name
      tag: tags
    )
    if request.payload.image
      media.attach 'image', request.payload.image, (err) ->
        if err
          reply Boom.forbidden(err)
          return
        media.save (err, item) ->
          if err
            reply Boom.forbidden(err)
            return
          reply item
          return
        return
    else
      media.save (err, item) ->
        if err
          reply Boom.forbidden(err)
          return
        reply item
        return

  putById: (request, reply) ->
    tags = []
    for tag in request.payload.tag.toString().split(',')
      tags.push
        name: tag
    request.payload.tag = tags

    Media.findByIdAndUpdate(request.params.id, { $set: request.payload },{ new: true } ).exec (err, item) ->
      if !err and item
        reply item
      else
        reply Boom.notFound(err)
      return
    return

  getAll: (request, reply) ->
    q = false
    q = decodeURI(request.query.q).toString().split(' ') if request.query.q

    #Media.find({}, {'tag.name': {$in: request.params} }
    query = {}
    if q
      reg = []
      for x in q
        reg.push {
          "tag.name":
            $regex: x
        }
      query =
        $and: reg

    page = request.query.page || 1
    limit = request.query.limit || 10
    Media.paginate(query, {page: page, limit: limit, lean: true}).then (items) ->
      if items.docs
        # fix url
        items.docs.forEach (item) ->
          for k,img of item.image
            try
              img.url = img.defaultUrl.replace(process.cwd() + '/public', '')
        reply(items.docs).header('X-Total-Count', items.total)
      return
    return

  getById: (request, reply) ->
    Media.findById(request.params.id).lean().exec (err,item) ->
      if err
        reply Boom.forbidden(err)
        return
      if item
        # fix url
        for k,img of item.image
          try
            img.url = img.defaultUrl.replace(process.cwd() + '/public', '')
        tags = []
        for tag in item.tag
          tags.push text:tag.name
        item.tag = tags
        reply item
      return
    return


  getTags: (request, reply) ->
    Media.distinct("tag.name").lean().exec (err, items) ->
      reply items
      return
    return

  removeById: (request,reply) ->

    # verifico se è stata usando inun'altra collection
    # inquesto caso avviso l'utente cha se cancellata non sarà più visibile
    Media.findById(request.params.id).lean().exec (err,res) ->
      if err
        reply Boom.notFound()
        return

      if res
        path = res.image.original.path
        query =
          $or:[
            { 'media.logo.path': path },
            { 'media.images.path': path },
            { 'media.skin.path' : path },
            { 'media.cover.path' : path },
            { 'media.footer.path' : path }
            ]

        categories  = Category.find( query , 'title').exec()

        items = []
        async.parallel [
            (cb) ->
              Campaign.find( query,'title').exec (err,resC) ->
                if err
                  cb()
                  return
                items.push _.map(resC,"title")
                cb()
            (cb) ->
              categories.then (resC) ->
                items.push _.map(resC,"title")
                cb()
          ],
          (err) ->

            items = _.flatten(items)
            if items.length == 0
              for k,img of res.image
                if fs.existsSync(img.path)
                  fs.unlinkSync(img.path)
              methods.deleteDoc(res._id, reply)
              return
            reply(items)
            return
      return

  deleteDoc: (id,reply) ->
    Media.remove _id: id, (err, item) ->
      if !err and item
        reply([])
      else
        reply Boom.notFound(err)
      return
    return

module.exports = methods