Joi = require 'joi'
Handlers = require './handlers'
Version = "V1"
Base = "media"
Media = require('./../../model')


module.exports = [
  {
    method: 'POST'
    path: '/V1/media'
    config:
      description: 'Upload media'
      notes: 'Carica una nuova immagine'
      tags: ['api']
      handler: Handlers.save
      plugins:
        'hapi-swagger':
          payloadType: 'form'
      payload:
        maxBytes: 99999999
        output: 'file'
        parse: true
        allow: 'multipart/form-data'
      validate:
        headers: Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        payload:
          name: Joi.string().required()
          image: Joi.any().required().meta({swaggerType: 'file'})
          tag: Joi.string().optional()
      auth:
        strategy: 'token'
        scope: ['admin']
  }
  {
    method: 'GET'
    path: "/#{Version}/#{Base}"
    config:
      description: 'All media'
      notes: 'List all media'
      tags: ['api', "#{Version}", "#{Base}"]
      handler: Handlers.getAll
      auth:
        strategy: 'token'
        scope: ['admin']
      validate:
        query:
          page: Joi.number().optional()
          limit: Joi.number().optional()
          q: Joi.string().allow('').optional()
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
  }
  {
    method: 'GET'
    path: "/#{Version}/#{Base}/{id}"
    config:
      description: 'get by id'
      notes: 'List all media'
      tags: ['api', "#{Version}", "#{Base}"]
      handler: Handlers.getById
      auth:
        strategy: 'token'
        scope: ['admin']
      validate:
        params:
          id: Joi.string().optional()
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
  }
  {
    method: 'GET'
    path: "/#{Version}/#{Base}/tags"
    config:
      description: 'All media'
      notes: 'List all media tags'
      tags: ['api', "#{Version}", "#{Base}"]
      handler: Handlers.getTags
      auth:
        strategy: 'token'
        scope: ['admin']
      validate:
        query:
          page: Joi.number().optional()
          limit: Joi.number().optional()
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
  }
  {
    method: 'DELETE'
    path: "/#{Version}/#{Base}/{id}"
    config:
      description: 'Remove media with _id'
      notes: 'Remove media with _id'
      tags: ['api', "#{Version}", "#{Base}"]
      handler: Handlers.removeById
      auth:
        strategy: 'token'
        scope: ['admin']
      validate:
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        params:
          id: Joi.string().required()
  }
  {
    method: 'PUT'
    path: "/#{Version}/#{Base}/{id}"
    config:
      description: 'Put media with _id'
      notes: 'Put media with _id'
      tags: ['api', "#{Version}", "#{Base}"]
      handler: Handlers.putById
      auth:
        strategy: 'token'
        scope: ['admin']
      validate:
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        params:
          id: Joi.string().required()
        payload:
          name: Joi.string().required()
          tag: Joi.string().optional()
  }
]