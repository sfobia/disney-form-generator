Boom      = require 'boom'
Joi       = require 'joi'

dist = process.env.NODE_FOLDER || 'dist'


Template  = require process.cwd()+'/'+dist+'/modules/template/model'
Mustache  = require('mustache')
Notify    = require('./../../model')
fs        = require('fs')

nodemailer 	  = require("nodemailer")
smtpTransport = require('nodemailer-smtp-transport')
sgTransport   = require('nodemailer-sendgrid-transport')
sesTransport  = require('nodemailer-ses-transport')


smtpConfig =
  host: process.env.NODE_ENV_EMAIL_HOST
  port: process.env.NODE_ENV_EMAIL_PORT
  secure: process.env.NODE_ENV_EMAIL_SSL
  auth:
    user: process.env.NODE_ENV_EMAIL_USER
    pass: process.env.NODE_ENV_EMAIL_PASSWORD

SesConfig =
  accessKeyId: process.env.NODE_ENV_AWSACCESSKEY
  secretAccessKey: process.env.NODE_ENV_AWSSECRETKEY
  rateLimit: 5

sgConfig =
  auth:
    "api_key": process.env.NODE_ENV_SG_KEY


if process.env.NODE_ENV_EMAIL_TYPE == "SG"
  smtpTransport = nodemailer.createTransport sgTransport(sgConfig)
if process.env.NODE_ENV_EMAIL_TYPE == "SES"
  smtpTransport = nodemailer.createTransport sesTransport(SesConfig)
if process.env.NODE_ENV_EMAIL_TYPE == "SMTP"
  smtpTransport = nodemailer.createTransport smtpTransport(smtpConfig)

if process.env.NODE_ENV_EMAIL_TYPE
  noSendMail = false
  console.log "EMAIL : "+process.env.NODE_ENV_EMAIL_TYPE
else
  noSendMail = true
  console.log "EMAIL NOT CONFIGURED"

# carico il template html
templateHtml = "{{& content }}"
fs.readFile process.cwd()+'/views/email/template.html', 'utf8',  (err,data) ->
  if data
    console.log "Template html OK"
    templateHtml = data

SchemaMail =
  from: Joi.string().email().required()
  to: Joi.string().email().required()
  subject: Joi.string().required()
  body: Joi.string().required()

methods =
  saveNotify: (data,userId) ->
    notify =
      user: userId
      from: data.from
      to: data.to
      subject: data.subject
      body: data.body
    Notify.dao.create notify, (err,item) ->
      item

  buildTemplate: (doc,data) ->
    email = {}
    email.subject = Mustache.render(doc.subject,data)
    email.body    = Mustache.render(doc.body,data)
    email.from    = process.env.NODE_ENV_EMAIL_FROM || "andrea.ciccu@softfobia.com"
    email.to      = data.email
    return email

  getTemplate: (identifier) ->
    Template.dao.findOne({identifier:identifier}).exec (err,item) ->
      if !err and item
        item
      return false

  send: (identifier,userId,data) ->
    template = methods.getTemplate(identifier)
    template.then(
      (doc)->
        email       = methods.buildTemplate doc,data
        # in caso di test invio sempre site+email
        if process.env.NODE_ENV == "test"
          doc.type = 'site'

        if doc.type == 'site' || doc.type == 'site+email'
          notPromise  = methods.saveNotify email, userId
          notPromise.then ( notify ) ->
            if doc.type == 'site+email'
              methods.sendMail(email,notify._id)

        else if doc.type == 'email'
          methods.sendMail(email)

      (err) ->
        console.log err
    )
    return

  sendMail: (data,notifyId) ->
    if noSendMail
      console.log 'noSendMail'
      return

    #if process.env.NODE_ENV == "stage"
      #data.to = 'matteo.casu@softfobia.com' #mod
    #if process.env.NODE_ENV == "development"
      #data.to = 'matteo.casu@softfobia.com'

    Joi.validate data, SchemaMail, (err, value) ->
      if !err
        html = Mustache.render(templateHtml, { content: data.body })
        smtpTransport.sendMail {
          text: data.body
          from: data.from
          to: data.to
          subject: data.subject
          html: html
        }, (err, message) ->
          if !err
            # aggiorno il campo
            now =
              emailSent: new Date()
            if notifyId
              Notify.dao.update(_id: notifyId ,{ "$set": now }).exec (err,item) ->
                return item
          return
      else
        throw err
    return

module.exports = methods