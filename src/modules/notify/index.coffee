exports.register = (server, options, next) ->
  server.route require('./version/v1')
  Handlers = require './version/v1/handlers'
  server.expose('service', Handlers);

  next()
  return

exports.register.attributes =
  name: 'notify'
  version: '1.0.0'