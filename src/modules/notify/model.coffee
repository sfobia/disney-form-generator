Mongoose        = require('mongoose')
Schema          = Mongoose.Schema
mongoosePaginate = require('mongoose-paginate')

NotifySchema  = new Schema(
  user:
    type: Schema.ObjectId
    ref: 'User'
    required: true
  from:
    type: String
    required: true
  subject:
    type: String
  body:
    type: String
  emailSent:
      type: Date
  read:
    type: Boolean
    default: false
  creation:
    type: Date
    required: true
    default: Date.now
)
NotifySchema.plugin(mongoosePaginate)

module.exports.dao = Mongoose.model('Notify', NotifySchema)