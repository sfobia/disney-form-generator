Mongoose         = require('mongoose')
mongoosePaginate = require('mongoose-paginate')
Schema           = Mongoose.Schema

CandidateSchema  = new Schema(
  user:
    type: Schema.ObjectId
    ref: 'User'
    required: true
  campaign:
    type: Schema.ObjectId
    ref: 'Campaign'
    required: true
  active:
    type: Boolean
    required: true
  rejected:
    type: Boolean
  preconfirmed:
    type: Boolean
  dates:
    candidation:
      type: Date
      required: true
      default: Date.now
    accepted:
      type: Date
    rejected:
      type: Date
    sentTracking:
      type: Date
    sentSurvey:
      type: Date
  sentMail:
    type: Boolean
    default: false
  ship:
    number: String
    info: String
    url: String
  requisite:
    type: Boolean
    default: false
  product:
    type: String
    default: null
)

CandidateSchema.plugin(mongoosePaginate)

module.exports.dao = Mongoose.model('Candidate', CandidateSchema)