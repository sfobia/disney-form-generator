Candidate   = require('./../../model')
mongoose = require('mongoose')
Campaign = mongoose.model('Campaign')
Profile = mongoose.model('Profile')

dist = process.env.NODE_FOLDER || 'dist'

NotifyHandlers  = require(process.cwd()+ '/'+dist+'/modules/notify/version/v1/handlers')
JoguHandlers  = require(process.cwd()+ '/'+dist+'/modules/jogu/version/v1/handlers')

json2csv = require('json2csv')
fs = require('fs')

csv = require('fast-csv')
rq = require('request')

comuni = require('./comuni')

Boom = require 'boom'
Moment = require 'moment'
async = require 'async'

methods =
  save: (request, reply) ->
    Candidate.dao.find({'user': request.payload.user, 'campaign': request.payload.campaign}).exec (err, item) ->
      if item.length > 0
        reply Boom.forbidden('user already signed in for this campaign')
      else
        candidate = new Candidate.dao(request.payload)
        candidate.save (err, item) ->
          if err
            reply Boom.forbidden(err)
          else
            reply item
          return
        return

  getById: (request, reply) ->
    Candidate.dao.findById(request.params.id).exec (err, item) ->
      if !err and item
        reply item
      else
        reply Boom.notFound(err)
      return
    return

  getAll: (request, reply) ->
    page = request.query.page || 1
    limit = request.query.limit || 10
    order = request.query.order || "date"
    sort = request.query.sort || ' '
    sortOrder = sort + order
    Candidate.dao.paginate({}, {select: "user campaign type active rejected _id dates", page: page, limit: limit, lean: true, sort: sortOrder}).then (item) ->
      if item.docs
        reply(item.docs).header('X-Total-Count', item.total)
      else
        reply Boom.notFound()
      return
    return

  putById: (request, reply) ->
    Candidate.dao.findByIdAndUpdate(request.params.id, { $set: request.payload }, { new: true } ).exec (err, item) ->
      if !err and item
        reply item
      else
        reply Boom.notFound(err)
      return
    return

  deleteById: (request, reply) ->
    reply Boom.badRequest('remove disabled')
    return
    Candidate.dao.remove _id: request.params.id, (err, item) ->
      if !err and item
        reply('ok')
      else
        reply Boom.notFound(err)
      return
    return

  query: (request, reply) ->

    query = {}
    if request.payload.hasOwnProperty('active')
      query.active = {"$eq": request.payload.active}
    if request.payload.hasOwnProperty('rejected')
      query.rejected =  {"$eq": request.payload.rejected}
    if request.payload.hasOwnProperty('user')
      query.user = request.payload.user
    if request.payload.hasOwnProperty('campaign')
      query.campaign = request.payload.campaign

    page = request.query.page || 1
    limit = request.query.limit || 10
    order = request.query.order || "date"
    sort = request.query.sort || '-'
    sortOrder = sort + order
    Candidate.dao.paginate(query, { page: page, limit: limit, lean: true, populate: [{ path: "user",select:"-password -token"},{ path: "campaign",select:"title form dates"}], sort: sortOrder}).then (item) ->
      if item.docs
        if ! item.docs.length
          reply([]).header('X-Total-Count', 0)
          return
        reply(item.docs).header('X-Total-Count', item.total)

        ###
        item.docs.forEach ( candidate, index ) ->
          Profile.findOne({ user: candidate.user }).then ( profile ) ->
            item.docs[index].profile = profile
            reply item.docs
            return
          return
        ###

      else
        reply Boom.notFound()
      return
    return

  totalCandidate: (request, reply) ->
    Candidate.dao.aggregate [
        {
          $match: {
            campaign: mongoose.Types.ObjectId(request.params.id)
          }
        }
        {
          $group: {
            _id: "$user"  , count: { $sum: 1 }
          }
        }
      ], (err, item) ->
        if err
          reply total: 0
        else
          reply total: item.length
        return

  totalCandidatePreconfirmed: (request, reply) ->
    Candidate.dao.count(campaign:request.params.id, preconfirmed: true).exec (err,item) ->
      if err
        reply total: 0
      else
        reply total: item
      return


  totalCandidateActived:  (request, reply) ->
    Candidate.dao.count(campaign:request.params.id, active: true ).exec (err,item) ->
      if err
        reply total: 0
      else
        reply total: item
      return

  totalCandidateRejected:  (request, reply) ->
    #todo da verificare
    Candidate.dao.aggregate [
      {
        $match: {
          campaign: mongoose.Types.ObjectId(request.params.id)
          active: false
        }
      }
      {
        $group: {
          _id: "$user"  , count: { $sum: 1 }
        }
      }
    ], (err, item) ->
      if err
        reply total: 0
      else
        reply total: item.length
      return

  history: (request, reply) ->
    query = {}
    if request.payload.hasOwnProperty('user')
      query.user = request.payload.user

    page = request.query.page || 1
    limit = request.query.limit || 10

    Candidate.dao.paginate(query, {page: page, limit: limit, lean: true, sort: 'date asc'}).then (item) ->
      if item.docs
        reply(item.docs).header('X-Total-Count', item.total)
      else
        reply Boom.notFound()
      return
    return

  historyActive: (request, reply) ->
    query = {}
    if request.payload.hasOwnProperty('user')
      query.user = request.payload.user
    if request.payload.hasOwnProperty('active')
      query.active = {"$eq": request.payload.active}

    page = request.query.page || 1
    limit = request.query.limit || 10

    Candidate.dao.paginate(query, {page: page, limit: limit, lean: true, sort: 'date asc'}).then (item) ->
      if item.docs
        reply(item.docs).header('X-Total-Count', item.total)
      else
        reply Boom.notFound()
      return
    return

  acceptCandidates: (request, reply) ->
    #aggiorna candidati con true su active e false su rejected
    #controllo uguale su campaign/api/handlers
    Candidate.dao.update({_id: {"$in": request.payload}}, $set: {
      active: true
      rejected: false
      'dates.accepted': new Moment()
    }, {multi: true}).exec (err, res) ->
      Campaign.update(_id: request.params.campaign, {
        $addToSet: {
          "candidates":
            $each: request.payload
        }
      }).exec (err, itemCampaign) ->
        reply res
        return
      return

  rejectCandidates: (request, reply) ->
    #aggiorna candidati con true su active e false su rejected
    #todo  upsert: true è stato tolto, poiche la sua funzione e quella di creare un oggetto se non esiste
    Candidate.dao.update({_id: {"$in": request.payload}}, $set: {
      active: false
      rejected: true
      'dates.rejected': new Moment()
    }, {multi: true}).exec (err, res) ->
      reply res
      return

  rejectAllCandidates: (request, reply) ->
    Candidate.dao.update({ campaign: request.params.campaign, active: false }, $set: {
      rejected: true
      'dates.rejected': new Moment()
    }, {multi: true}).exec (err, res) ->
      reply res
      return

  preconfirmCandidates:  (request, reply) ->
    Candidate.dao.findByIdAndUpdate({ _id:  request.params.id },
      $set: {
        preconfirmed: request.payload.preconfirmed
      }
    ).exec (err, res) ->
      reply res
    return


  sendMail: (request, reply) ->
    candidates = []
    confirmedCandidates = Candidate.dao.find({
      campaign: request.params.campaign, active: true, sentMail: false
    }).populate([
      {
        path: 'user', select:"email profile",
        populate: { path: 'profile', select: 'name', model: 'Profile'}
      },
      {
        path: 'campaign', select:"title media"
      }
    ]).lean().exec()
    confirmedCandidates.then (items) ->

      async.each items, ((item, cb) ->

        # fix url
        if item.campaign.media
          for k,img of item.campaign.media
            for imag in img
              if imag.path
                imag.url = imag.path.replace(process.cwd() + '/public', '')

        data = {}
        data.email = item.user.email
        data._id = item.user._id
        data.name = item.user.profile.name
        data.username = item.user.username

        data.product = {}
        data.product.title = item.campaign.title

        try
          if item.campaign.media && item.campaign.media.logo && item.campaign.media.logo[0]
            data.product.logo =  process.env.NODE_ENV_DOMAIN_ASSETS+item.campaign.media.logo[0].url

          if item.campaign.media && item.campaign.media.cover && item.campaign.media.cover[0]
            data.product.cover = process.env.NODE_ENV_DOMAIN_ASSETS+item.campaign.media.cover[0].url


        candidates.push item._id
        NotifyHandlers.send('CANDIDATE_ACCEPTED', data._id, data)

        jogu =
          "utente": data._id
          "canale": "campaign"
          "riferimento": request.params.campaign
        JoguHandlers.sendAttivita('candidatura_accettata', jogu)

        Candidate.dao.findByIdAndUpdate({ _id: item._id }, { $set: 'sentMail': true }).exec (err, updated) ->
          cb()
          return

      ), (err) ->
        if err
          reply Boom.badRequest(err)
          return

        if candidates
          reply { 'numberSentMail': candidates.length }
          ###
          Candidate.dao.update({ _id: { $in: candidates}}, $set: { sentMail: true}).exec (err, candidate) ->
            console.log candidate
            if err
              reply Boom.badRequest(err)
              return
            else
              reply {'numberSentMail': candidates.length}
              return
          ###
        else
          reply Boom.badRequest()
          return

        return
    return

  acceptPreconfirmed: (request, reply) ->

    ids = []

    async.waterfall [
      (cb) ->
        Candidate.dao.update({campaign: request.params.campaign, preconfirmed: true}, $set: {
          active: true
          rejected: false
          'dates.accepted': new Moment()
        }, {multi: true}).exec (err, items) ->
          if err
            reply Boom.notFound()
          else
            cb(null)
      (cb) ->
        Candidate.dao.find({campaign: request.params.campaign, active: true, preconfirmed: true}).exec (err, items) ->
          items.forEach (item) ->
            ids.push item._id
          addCandidates = Campaign.update(_id: request.params.campaign, {
            $addToSet: {
              "candidates":
                $each: ids
            }
          }).exec()
          addCandidates.then (candidates) ->
            cb(null, candidates)
    ], (err, res) ->
      if err
        reply Boom.badRequest(err)

      reply {'numberAccepted': ids.length}
      return
    return

  importCsv: (request, reply) ->
    total = 0
    if request.payload.csv

      options =
        headers: true
        delimiter: ';'

      csv.fromPath( request.payload.csv.path, options ).on('data', (data) ->
        update_object =
          ship:
            number: data.tracking_code
            info: data.tracking_info
            url: data.tracking_url

        if data.tracking_code
          total++
          Candidate.dao.findByIdAndUpdate( data.candidate_id , { $set: update_object }  ).exec (err, item) ->
            return
        return

      ).on 'end', ->
        reply { 'status': 200, total:total }
        return

      return

    else
      reply Boom.badRequest()

  exportCandidates: ( request, reply ) ->

    if request.query?
      if request.query.candidatesIds?
        multiProduct = true

    console.log "multiproduct: "+multiProduct

    populate = [
      { path: 'campaign', select: 'title slug' }
      { path: 'user', select: 'email profile', populate: path: 'profile', select: 'name form', model: 'Profile' }
    ]

    users = []

    getColumn = ( name ) ->
      name = name.toLowerCase()
      if name.indexOf('indirizzo') > -1
        return 'address'
      if name.indexOf('presso') > -1
        return 'address_spec'
      if name.indexOf('provincia') > -1
        return 'province'
      if name.indexOf('cap') > -1
        return 'cap'
      if name.indexOf('civico') > -1
        return 'house_number'
      if name.indexOf('citt') > -1
        return 'city'
      if name.indexOf('cellulare') > -1
        return 'phone'
      return null

    filename = "campaign"

    comuniGiusti = []
    for com in comuni
      comune = com.split ';'
      comuniGiusti[comune[0]] = comune[1]

    async.waterfall [
      (cb) ->
        candidates = ''

        if !multiProduct
          console.log 'no multip, prendo candidati solito metodo'
          Candidate.dao.find({ campaign: request.params.campaign, active: true }).populate( populate ).exec (err, items) ->
            candidates = items
            cb null, candidates
        else
          console.log 'multip, prendo candidati dagli id: '+request.query.candidatesIds
          Candidate.dao.find({ _id: {"$in": request.query.candidatesIds}}).populate( populate ).exec (err, items) ->
            candidates = items
            cb null, candidates

      (items, cb) ->

        avoidDuplicates = []
        items.forEach (item) ->
          filename = item.campaign.slug

          if item.user && item.user._id
            if avoidDuplicates.indexOf(item.user._id) == -1
              avoidDuplicates.push item.user._id

              console.log 'prodotto: '+item.product
              obj =
                candidate_id: item._id
                user_id: item.user._id
                email: item.user.email
                first_name: item.user.profile.name.first
                last_name: item.user.profile.name.last
                campaign_id: item.campaign._id
                campaign_title: item.campaign.title
                address: ""
                address_spec: ""
                house_number: ""
                city: ""
                region: ""
                province: ""
                cap: ""
                phone: ""
                zone: ""
                tracking_code: item.ship.number || ""
                tracking_info: item.ship.info || ""
                tracking_url: item.ship.url || ""
              if multiProduct
                obj.product = item.product || ""

              console.log obj

              shipping = item.user.profile.form.filter ( item ) ->
                return item.formType == 'shipping'

              if shipping[0]
                shipping[0].values.forEach ( value ) ->

                  cap_type = false

                  keyname = getColumn( value.name )

                  if keyname || value.type == 'cap'
                    if value.type == 'cap'

                      cap_type = true

                      obj['province'] = value.value.provincia
                      obj['region'] = value.value.regione
                      obj['zone'] = value.value.zona

                      if comuniGiusti[ value.value.citta ]
                        obj['city'] =  comuniGiusti[ value.value.citta ]
                      else
                        obj['city'] = value.value.citta

                      obj['cap'] = value.value.cap

                    else if value.type == 'checkbox'
                      obj[ keyname ] = ""

                    else
                      if !obj[ keyname ] && !cap_type
                        obj[ keyname ] = value.value

              users.push obj

          cb()

      (cb) ->

          fields = ['candidate_id', 'user_id', 'email', 'first_name', 'last_name', 'campaign_id', 'campaign_title', 'address', 'address_spec',
          'house_number', 'city', 'province', 'region', 'zone', 'cap', 'phone', 'tracking_code', 'tracking_info', 'tracking_url' ]

          if multiProduct
            fields.push 'product'

          csvfile = 'public/assets/backend/'+filename+'.csv'

          json2csv {
            data: users
            fields: fields
            quotes: ''
            del: ';'
          }, (err, csv) ->
            if err
              reply { msg: err, file: false }
              return
            fs.writeFile csvfile, csv, (err, s) ->
              if err
                throw err
                # sostituisco public per poter scaricare il file
              cb(null,csvfile)
              return
            return

    ], (err, res) ->
      if err
        reply Boom.badRequest(err)
        return

      reply { msg: 'file saved', file: res.replace('public/', '') }
      return

    return

  sendTracking: (request, reply) ->

    campaignId = request.params.campaign
    total = 0
    candidates = Candidate.dao.find({campaign: campaignId, active: true, 'dates.sentTracking': { $exists: false } }).populate({path: 'user', populate: {path: 'profile', select: 'name', model: 'Profile'}}).exec()
    candidates.then (items) ->
      async.each items, ((item, cb) ->

        if item.ship and item.ship.info and item.ship.number

          data = {}
          data.email = item.user.email
          data._id = item.user._id
          data.name = item.user.profile.name
          data.username = item.user.username
          data.number = item.ship.number
          data.info = item.ship.info
          if item.ship.url
            data.url = item.ship.url

          #todo aggiungere indirizzo utente

          NotifyHandlers.send( 'SEND_TRACKING', item.user._id, data  )

          Candidate.dao.findByIdAndUpdate(item._id, { $set: 'dates.sentTracking': new Moment() }).exec (err, updated) ->
            total++
            cb()
            return

        else
          cb()

      ), (err) ->
        if err
          reply Boom.badRequest(err)
          return
        reply total: total
        return
    return

  sendSurvey: (request, reply) ->

    campaignId = request.params.campaign
    total = 0
    candidates = Candidate.dao.find({campaign: campaignId, active: true, 'dates.sentSurvey': { $exists: false } }).populate({path: 'user', populate: {path: 'profile', select: 'name', model: 'Profile'}}).exec()
    candidates.then (items) ->
      async.each items, ((item, cb) ->

        data = {}
        data.email = item.user.email
        data._id = item.user._id
        data.name = item.user.profile.name
        data.username = item.user.username
        url = process.env.NODE_ENV_DOMAIN_PRIMARY || "http://localhost:8100"
        data.link = url+'/it/profilo.html#/listCampaignProfile'

        NotifyHandlers.send( 'SEND_SURVEY', item.user._id, data  )

        Candidate.dao.findByIdAndUpdate(item._id, { $set: 'dates.sentSurvey': new Moment() }).exec (err, updated) ->
          total++
          cb()
          return

      ), (err) ->
        if err
          reply Boom.badRequest(err)
          return
        reply total: total
        return
    return
module.exports = methods