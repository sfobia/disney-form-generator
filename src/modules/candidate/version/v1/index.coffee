Joi         = require 'joi'
Handlers    = require './handlers'
Version     = "V1"
Base        = "candidate"


candidateModel = Joi.object(
  _id: Joi.any().optional()
  __v: Joi.any().optional()
  id: Joi.any().required()
  user: Joi.any().required()
  campaign: Joi.any().required()
  active: Joi.boolean().required().description('indicates if candidate is active')
  rejected: Joi.boolean().optional().description('rejection of the candidate')
  sentMail: Joi.boolean().optional().description('sent email or not')
  preconfirmed: Joi.boolean().optional().description('preconfirmation of candidate')
  product: Joi.string().optional().description('product to give')
).meta(className: 'Campaign').options(allowUnknown: true,abortEarly: false )


payload = Joi.object(
  user: Joi.any().required()
  campaign: Joi.any().required()
  active: Joi.boolean().required().description('indicates if candidate is active')
  rejected: Joi.boolean().optional().description('rejection of the candidate')
  sentMail: Joi.boolean().optional().description('sent email or not')
  preconfirmed: Joi.boolean().optional().description('preconfirmation of candidate')
  product: Joi.string().optional().description('product to give')
)

payloadQuery = Joi.object(
  user: Joi.any().optional()
  campaign: Joi.any().optional()
  active: Joi.boolean().optional().description('indicates if candidate is active')
  rejected: Joi.boolean().optional().description('rejection of the candidate')
  sentMail: Joi.boolean().optional().description('sent email or not')
  preconfirmed: Joi.boolean().optional().description('preconfirmation of candidate')
  product: Joi.string().optional().description('product to give')
)

idOne = Joi.any().optional()
ids = Joi.array().items(idOne).meta(className: 'IdList')

listModel = Joi.array().items(candidateModel).meta(className: 'List')

module.exports = [
  {
    method: 'POST'
    path: "/#{Version}/#{Base}"
    config:
      description: 'Create new candidate'
      notes: 'Creates a new candidate'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.save
      auth:
        strategy: 'token'
        scope: ['admin']
      validate:
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        payload:
          payload
  }
  {
    method: 'GET'
    path: "/#{Version}/#{Base}/{id}"
    config :
      description: 'Get Candidate id'
      notes: 'Detail of a candidate providing a valid _id'
      tags: ['api',"#{Version}","#{Base}"]
      response:
        schema: candidateModel
      validate:
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        params:
          id: Joi.string().required().description('id candidate')
      handler: Handlers.getById
      auth:
        strategy: 'token'
        scope: ['admin']
  }
  {
    method: 'GET'
    path: "/#{Version}/#{Base}"
    config :
      description: 'All candidates'
      notes: 'List of all candidations without any filter'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.getAll
      auth:
        strategy: 'token'
        scope: ['admin']
      response: schema: listModel
      validate:
        query:
          page: Joi.number().optional()
          limit: Joi.number().optional()
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
  }
  {
    method: 'PUT'
    path: "/#{Version}/#{Base}/{id}"
    config :
      description: 'Put candidate'
      notes: 'Modify a candidate providing a valid _id'
      tags: ['api',"#{Version}","#{Base}"]
      response:
        schema: candidateModel
      validate:
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        params:
          id: Joi.string().required().description('id candidate')
        payload: payload
      handler: Handlers.putById
      auth:
        strategy: 'token'
        scope: ['admin']
  }
  {
    method: 'DELETE'
    path: "/#{Version}/#{Base}/{id}"
    config:
      description: 'Delete a candidate'
      notes: 'Delete the candidate with given _id'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.deleteById
      auth:
        strategy: 'token'
        scope: ['admin']
      validate:
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        params:
          id: Joi.string().required().description('candidate id')
  }
  {
    method: 'POST'
    path: "/#{Version}/#{Base}/query"
    config:
      description: 'Query candidates'
      notes: 'Search candidates with given parameters'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.query
      auth:
        strategy: 'token'
        scope: ['admin']
      validate:
        query:
          page: Joi.number().optional()
          limit: Joi.number().optional()
          sort: Joi.string().optional()
          order: Joi.string().optional()
        headers: Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        payload: payloadQuery

  }
  {
    method: 'GET'
    path: "/#{Version}/#{Base}/total/{id}"
    config:
      description: 'Total candidates for campaign'
      notes: 'Total candidates for campaign'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.totalCandidate
      auth:
        strategy: 'token'
        scope: ['admin']
      validate:
        headers: Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        params:
          id: Joi.string().required().description('campaign id')
  }
  {
    method: 'GET'
    path: "/#{Version}/#{Base}/total-preconfirmed/{id}"
    config:
      description: 'Total candidates preconfirmed for campaign'
      notes: 'Total candidates preconfirmed for campaign'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.totalCandidatePreconfirmed
      auth:
        strategy: 'token'
        scope: ['admin']
      validate:
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        params:
          id: Joi.string().required().description('campaign id')
  }
  {
    method: 'GET'
    path: "/#{Version}/#{Base}/total-accepted/{id}"
    config:
      description: 'Total candidates active for campaign for send mail'
      notes: 'Total candidates active for campaign'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.totalCandidateActived
      auth:
        strategy: 'token'
        scope: ['admin']
      validate:
        headers: Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        params:
          id: Joi.string().required().description('campaign id')
  }
  {
    method: 'GET'
    path: "/#{Version}/#{Base}/total-rejected/{id}"
    config:
      description: 'Total candidates rejected for campaign for send mail'
      notes: 'Total candidates rejected for campaign'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.totalCandidateRejected
      auth:
        strategy: 'token'
        scope: ['admin']
      validate:
        headers: Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        params:
          id: Joi.string().required().description('campaign id')
  }
  {
    method: 'POST'
    path: "/#{Version}/#{Base}/accepted/{campaign}"
    config:
      description: 'Accept candidates in the campaign'
      notes: 'Activate a candidate for the campaign'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.acceptCandidates
      auth:
        strategy: 'token'
        scope: ['admin']
      validate:
        payload: ids
        headers: Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        params:
          campaign: Joi.any().required()
  }
  {
    method: 'POST'
    path: "/#{Version}/#{Base}/preconfirm/{id}"
    config:
      description: 'preconfirm candidates in the campaign'
      notes: 'preconfirm a candidate for the campaign'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.preconfirmCandidates
      auth:
        strategy: 'token'
        scope: ['admin']
      validate:
        headers: Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        params:
          id: Joi.string().required()
        payload:
          preconfirmed: Joi.boolean().required()
  }
  {
    method: 'POST'
    path: "/#{Version}/#{Base}/reject/{campaign}"
    config:
      description: 'Reject candidates in the campaign'
      notes: 'Reject a candidate for the campaign'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.rejectCandidates
      auth:
        strategy: 'token'
        scope: ['admin']
      validate:
        payload: ids
        headers: Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        params:
          campaign: Joi.any().required()
  }
  {
    method: 'POST'
    path: "/#{Version}/#{Base}/reject/all/{campaign}"
    config:
      description: 'Reject all candidates in the campaign'
      notes: 'Reject all candidate for the campaign'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.rejectAllCandidates
      auth:
        strategy: 'token'
        scope: ['admin']
      validate:
        headers: Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        params:
          campaign: Joi.any().required()
  }
  {
    method: 'POST'
    path: "/#{Version}/#{Base}/acceptPreconfirmed/{campaign}"
    config:
      description: 'Accept preconfirmed candidates in the campaign'
      notes: 'Activate all preconfirmed candidates for the campaign'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.acceptPreconfirmed
      auth:
        strategy: 'token'
        scope: ['admin']
      validate:
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        params:
          campaign: Joi.any().required()
  }
  {
    method: 'POST'
    path: "/#{Version}/#{Base}/export/{campaign}"
    config:
      description: 'Export confirmed candidates in the campaign'
      notes: 'Export confirmed candidates for the campaign'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.exportCandidates
      auth:
        strategy: 'token'
        scope: ['admin']
      validate:
        headers: Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        params:
          campaign: Joi.any().required()
  }
  {
    method: 'POST'
    path: "/#{Version}/#{Base}/sendMail/{campaign}"
    config:
      description: 'Send mail to accepted candidates in the campaign'
      notes: 'Send mail to all accepted candidates for the campaign'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.sendMail
      auth:
        strategy: 'token'
        scope: ['admin']
      validate:
        headers: Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        params:
          campaign: Joi.any().required()
  }
  {
    method: 'POST'
    path: "/#{Version}/#{Base}/import/csv"
    config:
      description: 'Upload media'
      notes: 'Salva csv upload'
      tags: ['api', "#{Version}", "#{Base}"]
      handler: Handlers.importCsv
      plugins:
        'hapi-swagger':
          payloadType: 'form'
      payload:
        maxBytes: 999990000
        output: 'file'
        parse: true
        allow: 'multipart/form-data'
      validate:
        headers: Joi.object('authorization': Joi.string().required()).options(allowUnknown: true)
        payload:
          csv: Joi.any().required().meta({swaggerType: 'file'})
      auth:
        strategy: 'token'
        scope: ['admin']
  }
  {
    method: 'POST'
    path: "/#{Version}/#{Base}/sendTracking/{campaign}"
    config:
      description: 'Send mail with tracking codes to accepted candidates in the campaign'
      notes: 'Send mail with tracking codes to all accepted candidates for the campaign'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.sendTracking
      auth:
        strategy: 'token'
        scope: ['admin']
      validate:
        headers: Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        params:
          campaign: Joi.any().required()
  }
  {
    method: 'POST'
    path: "/#{Version}/#{Base}/sendSurvey/{campaign}"
    config:
      description: 'Send mail with survey to accepted candidates in the campaign'
      notes: 'Send mail with survey to all accepted candidates for the campaign'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.sendSurvey
      auth:
        strategy: 'token'
        scope: ['admin']
      validate:
        headers: Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        params:
          campaign: Joi.any().required()
  }
]