Mongoose        = require('mongoose')
os = require('os')
path = require('path')
attachments = require('mongoose-attachments-localfs')
Schema = Mongoose.Schema
mongoosePaginate = require('mongoose-paginate')


ProfileSchema  = new Schema(
  user:
    type: Schema.ObjectId
    ref: 'User'
    required: true
    unique: true
    index: true
  name:
    first:
      type: String
      required: false
    last:
      type: String
      required: false
  gender:
    type: String,
    enum: ['Male', 'Female']
    required: false
  birthDate:
    type: Date
    required: false
    default: Date.now
  province:
    type: String
  cover:
    type: String
  phone:
    mobile:
      type: String
      required: false
    home:
      type: String
      required: false
    office:
      type: String
      required: false
  newsletter: Schema.Types.Mixed
  accepted:
    privacy:
      type: Boolean
      required: false
    marketing:
      type: Boolean
      required: false
    dem:
      type: Boolean
      required: false
    profiling:
      type: Boolean
      required: false
    regulation:
      type: Boolean
      required: false
    clauses:
      type: Boolean
      required: false
  profileCompleted:
      type: Boolean
      default: false
  shippingCompleted:
    type: Boolean
    default: false
  psicoLinguisticaResult:
    type: Schema.Types.Mixed
  form: [
    {
      ref:
        type: Schema.ObjectId
        ref: 'Form'
        required: true
      campaign:
        type: Schema.ObjectId
        ref: 'Campaign'
      name:
        type: String
      prevalid:
        type: Boolean
        default: false
      values: [
        ref:
          type: String
        type:
          type: String
        name:
          type: String
        value:
          type: Schema.Types.Mixed
        refvalue:
          type: String
        info:
          type: Schema.Types.Mixed
        product:
          type: String
        altro:
          type: String
        multi:
          type: Schema.Types.Mixed
      ]
      formType: String
    }
  ]
)

# os.tmpdir() ||
tmpDirectory = process.cwd() + '/public/media'
if process.env.NODE_ENV_FOLDER_MEDIA
  tmpDirectory = process.cwd() + process.env.NODE_ENV_FOLDER_MEDIA

ProfileSchema.plugin attachments,
  directory: tmpDirectory
  storage:
    providerName: 'localfs'
    options:
      directory: process.cwd() + '/public/media'
  properties:
    photo:
      styles:
        original:
          '$format': 'png'
        thumb:
          thumbnail: '40x40^'
          gravity: 'center'
          extent: '40x40'
          '$format': 'png'
        medium:
          thumbnail: '340x340^'
          gravity: 'center'
          extent: '240x240'
          '$format': 'png'



module.exports.blacklist = ["password","token","scope","confirmToken"]
module.exports.pop = ->
  if ! module.exports.blacklist
    return ''
  else
    '-'+module.exports.blacklist.join(" -")

ProfileSchema.plugin(mongoosePaginate)
module.exports.dao = Mongoose.model('Profile', ProfileSchema)