Joi         = require 'joi'
Handlers    = require './handlers'
Version     = "V1"
Base        = "profile"
Profile   = require('./../../../model')

dist = process.env.NODE_FOLDER || 'dist'


Campaign = require process.cwd() + '/'+dist+'/modules/campaign/api/version/v1/schema'
Category = require process.cwd() + '/'+dist+'/modules/category/version/v1/schema'

answerModel = Joi.object(
  _id: Joi.any().optional()
  __v: Joi.any().optional()
  name: Joi.string().required().description('answer name')
  percent: Joi.string().optional().description('answer percent')
  image: Joi.string().optional().description('answer image')
)

questionModel = Joi.object(
  _id: Joi.any().optional()
  __v: Joi.any().optional()
  title: Joi.string().required().description('question title')
  type: Joi.string().required().description('question type')
  description: Joi.string().required().description('question description')
  answers: Joi.array().items(answerModel).required().description('answers')
)

profileSurveyModel = Joi.object(
  _id: Joi.any().optional()
  __v: Joi.any().optional()
  title: Joi.string().required().description('title')
  type: Joi.string().required().description('type')
  status:
    actual: Joi.string().optional().description('actual status')
    afterClose: Joi.string().optional().description('after conclusion status')
  questions: Joi.array().items(questionModel).optional().description('questions')
  campaign: Joi.any().optional().description('campaign id')
  published: Joi.boolean().optional().description('published or not')
)

candidateModel = Joi.object(
  _id: Joi.any().optional()
  __v: Joi.any().optional()
  id: Joi.any().required()
  user: Joi.any().required()
  campaign: Joi.any().required()
  active: Joi.boolean().required().description('indicates if candidate is active')
  rejected: Joi.boolean().optional().description('rejection of the candidate')
)

campaignImageModel = Joi.object(
  _id: Joi.any().optional()
  __v: Joi.any().optional()
  name: Joi.string().optional()
)

campaignTagModel = Joi.object(
  _id: Joi.any().optional()
  __v: Joi.any().optional()
  name: Joi.string().optional()
)

newlsetterModelItem = Joi.object(
  name: Joi.string().optional(),
  title: Joi.string().optional()
)

categoryModel = Joi.object(
  _id: Joi.any().optional()
  __v: Joi.any().optional()
  title: Joi.string().required().description('category title')
  description: Joi.string().optional().description('category description')
  media:
    logo: Joi.array().items(Category.categoryImageModel).optional()
    images: Joi.array().items(Category.categoryImageModel).optional()
  order: Joi.number().optional().description('category order')
  published: Joi.boolean().optional().description('indicate if the category is published')
)

profileCampaignModel = Joi.object(
  _id: Joi.any().required()
  __v: Joi.any().optional()
  form: Joi.any().optional() # qui dovrei prendere il model form facendo export dal model form
  title: Joi.string().required().description('campaign title')
  description: Joi.string().optional().description('campaign description')
  type: Joi.string().required().description('campaign type')
  dates:
    startCampaign: Joi.date().required().description('start date of the campaign')
    endCampaign: Joi.date().required().description('end date of the campaign')
    startCandidations: Joi.date().required().description('start date for candidations')
    endCandidations: Joi.date().required().description('end date for candidations')
    releaseFeedback: Joi.date().optional().description('release date of user feedbacks')
  media:
    logo: Joi.array().items(Campaign.campaignImageModel).optional()
    images: Joi.array().items(Campaign.campaignImageModel).optional()
  tag: Joi.array().items(campaignTagModel).optional()
  link: Joi.string().optional().description('campaign link')
  published: Joi.boolean().optional().description('indicate if the campaign is published')
  categories: Joi.array().items(categoryModel).optional().description('categories associated to the campaign')
  candidates: Joi.array().items(candidateModel).optional().description('candidates associated to the campaign')
)

profileModel = Joi.object(
  _id: Joi.any().optional()
  __v: Joi.any().optional()
  id: Joi.string().required()
  user: Joi.any().optional() # qui dovrei prendere il model user facendo export dal model user
  name:
    first: Joi.string().optional().description('first name')
    last: Joi.string().optional().description('last name')
  gender: Joi.string().optional().description('gender')
  birthDate: Joi.date().optional().description('birth date')
  phone:
    mobile: Joi.string().optional().description('mobile phone number')
    home: Joi.string().optional().description('home phone number')
    office: Joi.string().optional().description('office phone number')
  accepted:
    marketing: Joi.boolean().optional().description('accepted marketing policy')
    dem: Joi.boolean().optional().description('accepted dem policy')
    profiling: Joi.boolean().optional().description('accepted profiling policy')
    privacy: Joi.boolean().optional().description('accepted privacy policy')
    conditions: Joi.boolean().optional().description('accepted conditions policy')
  newsletter: Joi.any().optional().description('newsletter')
).meta(className: 'Profile').options(allowUnknown: true,abortEarly: false )

newlsetterModelItem = Joi.object(
  name: Joi.string().required()
)

listModel = Joi.array().items(profileModel).meta(className: 'List')
listCampaignModel = Joi.array().items(profileCampaignModel).meta(className: 'ListCampaignModel')

payload =
  user: Joi.string().required()
  name:
    first: Joi.string().optional()
    last: Joi.string().optional()
  gender: Joi.string().allow('Male', 'Female')
  birthDate: Joi.date()
  province: Joi.string().optional()
  phone:
    mobile: Joi.string().optional()
    home: Joi.string().optional()
    office: Joi.string().optional()
  accepted:
    marketing: Joi.boolean()
    dem: Joi.boolean().optional()
    profiling: Joi.boolean().optional()
    privacy: Joi.boolean().optional()
    conditions: Joi.boolean().optional()
  newsletter: Joi.any().optional()
  photo: Joi.string().optional()

payloadUpdate =
  status: Joi.string().required()

payloadQuery =
  user: Joi.string().optional()
  'user.email': Joi.string().optional()
  'name.first': Joi.string().optional()
  'name.last': Joi.string().optional()
  gender: Joi.string().allow('Male', 'Female').optional()
  birthDate: Joi.date().optional()
  province: Joi.string().optional()
  'accepted.marketing': Joi.boolean().optional()
  'accepted.dem': Joi.boolean().optional()
  'accepted.profiling': Joi.boolean().optional()
  'accepted.privacy': Joi.boolean().optional()
  'accepted.conditions': Joi.boolean().optional()
  newsletter: Joi.any().optional()
  photo: Joi.string().optional()
  q: Joi.string().optional()

payloadResp =
  type: Joi.string().optional()
  name: Joi.string().optional()
  value: Joi.string().optional()

payloadForm =
  ref: Joi.string().required()
  name: Joi.string().optional()
  values: Joi.array(payloadResp).optional()
  formType: Joi.string().optional()

module.exports = [
  {
    method: 'POST'
    path: "/#{Version}/#{Base}"
    config:
      description: 'Create new profile'
      notes: 'Creates a new profile for a User'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.save
      auth:
        strategy: 'token'
        scope: ['admin']
      validate:
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        payload: payload
  }
  {
    method: 'DELETE'
    path: "/#{Version}/#{Base}/{id}"
    config:
      description: 'Delete a profile'
      notes: 'Delete the profile with given _id'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.deleteById
      auth:
        strategy: 'token'
        scope: ['admin']
      validate:
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        params:
          id: Joi.string().required().description('id profile')
  }
  {
    method: 'GET'
    path: "/#{Version}/#{Base}"
    config :
      description: 'All profiles'
      notes: 'List of all profiles without any filter'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.getAll
      auth:
        strategy: 'token'
        scope: ['admin']
      response: schema: listModel
      validate:
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
  }
  {
    method: 'GET'
    path: "/#{Version}/#{Base}/{id}/{campaignId?}"
    config :
      description: 'Get Post id'
      notes: 'Detail of a profile providing a valid _id'
      tags: ['api',"#{Version}","#{Base}"]
      response:
        schema: profileModel
      auth:
        strategy: 'token'
        scope: ['admin']
      validate:
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        params:
          id: Joi.string().required().description('id profile')
          campaignId: Joi.string().optional().description('optional id campaign filter')
      handler: Handlers.getById
  }
  {
    method: 'PUT'
    path: "/#{Version}/#{Base}/{id}"
    config :
      description: 'Put profile'
      notes: 'Modify a profile providing a valid _id'
      tags: ['api',"#{Version}","#{Base}"]
      auth:
        strategy: 'token'
        scope: ['admin']
      response:
        schema: profileModel
      validate:
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        params:
          id: Joi.string().required().description('id profile')
        payload: payloadUpdate
      handler: Handlers.putById
  }
  {
    method: 'POST'
    path: "/#{Version}/#{Base}/query"
    config:
      description: 'Query profiles'
      notes: 'Search profiles with given parameters'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.query
      auth:
        strategy: 'token'
        scope: ['admin']
      validate:
        payload:
          payloadQuery
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
  }
  {
    method: 'POST'
    path: "/#{Version}/#{Base}/search"
    config:
      description: 'Query profiles'
      notes: 'Search profiles with given parameters'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.querySearch
      auth:
        strategy: 'token'
        scope: ['admin']
      validate:
        query:
          page: Joi.number().optional()
          limit: Joi.number().optional()
          order: Joi.string().optional()
          sort: Joi.string().optional()
        headers: Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        payload: payloadQuery
  }
  {
    method: 'POST'
    path: "/#{Version}/#{Base}/query/accepted"
    config:
      description: 'Query profiles with accepted policies'
      notes: 'Search profiles with accepted policies'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.queryByAccepted
      auth:
        strategy: 'token'
        scope: ['admin']
      validate:
        payload:
          marketing: Joi.boolean().optional()
          dem: Joi.boolean().optional()
          profiling: Joi.boolean().optional()
          privacy: Joi.boolean().optional()
          conditions: Joi.boolean().optional()
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
  }
  {
    method: 'GET'
    path: "/#{Version}/#{Base}/campaigns"
    config:
      description: 'Query campaigns of the profile'
      notes: 'Query campaigns of the profile'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.getUserCampaigns
      auth:
        strategy: 'token'
        scope: ['user']
      validate:
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
  }
  {
    method: 'GET'
    path: "/#{Version}/#{Base}/campaigns/{id}"
    config:
      description: 'Query a campaign of the profile'
      notes: 'Query campaign of the profile'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.getUserCampaign
      response:
        schema: profileCampaignModel
      auth:
        strategy: 'token'
        scope: ['user']
      validate:
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        params:
          id: Joi.string().required().description('id campaign')
  }
  {
    method: 'GET'
    path: "/#{Version}/#{Base}/campaigns/{id}/surveys"
    config:
      description: 'Query surveys of a campaign of the profile'
      notes: 'Query surveys of a campaign of the profile'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.getUserCampaignSurveys
      auth:
        strategy: 'token'
        scope: ['user']
      validate:
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        params:
          id: Joi.string().required().description('id campaign')
  }
  {
    method: 'GET'
    path: "/#{Version}/#{Base}/campaigns/{id}/surveys/{surveyId}"
    config:
      description: 'Query surveys of a campaign of the profile'
      notes: 'Query surveys of a campaign of the profile'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.getUserCampaignSurvey
      response:
        schema: profileSurveyModel
      auth:
        strategy: 'token'
        scope: ['user']
      validate:
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        params:
          id: Joi.string().required().description('id campaign')
          surveyId: Joi.string().required().description('id survey')
  }
  {
    method: 'GET'
    path: "/#{Version}/#{Base}/surveys"
    config:
      description: 'Query of surveys for every profile'
      notes: 'Query of surveys for every profile'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.getAllSurveysForm
      #response:
       #schema: profileSurveyModel
      auth:
        strategy: 'token'
        scope: ['admin']
      validate:
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
  }
  {
    method: 'GET'
    path: "/#{Version}/#{Base}/surveys/{idprofilo}/{idform}"
    config:
      description: 'Survey by profile and form'
      notes: 'Survey by profile and form'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.getSurveyByProfileForm
      auth:
        strategy: 'token'
        scope: ['admin']
      validate:
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        params:
          idprofilo: Joi.string().required().description('id profilo')
          idform: Joi.string().required().description('id form')
  }
  {
    method: 'POST'
    path: "/#{Version}/#{Base}/form/{formId}"
    config:
      description: 'Submission of a form in the profile'
      notes: 'Creates a form responses in the user profile'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.saveFormSubmission
#      auth:
#        strategy: 'token'
#        scope: ['user']
      validate:
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        params:
          formId: Joi.string().required().description('id form')
        payload: payloadForm
  }
  {
    method: 'GET'
    path: "/#{Version}/#{Base}/info"
    config:
      description: 'Info about user'
      notes: 'List of info about user'
      tags: ['api', "#{Version}", "#{Base}"]
      handler: Handlers.getInfo
      auth:
        strategy: 'token'
        scope: ['admin']
      validate:
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
  }
  {
    method: 'GET'
    path: "/#{Version}/#{Base}/bench"
    config:
      description: 'Info about user'
      notes: 'List of info about user'
      tags: ['api', "#{Version}", "#{Base}"]
      handler: Handlers.benchUsers
  }
  {
    method: 'POST'
    path: "/#{Version}/#{Base}/mailSearch"
    config:
      description: 'Query users'
      notes: 'Search users with given parameters'
      tags: ['api']
      handler: Handlers.queryEmail
      auth:
        strategy: 'token'
        scope: ['admin']
      validate:
        query:
          page: Joi.number().optional()
          limit: Joi.number().optional()
          order: Joi.string().optional()
          sort: Joi.string().optional()
        payload:
          email: Joi.string().required()
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
  }
]