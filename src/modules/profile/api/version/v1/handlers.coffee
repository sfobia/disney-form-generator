Profile   = require('./../../../model')

dist = process.env.NODE_FOLDER || 'dist'



Campaign = require process.cwd() + '/'+dist+'/modules/campaign/model'
Candidate = require process.cwd() + '/'+dist+'/modules/candidate/model'
Survey = require process.cwd() + '/'+dist+'/modules/survey/model'
User = require process.cwd() + '/'+dist+'/modules/user/user'
Boom      = require 'boom'
Mongoose = require('mongoose')
async     = require('async')


rnd = ->
  length = 30
  charset = "abcdefghijklnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
  retVal = ""
  i = 0
  n = charset.length
  while i < length
    retVal += charset.charAt(Math.floor(Math.random() * n))
    ++i
  retVal

urlAssets = process.env.NODE_ENV_DOMAIN_ASSETS || "http://localhost:8000"

methods =
  getAll: (request, reply) ->
    Profile.dao.find({}).populate("user", Profile.pop()).exec (err, item) ->
      if !err and item
        reply item
      else
        reply Boom.notFound(err)
      return
    return

  getById: (request, reply) ->
    Profile.dao.findById(request.params.id).populate("user form.survey form.campaign", Profile.pop()).exec (err, item) ->

      if err || !item
        reply Boom.notFound(err)
        return

      if item
        item = item.toObject()
        item.id = item._id.toString()
        item.registrationData = item.form.filter (form) ->
          form.formType == 'registration'
        item.shippingData = item.form.filter (form) ->
          form.formType == 'shipping'

        if item.photo
          if item.photo.medium
            item.cover = urlAssets + item.photo.medium.defaultUrl.replace(process.cwd() + '/public', '')

        if request.params.campaignId
          ###
          item.surveys = item.surveys.filter (survey) ->
            survey.ref.campaign.toString() == request.params.campaignId
          ###

          item.form = item.form.filter (form) ->
            if form.campaign != undefined
              form.campaign._id.toString() == request.params.campaignId

        reply item
    return

  putById: (request, reply) ->
    #todo attenzione due findOneAndUpdate annidati possono causare errori
    Profile.dao.findOne({_id: request.params.id}).exec (err, item) ->
      if err
        reply Boom.notFound(err)
        return
      if item
        User.dao.findOneAndUpdate({_id: item.user}, { $set: request.payload}, { new: true} ).exec (err2, itemU) ->
          if err2
            reply Boom.notFound(err2)
            return
          if itemU
            reply itemU
        return
    return

  getInfo: ( request, reply ) ->
    item =
      enumStatus: User.enumStatus
    reply item
    return

  querySearch: (request, reply) ->
    reg = []
    regUser = {}

    if request.payload.hasOwnProperty('q')
      reg.push {
        "name.first":
          $regex: request.payload.q
          $options: 'i'
      }
      reg.push {
        "name.last":
          $regex: request.payload.q
          $options: 'i'
      }
      reg.push {
        "gender":
          $regex: request.payload.q
          $options: 'i'
      }
      query =
        $or: reg

    else
      if request.payload.hasOwnProperty('user.email')
        regUser = {
          "email":
            $regex: request.payload['user.email']
            $options: 'i'
        }

      if request.payload.hasOwnProperty('name.first')
        reg.push {
          "name.first":
            $regex: request.payload['name.first']
            $options: 'i'
        }
      if request.payload.hasOwnProperty('name.last')
        reg.push {
          "name.last":
            $regex: request.payload['name.last']
            $options: 'i'
        }
      if request.payload.hasOwnProperty('gender')
        reg.push {
          "gender":
            $regex: request.payload.gender
            $options: 'i'
        }

      if reg.length > 0
        query =
          $and: reg
      else
        query = {}

      if regUser.length > 0
        queryregUser =
          $and: regUser
      else
        queryregUser = {}

    populate =
      path: 'user'
      select:  Profile.pop()
      match: regUser

    page = request.query.page || 1
    limit = request.query.limit || 10
    order = request.query.order || "user.creationDate"
    sort = request.query.sort || ''
    sortOrder = sort + order

    Profile.dao.paginate( query, {page: page, limit: limit, lean: true, populate: populate, sort: '_id':1}).then (item) ->
      if item.docs
        reply(item.docs).header('X-Total-Count', item.total)
      else
        reply Boom.notFound(err)
      return
    return

  query: (request, reply) ->
    Profile.dao.find(request.payload).populate("user",Profile.pop()).exec (err, item) ->
      if !err and item
        reply item
      else
        reply Boom.notFound(err)
      return
    return

  queryByAccepted: (request, reply) ->

    if request.payload.hasOwnProperty('dem')
      query = {'accepted.dem': [{ "$exists" : true }, { "$eq" : request.payload.dem }]}
    else if request.payload.hasOwnProperty('marketing')
      query = {'accepted.marketing': [{ "$exists" : true }, { "$eq" : request.payload.marketing }]}
    else
      query = {'accepted.profiling': [{ "$exists" : true }, { "$eq" : request.payload.profiling }]}

    Profile.dao.find(query).populate("user").exec (err, item) ->
      if !err and item
        reply item
      else
        reply Boom.notFound(err)
      return
    return

  queryEmail: (request, reply) ->

    page = request.query.page || 1
    limit = request.query.limit || 10
    order = request.query.order || "user.creationDate"
    sort = request.query.sort || ''
    sortOrder = sort + order

    populate = {path: 'Profile', select: 'name'}

    User.dao.paginate({ email: {$regex: request.payload.email, $options: 'i'} }, {page: page, limit: limit, lean: true, populate: populate, sort: '_id':1}).then (users) ->
      if users.docs
        reply(users.docs).header('X-Total-Count', users.total)
    return


  deleteById: (request, reply) ->
    userId = request.params.id

    if !userId
      reply Boom.badRequest("Errore ID Utente")
      return

    checkUtente = {}
    checkProfilo = {}
    checkCandidature = {}

    async.series [
      (cb) ->

        Candidate.dao.remove({ user: userId }).exec (errCandidate, resCandidate) ->
          if errCandidate
            return cb(errCandidate)
          checkCandidature = resCandidate.result if resCandidate
          cb()

      (cb) ->

        Profile.dao.findOneAndRemove({ user: userId }).exec (errProfile, resProfile) ->
          if errProfile
            return cb(errProfile)
          if resProfile
            checkProfilo = 'ok'

          cb()

      (cb) ->

        User.dao.findOneAndRemove({_id: userId}).exec (errUser, resUser) ->
          if errUser
            return cb(errUser)
          if resUser
            checkUtente = 'ok'

          cb()

    ], (err, res) ->
      if err
        reply Boom.badRequest(err)
        return

      response =
        user: checkUtente
        candidate: checkCandidature
        profilo: checkProfilo

      reply( status: 'ok', message: response)
    return



  save: (request, reply) ->
    profile = new Profile.dao(request.payload)
    profile.save (err, item) ->
      if err
        reply Boom.forbidden(err)
      else
        reply item
      return
    return

  saveFormSubmission: (request, reply) ->

    console.log( request )

    Profile.dao.find({'user': request.auth.credentials._id, 'form.ref': request.params.formId}).then (item) ->
      if item.length == 0
        Profile.dao.findOneAndUpdate({'user': request.auth.credentials._id}, { $push: {'form': request.payload}}, {new: true} ).exec (err, doc) ->
          reply doc
          return
      else
        Profile.dao.findOneAndUpdate({'user': request.auth.credentials._id, 'form.ref': request.params.formId}, { $set: {'form.$': request.payload}}, {new: true} ).exec (err, doc) ->
          reply doc
          return

  getUserCampaigns: (request, reply) ->
    campaigns = []
    query = user: request.auth.credentials._id
    Candidate.dao.find(query, 'campaign').populate('campaign').lean().exec (err, items) ->
      for item in items
        campaigns.push(item.campaign)
      reply campaigns
      return
    return

  getUserCampaign: (request, reply) ->
    Campaign.dao.findById(request.params.id).lean().exec (err, item) ->
      if !err and item
        reply item
      else
        reply Boom.notFound(err)
      return
    return

  getUserCampaignSurveys: (request, reply) ->
    campaignId = request.params.id
    query =
      campaign: campaignId
    Survey.dao.find(query).exec (err, item) ->
      if !err and item
        reply item
      else
        reply Boom.notFound(err)
    return

  getUserCampaignSurvey: (request, reply) ->
    query =
      campaign: request.params.id
      _id: request.params.surveyId
    Survey.dao.find(query, "-questions.answers.responses").lean().exec (err, item) ->
      if !err and item
        reply item[0]
      else
        reply Boom.notFound(err)
    return

  benchUsers: (request,reply) ->
    reply()


  getAllSurveysForm: (request, reply) ->

    query = {'form':  {$elemMatch :{ "formType" : "survey" }}}

    page = request.query.page || 1
    limit = request.query.limit || 10

    #populate = [{path: 'form.campaign', select: 'title'}, {path: 'form.ref'}, {path: "user", select: "userName email"}]
    populate = [{path: 'form.campaign'}, {path: 'form.ref'}, {path: "user", select: "userName email"}]

    options = { select: { "form" , "user"}, page: page, limit: limit, lean: true, populate: populate, sort: '_id':-1 }

    Profile.dao.paginate(query, options).then (item) ->
      if item.docs
        #console.log item.docs
        surveyResponse = []
        for form in item.docs
          for elementForm in form.form
            surveyElement = {}

            if elementForm.formType == "survey"
              surveyElement._id = form._id
              surveyElement.user = form.user
              surveyElement.form = elementForm
              surveyResponse.push surveyElement

              #recupero info nel caso di tipo campo commento-voto
              for campo in elementForm.values
                if campo.type == "commento-voto"
                  surveyElement.pubcomment = campo.value.pubcomment
                  surveyElement.voto = campo.value.voto
                  surveyElement.commento = campo.value.commento
        #reply (item.docs).header('X-Total-Count', items.total);
        reply surveyResponse
      else
        reply Boom.notFound()

    return

  getSurveyByProfileForm: (request, reply) ->

    idprofilo = request.params.idprofilo
    idform    = request.params.idform

    query = { _id: idprofilo }

    surveyResponse = {}

    populate = [{path: 'form.campaign'}, {path: "user", select: "userName email"}]
    options = { select: { "form" , "user"}, lean: true, populate: populate }

    Profile.dao.findOne(query).populate(populate).lean().exec (err, item) ->  #recupero profilo by id
      if !err and item

        for elementForm in item.form #ciclo i form

          if elementForm.formType == "survey" #se tipo survey
            formId = elementForm._id.toString()
            if formId == idform #controllo che id del form survey sia uguale a quello passato per parametro
              surveyResponse.user = item.user
              surveyResponse.form = elementForm
              #recupero altre info nel caso di tipo campo commento-voto - non servirebbe -> recupero per fare prima
              for campo in elementForm.values
                if campo.type == "commento-voto"
                  surveyResponse.pubcomment = campo.value.pubcomment
                  surveyResponse.voto = campo.value.voto
                  surveyResponse.commento = campo.value.commento
        console.log surveyResponse
        reply surveyResponse
      else
        reply Boom.notFound()


    return

module.exports = methods