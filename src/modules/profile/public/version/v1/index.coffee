Joi         = require 'joi'
Handlers    = require './handlers'
Version     = "V1"
Base        = "profile"
Profile   = require('./../../../model')

dist = process.env.NODE_FOLDER || 'dist'


Campaign = require process.cwd() + '/'+dist+'/modules/campaign/public/version/v1/schema'
Category = require process.cwd() + '/'+dist+'/modules/category/version/v1/schema'

answerModel = Joi.object(
  _id: Joi.any().optional()
  __v: Joi.any().optional()
  name: Joi.string().required().description('answer name')
  percent: Joi.string().optional().description('answer percent')
  image: Joi.string().optional().description('answer image')
)

questionModel = Joi.object(
  _id: Joi.any().optional()
  __v: Joi.any().optional()
  title: Joi.string().required().description('question title')
  type: Joi.string().required().description('question type')
  description: Joi.string().required().description('question description')
  answers: Joi.array().items(answerModel).required().description('answers')
)

profileSurveyModel = Joi.object(
  _id: Joi.any().optional()
  __v: Joi.any().optional()
  title: Joi.string().required().description('title')
  type: Joi.string().required().description('type')
  status:
    actual: Joi.string().optional().description('actual status')
    afterClose: Joi.string().optional().description('after conclusion status')
  questions: Joi.array().items(questionModel).optional().description('questions')
  campaign: Joi.any().optional().description('campaign id')
  published: Joi.boolean().optional().description('published or not')
)

candidateModel = Joi.object(
  _id: Joi.any().optional()
  __v: Joi.any().optional()
  id: Joi.any().required()
  user: Joi.any().required()
  campaign: Joi.any().required()
  active: Joi.boolean().required().description('indicates if candidate is active')
  rejected: Joi.boolean().optional().description('rejection of the candidate')
)

campaignImageModel = Joi.object(
  _id: Joi.any().optional()
  __v: Joi.any().optional()
  name: Joi.string().optional()
)

campaignTagModel = Joi.object(
  _id: Joi.any().optional()
  __v: Joi.any().optional()
  name: Joi.string().optional()
)

categoryModel = Joi.object(
  _id: Joi.any().optional()
  __v: Joi.any().optional()
  title: Joi.string().required().description('category title')
  description: Joi.string().optional().description('category description')
  media:
    logo: Joi.array().items(Category.categoryImageModel).optional()
    images: Joi.array().items(Category.categoryImageModel).optional()
  order: Joi.number().optional().description('category order')
  published: Joi.boolean().optional().description('indicate if the category is published')
)

profileCampaignModel = Joi.object(
  _id: Joi.any().required()
  __v: Joi.any().optional()
  form: Joi.any().optional() # qui dovrei prendere il model form facendo export dal model form
  title: Joi.string().required().description('campaign title')
  description: Joi.string().optional().description('campaign description')
  type: Joi.string().required().description('campaign type')
  media:
    logo: Joi.array().items(Campaign.campaignImageModel).optional()
    images: Joi.array().items(Campaign.campaignImageModel).optional()
  tag: Joi.array().items(campaignTagModel).optional()
  link: Joi.string().optional().description('campaign link')
  published: Joi.boolean().optional().description('indicate if the campaign is published')
  categories: Joi.array().items(categoryModel).optional().description('categories associated to the campaign')
)

profileModel = Joi.object(
  _id: Joi.any().optional()
  __v: Joi.any().optional()
  id: Joi.string().required()
  user: Joi.any().optional() # qui dovrei prendere il model user facendo export dal model user
  name:
    first: Joi.string().optional().description('first name')
    last: Joi.string().optional().description('last name')
  gender: Joi.string().optional().description('gender')
  birthDate: Joi.date().optional().description('birth date')
  phone:
    mobile: Joi.string().optional().description('mobile phone number')
    home: Joi.string().optional().description('home phone number')
    office: Joi.string().optional().description('office phone number')
  accepted:
    marketing: Joi.boolean().optional().description('accepted marketing policy')
    dem: Joi.boolean().optional().description('accepted dem policy')
    profiling: Joi.boolean().optional().description('accepted profiling policy')
    privacy: Joi.boolean().optional().description('accepted privacy policy')
    conditions: Joi.boolean().optional().description('accepted conditions policy')
  newsletter: Joi.any().optional().description('newsletter')
  photo: Joi.string().optional().description('photo')
).meta(className: 'Profile').options(allowUnknown: true,abortEarly: false )

listModel = Joi.array().items(profileModel).meta(className: 'List')
listCampaignModel = Joi.array().items(profileCampaignModel).meta(className: 'ListCampaignModel')

payloadUpdate =
  name:
    first: Joi.string().required()
    last: Joi.string().required()
  gender: Joi.string().allow('Male', 'Female')
  birthDate: Joi.date()
  province: Joi.string().optional()
  phone:
    mobile: Joi.string().optional()
    home: Joi.string().optional()
    office: Joi.string().optional()
  accepted:
    marketing: Joi.boolean()
    dem: Joi.boolean().optional()
    profiling: Joi.boolean().optional()
    privacy: Joi.boolean().optional()
    conditions: Joi.boolean().optional()
  newsletter: Joi.any().optional().description('newsletter')
  photo: Joi.string().optional()

payloadResp =
  product: Joi.string().optional()
  ref: Joi.string().optional()
  type: Joi.string().optional()
  name: Joi.string().optional()
  value: Joi.string().optional()
  refvalue: Joi.string().optional()
  info: Joi.any().optional()
  altro: Joi.string().optional()
  multi: Joi.any(payloadMulti).optional()

payloadMulti =
  ref: Joi.string().optional()
  fieldref: Joi.string().optional()
  radiovalue: Joi.string().optional()

payloadForm =
  product: Joi.string().optional()
  ref: Joi.string().required()
  campaign: Joi.string().optional()
  name: Joi.string().optional()
  prevalid: Joi.boolean().optional()
  values: Joi.array(payloadResp).optional()
  formType: Joi.string().optional()
  prevalid: Joi.boolean().optional()

payloadSurvey =
  ref: Joi.string().required()
  questions: Joi.array(payloadQuestions).optional()

payloadCandidation =
  campaign: Joi.string().required()

payloadPsico =
  psicoLinguisticaResult: Joi.any().optional()


payloadQuestions =
  questionId: Joi.string().optional()
  question: Joi.string().required()
  answers: Joi.array(payloadAnswers).optional()

payloadAnswers =
  answerId: Joi.string().optional()
  name: Joi.string().required()
  response: Joi.string().optional()

notifyModel = Joi.object(
  _id: Joi.any().optional()
  __v: Joi.any().optional()
  from: Joi.string().optional().description('title')
  user: Joi.any().required().description('user')
  subject: Joi.string().optional().description('actual status')
  body: Joi.string().optional().description('after conclusion status')
  creation: Joi.any().optional().description('after conclusion status')
  read: Joi.boolean().optional().description('after conclusion status')
  emailSent: Joi.any().optional()
  id: Joi.any().optional()
)

listNotifyModel = Joi.array().items(notifyModel).meta(className: 'listNotifyModel')

getFormTypeModel = Joi.object(
  formType: Joi.string().required().description('form type')
)

module.exports = [
  {
    method: 'GET'
    path: "/#{Version}/#{Base}/me"
    config :
      description: 'Get the profile of the user'
      notes: 'Detail of the user profile'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.getById
#      auth:
#        strategy: 'token'
#        scope: ['user']
      response:
        schema: profileModel
      validate:
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
  }
  {
    method: 'PUT'
    path: "/#{Version}/#{Base}/me"
    config :
      description: 'Put profile'
      notes: 'Modify a current user profile'
      tags: ['api',"#{Version}","#{Base}"]
#      auth:
#        strategy: 'token'
#        scope: ['user']
      validate:
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        payload:
          user:
            email: Joi.string().email().required().description('actual status')
            password: Joi.string().min(6).optional().allow('').description('actual status')
            userName: Joi.string().min(6).required().description('actual status')
          profile:
            birthDate: Joi.string().required().description('actual status')
            gender: Joi.string().required().description('actual status')
            province: Joi.string().required().description('actual status')
            name:
              last: Joi.string().required().description('actual status')
              first: Joi.string().required().description('actual status')
            accepted:
              marketing: Joi.boolean().optional().description('accepted marketing policy')
              dem: Joi.boolean().optional().description('accepted dem policy')
              profiling: Joi.boolean().optional().description('accepted profiling policy')
              privacy: Joi.boolean().optional().description('accepted privacy policy')
              conditions: Joi.boolean().optional().description('accepted conditions policy')
            newsletter: Joi.any().optional()
      handler: Handlers.putMe
  }
  {
    method: 'POST'
    path: "/#{Version}/#{Base}/me/picture"
    config:
      description: 'Upload media'
      notes: 'Carica una nuova immagine'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.savePicture
      plugins:
        'hapi-swagger':
          payloadType: 'form'
      payload:
        maxBytes: 300000
        output: 'file'
        parse: true
        allow: 'multipart/form-data'
      validate:
        headers: Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        payload:
          image: Joi.any().required().meta({swaggerType: 'file'})
#      auth:
#        strategy: 'token'
#        scope: ['user']
  }
  {
    method: 'GET'
    path: "/#{Version}/#{Base}/campaigns"
    config:
      description: 'Query campaigns of the profile'
      notes: 'Query campaigns of the profile'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.getUserCampaigns
      auth:
        strategy: 'token'
        scope: ['user']
      validate:
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
  }
  {
    method: 'GET'
    path: "/#{Version}/#{Base}/notify"
    config:
      description: 'List notify for user'
      notes: 'profile notify'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.getNotify
      response:
        schema: listNotifyModel
      auth:
        strategy: 'token'
        scope: ['user']
      validate:
        headers: Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
  }
  {
    method: 'GET'
    path: "/#{Version}/#{Base}/notify/{id}"
    config:
      description: 'List notify for user and id'
      notes: 'profile notify'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.getNotifyById
      response:
        schema: notifyModel
      auth:
        strategy: 'token'
        scope: ['user']
      validate:
        headers: Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        params:
          id: Joi.string().required().description('id campaign')
  }
  {
    #todo aggiungere response schema
    method: 'GET'
    path: "/#{Version}/#{Base}/profilation/registration"
    config:
      description: 'Retrieve registration form'
      notes: 'Registration form'
      tags: ['api', "#{Version}", "#{Base}"]
      handler: Handlers.getRegistrationForm
      auth:
        strategy: 'token'
        scope: ['user']
      validate:
        headers: Joi.object('authorization': Joi.string().required()).options(allowUnknown: true)
  }
  {
    #todo aggiungere response schema
    method: 'GET'
    path: "/#{Version}/#{Base}/profilation/shipping"
    config:
      description: 'Retrieve shipping form'
      notes: 'Shipping form'
      tags: ['api', "#{Version}", "#{Base}"]
      handler: Handlers.getShippingForm
      auth:
        strategy: 'token'
        scope: ['user']
      validate:
        headers: Joi.object('authorization': Joi.string().required()).options(allowUnknown: true)
  }
  {
    #todo aggiungere response schema
    method: 'GET'
    path: "/#{Version}/#{Base}/profilation/survey"
    config:
      description: 'Retrieve survey form'
      notes: 'Survey form'
      tags: ['api', "#{Version}", "#{Base}"]
      handler: Handlers.getSurveyForm
      auth:
        strategy: 'token'
        scope: ['user']
      validate:
        headers: Joi.object('authorization': Joi.string().required()).options(allowUnknown: true)
  }
  {
    #todo aggiungere response schema
    method: 'GET'
    path: "/#{Version}/#{Base}/form/{id}"
    config:
      description: 'Retrieve form by id'
      notes: 'form by id'
      tags: ['api', "#{Version}", "#{Base}"]
      handler: Handlers.getFormById
      auth:
        strategy: 'token'
        scope: ['user']
      validate:
        headers: Joi.object('authorization': Joi.string().required()).options(allowUnknown: true)
  }
  {
    method: 'POST'
    path: "/#{Version}/#{Base}/form/{formId}/{campaignId?}"
    config:
      description: 'Submission of a form in the profile'
      notes: 'Creates a form responses in the user profile'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.saveFormSubmission
#      auth:
#        strategy: 'token'
#        scope: ['user']
      validate:
        headers: Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        params:
          formId: Joi.string().required().description('id form')
          campaignId: Joi.string().optional().description('campaignid form')
        payload: payloadForm
  }
  {
    method: 'POST'
    path: "/#{Version}/#{Base}/psicolinguistica"
    config:
      description: 'Submission of psicolinguistica result'
      notes: 'Submission of psicolinguistica result'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.savePsicoLinguistica
      auth:
        strategy: 'token'
        scope: ['user']
      validate:
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        params: payloadPsico
        #params:
          # psicoLinguisticaResult: Joi.string().optional().description('psico')
  }
  {
    method: 'POST'
    path: "/#{Version}/#{Base}/candidate"
    config:
      description: 'Submission of a candidation for a campaign'
      notes: 'Creates a user candidation for a campaign'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.saveCandidation
      auth:
        strategy: 'token'
        scope: ['user']
      validate:
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        payload: payloadCandidation
  }
  {
    method: 'GET'
    path: "/#{Version}/#{Base}/campaigns/{id}"
    config:
      description: 'Restituisco il form della campagna'
      notes: 'Query campaign of the profile'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.getUserCampaign
      auth:
        strategy: 'token'
        scope: ['user']
      validate:
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        params:
          id: Joi.string().required().description('id campaign')
  }
  {
    method: 'GET'
    path: "/#{Version}/#{Base}/surveys/{id}"
    config:
      description: 'Restituisco il survey della campagna'
      notes: 'Query survey of the profile'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.getSurveyCampaign
      auth:
        strategy: 'token'
        scope: ['user']
      validate:
        headers: Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        params:
          id: Joi.string().required().description('id campaign')
  }
  {
    method: 'GET'
    path: "/#{Version}/#{Base}/campaigns/values/{id}"
    config:
      description: 'Restituisco il form della campagna'
      notes: 'Query campaign of the profile'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.getUserCampaignValues
      auth:
        strategy: 'token'
        scope: ['user']
      validate:
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        params:
          id: Joi.string().required().description('id campaign')
  }
  {
    method: 'POST'
    path:"/#{Version}/#{Base}/profilationservice"
    config:
      description: 'Profilation'
      notes: 'Profilation'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.psicoLinguisticaService
      validate:
        query:
          method: Joi.string().required().description('metodo servizio')
          formdata: Joi.any().optional().description('param data')
  }
]
