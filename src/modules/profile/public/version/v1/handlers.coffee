Profile   = require('./../../../model')

dist = process.env.NODE_FOLDER || 'dist'


Campaign  = require process.cwd() + '/'+dist+'/modules/campaign/model'
Candidate = require process.cwd() + '/'+dist+'/modules/candidate/model'
Survey    = require process.cwd() + '/'+dist+'/modules/survey/model'
Notify    = require process.cwd() + '/'+dist+'/modules/notify/model'
NotifyHandlers  = require(process.cwd()+ '/'+dist+'/modules/notify/version/v1/handlers')
User      = require process.cwd() + '/'+dist+'/modules/user/user'
Form      = require process.cwd() + '/'+dist+'/modules/form/model'
JoguHandlers  = require(process.cwd()+ '/'+dist+'/modules/jogu/version/v1/handlers')

Review = require(process.cwd() + '/'+dist+'/modules/review/model').dao

Boom      = require 'boom'
fs        = require('fs')
moment    = require 'moment'
async     = require('async')

requestpost = require 'request';

internals = {}
internals.getErrorMessage = (err) ->
  message = ''
  if err.code
    switch err.code
      when 11000
        message = 'Email o username già presente nel sistema'
      when 11001
        message = 'Email o username già presente nel sistema'
      else
        message = 'Errore riprova'
  else
    for errName of err.errors
      if err.errors[errName].message
        message = err.errors[errName].message
  message


urlAssets = process.env.NODE_ENV_DOMAIN_ASSETS || "http://localhost:8000"
methods =
  getById: (request, reply) ->

#    userId = request.auth.credentials._id

    userToken = request.orig.headers.authorization
    userToken = userToken.replace("Bearer ", "")

    response            = {}
    totaleNotifiche     = 0

    async.parallel [
      (cb) ->

        User.dao.findOne({token: userToken}, "-form.prevalid").lean().exec (err,res) ->

          now = moment()

          if err
            cb(Boom.notFound())
            return

          if res

            response              = res
            response.id           = response._id.toString()
            response.campaigns    = []

          if ( res == null )


            userTokenDecoded = JSON.parse(userToken)


            if( userTokenDecoded.id )
              user =
                userName: userTokenDecoded.username,
                password: now.unix()
                email: userTokenDecoded.email
                confirmed: true
                token: userToken
            else
              user =
                userName: now.unix(),
                password: now.unix()
                email: now.unix()+"@disney.it"
                confirmed: true
                token: userToken

            user = new User.dao( user )
            user.save (err, item ) ->

              if err
                msg = internals.getErrorMessage err
                cb(Boom.methodNotAllowed(msg))
                return

              if item


                if( userTokenDecoded.id )
                  profile =
                    name:
                      first: userTokenDecoded.username
                      last: userTokenDecoded.id
                else
                  profile =
                    name:
                      first: now.unix()
                      last: "User"


                profile.user = item._id
                userprofile = new Profile.dao( profile )
                userprofile.save (errp, itempro ) ->

                  if errp
                    msg = internals.getErrorMessage errp
                    cb(Boom.notFound())
                    return

                  if itempro


                    response              = item
                    response.id           = item._id.toString()
                    response.campaigns    = []

                    cb(null)
                  return
              else

                cb(Boom.notFound())

              return

            return

          cb(null)

      (cb) ->

        cb(null)

    ],
      (err) ->
        if err
          reply err
          return
#        response.notifyRead = totaleNotifiche


#        console.log(response)
        reply response
    return

  putMe: (request, reply) ->

    async.waterfall [
      (cb) ->
        if request.payload.profile
          Profile.dao.findOneAndUpdate({ user: request.auth.credentials._id }, { $set: request.payload.profile } ).exec (err, item) ->
            if err
              cb(Boom.notFound())
              return
            if item
              cb()
        else
          cb()
      (cb) ->
        if request.payload.user
          password = undefined

          if request.payload.user.password
            password = request.payload.user.password
            delete request.payload.user.password

          User.dao.findOneAndUpdate({ _id: request.auth.credentials._id }, { $set: request.payload.user } ).exec (err, item) ->
            if err
              msg = internals.getErrorMessage err
              cb(Boom.methodNotAllowed(msg))
              return

            if item
              # controllo se ha cambiato la mail
              if item.email != request.payload.user.email or password

                require('crypto').randomBytes 50, (ex, buf) ->
                  #cambio il token
                  item.token        = buf.toString('hex')

                  require('crypto').randomBytes 40, (ex, buf) ->

                    changed = ""
                    mod_email = false
                    if item.email != request.payload.user.email
                      mod_email = true

                      url               = process.env.NODE_ENV_DOMAIN_PUBLIC || "http://localhost:8100"
                      item.tokenConfirm = buf.toString('hex')
                      item.tokenLink    = url+'/account/activate/'+item.tokenConfirm
                      item.confirmed    = false
                      item.email        = request.payload.user.email

                      changed += "changed email "

                    pw = false
                    if password
                      item.password = password
                      pw = true
                      changed += "changed password "

                    item.save (err,res) ->
                      if mod_email
                        NotifyHandlers.send( 'CONFIRM_MAIL', res._id, item )
                        cb(null,{ email:true, password: pw, message:changed })
                        return
                      cb(null,{ email:false, password: pw, message:changed })
                      return
              else
                cb()
                return
          return
    ],
      (err,response) ->
        if err
          reply err
          return
        reply(response)
    return

  savePicture: (request, reply) ->
    if request.payload.image
      userId = request.auth.credentials._id
      Profile.dao.findOne({user: userId}).exec (err, item) ->
        item.attach 'photo', request.payload.image, (err) ->
          if err
            reply Boom.forbidden(err)
            return
          item.save (err, item) ->
            if err
              reply Boom.forbidden(err)
              return
            reply item.photo
            return
          return
      return
    reply Boom.badRequest()

  saveFormSubmission: (request, reply) ->

#    console.log( "# saveFormSubmission" )
#    console.log( request.payload )
#    console.log( request.payload.values[0].multi )
#
#
#    reply 200
#    return


    today = moment()

#    console.log( today )
#
    userToken = request.orig.headers.authorization
    userToken = userToken.replace("Bearer ", "")


    ###
    if request.payload.formType == "survey"
      query['form.formType'] = "survey"

    if request.payload.formType == "campaign"
      query['form.formType'] = "campaign"
    ###


    completed = {}
    query = {}
    prevalid = true

#    userId = 0
#    campaignId = 0

    async.waterfall [
      (cb) ->

        User.dao.findOne({token: userToken}, "-form.prevalid").lean().exec (err,res) ->

          now = moment()

          if err
            cb(Boom.notFound())
            return

          if res

            query =
              'user': res._id
              'form.ref': request.params.formId

            if request.params.campaignId
              query['form.campaign'] = request.params.campaignId
              campaignId = request.params.campaignId

        cb()
        return

      (cb) ->


        if request.params.campaignId
          today = moment()
          if request.payload.formType == 'campaign'
            where = { _id: request.params.campaignId, 'dates.startCampaign': {$lt: today.toDate()}, 'dates.endCampaign': {$gte: today.toDate()}}

#          if request.payload.formType == 'survey'
#            where = { _id: campaignId, 'dates.startSurvey': {$lt: today.toDate()}, 'dates.endSurvey': {$gte: today.toDate()}}


#          console.log( "Campaign" )
#          console.log( where )


          Campaign.dao.findOne(where).exec (err, item) ->

            if err
              cb(Boom.badRequest('La campagna non è attiva.'))
              return

            if item
              cb()
              return
            else
              cb(Boom.badRequest('La campagna non è attiva -'))
              return
        else
          cb()

      (cb) ->

#        console.log( "Form" )

        Form.dao.findOne( _id: request.params.formId ).exec (err, form) ->
          if err
            cb(Boom.badRequest('La campagna non è attiva --'))
            return
          else
            cb(null, form)

      (form, cb) ->

        request.payload.prevalid = prevalid

        Profile.dao.find( query ).then (items) ->

#          console.log( items.length )

          if form.formType == 'campaign'
            Campaign.dao.findOne({_id: request.params.campaignId, 'dates.startCampaign': {$lt: today.toDate()}, 'dates.endCampaign': {$gte: today.toDate()}}).exec (err, item) ->
              if err
                cb(Boom.badRequest('non è possibile candidarsi ora per questa campagna'))
                return
              if !item
                cb(Boom.badRequest('non è possibile candidarsi ora per questa campagna'))
                return


#          console.log( request.payload )

          Profile.dao.findOneAndUpdate({'user': query.user }, { $set: completed , $push: {'form': request.payload}  }, {new: true} ).exec (err, doc) ->

            if err
              cb(Boom.notFound())
              return

            cb(null, 'ok')
            return


    ], (err, res) ->
      if !err and res
        reply status: res
        return
      else
        reply Boom.badRequest(err)
        return
    return

  savePsicoLinguistica: (request, reply) ->

    psicoLinguistiObject =  request.payload

    Profile.dao.findOne({'user': request.auth.credentials._id}).exec (err, item) ->
      if err
        console.log "ERR FIND: " + err
        reply Boom.forbidden('Errore ricerca profilo')
        return

      item.psicoLinguisticaResult = psicoLinguistiObject.psicoLinguisticaResult
      item.save (err,res) ->
        if err
          console.log "ERR SAVE: " + err
          reply Boom.forbidden('Errore Salvataggio')
          return
        else
          reply res
          return
        return

  saveSurveySubmission: (request, reply) ->
    Profile.dao.find({'user': request.auth.credentials._id, 'surveys.ref': request.params.surveyId}).then (item) ->
      if item.length == 0
        Profile.dao.findOneAndUpdate({'user': request.auth.credentials._id}, { $push: {'surveys': request.payload}}, {new: true} ).exec (err, doc) ->
          reply doc
          return
      else
        reply Boom.unauthorized()
        return

  getCandidate: (user, campaign, preconfirmed, active, rejected,requisite, product) ->
    candidate =new Candidate.dao(
      user: user
      campaign: campaign
      preconfirmed: preconfirmed
      active: active
      rejected: rejected
      requisite: requisite || false
      product: product || null
    )
    candidate

  checkPrevalid: (profile, campaignId) ->
    prevalid = false
    if profile.form
      forms = profile.form
      for item in forms
        if item.formType.toString() == 'campaign' and item.campaign.toString() == campaignId.toString() and item.prevalid
          prevalid = true
    prevalid

  saveCandidation: (request, reply) ->

    product = null
    formSubmitted = ''

    async.waterfall [
      (cb) ->

        today = moment()
        Campaign.dao.findOne({_id: request.payload.campaign, 'dates.startCandidations': {$lt: today.toDate()}, 'dates.endCandidations': {$gte: today.toDate()}}).exec (err, item) ->
          if item
            cb(null)
          else
            reply Boom.forbidden('data candidatura scaduta')
            return

      (cb) ->

        Profile.dao.find({user: request.auth.credentials._id, 'form': {$elemMatch: {formType: 'registration'}}}).exec (err, item) ->
          if item.length > 0
            cb(null)
          else
            reply Boom.forbidden('profilazione non inserita')
            return

      (cb) ->

        Profile.dao.findOne({user: request.auth.credentials._id, 'form': {$elemMatch: {campaign: request.payload.campaign}}}).exec (err, item) ->
          if item
            formSubmitted = item
            cb(null, item)
          else
            reply Boom.forbidden('form di candidatura non compilato')
            return

      (item, cb) ->

        prevalid = methods.checkPrevalid item, request.payload.campaign # true se l'utente ha risposto correttamente alle domande chiave o non ci sono domande chiave
        Candidate.dao.find({user: request.auth.credentials._id, campaign: request.payload.campaign}).exec (err, item) ->
          if item.length == 0
            cb(null, prevalid)
          else
            reply Boom.forbidden('gia candidato per questa campagna')
            return

      (prevalid, cb) ->

        checkSameWeekCandidations = false
        Campaign.dao.findOne({_id: request.payload.campaign}).exec (err, item)->
          weekNumber = item.weekNumber
          Candidate.dao.find({user: request.auth.credentials._id}).populate('campaign','weekNumber').exec (err, items) ->
            for item in items
              if item.campaign.weekNumber == weekNumber
                if item.preconfirmed == true
                  checkSameWeekCandidations = true #se emerge altra candidatura, restituire true
            cb(null, checkSameWeekCandidations, prevalid)


      (checkSameWeekCandidations, prevalid, cb) ->

        checkMultiplePrecandidations = false
        Candidate.dao.find({user: request.auth.credentials._id, preconfirmed: true}).exec (err, items) ->
          if items.length == 2
            checkMultiplePrecandidations = true
          cb(null, checkMultiplePrecandidations, checkSameWeekCandidations, prevalid)


      (checkMultiplePrecandidations, checkSameWeekCandidations, prevalid, cb) ->
        Campaign.dao.findOne({_id: request.payload.campaign}).exec (err, item) ->
          cb(null, item, checkSameWeekCandidations, checkMultiplePrecandidations, prevalid)

      (campaign, checkMultiplePrecandidations, checkSameWeekCandidations, prevalid, cb) ->

          Form.dao.findOne({ _id: campaign.form}).exec (err, item) ->
            if item.multiProduct == true #mod per multiproduct
              if formSubmitted != ''
                for value, k in formSubmitted.form
                  if value.campaign?
                    if value.campaign.toString() == request.payload.campaign #va a toString perché value.campaign è un oggetto
                      formAnswers = value.values
              #foreach per avere l'identifier/ref della risposta assegnaprodotto
              for field in item.fields
                for value in field.value
                  for answer in formAnswers
                    if value.identifier == answer.refvalue && value.product != null
                      product = value.product
                      cb(null, item, checkSameWeekCandidations, checkMultiplePrecandidations, prevalid) #esce appena lo trova
            else
              cb(null, campaign, checkSameWeekCandidations, checkMultiplePrecandidations, prevalid)

      (item, checkSameWeekCandidations, checkMultiplePrecandidations, prevalid, cb) ->

        pieces = item.pieces if item.pieces || 0
        if pieces > 0 and checkSameWeekCandidations == false and prevalid and checkMultiplePrecandidations == false
          preconfirmed =  true
          Campaign.dao.findOneAndUpdate({_id: request.payload.campaign}, {$inc: {pieces: -1}}, {new: true}).exec (err, item) ->
            if err
              reply Boom.forbidden(err)
            return
        else
          preconfirmed = false

        candidate = methods.getCandidate request.auth.credentials._id, request.payload.campaign, preconfirmed, false, false, prevalid, product
        candidate.save (err, item) ->
          if err
            reply Boom.forbidden(err)
            return
          else
            jogu =
              "utente": request.auth.credentials._id
              "canale": "campaign"
              "riferimento": request.payload.campaign
            JoguHandlers.sendAttivita('candidatura', jogu)

            cb(null, item)

    ], (err, res) ->
      if err
        reply err
        return

      if res && res._id
        if process.env.NODE_ENV == "test"
          reply res
        else
          reply _id: res._id
    return

  getUserCampaigns: (request, reply) ->
    #todo ricordarsi di finire paginazione ora è impostato a 100
    query = user: request.auth.credentials._id
    Candidate.dao.paginate(query, { select: 'active rejected dates campaign ship', populate:  { path: 'campaign', select: '-candidates -responses -client -content -status -pieces -piecesInitial', populate: { path: 'categories', model: 'Category'} } , lean: true, page: 1, limit: 100 } ).then (items) ->
      reply(items.docs).header('X-Total-Count', items.total)
    return

  getNotify: (request, reply) ->
    page = request.query.page || 1
    limit = request.query.limit || 99999
    order = request.query.order || "creation"
    sort = request.query.sort || '-'
    sortOrder = sort + order
    userId = request.auth.credentials._id
    read    = 0
    Notify.dao.paginate({ user: userId }, {page: page, limit: limit, lean: true, sort: sortOrder}).then (items) ->
      if items.docs
        for x in items.docs
          read++ if x.read == false
        reply(items.docs).header('X-Total-Count', items.total).header('X-Total-Count-read', read)
      else
        reply Boom.notFound()
      return
    return

  getNotifyById: (request, reply) ->
    userId = request.auth.credentials._id
    Notify.dao.findOneAndUpdate({ user: userId, _id: request.params.id },{ $set:{ read: true } }).lean().exec (err, item) ->
      if !err and item
        reply item
      else
        reply Boom.notFound(err)
      return
    return

  getRegistrationForm: (request, reply) ->
    payload =
      formType: 'registration'

    userId = request.auth.credentials._id
    Profile.dao.findOne({user: userId}).exec (err, item) ->

      if item and item.profileCompleted
        reply Boom.badRequest('utente gia registrato')
        return

      Form.dao.findOne( payload ).exec (err, item) ->
        if !err and item
          reply item
        else
          reply Boom.notFound(err)
        return
      return

  getShippingForm: (request, reply) ->
    payload =
      formType: 'shipping'

    Form.dao.findOne( payload ).exec (err, item) ->
      if !err and item
        reply item
      else
        reply Boom.notFound(err)
      return
    return

  getSurveyForm: (request, reply) ->
    payload =
      formType: 'survey'

    Form.dao.findOne( payload ).exec (err, item) ->
      if !err and item
        reply item
      else
        reply Boom.notFound(err)
      return
    return

  getFormById: ( request, reply ) ->
    Form.dao.findById( request.params.id, '-fields.value.valid' ).exec (err, item) ->
      if !err and item
        reply item
      else
        reply Boom.notFound(err)
      return
    return


  getUserCampaign: (request, reply) ->

    query =
      'user': request.auth.credentials._id
      'form.campaign': request.params.id
      'form.formType': 'campaign'

    Profile.dao.findOne(query).exec (err, item) ->
      if item
        reply Boom.badRequest('form gia compilato')
        return

      Campaign.dao.findOne(_id: request.params.id,'-candidates -responses -client -content -status -pieces -piecesInitial').populate("form","-fields.value.valid").lean().exec (err,item)->
        if err
          reply Boom.notFound()
          return
        if item
          reply item.form
      return
    return

  getSurveyCampaign: (request, reply) ->

    async.waterfall [
      (cb) ->

        candidate_query =
          user: request.auth.credentials._id
          campaign: request.params.id
          active: true

        Candidate.dao.findOne( candidate_query ).exec ( err, item ) ->
          if err || !item
            cb('Non hai i permessi per compilare il questionario')
          if item
            cb()

      (cb) ->

        query =
          'user': request.auth.credentials._id
          #'form.campaign': request.params.id
          #'form.formType': 'survey'

        Profile.dao.findOne(query,"form").exec (err, item) ->
          if item
            checkSurvey = item.form.filter (rec) ->
              String(rec.campaign) == request.params.id and rec.formType == 'survey'
            # controllo manualmente con filter
            if checkSurvey.length > 0
              cb( 'Il questionario è stato gia compilato' )
              return
          cb()

      (cb) ->
        Campaign.dao.findOne(_id: request.params.id,'-candidates -responses -client -content -status -pieces -piecesInitial').populate("survey","-fields.value.valid").lean().exec (err,item)->
          if err
            cb('Campagna non trovata')
            return
          if item
            cb(null, item.survey)
            return
          cb()

    ], (err, res) ->
      if err
        reply Boom.badRequest(err)
        return
      reply res
      return

  getUserCampaignValues: (request, reply) ->

    query =
      'user': request.auth.credentials._id
      'form.campaign': request.params.id

    Profile.dao.findOne(query).exec (err, item) ->
      if item
        reply Boom.badRequest('form gia compilato')
        return

      Campaign.dao.findOne(_id: request.params.id,'-candidates -responses -client -content -status -pieces -piecesInitial').populate("form").lean().exec (err,item)->
        if err
          reply Boom.notFound()
          return
        if item
          reply item
      return
    return

  psicoLinguisticaService: (request, reply) ->

    username = ""
    password = ""

    if process.env.NODE_ENV == "stage" || process.env.NODE_ENV == "development"
      username = ""
      password = ""
    else
      username = ""
      password = ""

    method    = request.payload.method
    formData  = request.payload.formData

    if method != "complete"
      formData.Username = username
      formData.Password = password


    headers =
      'Content-Type': 'application/json'

    options =
      uri: 'https://emolab.emotional-lab.it:443/' + method
      method: 'POST'
      headers: headers
      json:  true
      body:  formData

    requestpost options, (error, response, body) ->
      if !error and response.statusCode == 200
        reply body
      else
        console.log "ERROR REQUEST PSICOLINGUISTICA: " + method
        reply Boom.notFound()
      return

    return

module.exports = methods