exports.register = (server, options, next) ->
  Mongoose  = require('mongoose')
  #Mongoose.set('debug', true)
  optionDefault   = server.realm.pluginOptions.options
  option          = {}
  option.host     = process.env.NODE_MONGODB_HOST || optionDefault.host
  option.db       = process.env.NODE_MONGODB_DB || optionDefault.db

  Mongoose.connect 'mongodb://' + option.host + '/' + option.db + "-" + process.env.NODE_ENV , config: autoIndex: false


  db = Mongoose.connection
  db.on 'error', console.error.bind(console, 'connection error es: ./bin/mongod --dbpath data/db/ ***********')
  db.once 'open', ->
    console.info 'MONGODB DB: ' + option.db + "-" + process.env.NODE_ENV
    return
  next()
  return

exports.register.attributes =
  name: 'mongodb'
  version: '1.0.0'