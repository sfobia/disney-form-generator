Mongoose        = require('mongoose')
Schema          = Mongoose.Schema
mongoosePaginate = require('mongoose-paginate')

ClientSchema  = new Schema(
  name:
    type: String
    required: true
  referent:
    type: String
  email:
    type: String
    required: true
  phone:
    mobile:
      type: String
      required: false
    office:
      type: String
      required: false
)

ClientSchema.plugin(mongoosePaginate)

module.exports.dao = Mongoose.model('Client', ClientSchema)