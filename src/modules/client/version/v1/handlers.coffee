Client   = require('./../../model')
Boom = require 'boom'

methods =
  save: (request, reply) ->
    client = new Client.dao(request.payload)
    client.save (err, item) ->
      if err
        reply Boom.forbidden(err)
      else
        reply item
      return
    return

  getById: (request, reply) ->
    Client.dao.findById(request.params.id).exec (err, item) ->
      if !err and item
        reply item
      else
        reply Boom.notFound(err)
      return
    return

  getAll: (request, reply) ->
    page = request.query.page || 1
    limit = request.query.limit || 9999
    Client.dao.paginate({}, {page: page, limit: limit, lean: true}).then (item) ->
      if item.docs
        reply(item.docs).header('X-Total-Count', item.total)
      else
        reply Boom.notFound()
      return
    return

  putById: (request, reply) ->
    Client.dao.findByIdAndUpdate(request.params.id, { $set: request.payload} ).exec (err, item) ->
      if !err and item
        reply item
      else
        reply Boom.notFound(err)
      return
    return

  query: (request, reply) ->
    page = request.query.page || 1
    limit = request.query.limit || 10
    Client.dao.paginate(request.payload).then (item) ->
      if item.docs
        reply(item.docs).header('X-Total-Count', item.total)
      else
        reply Boom.notFound()
      return
    return

  deleteById: (request, reply) ->
    reply Boom.badRequest('remove disabled')
    return
    Client.dao.remove _id: request.params.id, (err, item) ->
      if !err and item
        reply('ok')
      else
        reply Boom.notFound(err)
      return
    return

module.exports = methods