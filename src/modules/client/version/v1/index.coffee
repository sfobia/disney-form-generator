Joi         = require 'joi'
Handlers    = require './handlers'
Version     = "V1"
Base        = "client"

clientModel = Joi.object(
  _id: Joi.any().required()
  __v: Joi.any().optional()
  name: Joi.string().required().description('client name')
  referent: Joi.string().optional().description('client referent')
  email: Joi.string().required().description('client email')
  phone:
    office: Joi.string().optional().description('client office phone')
    mobile: Joi.string().optional().description('client mobile phone')
).meta(className: 'Client').options(allowUnknown: true, abortEarly: false)

listModel = Joi.array().items(clientModel).meta(className: 'List')

payload = Joi.object(
  _id: Joi.any().optional()
  __v: Joi.any().optional()
  name: Joi.string().required().description('client name')
  referent: Joi.string().optional().description('client referent')
  email: Joi.string().required().description('client email')
  phone:
    office: Joi.string().optional().description('client office phone')
    mobile: Joi.string().optional().description('client mobile phone')
)

payloadQuery = Joi.object(
  name: Joi.string().optional()
  referent: Joi.string().optional()
  email: Joi.string().optional()
  phone:
    office: Joi.string().optional()
    mobile: Joi.string().optional()
)

module.exports = [
  {
    method: 'POST'
    path: "/#{Version}/#{Base}"
    config:
      description: 'Create new client'
      notes: 'Creates a new client'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.save
      auth:
        strategy: 'token'
        scope: ['admin']
      validate:
        payload:
          payload
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
  }
  {
    method: 'GET'
    path: "/#{Version}/#{Base}/{id}"
    config :
      description: 'Get client by id'
      notes: 'Detail of a client providing a valid _id'
      tags: ['api',"#{Version}","#{Base}"]
      response:
        schema: clientModel
      validate:
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        params:
          id: Joi.string().required().description('client id')
      handler: Handlers.getById
      auth:
        strategy: 'token'
        scope: ['admin']
  }
  {
    method: 'GET'
    path: "/#{Version}/#{Base}"
    config :
      description: 'All clients'
      notes: 'List of all clients without any filter'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.getAll
      auth:
        strategy: 'token'
        scope: ['admin']
      response:
        schema: listModel
      validate:
        query:
          page: Joi.number().optional()
          limit: Joi.number().optional()
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
  }
  {
    method: 'PUT'
    path: "/#{Version}/#{Base}/{id}"
    config :
      description: 'Put client'
      notes: 'Modify a client providing a valid _id'
      tags: ['api',"#{Version}","#{Base}"]
      response:
        schema: clientModel
      validate:
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        params:
          id: Joi.string().required().description('client id')
        payload: payload
      handler: Handlers.putById
      auth:
        strategy: 'token'
        scope: ['admin']
  }
  {
    method: 'DELETE'
    path: "/#{Version}/#{Base}/{id}"
    config:
      description: 'Delete a client'
      notes: 'Delete the client with given _id'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.deleteById
      auth:
        strategy: 'token'
        scope: ['admin']
      validate:
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        params:
          id: Joi.string().required().description('client id')
  }
  {
    method: 'POST'
    path: "/#{Version}/#{Base}/query"
    config:
      description: 'Query clients'
      notes: 'Search clients with given parameters'
      tags: ['api',"#{Version}","#{Base}"]
      handler: Handlers.query
      auth:
        strategy: 'token'
        scope: ['admin']
      validate:
        query:
          page: Joi.number().optional()
          limit: Joi.number().optional()
        headers:
          Joi.object('authorization': Joi.string().required()).options(allowUnknown: true )
        payload:
          payloadQuery
  }
]