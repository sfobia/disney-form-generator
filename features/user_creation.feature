Feature: User creation
  Scenario: a user wants to register
    Given A user access to the system and wants to create a profile
    When he fills the registration fields and submits the form
    Then he gets an account