var gulp 		= require('gulp');
var path 		= require('path');
var concat 		= require('gulp-concat');
var uglify 		= require('gulp-uglify');
var fs 			= require('fs');
var runSequence = require('run-sequence');
var gutil       = require('gulp-util');
var coffee      = require('gulp-coffee');
var gulpif      = require('gulp-if');
var stylus      = require('gulp-stylus');
var nodemon     = require('gulp-nodemon');
var ngConstant  = require('gulp-ng-constant');

var assets = {
    html: ['views/*.jade', 'views/**/*.jade', 'views/**/*/*.jade'],
     frontend:{
         js:[
             'public/components/angular-paging/dist/paging.min.js',
             'public/components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js',
             'public/components/clipboard/dist/clipboard.min.js',
             'public/components/ngInfiniteScroll/build/ng-infinite-scroll.min.js',
             'assets/js/public/*.module.coffee',
             'assets/js/public/*.js',
             'assets/js/public/Utils/*.coffee',
             'assets/js/public/**/*.module.coffee',
             'assets/js/public/*.coffee',
             'assets/js/public/**/*.coffee'
         ]
     },
    backend:{
        css:[
            'public/components/bootstrap/dist/css/bootstrap.min.css',
            'public/components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css',
            'public/components/ng-tags-input/ng-tags-input.css',
            'public/components/ng-tags-input/ng-tags-input.bootstrap.css',
            'public/components/Bootflat/css/site.min.css',
            'public/components/trumbowyg/dist/ui/trumbowyg.min.css',
            'assets/css/backend/*.{styl,css}'
        ],
        js:[
            'public/components/jquery/dist/jquery.min.js',
            'public/components/bootstrap/dist/js/bootstrap.min.js',
            'public/components/angular/angular.min.js',
            'public/components/angular-resource/angular-resource.js',
            'public/components/angular-ui-router/release/angular-ui-router.min.js',
            'public/components/ng-tags-input/ng-tags-input.min.js',
            'public/components/angular-cookie/angular-cookie.min.js',
            'public/components/ng-token-auth/dist/ng-token-auth.min.js',
            'public/components/angular-paging/dist/paging.min.js',
            'public/components/trumbowyg/dist/trumbowyg.js',
            'public/components/trumbowyg-ng/dist/trumbowyg-ng.js',
            'public/components/moment/moment.js',
            'public/components/angular-moment/angular-moment.min.js',
            'public/components/angular-filter/dist/angular-filter.min.js',
            'assets/js/backend/*.module.coffee',
            'assets/js/backend/*.js',
            'assets/js/backend/*.factory.coffee',
            'assets/js/backend/**/*.module.coffee',
            'assets/js/backend/**/*.factory.coffee',
            'assets/js/backend/*.coffee',
            'assets/js/backend/**/*.coffee'
        ]
    }
};

gulp.task('constants-frontend', function () {
    var myConfig = require('./ng-config.json');
    var env = process.env.NODE_ENV || "development";
    console.log('NODE_ENV:'+env);
    var envConfig = myConfig[env];
    return ngConstant({
        constants: envConfig,
        stream: true
    }).pipe(gulp.dest('assets/js/public'));
});


gulp.task('constants-backend', function () {
    var myConfig = require('./ng-config.json');
    var env = process.env.NODE_ENV || "development";
    console.log('NODE_ENV:'+env);
    var envConfig = myConfig[env];
    return ngConstant({
        constants: envConfig,
        stream: true
    }).pipe(gulp.dest('assets/js/backend'));
});


gulp.task('watch', function(){
    //gulp.watch(assets.html, ['frontend-js']);
    gulp.watch(assets.frontend.js, ['frontend-js']);
    gulp.watch(assets.backend.js,['backend-js']);
    gulp.watch(assets.backend.css,['backend-css']);
});

gulp.task('default',function(){
    runSequence('constants-frontend','constants-backend','frontend-js','backend-js','backend-css','watch');
});

gulp.task('build',function(){
    runSequence('constants-frontend','constants-backend','frontend-js','backend-js','backend-css');
});

gulp.task('build-frontend',function(){
    runSequence('constants-frontend','frontend-js');
});

gulp.task('build-backend',function(){
    runSequence('constants-backend','backend-js','backend-css');
});


gulp.task('frontend-js',function () {
    gulp.src(assets.frontend.js)
        .pipe(gulpif(/[.]coffee$/, coffee()))
        .pipe(uglify())
        .pipe(concat('sdk.js'))
        .pipe(gulp.dest('public/assets/frontend'));
});

gulp.task('frontend-css',function () {
    gulp.src(assets.frontend.css)
        .pipe(gulpif(/[.]styl/, stylus()))
        .pipe(concat('production.css'))
        .pipe(gulp.dest('public/assets/frontend'));
});

gulp.task('backend-js',function () {
    gulp.src(assets.backend.js)
        .pipe(gulpif(/[.]coffee$/, coffee()))
        .pipe(concat('production.js'))
        .pipe(gulp.dest('public/assets/backend'));
});

gulp.task('backend-css',function () {
    gulp.src(assets.backend.css)
        .pipe(gulpif(/[.]styl/, stylus()))
        .pipe(concat('production.css'))
        .pipe(gulp.dest('public/assets/backend'));
});