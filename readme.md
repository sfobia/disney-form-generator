# framework
http://hapijs.com/

# swagger
http://localhost:8000/documentation

# todo
https://github.com/viniciusbo/hapi-suricate

# paginazione
https://www.npmjs.com/package/mongoose-paginate


Installare pre-commit

# setup deploy
pm2 deploy ecosystem.json production setup

# per npm make
sudo apt-get install build-essential

# start deploy
pm2 deploy ecosystem.json production

# skip commit
pm2 deploy ecosystem.json production --force
pm2 deploy ecosystem.json development --force
pm2 deploy ecosystem.json stage --force

# disaster revert
pm2 deploy ecosystem.json production revert [n] dove n è il numero opzionale di commit a cui tornare indietro

# installare imageMagic

## Users

# candidates
- canditate ref a users
- campaign ref
- active
- rifiutata

## Campaigns
- image
- content:
    title
    abstract
    description
- link DINAMICO
- tipologia
- nome
- data inizio e fine
- ref a forms
- logo
- images [img]
    
# forms
- default
- custom

# notifications
- lista di tutte le notifiche inviate

# templates
- type ENUM(email,view)
- key
- content html
- subject

# survey
- type
- title
- status
- stats generato dopo la chiusura del concorso, volendo possiamo anche svuotare la lista voti survey_results
- question [
    - title
    - type (radio,checkbox,input)
    - description
    - answer [
        value
    ]
 ]

# survey_results
- user
- survey -> res


# todo
multilingua
mettere link simbolico alla cartella public/media



